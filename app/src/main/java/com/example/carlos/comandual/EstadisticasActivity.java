package com.example.carlos.comandual;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

public class EstadisticasActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    DataBaseManager manager;
    ArrayList<ComandaClass> comandas;
    Button analisis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estadisticas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();

        MenuItem nav = menu.findItem(R.id.nav_graphs);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        this.obtenerTotalComandas();

        analisis = (Button) findViewById(R.id.btn_analisis);
        analisis.setOnClickListener(this);

    }

    public void obtenerTotalComandas(){

        comandas = new ArrayList<>();
        String get_comandas = DataBaseManager.ip+"comandas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        if(comandas.size()==0){
            request = new StringRequest(Request.Method.GET, get_comandas, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONArray array = new JSONArray(response);
                        JSONObject object;
                        for(int i = 0 ; i < array.length();i++){
                            object = array.getJSONObject(i);

                            String rest = object.getString("user");
                            if(rest.equals(DataBaseManager.restaurante)) {
                                String id = object.getString("id");
                                String mesa = object.getString("mesa");
                                String producto = object.getString("producto");
                                String categoria = object.getString("categoria");
                                String estado = object.getString("estado");
                                String cantidad = object.getString("cantidad");
                                String comentario = object.getString("comentario");
                                String token = object.getString("token");
                                String fecha = object.getString("fecha");
                                String hora = object.getString("hora");

                                ComandaClass comanda = new ComandaClass(id,mesa,producto,comentario,estado,categoria,Integer.parseInt(cantidad),fecha,hora,token);
                                comandas.add(comanda);
                            }
                        }
                        iniciarGrafica();

                    }catch (Exception e){
                        String err = e.toString();
                        Log.e("Error",err);
                    }
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                    String err = error.toString();
                    Log.e("Error",err);
                }
            });
            //request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequestQueue.add(request);
        }
    }

    public void iniciarGrafica(){
        DateFormat dateFormat = new SimpleDateFormat("MM");
        String[] meses = new String[6];
        int[] ventas = new int[]{0,0,0,0,0,0};
        Date date = new Date();
        int month = Integer.valueOf(dateFormat.format(date));
        //month = 3;
        int mes;
        if(month>=6){
            mes = month-5;
        }else{
            mes = 7+month;
        }
        for(int i = 0; i < 6; i++){
            int a = comandas.size();
            switch (mes){
                case 1:
                    meses[i] = "ene";
                    for(int j = 0 ; j < a;j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("1")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 2:
                    meses[i] = "feb";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("2")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 3:
                    a = comandas.size();
                    meses[i] = "mar";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("3")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 4:
                    meses[i] = "abr";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("4")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 5:
                    meses[i] = "may";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("5")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 6:
                    meses[i] = "jun";
                    for(int j = 0 ; j < comandas.size();j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("6")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 7:
                    meses[i] = "jul";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("7")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 8:
                    meses[i] = "ago";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("8")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 9:
                    meses[i] = "sep";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("9")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 10:
                    meses[i] = "oct";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("10")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 11:
                    meses[i] = "nov";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("11")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
                case 12:
                    meses[i] = "dic";
                    for(int j = 0 ; j < a; j++){
                        ComandaClass comanda = comandas.get(j);
                        String fecha = comanda.getFecha();
                        StringTokenizer tokenizer = new StringTokenizer(fecha,"-");
                        tokenizer.nextToken();
                        String m = tokenizer.nextToken();
                        if(m.equals("12")){
                            ventas[i]++;
                            comandas.remove(j);
                            a--;
                            j--;
                        }
                    }
                    break;
            }
            mes++;
            if(mes>12){
                mes = 1;
            }

        }

        DataPoint[] datas = new DataPoint[6];
        for (int i = 0 ; i < 6; i++){
            datas[i] = new DataPoint(i,ventas[i]);
        }

        GraphView graph = (GraphView) findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(datas);

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(meses);
        //staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);

        graph.addSeries(series);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            case R.id.action_call:
                //makeJsonObjReq();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_analisis:
                Intent intent = new Intent(getApplicationContext(),AnalisisActivity.class);
                startActivity(intent);
                break;
        }
    }
}
