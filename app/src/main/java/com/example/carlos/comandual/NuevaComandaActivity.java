package com.example.carlos.comandual;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
//listview expandible https://www.youtube.com/watch?v=iMoN414EfSQ
public class NuevaComandaActivity extends Activity implements AdapterView.OnItemSelectedListener{

    private AutoCompleteTextView autoCompleteIngredientes;
    private AutoCompleteTextView autocompleteComida;

    private ArrayList<Producto> arrayProductos;
    private ArrayList<String> categorias;
    private ArrayList<Producto> arrayPedido;
    private ArrayList<String> ingredientes_producto;
    private ArrayList<String> arrayIngredientes;
    private ArrayList<String> arrayAdapterListView;
    private ArrayList<CheckBox> checkBoxesIngredientes;
    private ArrayList<CheckBox> ingredientesIncluidos;
    private ArrayList<CheckBox> ingredientesEliminados;

    private ImageButton search;

    private Button ingredientes;
    private Button button;
    private Button confirmar;
    private CheckBox media;

    private DataBaseManager manager;

    private ElegirProductoAdapter adapter;

    private GridLayout gridLayout;

    private IngredientesAdapter ingredientesAdapter;

    private TreeMap<String,ArrayList<Producto>> mapa;

    private Map<String,ArrayList<String>> mapaCategoria;
    private LinearLayout layoutIngredientes;

    private LinearLayout layout;
    private ListView listViewProductos;

    private ListView listViewIngredientes;
    //private String producto;

    private android.widget.ExpandableListAdapter listAdapter;
    private ExpandableListView listViewExpandible;
    private List<String> valoresCategorias;
    private Map<String,List<String>> dropdow;

    private FragmentActivity myContext;

    private Producto productoClass;

    private NumberPicker cantidad;

    private String categoria;
    private static final String urlApi = "http://192.168.0.100:3000/api/product";
    private static final String urlCMS = "https://fcm.googleapis.com/fcm/send";

    private Spinner spinner;
    private Spinner spinnerCategorias;

    private int idMesa;

    private StringRequest request;
    private RequestQueue mRequestQueue;

    //private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_comanda);


        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        Bundle bundle = getIntent().getExtras();
        idMesa = bundle.getInt("idMesa");

        manager = new DataBaseManager(this);

        arrayPedido = new ArrayList<>();
        adapter = new ElegirProductoAdapter(this, arrayPedido);

        autocompleteComida = (AutoCompleteTextView) findViewById(R.id.autoCompleteComida);

        ArrayList<String> listaComida = manager.obtenerNombreProductos();
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,listaComida);
        autocompleteComida.setThreshold(1);
        autocompleteComida.setAdapter(arrayAdapter);
        autocompleteComida.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                String name = autocompleteComida.getText().toString();
                Producto producto = manager.obtenerProducto(name);

                popUpDetalles(producto,-1);
                autocompleteComida.setText("");
            }
        });

        listViewProductos = (ListView) findViewById(R.id.listViewElegirProducto);
        listViewProductos.setAdapter(adapter);

        listViewProductos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, final int position, long id) {

                return false;
            }
        });

        listViewProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Producto producto = arrayPedido.get(position);
                popUpDetalles(producto,position);
            }
        });

        confirmar = (Button) findViewById(R.id.btnConfirmarComanda);
        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(arrayPedido.size()>0){
                    for(int i = 0;i<arrayPedido.size();i++){
                        Producto producto = arrayPedido.get(i);
                        int id = manager.obtenerUlitmoIdComanda();
                        String token = "";
                        if(DataBaseManager.tipoUsuario.equals("empleado")){
                            token = manager.obtenerToken();
                        }else{
                            String idEmpleado = manager.idUsuario;
                        }

                        Calendar calendarNow =new GregorianCalendar(TimeZone.getTimeZone("Europe/Madrid"));
                        int day = calendarNow.get(Calendar.DAY_OF_MONTH);
                        int month = calendarNow.get(Calendar.MONTH)+1;
                        int year = calendarNow.get(Calendar.YEAR);

                        int hour = calendarNow.get(Calendar.HOUR_OF_DAY);
                        int minute = calendarNow.get(Calendar.MINUTE);

                        String fecha = day+"-"+month+"-"+year;
                        String hora = hour+":"+minute;

                        //fecha = "20-8-2017";
                        //hora = "12:21";
                        for(int j = 0;j < producto.getCantidad();j++){

                            manager.insertarComandaApi(String.valueOf(id),String.valueOf(idMesa),String.valueOf(producto.getId()),producto.getCategoria(),producto.getAlteracionIngredientes(),"COLA","1",token,fecha,hora);
                            manager.modificarEstadoMesa(String.valueOf(idMesa),"PENDIENTE");
                        }
                    }
                    makeJsonObjReq();
                    if(manager.tipoUsuario.equals("empleado")){
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(getApplicationContext(),MesaActivity.class);
                        intent.putExtra("idMesa",idMesa);
                        startActivity(intent);
                    }
                }
            }
        });

        search = (ImageButton) findViewById(R.id.btn_search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpProductos();
            }
        });
    }

    private void makeJsonObjReq(){
        String comanda = "";
        for(int i = 0 ; i < arrayPedido.size(); i++){
            Producto producto = arrayPedido.get(i);
            comanda +="'0" + (i+1) + " " + producto.getNombre() + " ':' " + producto.getCantidad() + "'";

            if(!producto.getAlteracionIngredientes().equals("")){
                comanda += ",'0" + (i+1) + " " + "extra ':' " + producto.getAlteracionIngredientes() + "'";
            }
            if(i!=arrayPedido.size()-1){
                comanda +=",";
            }
        }
        String title = "Nueva comanda";
        String body = "Mesa " + idMesa;
        String token = manager.obtenerToken();
        final String jsonString =
                "{" +
                        //"'to':'ddp0zVEORB4:APA91bEvq1z7eDONhLehlSZXR-pjHZrIec9JeCDdYmQM3VDlHkG0eDePAFzW7qHlBc4ohAyqZXV4TlConEWgDY0AZZq05Ki75pZ23SFb07HyvVDWKK9I012kyc9KEsyP_lW0tKsYcCpx',\n" +
                        "'to':'/topics/empleados'," +
                    "'notification':{" +
                        "'title':'" + title + "'," +
                        "'body':'" + body + "'" +
                    "}" +
                "}";
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    urlCMS, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(getApplicationContext(),"Comanda enviada a cocina",Toast.LENGTH_SHORT).show();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Response", "Error: " + error.getMessage());
                }
            }) {
                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("Content-Type","application/json");
                    params.put("Authorization","key=AIzaSyDAE9MwmYI9VjGjTqd6qLzOadga0tqvurs");
                    return params;
                }
            };
            mRequestQueue.add(jsonObjReq);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void popUpDetalles(final Producto producto, final int position){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_detalle_producto, null);

        TextView textView = (TextView) v.findViewById(R.id.tv_categoria);
        textView.setText(producto.getCategoria());

        cantidad = (NumberPicker) v.findViewById(R.id.number_cantidad);
        float oldCantidad = producto.getCantidad();
        int old = Math.round(oldCantidad);
        cantidad.setMinValue(0);
        cantidad.setMaxValue(50);
        if(old>0){
            cantidad.setValue(old);
        }else{
            cantidad.setValue(1);
        }

        ingredientes = (Button) v.findViewById(R.id.btnIngredientes);
        ingredientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpIngredientes(producto);
            }
        });

        builder.setTitle(producto.getNombre());

        builder.setView(v);

        if(!producto.getStock()) {
            Toast.makeText(v.getContext(), "Este producto no se encuentra disponible", Toast.LENGTH_SHORT).show();
            cantidad.setValue(0);
            cantidad.setEnabled(false);
            ingredientes.setEnabled(false);

        }else{
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if(producto.getStock()){
                        float cant = cantidad.getValue();
                        int pos = arrayPedido.indexOf(producto);
                        if(!arrayPedido.contains(producto)){
                            producto.setCantidad(cant);
                            arrayPedido.add(producto);
                        }else{
                            arrayPedido.get(pos).setCantidad(cant);
                        }

                        if(cant == 0){
                            if(pos < 0){
                                arrayPedido.remove(0);
                            }else{
                                arrayPedido.remove(pos);
                            }
                        }

                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                if(position!=-1){
                    new android.support.v7.app.AlertDialog.Builder(v.getContext())
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Eliminar Producto")
                            .setMessage("Estas seguro de que deseas eliminar el producto " + arrayPedido.get(position) + "? ")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    arrayPedido.remove(position);
                                    adapter.notifyDataSetChanged();
                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                }

            }
        });
        builder.show();
    }

    public void popUpIngredientes(final Producto producto){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_ingredientes_producto, null);

        ingredientesEliminados = new ArrayList<>();
        ingredientesIncluidos = new ArrayList<>();
        ingredientes_producto = manager.obtenerProducto_Ingredientes(producto.getNombre());
        arrayIngredientes = manager.obtenerIngredientes();
        arrayAdapterListView = new ArrayList<>();
        arrayAdapterListView.addAll(arrayIngredientes);

        listViewIngredientes = (ListView) v.findViewById(R.id.listViewIngredientes);
        ingredientesAdapter = new IngredientesAdapter(this,arrayAdapterListView,ingredientes_producto/*,ingredientesIncluidos,ingredientesEliminados*/);
        listViewIngredientes.setAdapter(ingredientesAdapter);
        listViewIngredientes.setItemsCanFocus(true);

        final ArrayAdapter adapterIngredientes = new ArrayAdapter(v.getContext(), R.layout.support_simple_spinner_dropdown_item,new ArrayList());
        autoCompleteIngredientes = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteIngredientes);
        autoCompleteIngredientes.setHint("Buscar Ingrediente");
        autoCompleteIngredientes.setAdapter(adapterIngredientes);

        autoCompleteIngredientes.setThreshold(1);
        autoCompleteIngredientes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String aux = s.toString();
                aux = aux.toLowerCase();
                s = aux;
                actualizarLista(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        builder.setTitle(producto.getNombre());
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArrayList<CheckBox> arrayList = ingredientesAdapter.getCheckBoxArrayList();

                String ingrediente;
                String cadena = "";
                for(int i = 0 ; i < arrayList.size() ; i++){
                    CheckBox checkBox = arrayList.get(i);
                    String ischeck = "";
                    if(checkBox.isChecked()){
                        ischeck = "true";
                        ingredientesIncluidos.add(checkBox);
                    }else{
                        ischeck = "false";
                        ingredientesEliminados.add(checkBox);
                    }
                }

                for(int i = 0 ; i < ingredientesIncluidos.size();i++){
                    ingrediente = ingredientesIncluidos.get(i).getText().toString();
                    if(!ingredientes_producto.contains(ingrediente)){
                        ingredientes_producto.add(ingrediente);
                        cadena += "Con "+ ingrediente + " ";
                    }
                }
                for(int i = 0; i < ingredientesEliminados.size();i++){
                    ingrediente = ingredientesEliminados.get(i).getText().toString();
                    if(ingredientes_producto.contains(ingrediente)){
                        ingredientes_producto.remove(ingrediente);
                        cadena += "Sin " + ingrediente +" ";
                    }
                }
                producto.setAlteracionIngredientes(cadena);
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void popUpProductos(){
        // Use the Builder class for convenient dialog construction

        //dropdow https://amsheer007.wordpress.com/2015/03/20/show-listview-as-dropdown-in-android-a-spinner-alternative/
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_spinner_productos, null);
        layout = (LinearLayout) v.findViewById(R.id.layout_spinner_productos);

        ExpandableListAdapter mAdapter;
        listViewExpandible = (ExpandableListView) v.findViewById(R.id.listView_categorias);

        categorias = manager.obtenerCategorias();

        mapaCategoria = new TreeMap<>();

        for(String categoria:categorias){
            ArrayList<String > arrayList = new ArrayList<>();
            mapaCategoria.put(categoria,arrayList);
        }

        actualizarMapa();

        builder.setTitle("Productos");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
               // Toast.makeText(getApplicationContext(),"Hola",Toast.LENGTH_SHORT).show();
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void actualizarMapa(){
        ArrayList<Producto> productos = manager.obtenerProductos();
        String categoria;
        ArrayList arrayList;
        for(Producto producto:productos){
            categoria = manager.obtenerCategoriabyId(producto.getCategoria()).get(1);
            arrayList = mapaCategoria.get(categoria.toLowerCase());
            arrayList.add(producto.getNombre().toLowerCase());
            mapaCategoria.put(categoria,arrayList);
        }
        pintarExpandible();
    }

    public void pintarExpandible(){
        listAdapter = new MiExpandableListAdapter(this,categorias,mapaCategoria);

        listViewExpandible.setAdapter(listAdapter);

        listViewExpandible.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
                //Toast.makeText(getApplicationContext(),categorias.get(groupPosition)+ ":" + mapaCategoria.get(categorias.get(groupPosition)).get(childPosition),Toast.LENGTH_SHORT).show();
                Producto producto = manager.obtenerProducto(mapaCategoria.get(categorias.get(groupPosition)).get(childPosition));
                popUpDetalles(producto,-1);
                return false;
            }
        });
    }

    private ArrayList<Producto> obtenerProductos(String categoria){
        ArrayList<Producto> productos = new ArrayList<>();

        ArrayList<ArrayList<String>> array = manager.obtenerProductosPorCategoria(categoria);

        for(int i = 0; i < array.size();i++){
            productos.add(new Producto(Integer.parseInt(array.get(i).get(0)), array.get(i).get(1), array.get(i).get(2), 0));
        }

        return productos;
    }

    public void actualizarLista(CharSequence s){
        ArrayList<String> array = new ArrayList<>();

        if(s.toString().equals("")){
            array = manager.obtenerIngredientes();
        }else{
            for(int i = 0;i<arrayAdapterListView.size();i++){
                if(arrayAdapterListView.get(i).contains(s)){
                    array.add(arrayAdapterListView.get(i));
                }
            }
        }

        arrayAdapterListView.clear();
        arrayAdapterListView.addAll(array);
        ingredientesAdapter.notifyDataSetChanged();
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        //Toast.makeText(v.getContext(),v.getId()+"",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}