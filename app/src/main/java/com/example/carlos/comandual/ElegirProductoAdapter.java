package com.example.carlos.comandual;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 07/03/2016
 */
public class ElegirProductoAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<Producto> productos;
    private View v;
    /**/
    private TextView nombre;
    private TextView categoria;
    private TextView cantidad;
    private String alteracion;

    public ElegirProductoAdapter(Activity activity, ArrayList<Producto> productos) {
        this.activity = activity;
        this.productos = productos;
    }

    @Override
    public int getCount() {
        return productos.size();
    }

    @Override
    public Object getItem(int position) {
        return productos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return productos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        v = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_elegir_producto, null);
        }

        Producto producto = productos.get(position);

        nombre = (TextView) v.findViewById(R.id.tvProducto);
        nombre.setText(producto.getNombre());

        categoria = (TextView) v.findViewById(R.id.tvCategoria);
        categoria.setText(producto.getCategoria());

        alteracion = producto.getAlteracionIngredientes();

        if(alteracion != null){
            categoria.append("\n" + alteracion);
        }

        cantidad = (TextView) v.findViewById(R.id.tvCantidad);
        cantidad.setText(producto.getCantidad()+"");

        return v;
    }

}
