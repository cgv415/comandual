package com.example.carlos.comandual;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 25/08/2017
 */

public class Valoracion {
    private String id;
    private double valoracion;
    private String comentario;
    private String cliente;
    private String empleado;
    private String fecha;
    private String mesa;

    public Valoracion(String id, double valoracion, String comentario, String cliente, String empleado, String fecha, String mesa) {
        this.id = id;
        this.valoracion = valoracion;
        this.comentario = comentario;
        this.cliente = cliente;
        this.empleado = empleado;
        this.fecha = fecha;
        this.mesa = mesa;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getValoracion() {
        return valoracion;
    }

    public void setValoracion(double valoracion) {
        this.valoracion = valoracion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMesa() {
        return mesa;
    }

    public void setMesa(String mesa) {
        this.mesa = mesa;
    }
}
