package com.example.carlos.comandual;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistroCliente extends AppCompatActivity implements View.OnClickListener{

    EditText etNombreCompleto;
    EditText etNombreUsuario;
    EditText etApellidos;
    EditText etEmail;
    EditText etCodigoPostal;
    AutoCompleteTextView etLocalidad;
    EditText etAnoNacimiento;
    EditText password;
    EditText confirmarPassword;
    AutoCompleteTextView etProvincia;

    Button cancelar;
    Button registrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_cliente);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String[] provincias = {"Alava","Albacete","Alicante","Almería","Asturias","Avila","Badajoz","Barcelona","Burgos","Cáceres",
                "Cádiz","Cantabria","Castellón","Ciudad Real","Córdoba","La Coruña","Cuenca","Gerona","Granada","Guadalajara",
                "Guipúzcoa","Huelva","Huesca","Islas Baleares","Jaén","León","Lérida","Lugo","Madrid","Málaga","Murcia","Navarra",
                "Orense","Palencia","Las Palmas","Pontevedra","La Rioja","Salamanca","Segovia","Sevilla","Soria","Tarragona",
                "Santa Cruz de Tenerife","Teruel","Toledo","Valencia","Valladolid","Vizcaya","Zamora","Zaragoza"};

        etNombreCompleto = (EditText) findViewById(R.id.et_nombreCompleto);
        etNombreUsuario = (EditText) findViewById(R.id.et_nombreUsuario);
        etApellidos = (EditText) findViewById(R.id.et_apellidos);
        etEmail = (EditText) findViewById(R.id.et_email);
        etCodigoPostal = (EditText) findViewById(R.id.et_codigoPostal);

        etProvincia = (AutoCompleteTextView) findViewById(R.id.et_provincia);
        ArrayAdapter<String> adapterProvincia = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, provincias);
        etProvincia.setAdapter(adapterProvincia);
        etProvincia.setThreshold(1);

        etLocalidad = (AutoCompleteTextView) findViewById(R.id.et_localidad);
        ArrayAdapter<String> adapterLocalidad = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, Localidades.getLocalidades());
        etLocalidad.setThreshold(1);
        etLocalidad.setAdapter(adapterLocalidad);

        etAnoNacimiento = (EditText) findViewById(R.id.et_anoNacimiento);

        password = (EditText) findViewById(R.id.et_password);

        confirmarPassword = (EditText) findViewById(R.id.et_confirmarPassword);

        cancelar = (Button) findViewById(R.id.btn_cancelar);
        cancelar.setOnClickListener(this);
        registrar = (Button) findViewById(R.id.btn_registrar);
        registrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_registrar:
                comprobarDatos();
                break;
            case R.id.btn_cancelar:
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
        }
    }

    public boolean comprobarDatos(){
        registrado(etNombreUsuario.getText().toString(), etEmail.getText().toString());
        if(etNombreUsuario.getText().toString().equals("")){
            etNombreUsuario.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Nombre de usuario vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etNombreUsuario.setBackgroundColor(Color.WHITE);}

        if(etNombreCompleto.getText().toString().equals("")){
            etNombreCompleto.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Nombre completo vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etNombreCompleto.setBackgroundColor(Color.WHITE);}

        if(etApellidos.getText().toString().equals("")){
            etApellidos.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Apellidos vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etApellidos.setBackgroundColor(Color.WHITE);}

        if(etEmail.getText().toString().equals("")){
            etEmail.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Email vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else {
            if (!isEmailValid(etEmail.getText().toString())) {
                etEmail.setBackgroundColor(Color.RED);
                Toast.makeText(getApplicationContext(), "Email incorrecto", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                etEmail.setBackgroundColor(Color.WHITE);}
        }

        if(etCodigoPostal.getText().toString().equals("")){
            etCodigoPostal.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Codigo postal vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etCodigoPostal.setBackgroundColor(Color.WHITE);}

        if(etProvincia.getText().toString().equals("")){
            etProvincia.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Provincia vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etProvincia.setBackgroundColor(Color.WHITE);}

        if(etLocalidad.getText().toString().equals("")){
            etLocalidad.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Localidad vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etProvincia.setBackgroundColor(Color.WHITE);}

        if(etAnoNacimiento.getText().toString().equals("")){
            etAnoNacimiento.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Año de nacimiento vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            Calendar calendarNow = new GregorianCalendar(TimeZone.getTimeZone("Europe/Madrid"));
            int year =calendarNow.get(Calendar.YEAR);
            int fecha = Integer.parseInt(etAnoNacimiento.getText().toString());
            if((year-fecha) < 10 || (year-fecha) > 100 ){
                etAnoNacimiento.setBackgroundColor(Color.RED);
                Toast.makeText(getApplicationContext(),"Año de nacimiento incorrecto",Toast.LENGTH_SHORT).show();
                return false;
            }else{
                etAnoNacimiento.setBackgroundColor(Color.WHITE);
            }
        }

        if(password.getText().toString().equals("")){
            password.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Password vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{password.setBackgroundColor(Color.WHITE);}

        if(password.getText().toString().length()<4){
            password.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Password demasiado corta (min 4)",Toast.LENGTH_SHORT).show();
            return false;
        }else{password.setBackgroundColor(Color.WHITE);}

        if(confirmarPassword.getText().toString().equals("")){
            confirmarPassword.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Confirmar Password vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{confirmarPassword.setBackgroundColor(Color.WHITE);}

        if(!password.getText().toString().equals(confirmarPassword.getText().toString())){
            password.setBackgroundColor(Color.RED);
            confirmarPassword.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Password y Confirmar password no coinciden",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            password.setBackgroundColor(Color.WHITE);
            confirmarPassword.setBackgroundColor(Color.WHITE);
        }

        return true;
    }

    public void registrado(final String username, final String email){
        String url = DataBaseManager.ip+"clientes/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    boolean registrado = false;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String nombre = object.getString("nombre");
                        String em = object.getString("email");
                        if(username.equals(nombre)){
                            mostrarMensaje("Usuario ya existe");
                            registrado = true;
                            break;
                        }else if(email.equals(em)){
                            mostrarMensaje("Email ya existe");
                            registrado = true;
                            break;
                        }
                    }
                    if(!registrado){
                        registrarCliente();
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();

            }
        });
        mRequestQueue.add(request);
    }

    public void mostrarMensaje(String mensaje){
        Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_SHORT).show();
    }

    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    /*public void registrarUsuario(){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"/api/user/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String id = object.getString("id");

                            registrarCliente(id);

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                String username = DataBaseManager.removeAcentos(etNombreUsuario.getText().toString());
                String first_name = DataBaseManager.removeAcentos(etNombreCompleto.getText().toString());
                String last_name = DataBaseManager.removeAcentos(etApellidos.getText().toString());
                String email = DataBaseManager.removeAcentos(etEmail.getText().toString());

                String pass = "";
                try {
                    pass = Encrypt.encrypt("Your_seed_....",password.getText().toString());

                }catch (Exception e){
                    Log.d("Err",e.toString());
                }
                Map<String, String>  params = new HashMap<>();
                params.put("username",username);
                params.put("first_name", first_name);
                params.put("last_name", last_name);
                params.put("email", email);
                params.put("password",pass);
                params.put("groups","2");

                //params.put("last_name",restaurante);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
    }*/

    public void registrarCliente(){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"clientes/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);
                        try {

                            //JSONObject object = new JSONObject(response);
                            //String id = object.getString("id");
                            mostrarMensaje("Usuario registrado con exito");
                            iniciarActivity();

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {

                String pass = "";
                try {
                    pass = Encrypt.encrypt("Your_seed_....",password.getText().toString());
                }catch (Exception e){
                    Log.d("Err",e.toString());
                }
                String nombreUsuario = DataBaseManager.removeAcentos(etNombreUsuario.getText().toString());
                String provincia = DataBaseManager.removeAcentos(etProvincia.getText().toString());
                String localidad = DataBaseManager.removeAcentos(etLocalidad.getText().toString());
                String nombre_completo = DataBaseManager.removeAcentos(etNombreCompleto.getText().toString());
                String apellidos = DataBaseManager.removeAcentos(etApellidos.getText().toString());
                String email = DataBaseManager.removeAcentos(etEmail.getText().toString());

                Map<String, String>  params = new HashMap<>();
                params.put("nombre", nombreUsuario);
                params.put("codigo", etCodigoPostal.getText().toString());
                params.put("nacimiento", etAnoNacimiento.getText().toString());
                params.put("provincia", provincia);
                params.put("localidad", localidad);
                params.put("nombre_completo", nombre_completo);
                params.put("apellidos", apellidos);
                params.put("email", email);
                params.put("password",pass);
                //params.put("idUsuario",id);
                //params.put("last_name",restaurante);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
    }

    public void iniciarActivity(){

        finish();
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
    }

}
