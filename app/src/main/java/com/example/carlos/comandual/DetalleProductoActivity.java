package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class DetalleProductoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    private DataBaseManager manager;
    private MultiAutoCompleteTextView complete_ingredientes;
    private Producto producto;
    private CheckBox checkBox;
    private ArrayList<String> arrayIngredientes;
    private StockAdapter adapter;
    private EditText etNombre;
    private EditText etPrecio;
    private Spinner spCategoria;
    private String nombreProducto;
    private EditText etDescripcion;
    private ArrayList<String> categorias;
    private MultiAutoCompleteTextView multiCompleteIngredientes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_producto);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView.setNavigationItemSelectedListener(this);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem nav = menu.findItem(R.id.nav_stock);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView.setNavigationItemSelectedListener(this);

        Bundle bundle = getIntent().getExtras();
        nombreProducto = bundle.getString("producto");

        setTitle(nombreProducto);


        producto = manager.obtenerProducto(nombreProducto);

        arrayIngredientes = manager.obtenerProducto_Ingredientes(producto.getNombre());
        String strIngredientes = "";

        for(String ing:arrayIngredientes){
            strIngredientes +=ing+", ";
        }

        multiCompleteIngredientes = new MultiAutoCompleteTextView(this);
        multiCompleteIngredientes.setText(strIngredientes);

        checkBox = (CheckBox) findViewById(R.id.checkBoxStockIngrediente);
        checkBox.setChecked(producto.getStock());

        etNombre = (EditText) findViewById(R.id.et_nombre);
        etNombre.setText(nombreProducto);

        etPrecio = (EditText) findViewById(R.id.et_precio);
        etPrecio.setText(String.valueOf(producto.getPrecio()));

        categorias = manager.obtenerCategorias();
        int pos = -1;
        for(int i = 0; i < categorias.size();i++){
            if(categorias.get(i).equals(producto.getCategoria())){
                pos = i;
                break;
            }
        }

        spCategoria = (Spinner) findViewById(R.id.spinner_categoria);
        ArrayAdapter<String> categoriasAdapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,categorias);
        spCategoria.setAdapter(categoriasAdapter);

        if(pos!= -1){
            spCategoria.setSelection(pos);
        }

        Button btn_eliminar = (Button) findViewById(R.id.btn_eliminar);
        btn_eliminar.setOnClickListener(this);

        if(DataBaseManager.tipoUsuario.equals("cliente")){
            checkBox.setClickable(false);
            etNombre.setEnabled(false);
            btn_eliminar.setVisibility(View.GONE);
        }

        etDescripcion = (EditText) findViewById(R.id.et_descripcion);
        etDescripcion.setText(producto.getDescripcion());

        complete_ingredientes = (MultiAutoCompleteTextView) findViewById(R.id.autocomplete_ingredientes);

        final ArrayList<String> completeIngredientes = manager.obtenerIngredientes();
        ArrayAdapter<String> ingredientesProductoAdapter = new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line, completeIngredientes);

        complete_ingredientes.setAdapter(ingredientesProductoAdapter);
        complete_ingredientes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        complete_ingredientes.setThreshold(1);


        complete_ingredientes.setText(multiCompleteIngredientes.getText().toString());

        Button button = (Button) findViewById(R.id.btn_aceptar);
        button.setOnClickListener(this);

        /*Button ingredientes = (Button) findViewById(R.id.btn_modificar);
        ingredientes.setOnClickListener(this);
        if (DataBaseManager.tipoUsuario.equals("cliente")) {
            ingredientes.setVisibility(View.GONE);
        }*/

        ListView listView = (ListView) findViewById(R.id.listViewIngredientesProducto);

        arrayIngredientes = manager.obtenerProducto_Ingredientes(nombreProducto);
        adapter = new StockAdapter(this,arrayIngredientes,"ingredientes");
        listView.setAdapter(adapter);
        if(DataBaseManager.tipoUsuario.equals("empleado")){
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    final String nombre = arrayIngredientes.get(position);
                    if(manager.inStrockIngrediente(nombre)){
                        new AlertDialog.Builder(view.getContext())
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle(nombre)
                                .setMessage("Desea seleccionar el ingrediente " + nombre + " como FUERA de stock?")
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        manager.modificarStockIngrediente(nombre,false);
                                        adapter.notifyDataSetChanged();
                                    }

                                })
                                .setNegativeButton("No", null)
                                .show();

                    }else{
                        new AlertDialog.Builder(view.getContext())
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle(nombre)
                                .setMessage("Desea seleccionar el ingrediente " + nombre + " como DISPONIBLE en stock?")
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        manager.modificarStockIngrediente(nombre,true);
                                        adapter.notifyDataSetChanged();
                                    }

                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
            });
        }
        //final View v = getCurrentFocus();

        if(DataBaseManager.tipoUsuario.equals("cliente")){
            etDescripcion.setEnabled(false);
            spCategoria.setEnabled(false);
            etPrecio.setEnabled(false);
            checkBox.setEnabled(false);
        }
    }

    /*public void popUpIngredientes(){
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_modificar_ingredientes, null);

        complete_ingredientes = (MultiAutoCompleteTextView) v.findViewById(R.id.autocomplete_ingredientes);

        final ArrayList<String> completeIngredientes = manager.obtenerIngredientes();
        ArrayAdapter<String> ingredientesProductoAdapter = new ArrayAdapter<>(v.getContext(),android.R.layout.simple_dropdown_item_1line, completeIngredientes);

        complete_ingredientes.setAdapter(ingredientesProductoAdapter);
        complete_ingredientes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        complete_ingredientes.setThreshold(1);


        complete_ingredientes.setText(multiCompleteIngredientes.getText().toString());

        builder.setTitle("Añadir/Quitar producto");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                multiCompleteIngredientes.setText(complete_ingredientes.getText().toString());
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_aceptar:

                final String nombreIngredientes = complete_ingredientes.getText().toString();
                final String[] arrayIngredientes = nombreIngredientes.split(", ");

                String aux = "[";
                try {
                    for (int i = 0; i < arrayIngredientes.length; i++) {
                        aux += manager.obtenerIngredienteByNombre(arrayIngredientes[i]).get(0);
                        if (i < arrayIngredientes.length - 1) {
                            aux += ",";
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "No se han seleccionado ingredientes", Toast.LENGTH_SHORT).show();
                }
                aux += "]";

                final String idIngredientes = aux;

                producto.setNombre(DataBaseManager.removeAcentos(etNombre.getText().toString()));
                producto.setDescripcion(DataBaseManager.removeAcentos(etDescripcion.getText().toString()));
                producto.setCategoria(DataBaseManager.removeAcentos(categorias.get(spCategoria.getSelectedItemPosition())));
                producto.setStock(checkBox.isChecked());
                producto.setPrecio(Double.parseDouble(DataBaseManager.removeAcentos(etPrecio.getText().toString())));
                producto.setAlteracionIngredientes(DataBaseManager.removeAcentos(idIngredientes));

                manager.modificarProducto(producto);

                /*if(producto.getStock() != checkBox.isChecked()){
                    //manager.modificarStockProducto(producto);
                }
                if(!nombreProducto.equals(etNombre.getText().toString())){
                    producto.setNombre(etNombre.getText().toString());
                    manager.modificarNombreProducto(producto);
                }
                if(!producto.getDescripcion().equals(etDescripcion.getText().toString())){
                    producto.setDescripcion(etDescripcion.getText().toString());
                }
                if(!producto.getPrecio().equals(etPrecio.getText().toString())){
                    producto.setPrecio(Double.parseDouble(etPrecio.getText().toString()));
                }
                if(!producto.getCategoria().equals(categorias.get(spCategoria.getSelectedItemPosition()))){
                    producto.setCategoria(categorias.get(spCategoria.getSelectedItemPosition()));
                }*/

                finish();
                Intent intent = new Intent(getApplicationContext(),StockActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_eliminar:
                new AlertDialog.Builder(v.getContext())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Eliminar producto")
                        .setMessage("Desea eliminar el producto " + producto.getNombre() +" del local?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                manager.eliminarProducto(producto.getNombre());
                                adapter.notifyDataSetChanged();

                                finish();
                                Intent intent = new Intent(getApplicationContext(),StockActivity.class);
                                startActivity(intent);

                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                break;
        }
    }
}
