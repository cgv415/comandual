package com.example.carlos.comandual;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 27/06/2017
 */

class PDFCreator {

    Context context;

    PDFCreator(Context context, ArrayList<ArrayList<String>> mesas){
        this.context = context;
        createPDF2(mesas);
    }

    PDFCreator(Context context, String id, String nombre){
        this.context = context;
        String texto = DataBaseManager.restaurante + "-" + id;
        createPDF(nombre, texto);
    }

    private void createPDF(String nombre, String texto)
    {
        Document doc = new Document();


        try {
            String path = Environment.getExternalStorageDirectory().getPath() + "/comandUal";
            File dir = new File(path);

            if(!dir.exists()){
                if(!dir.mkdirs()){
                    Toast.makeText(context,"No se ha dado permiso a la aplicación para generar el archivo",Toast.LENGTH_SHORT).show();
                }
            }

            Log.d("PDFCreator", "PDF Path: " + path);

            File file = new File(dir, nombre + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try{
                BitMatrix bitMatrix = multiFormatWriter.encode(texto, BarcodeFormat.QR_CODE,500,500);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100 , stream);
                Image myImg = Image.getInstance(stream.toByteArray());
                myImg.setAlignment(Image.MIDDLE);

                //add image to document
                doc.add(myImg);

            }catch (WriterException e){
                e.printStackTrace();
            }

            //set footer
            Phrase footerText = new Phrase("This is an example of a footer");
            HeaderFooter pdfFooter = new HeaderFooter(footerText, false);
            doc.setFooter(pdfFooter);

            Toast.makeText(context,"documento creado en " + path + "/" + texto, Toast.LENGTH_SHORT).show();

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally
        {
            doc.close();
        }

    }

    private void createPDF2(ArrayList<ArrayList<String>> mesas)
    {
        Document doc = new Document();

        try {
            String path = Environment.getExternalStorageDirectory().getPath() + "/comandUal";
            File dir = new File(path);
            if(!dir.exists()){
                if(!dir.mkdirs()){
                    Toast.makeText(context,"No se ha dado permiso a la aplicación para generar el archivo",Toast.LENGTH_SHORT).show();
                }
            }

            Log.d("PDFCreator", "PDF Path: " + path);

            File file = new File(dir, "mesas.pdf");
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();


            try{
                for(ArrayList<String> mesa:mesas){

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                    String textoqr = DataBaseManager.restaurante+"-"+mesa.get(0);
                    BitMatrix bitMatrix = multiFormatWriter.encode(textoqr, BarcodeFormat.QR_CODE,500,500);
                    BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                    Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100 , stream);

                    Image myImg = Image.getInstance(stream.toByteArray());
                    myImg.setAlignment(Image.MIDDLE);

                    //add image to document
                    doc.add(myImg);

                    Paragraph p1 = new Paragraph(mesa.get(5));
                    Font paraFont= new Font(Font.COURIER);
                    p1.setAlignment(Paragraph.ALIGN_CENTER);

                    p1.setFont(paraFont);

                    //add paragraph to document
                    doc.add(p1);
                    doc.newPage();
                }


            }catch (WriterException e){
                e.printStackTrace();
            }



            //set footer
            Phrase footerText = new Phrase("This is an example of a footer");
            HeaderFooter pdfFooter = new HeaderFooter(footerText, false);
            doc.setFooter(pdfFooter);

            Toast.makeText(context,"documento creado en " + path + "/mesas.pdf",Toast.LENGTH_SHORT).show();

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally
        {
            doc.close();
        }

    }

}
