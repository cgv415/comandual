package com.example.carlos.comandual;

import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 01/06/2017
 */

class ComandaClass {
    private String idComanda;
    private String producto;
    private String comentario;
    private String estado;
    private int cantidad;
    private String tipo;
    private String idMesa;
    private String fecha;
    private String hora;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    ComandaClass(){

    }
    ComandaClass(String idComanda,String idMesa, String producto, String comentario, String estado,String tipo, int cantidad, String fecha, String hora, String token) {
        this.idComanda = idComanda;
        this.idMesa = idMesa;
        this.producto = producto;
        this.comentario = comentario;
        this.estado = estado;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.fecha = fecha;
        this.hora = hora;
        this.token = token;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(String idMesa) {
        this.idMesa = idMesa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    String getIdComanda() {
        return idComanda;
    }

    void setIdComanda(String idComanda) {
        this.idComanda = idComanda;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    String getComentario() {
        return comentario;
    }

    void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        boolean equal = true;
        ComandaClass otro = (ComandaClass) o;
        if(!producto.equals(otro.getProducto())){
            equal = false;
        }else if(!comentario.equals(otro.getComentario())){
            equal = false;
        }else if(!estado.equals(otro.getEstado())){
            equal = false;
        }else if(!idMesa.equals(otro.getIdMesa())){
            equal = false;
        }
        return equal;
    }

    @Override
    public int hashCode() {
        int result = idComanda != null ? idComanda.hashCode() : 0;
        result = 31 * result + (producto != null ? producto.hashCode() : 0);
        result = 31 * result + (comentario != null ? comentario.hashCode() : 0);
        result = 31 * result + (estado != null ? estado.hashCode() : 0);
        return result;
    }

    /*public boolean contains(ArrayList<ComandaClass> comandas){
        for(ComandaClass c:comandas){
            boolean equal = equals(c);
            if(equal){
                return true;
            }
        }
        return false;
    }*/

    public int getIndexContains(ArrayList<ComandaClass> comandas){
        int pos = -1;

        for(int i = 0 ; i < comandas.size();i++){
            ComandaClass c = comandas.get(i);
            boolean equal = equals(c);
            if(equal){
                pos = i;
                break;
            }
        }
        return pos;
    }

    public ArrayList<String> getArrayComanda(ComandaClass comanda){
        ArrayList<String> arrComanda = new ArrayList<>();

        return arrComanda;
    }

    public ArrayList<ArrayList<String>> getArrayComandas(ArrayList<ComandaClass> comandas){
        ArrayList<ArrayList<String>> arrComandas = new ArrayList<>();
        ArrayList<String> arrComanda = new ArrayList<>();

        return arrComandas;
    }

}
