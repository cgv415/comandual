package com.example.carlos.comandual;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AnalisisValoracionesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AnalisisValoracionesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnalisisValoracionesFragment extends Fragment implements View.OnClickListener{
    private static final String ARG_FECHAIN = "fechaIn";
    private static final String ARG_FECHAFIN = "fechaFin";
    private static final String ARG_HORAIN = "horaIn";
    private static final String ARG_HORAFIN = "horaFin";

    private String fechaIn;
    private String fechaFin;
    private String horaIn;
    private String horaFin;
    
    private TextView tv_valoraciones;

    private OnFragmentInteractionListener mListener;
    private TextView tv_mesaMasValorada;
    private TextView tv_fechaMasValorada;
    private TextView tv_empleadoMasValorado;
    private TextView tv_masValorado;

    private DataBaseManager manager;
    private Map<String,ArrayList<Double>> mesas;
    private Map<String,ArrayList<Double>> fechas;
    private Map<Double,Integer> valores;
    private Map<String,ArrayList<Double>> empleados;

    private ArrayList<Valoracion> valoraciones;
    private ToggleButton tb_tipo;
    private ListView listView;
    private TextView tv_valoracionMedia;

    public AnalisisValoracionesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param fechaIn Parameter 1.
     * @param fechaFin Parameter 2.
     * @param horaIn Parameter 3.
     * @param horaFin Parameter 4.
     * @return A new instance of fragment AnalisisValoracionesFragment.
     */
    public static AnalisisValoracionesFragment newInstance(String fechaIn, String fechaFin, String horaIn, String horaFin) {
        AnalisisValoracionesFragment fragment = new AnalisisValoracionesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FECHAIN, fechaIn);
        args.putString(ARG_FECHAFIN, fechaFin);
        args.putString(ARG_HORAIN, horaIn);
        args.putString(ARG_HORAFIN, horaFin);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fechaIn = getArguments().getString(ARG_FECHAIN);
            fechaFin = getArguments().getString(ARG_FECHAFIN);
            horaIn = getArguments().getString(ARG_HORAIN);
            horaFin = getArguments().getString(ARG_HORAFIN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_analisis_valoraciones, container, false);

        manager = new DataBaseManager(v.getContext());
        mesas = new TreeMap<>();
        fechas = new TreeMap<>();
        valores = new TreeMap<>();
        empleados = new TreeMap<>();

        tv_valoracionMedia = (TextView) v.findViewById(R.id.tv_valoracionMedia);
        tv_valoraciones = (TextView) v.findViewById(R.id.tv_comandasGeneradas);
        tv_masValorado = (TextView) v.findViewById(R.id.tv_valoracionMas);
        tv_mesaMasValorada = (TextView) v.findViewById(R.id.tv_mesaSolicitada);
        tv_fechaMasValorada = (TextView) v.findViewById(R.id.tv_fechasolicitada);
        tv_empleadoMasValorado = (TextView) v.findViewById(R.id.tv_empleadoValorado);

        ImageButton btnMesas = (ImageButton) v.findViewById(R.id.btn_mesa);
        btnMesas.setOnClickListener(this);
        ImageButton btnFechas = (ImageButton) v.findViewById(R.id.btn_fecha);
        btnFechas.setOnClickListener(this);
        ImageButton btnEmpleados = (ImageButton) v.findViewById(R.id.btn_empleado);
        btnEmpleados.setOnClickListener(this);

        ImageButton btnValoracion = (ImageButton) v.findViewById(R.id.btn_valoracion);
        btnValoracion.setOnClickListener(this);

        obtenerTotalComandas();
        return v;
    }

    public void obtenerTotalComandas(){
        valoraciones = new ArrayList<>();
        String get_comandas = DataBaseManager.ip+"valoraciones/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        if(valoraciones.size()==0){
            request = new StringRequest(Request.Method.GET, get_comandas, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONArray array = new JSONArray(response);
                        JSONObject object;
                        for(int i = 0 ; i < array.length();i++){
                            object = array.getJSONObject(i);

                            String rest = object.getString("restaurante");
                            if(rest.equals(DataBaseManager.restaurante)) {
                                String id = object.getString("id");
                                String mesa = object.getString("mesa");
                                double valor = object.getDouble("valoracion");
                                String comentario = object.getString("comentario");
                                String fecha = object.getString("fecha");
                                String empleado = object.getString("empleado");
                                String cliente = object.getString("cliente");

                                boolean insert = true;

                                //Fecha de la comanda
                                StringTokenizer tokenizerFechaApi = new StringTokenizer(fecha,"-");
                                Integer diaApi = Integer.parseInt(tokenizerFechaApi.nextToken());
                                Integer mesApi = Integer.parseInt(tokenizerFechaApi.nextToken());
                                Integer añoApi = Integer.parseInt(tokenizerFechaApi.nextToken());

                                //Fecha de inicio
                                StringTokenizer tokenizerFechaIn = new StringTokenizer(fechaIn,"-");
                                Integer diaIn = Integer.parseInt(tokenizerFechaIn.nextToken());
                                Integer mesIn = Integer.parseInt(tokenizerFechaIn.nextToken());
                                Integer añoIn = Integer.parseInt(tokenizerFechaIn.nextToken());

                                //Fecha de fin
                                StringTokenizer tokenizerFechaFin = new StringTokenizer(fechaFin,"-");
                                Integer diaFin = Integer.parseInt(tokenizerFechaFin.nextToken());
                                Integer mesFin = Integer.parseInt(tokenizerFechaFin.nextToken());
                                Integer añoFin = Integer.parseInt(tokenizerFechaFin.nextToken());

                                if(añoIn>añoApi||añoApi>añoFin){
                                    insert = false;
                                }else if(añoIn.equals(añoApi)||añoFin.equals(añoApi)){
                                    if(añoIn.equals(añoApi)&&mesIn>mesApi){
                                        insert = false;
                                    }else if(añoIn.equals(añoApi)&&mesApi>mesFin){
                                        insert = false;
                                    }else if(mesIn.equals(mesApi)||mesApi.equals(mesFin)){
                                        if(mesIn.equals(mesApi)&&diaIn<diaApi){
                                            insert = false;
                                        }else if(mesIn.equals(mesApi)&&diaApi>diaFin){
                                            insert = false;
                                        }
                                    }
                                }
                                if(insert){
                                    Valoracion valoracion = new Valoracion(id,valor,comentario,cliente,empleado,fecha,mesa);
                                    valoraciones.add(valoracion);

                                    insertarFecha(valoracion);
                                    insertarMesa(valoracion);
                                    insertarValor(valoracion.getValoracion());
                                    insertarEmpleado(valoracion);
                                }
                            }
                        }
                        tv_valoraciones.setText(String.valueOf(valoraciones.size()));
                        String media = obtenerMedia();
                        tv_valoracionMedia.setText(media);
                        String fecha = obtenerFecha();
                        tv_fechaMasValorada.setText(fecha);
                        String nombreMesa = obtenerMesa();
                        tv_mesaMasValorada.setText(nombreMesa);
                        String empleado = obtenerEmpleado();
                        tv_empleadoMasValorado.setText(empleado);
                        String valoracion = obtenerValores();
                        tv_masValorado.setText(valoracion);

                    }catch (Exception e){
                        String err = e.toString();
                        Log.e("Error",err);
                    }
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                    String err = error.toString();
                    Log.e("Error",err);
                }
            });
            mRequestQueue.add(request);
        }
    }

    public void popUpGrafica(final Context context, final String nombre){
        //true: ascendente
        //false: descendente

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_graph, null);
        final GraphView graph = (GraphView) v.findViewById(R.id.graph);

        tb_tipo = (ToggleButton) v.findViewById(R.id.toggleButton_tipo);
        tb_tipo.setChecked(true);

        final Spinner spinnerFiltro = (Spinner) v.findViewById(R.id.spinner_filtro);
        String[] filtro = new String[]{"Tres elementos","Cinco elementos","Diez elementos","Quince elementos","Todos los elementos"};
        spinnerFiltro.setAdapter(new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,filtro));
        spinnerFiltro.setSelection(1);

        switch (nombre) {
            case "Mesas": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerGraficaMesas(position,graph,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerGraficaMesas(spinnerFiltro.getSelectedItemPosition(),graph,true);
                        }else{
                            obtenerGraficaMesas(spinnerFiltro.getSelectedItemPosition(),graph,false);
                        }
                    }
                });
                break;
            }
            case "Fechas": {
                obtenerGraficaFechas(graph);
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);
                break;
            }
            case "Empleados": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerGraficaEmpleados(position,graph,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerGraficaEmpleados(spinnerFiltro.getSelectedItemPosition(),graph,true);
                        }else{
                            obtenerGraficaEmpleados(spinnerFiltro.getSelectedItemPosition(),graph,false);
                        }
                    }
                });
                break;
            }
            case "Valoraciones":{
                spinnerFiltro.setVisibility(View.GONE);
                obtenerGraficaValoraciones(graph,tb_tipo.isChecked());
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerGraficaValoraciones(graph,true);
                        }else{
                            obtenerGraficaValoraciones(graph,false);
                        }
                    }
                });
                break;
            }
        }

        builder.setTitle(nombre);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


            }
        }).setNeutralButton("Ver lista", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                popUpLista(context,nombre);
            }
        });
        builder.show();
    }

    public void popUpLista(final Context context,final String nombre){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_list, null);


        final ToggleButton tb_tipo = (ToggleButton) v.findViewById(R.id.toggleButton_tipo);
        tb_tipo.setChecked(true);
        final Spinner spinnerFiltro = (Spinner) v.findViewById(R.id.spinner_filtro);
        String[] filtro = new String[]{"Tres elementos","Cinco elementos","Diez elementos","Quince elementos","Todos los elementos"};
        spinnerFiltro.setAdapter(new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,filtro));
        spinnerFiltro.setSelection(1);

        listView = (ListView)v.findViewById(R.id.lv_analisisProductos);

        switch (nombre) {
            case "Mesas": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerListaMesas(position,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerListaMesas(spinnerFiltro.getSelectedItemPosition(),true);
                        }else{
                            obtenerListaMesas(spinnerFiltro.getSelectedItemPosition(),false);
                        }
                    }
                });
                break;
            }
            case "Fechas": {
                obtenerListaFechas();
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);
                break;
            }
            case "Empleados": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerListaEmpleados(position,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerListaEmpleados(spinnerFiltro.getSelectedItemPosition(),true);
                        }else{
                            obtenerListaEmpleados(spinnerFiltro.getSelectedItemPosition(),false);
                        }
                    }
                });
                break;
            }
            case "Valoraciones":{
                spinnerFiltro.setVisibility(View.GONE);
                obtenerListaValoraciones(tb_tipo.isChecked());
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerListaValoraciones(true);
                        }else{
                            obtenerListaValoraciones(false);
                        }
                    }
                });
                break;
            }
        }

        builder.setTitle(nombre);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


            }
        }).setNeutralButton("Ver gráfica", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                popUpGrafica(context,nombre);
            }
        });
        builder.show();
    }

    public void obtenerGraficaValoraciones(GraphView graph, boolean orden){
        graph.removeAllSeries();
        Set<Map.Entry<Double, Integer>> set = valores.entrySet();
        Iterator<Map.Entry<Double,Integer>> iterator = set.iterator();

        String[] horizontal = new String[valores.size()];
        int[] ventas = new int[valores.size()];

        int i = 0;
        while(iterator.hasNext()){
            Map.Entry<Double,Integer> entry = iterator.next();
            horizontal[i] = String.format( "%.1f", entry.getKey() );
            ventas[i] = entry.getValue();
            i++;
        }

        if(!orden){
            String[] auxHor = horizontal.clone();
            horizontal = new String[auxHor.length];

            int[] auxVent = ventas.clone();
            ventas = new int[auxVent.length];
            i = ventas.length-1;
            for(int h = 0 ; h < auxHor.length; h++){
                horizontal[h] = auxHor[i];
                ventas[h] = auxVent[i];
                i--;
            }
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        //staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);
    }

    public void obtenerListaValoraciones(boolean orden){
        Set<Map.Entry<Double, Integer>> set = valores.entrySet();
        Iterator<Map.Entry<Double,Integer>> iterator = set.iterator();

        String[] horizontal = new String[valores.size()];
        int[] ventas = new int[valores.size()];

        int i = 0;
        while(iterator.hasNext()){
            Map.Entry<Double,Integer> entry = iterator.next();
            horizontal[i] = String.format( "%.1f", entry.getKey() );
            ventas[i] = entry.getValue();
            i++;
        }

        if(!orden){
            String[] auxHor = horizontal.clone();
            horizontal = new String[auxHor.length];

            int[] auxVent = ventas.clone();
            ventas = new int[auxVent.length];
            i = ventas.length-1;
            for(int h = 0 ; h < auxHor.length; h++){
                horizontal[h] = auxHor[i];
                ventas[h] = auxVent[i];
                i--;
            }
        }

        ArrayList<String> arrValor = new ArrayList<>();
        ArrayList<Integer> arrVentas = new ArrayList<>();
        for(int j = 0 ; j < horizontal.length; j++){
            arrValor.add(horizontal[j]);
            arrVentas.add(ventas[j]);
        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),arrValor,arrVentas);
        listView.setAdapter(mesasAdapter);


    }

    public void obtenerListaFechas(){
        StringTokenizer fecha = new StringTokenizer(fechaIn, "-");
        fecha.nextToken();
        int mesIn = Integer.parseInt(fecha.nextToken());
        Map<String, Double> mapaMeses = new TreeMap<>();

        ArrayList<Double> ventas = new ArrayList<>();

        fecha = new StringTokenizer(fechaFin, "-");
        fecha.nextToken();
        int mesFin = Integer.parseInt(fecha.nextToken());

        int cantMeses;
        if (mesIn < mesFin) {
            cantMeses = mesFin - mesIn + 1;
        } else {
            cantMeses = mesIn - mesFin;
        }
        ArrayList<String> meses = new ArrayList<>();
        for (int j = 0; j < cantMeses; j++) {
            mapaMeses.put(String.valueOf(mesIn), 0.0);
            switch (mesIn) {
                case 1:
                    meses.add("ene");
                    break;
                case 2:
                    meses.add("feb");
                    break;
                case 3:
                    meses.add("mar");
                    break;
                case 4:
                    meses.add("abr");
                    break;
                case 5:
                    meses.add("may");
                    break;
                case 6:
                    meses.add("jun");
                    break;
                case 7:
                    meses.add("jul");
                    break;
                case 8:
                    meses.add("ago");
                    break;
                case 9:
                    meses.add("sep");
                    break;
                case 10:
                    meses.add("oct");
                    break;
                case 11:
                    meses.add("nov");
                    break;
                case 12:
                    meses.add("dic");
                    break;
            }
            mesIn++;
            if (mesIn == 13) {
                mesIn = 1;
            }
        }
        Set<Map.Entry<String, ArrayList<Double>>> set = fechas.entrySet();
        for (Map.Entry<String, ArrayList<Double>> entry : set) {
            StringTokenizer tokenizerFecha = new StringTokenizer(entry.getKey(), "-");
            tokenizerFecha.nextToken();
            ArrayList<Double> valores;
            double media;
            double cant;
            String tokenMes = tokenizerFecha.nextToken();
            if (mapaMeses.containsKey(tokenMes)) {
                media = 0;
                valores = entry.getValue();
                for (double valor : valores) {
                    media += valor;
                }
                media /= valores.size();
                cant = mapaMeses.get(tokenMes);
                if (cant != 0) {
                    cant = (cant + media) / 2;
                } else {
                    cant = media;
                }
                mapaMeses.put(tokenMes, cant);
            }
        }
        Set<Map.Entry<String, Double>> set2 = mapaMeses.entrySet();
        for (Map.Entry<String, Double> entry : set2) {
            ventas.add(entry.getValue());
        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),meses,ventas);
        listView.setAdapter(mesasAdapter);
    }

    public void obtenerGraficaFechas(GraphView graph){
        graph.removeAllSeries();
        StringTokenizer fecha = new StringTokenizer(fechaIn, "-");
        fecha.nextToken();
        int mesIn = Integer.parseInt(fecha.nextToken());
        Map<String, Double> mapaMeses = new TreeMap<>();

        fecha = new StringTokenizer(fechaFin, "-");
        fecha.nextToken();
        int mesFin = Integer.parseInt(fecha.nextToken());

        int cantMeses;
        if (mesIn < mesFin) {
            cantMeses = mesFin - mesIn + 1;
        } else {
            cantMeses = mesIn - mesFin;
        }
        String[] horizontal = new String[cantMeses];
        double[] ventas = new double[horizontal.length];
        for (int j = 0; j < cantMeses; j++) {
            mapaMeses.put(String.valueOf(mesIn), 0.0);
            switch (mesIn) {
                case 1:
                    horizontal[j] = "ene";
                    break;
                case 2:
                    horizontal[j] = "feb";
                    break;
                case 3:
                    horizontal[j] = "mar";
                    break;
                case 4:
                    horizontal[j] = "abr";
                    break;
                case 5:
                    horizontal[j] = "may";
                    break;
                case 6:
                    horizontal[j] = "jun";
                    break;
                case 7:
                    horizontal[j] = "jul";
                    break;
                case 8:
                    horizontal[j] = "ago";
                    break;
                case 9:
                    horizontal[j] = "sep";
                    break;
                case 10:
                    horizontal[j] = "oct";
                    break;
                case 11:
                    horizontal[j] = "nov";
                    break;
                case 12:
                    horizontal[j] = "dic";
                    break;
            }
            mesIn++;
            if (mesIn == 13) {
                mesIn = 1;
            }
        }
        Set<Map.Entry<String, ArrayList<Double>>> set = fechas.entrySet();
        for (Map.Entry<String, ArrayList<Double>> entry : set) {
            StringTokenizer tokenizerFecha = new StringTokenizer(entry.getKey(), "-");
            tokenizerFecha.nextToken();
            ArrayList<Double> valores;
            double media;
            double cant;
            String tokenMes = tokenizerFecha.nextToken();
            if (mapaMeses.containsKey(tokenMes)) {
                media = 0;
                valores = entry.getValue();
                for (double valor : valores) {
                    media += valor;
                }
                media /= valores.size();
                cant = mapaMeses.get(tokenMes);
                if (cant != 0) {
                    cant = (cant + media) / 2;
                } else {
                    cant = media;
                }
                mapaMeses.put(tokenMes, cant);
            }
        }
        Set<Map.Entry<String, Double>> set2 = mapaMeses.entrySet();
        Iterator<Map.Entry<String, Double>> it2 = set2.iterator();
        int i = 0;
        while (it2.hasNext()) {
            Map.Entry<String, Double> entry = it2.next();
            ventas[i] = entry.getValue();
            i++;
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);
    }

    public void obtenerListaMesas(int numElement, boolean orden){
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = mesas.size();
                break;
        }

        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Double> arrVentas = new ArrayList<>();
        Map<String, ArrayList<Double>> auxMesas = new TreeMap<>();
        auxMesas.putAll(mesas);
        Set<Map.Entry<String, ArrayList<Double>>> setEntry = auxMesas.entrySet();
        for (int i = 0; i < numElement; i++) {
            String mes = "";
            String idMesa = "";
            double valMesa;

            if(orden){
                valMesa = 0;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media > valMesa) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        valMesa = media;
                    }
                }
            }else{
                valMesa = -1;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media < valMesa || valMesa== -1) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        valMesa = media;
                    }
                }
            }
            if(!idMesa.equals("")){
                auxMesas.remove(idMesa);
                arrHorizontal.add(mes);
                arrVentas.add(valMesa);
            }
        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),arrHorizontal,arrVentas);
        listView.setAdapter(mesasAdapter);
    }

    public void obtenerGraficaMesas(int numElement,GraphView graph, boolean orden){
        graph.removeAllSeries();
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = mesas.size();
                break;
        }

        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Double> arrVentas = new ArrayList<>();
        Map<String, ArrayList<Double>> auxMesas = new TreeMap<>();
        auxMesas.putAll(mesas);
        Set<Map.Entry<String, ArrayList<Double>>> setEntry = auxMesas.entrySet();
        for (int i = 0; i < numElement; i++) {
            String mes = "";
            String idMesa = "";
            double valMesa;

            if(orden){
                valMesa = 0;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media > valMesa) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        valMesa = media;
                    }
                }
            }else{
                valMesa = -1;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media < valMesa || valMesa== -1) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        valMesa = media;
                    }
                }
            }
            if(!idMesa.equals("")){
                auxMesas.remove(idMesa);
                arrHorizontal.add(mes);
                arrVentas.add(valMesa);
            }


        }
        String[] horizontal;
        double[] ventas;
        if(arrHorizontal.size()<2){
            horizontal = new String[2];
        }else{
            horizontal = new String[arrHorizontal.size()];
        }
        for(int i = 0 ; i < arrHorizontal.size(); i++){
            horizontal[i] = arrHorizontal.get(i);
        }
        if(arrVentas.size()<2){
            ventas = new double[2];
        }else{
            ventas = new double[arrVentas.size()];
        }
        for(int i = 0 ; i < arrVentas.size(); i++){
            ventas[i] = arrVentas.get(i);
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        //staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);
    }

    public void obtenerGraficaEmpleados(int numElement,GraphView graph, boolean orden){
        graph.removeAllSeries();
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = mesas.size();
                break;
        }

        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Double> arrVentas = new ArrayList<>();
        Map<String, ArrayList<Double>> auxEmpleados = new TreeMap<>();
        auxEmpleados.putAll(empleados);
        Set<Map.Entry<String, ArrayList<Double>>> setEntry = auxEmpleados.entrySet();
        for (int i = 0; i < numElement; i++) {
            String emp = "";
            String idEmp = "";
            double valEmp;

            if(orden){
                valEmp = 0;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media > valEmp) {
                        idEmp = entry.getKey();
                        emp = manager.obtenerEmpleado(entry.getKey()).getNombre();
                        valEmp = media;
                    }
                }
            }else{
                valEmp = -1;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media < valEmp || valEmp== -1) {
                        idEmp = entry.getKey();
                        emp = manager.obtenerEmpleado(entry.getKey()).getNombre();
                        valEmp = media;
                    }
                }
            }
            if(!idEmp.equals("")){
                auxEmpleados.remove(idEmp);
                arrHorizontal.add(emp);
                arrVentas.add(valEmp);
            }


        }
        String[] horizontal;
        double[] ventas;
        if(arrHorizontal.size()<2){
            horizontal = new String[2];
        }else{
            horizontal = new String[arrHorizontal.size()];
        }
        for(int i = 0 ; i < arrHorizontal.size(); i++){
            horizontal[i] = arrHorizontal.get(i);
        }
        if(arrVentas.size()<2){
            ventas = new double[2];
        }else{
            ventas = new double[arrVentas.size()];
        }
        for(int i = 0 ; i < arrVentas.size(); i++){
            ventas[i] = arrVentas.get(i);
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);

    }

    public void obtenerListaEmpleados(int numElement, boolean orden){
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = mesas.size();
                break;
        }

        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Double> arrVentas = new ArrayList<>();
        Map<String, ArrayList<Double>> auxEmpleados = new TreeMap<>();
        auxEmpleados.putAll(empleados);
        Set<Map.Entry<String, ArrayList<Double>>> setEntry = auxEmpleados.entrySet();
        for (int i = 0; i < numElement; i++) {
            String emp = "";
            String idEmp = "";
            double valEmp;

            if(orden){
                valEmp = 0;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media > valEmp) {
                        idEmp = entry.getKey();
                        emp = manager.obtenerEmpleado(entry.getKey()).getNombre();
                        valEmp = media;
                    }
                }
            }else{
                valEmp = -1;
                for (Map.Entry<String, ArrayList<Double>> entry : setEntry) {
                    ArrayList<Double> valores = entry.getValue();
                    double media = 0;
                    for(double valor:valores){
                        media+=valor;
                    }
                    media /=valores.size();
                    if (media < valEmp || valEmp== -1) {
                        idEmp = entry.getKey();
                        emp = manager.obtenerEmpleado(entry.getKey()).getNombre();
                        valEmp = media;
                    }
                }
            }
            if(!idEmp.equals("")){
                auxEmpleados.remove(idEmp);
                arrHorizontal.add(emp);
                arrVentas.add(valEmp);
            }


        }

        /*switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = productos.size();
                break;
        }
        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Integer> arrVentas = new ArrayList<>();
        Map<String, Integer> auxProductos = new TreeMap<>();
        auxProductos.putAll(productos);
        Set<Map.Entry<String, Integer>> setEntry = auxProductos.entrySet();
        for (int i = 0; i < numElement; i++) {
            String pro = "";
            String idPro = "";
            int cantPro;

            if(orden){
                cantPro = 0;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() > cantPro) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }else{
                cantPro = -1;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() < cantPro || cantPro== -1) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }
            if(!idPro.equals("")){
                auxProductos.remove(idPro);
                arrHorizontal.add(pro);
                arrVentas.add(cantPro);
            }


        }*/

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),arrHorizontal,arrVentas);
        listView.setAdapter(mesasAdapter);

    }

    public void insertarFecha(Valoracion valoracion){
        double valor = valoracion.getValoracion();
        String fecha = valoracion.getFecha();
        ArrayList<Double> valores;
        if(fechas.containsKey(fecha)){
            valores = fechas.get(fecha);
            valores.add(valor);
            fechas.put(fecha,valores);
        }else{
            valores = new ArrayList<>();
            valores.add(valor);
            fechas.put(fecha,valores);
        }
    }

    public String obtenerFecha(){
        String fecha="";
        double cant = 0;
        ArrayList<Double> valores = new ArrayList<>();

        Set<Map.Entry<String, ArrayList<Double>>> entry= fechas.entrySet();
        for (Map.Entry<String, ArrayList<Double>> ent : entry) {
            valores = ent.getValue();
            double media = 0;
            for (double valor:valores){
                media+=valor;
            }
            media /=valores.size();
            if (cant < media) {
                cant = media;
                fecha = ent.getKey();
            }
        }
        return String.format("%s (%s estrellas en %d valoraciones)", fecha, String.format("%.1f", cant), valores.size());
    }

    public void insertarMesa(Valoracion valoracion){
        double valor = valoracion.getValoracion();
        String mesa = valoracion.getMesa();
        ArrayList<Double> valores;
        if(mesas.containsKey(mesa)){
            valores = mesas.get(mesa);
            valores.add(valor);
            mesas.put(mesa,valores);
        }else{
            valores = new ArrayList<>();
            valores.add(valor);
            mesas.put(mesa,valores);
        }
    }

    public String obtenerMesa(){
        String mesa="";
        double cant = 0;
        ArrayList<Double> valores = new ArrayList<>();
        Set<Map.Entry<String, ArrayList<Double>>> entryMesa= mesas.entrySet();
        for (Map.Entry<String, ArrayList<Double>> entry : entryMesa) {
            valores = entry.getValue();
            double media = 0;
            for (double valor:valores){
                media+=valor;
            }
            media /=valores.size();
            if (cant < media) {
                cant = media;
                mesa = entry.getKey();
            }

        }
        mesa = manager.obtenerMesa(Integer.parseInt(mesa)).get(5);
        return mesa + " (" + String.format( "%.1f", cant ) + " estrellas en " + valores.size() + " valoraciones)";
    }

    public void insertarValor(double valor){
        if(valores.containsKey(valor)){
            int x = valores.get(valor);
            valores.put(valor,x+1);
        }else{
            valores.put(valor,1);
        }
    }

    public String obtenerValores(){
        String valor = "";
        int cant = -1;
        Set<Map.Entry<Double, Integer>> entryValor= valores.entrySet();
        for (Map.Entry<Double, Integer> entry : entryValor) {
            if (cant < entry.getValue()) {
                cant = entry.getValue();
                valor = String.valueOf(entry.getKey());
            }

        }
        valor = valor + " estrellas";
        return valor+" (" + cant + ")";
    }

    public void insertarEmpleado(Valoracion valoracion){
        double valor = valoracion.getValoracion();
        String empleado = valoracion.getEmpleado();
        ArrayList<Double> valores;
        if(empleados.containsKey(empleado)){
            valores = empleados.get(empleado);
            valores.add(valor);
            empleados.put(empleado,valores);
        }else{
            valores = new ArrayList<>();
            valores.add(valor);
            empleados.put(empleado,valores);
        }
    }

    public String obtenerEmpleado(){
        String empleado="";
        double cant = 0;
        ArrayList<Double> valores = new ArrayList<>();
        Set<Map.Entry<String, ArrayList<Double>>> entryMesa= empleados.entrySet();
        for (Map.Entry<String, ArrayList<Double>> entry : entryMesa) {
            valores = entry.getValue();
            double media = 0;
            for (double valor:valores){
                media+=valor;
            }
            media /=valores.size();
            if (cant < media) {
                cant = media;
                empleado = entry.getKey();
            }

        }
        empleado = manager.obtenerEmpleado(empleado).getNombre();
        return String.format(Locale.getDefault(),"%s (%s estrellas en %d valoraciones)", empleado, String.format("%.1f", cant), valores.size());
    }

    public String obtenerMedia(){
        double media = 0;
        int cant = 0;
        Set<Map.Entry<Double,Integer>> set = valores.entrySet();
        Iterator<Map.Entry<Double,Integer>> iterator = set.iterator();
        while(iterator.hasNext()){
            Map.Entry<Double,Integer> entry = iterator.next();
            media += entry.getKey()*entry.getValue();
            cant +=entry.getValue();
        }
        media /= cant;
        return String.format(Locale.getDefault(),"%.1f",media) + " estrellas";
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_mesa:
                popUpGrafica(v.getContext(),"Mesas");
                break;
            case R.id.btn_fecha:
                popUpGrafica(v.getContext(),"Fechas");
                break;
            case R.id.btn_empleado:
                popUpGrafica(v.getContext(),"Empleados");
                break;
            case R.id.btn_valoracion:
                popUpGrafica(v.getContext(),"Valoraciones");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
