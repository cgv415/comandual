package com.example.carlos.comandual;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 11/12/2016
 */

public class EstadoAdapter extends BaseAdapter {
    private DataBaseManager manager;
    private TextView tvCantidad;
    private TextView tvProducto;
    private Activity activity;
    private TextView tvMesa;
    private TextView tvEstado;
    private Spinner spinner;
    //Map<Producto,Mesa>
    private ArrayList<ComandaClass> productos;
    String tipo;
    private static int gratis;

    public EstadoAdapter(Activity activity, ArrayList<ComandaClass> productos,String tipo) {
        this.activity = activity;
        this.productos = productos;
        this.tipo = tipo;

        manager = new DataBaseManager(activity.getApplicationContext());
        gratis = 0;
        tapaGratis();

    }

    @Override
    public int getCount() {
        return productos.size();
    }

    @Override
    public Object getItem(int position) {
        ComandaClass item = productos.get(position);
        return item;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_estado_comanda, null);
        }

        if(tipo.equals("mesa")){//Si son las comandas de una mesa
            ComandaClass producto = (ComandaClass) getItem(position);
            int cantidad = Integer.valueOf(producto.getCantidad());
            Double precioUnidad = manager.obtenerProducto(producto.getProducto()).getPrecio();

            Double precio = precioUnidad * cantidad;

            if( producto.getTipo().equals("tapa")){
                int cantidadGratis = gratis - cantidad;
                int cantidadPagar = 0;
                if(cantidadGratis<0){
                    cantidadPagar = Math.abs(cantidadGratis);
                    gratis = 0;
                }else{
                    gratis -= cantidad;
                }

                precio = cantidadPagar * precioUnidad;

                tvMesa = (TextView) v.findViewById(R.id.tvMesa);
                tvMesa.setText(String.format( "%.2f", precio ));
            }else{
                tvMesa = (TextView) v.findViewById(R.id.tvMesa);
                tvMesa.setText(String.format( "%.2f", precio ));
            }


            tvCantidad = (TextView) v.findViewById(R.id.tvCantidad);
            tvCantidad.setText(String.valueOf(producto.getCantidad()));

            tvProducto = (TextView) v.findViewById(R.id.tvProducto);
            tvProducto.setText(producto.getProducto());

            tvEstado = (TextView) v.findViewById(R.id.tvEstado);
            tvEstado.setText(producto.getEstado());

        }else{//Si son todas las comandas
            ComandaClass producto = (ComandaClass) getItem(position);
            tvMesa = (TextView) v.findViewById(R.id.tvMesa);
            String nombreMesa = manager.obtenerMesa(Integer.parseInt(producto.getIdMesa())).get(5);
            tvMesa.setText(nombreMesa);

            tvCantidad = (TextView) v.findViewById(R.id.tvCantidad);
            tvCantidad.setText(String.valueOf(producto.getCantidad()));

            tvProducto = (TextView) v.findViewById(R.id.tvProducto);
            tvProducto.setText(producto.getProducto());
        }

        return v;
    }

    public void tapaGratis(){
        String categoria;
        for(ComandaClass producto:productos){
            categoria = producto.getTipo();
            if(categoria.equals("bebidas")){
                gratis = gratis + Integer.valueOf(producto.getCantidad());
            }
        }
    }
}
