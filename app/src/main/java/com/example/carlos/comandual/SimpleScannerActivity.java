package com.example.carlos.comandual;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.StringTokenizer;

//Generador
//https://github.com/dm77/barcodescanner
//Lector
//https://www.youtube.com/watch?v=W6-kK6tAML4

public class SimpleScannerActivity extends AppCompatActivity{
    //qr code scanner object
    private DataBaseManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_scanner);

        manager = new DataBaseManager(this);
        //intializing scan object
        IntentIntegrator qrScan = new IntentIntegrator(this);
        qrScan.initiateScan();
    }

    //Getting the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                try{
                    StringTokenizer str = new StringTokenizer(result.getContents(),"-");
                    String idRest = str.nextToken();

                    int mesa = Integer.valueOf(str.nextToken());

                    //if qr contains data
                    DataBaseManager.restaurante = idRest;
                    generarMesas(idRest);
                    //manager.modificarMesaClienteApi(String.valueOf(mesa));
                    //manager.modificarEstadoMesa(String.valueOf(mesa),"OCUPADA");
                    manager.actualizarCliente();
                    try{
                        Thread.sleep(1000);
                        manager.asignarMesaCliente(String.valueOf(mesa));
                        Intent intent = new Intent(getApplicationContext(),MesaActivity.class);
                        intent.putExtra("idMesa",mesa);
                        startActivity(intent);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    //Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void generarMesas(final String restaurante){
        manager.crearTablaMesa();

        String get_mesas = DataBaseManager.ip+"mesas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_mesas, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String rest = object.getString("user");
                        if(rest.equals(restaurante)){
                            //{"id":3,"nombre":"Mesa Baja 1","capacidad":4,"forma":"CUADRADA","estado":"LIBRE","espacio":11,"user":8}
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String capacidad = object.getString("capacidad");
                            String forma = object.getString("forma");
                            String estado = object.getString("estado");
                            String espacio = object.getString("espacio");
                            String cliente = object.getString("cliente");
                            String empleado = object.getString("empleado");
                            manager.insertarMesa(id,nombre, capacidad, forma, estado, espacio, cliente,empleado);
                        }
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }

}
