package com.example.carlos.comandual;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 14/11/2016
 */

public class IngredientesAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<String> todosIngredientes;
    private ArrayList<String> ingredientesProducto;
    private ArrayList<CheckBox> checkBoxArrayList;

    private DataBaseManager manager;

    private CheckBox checkBox;

    public IngredientesAdapter(Activity activity, ArrayList<String> todosIngredientes, ArrayList<String> ingredientesProducto) {
        this.activity = activity;
        this.todosIngredientes = todosIngredientes;
        this.ingredientesProducto = ingredientesProducto;
    }

    @Override
    public int getCount() {
        return todosIngredientes.size();
    }

    @Override
    public Object getItem(int position) {
        return todosIngredientes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View v = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_elegir_ingredientes, null);
        }

        manager = new DataBaseManager(v.getContext());

        if(checkBoxArrayList == null){
            checkBoxArrayList = new ArrayList<>();
            String ingrediente = "";
            for(int i = 0 ; i < todosIngredientes.size();i++){
                ingrediente = todosIngredientes.get(i);
                CheckBox checkBox = new CheckBox(v.getContext());
                checkBox.setText(ingrediente);
                if(ingredientesProducto.contains(ingrediente)){
                    checkBox.setChecked(true);
                }else{
                    checkBox.setChecked(false);
                }
                checkBoxArrayList.add(checkBox);
            }
        }

        String ingrediente = todosIngredientes.get(position);
        checkBox = (CheckBox) v.findViewById(R.id.checkBoxIngrediente);
        checkBox.setText(ingrediente);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                actualizarArrayCheckBox(buttonView.getText().toString(),isChecked);
            }
        });

        if(manager.inStrockIngrediente(ingrediente)){
            if(isCheck(ingrediente)){
                checkBox.setChecked(true);
            }else{
                checkBox.setChecked(false);
            }
            checkBox.setEnabled(true);
        }else{
            checkBox.setChecked(false);
            checkBox.setEnabled(false);
        }


        return v;
    }

    public boolean isCheck(String nombre){
        for(int i = 0; i < checkBoxArrayList.size();i++){
            if(checkBoxArrayList.get(i).getText().toString().equals(nombre)){
                return checkBoxArrayList.get(i).isChecked();
            }
        }
        return false;
    }

    public void actualizarArrayCheckBox(String nombre,boolean check){
        for(int i = 0; i < checkBoxArrayList.size();i++){
            if(checkBoxArrayList.get(i).getText().toString().equals(nombre)){
                checkBoxArrayList.get(i).setChecked(check);
            }
        }
    }

    public ArrayList<CheckBox> getCheckBoxArrayList(){
        return checkBoxArrayList;
    }

}
