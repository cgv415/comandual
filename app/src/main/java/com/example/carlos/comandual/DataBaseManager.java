package com.example.carlos.comandual;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

//https://www.youtube.com/watch?v=ma8z1rcFyjI

//https://www.youtube.com/watch?v=sD-pz-vKlnI
public class DataBaseManager implements Database, DatabaseApi {

    //static String ipPublic = "http://46.6.101.215";

    private static final String SEED = "Your_seed_....";

    //Ip para el movil (sin wifi)
    //static String ip ="http://188.127.171.76";
    private static final String TABLE_PERSONAL = "personal";
    private static final String CN_ID = "_id";
    private static final String CN_NOMBRE = "nombre";
    private static final String CN_APELLIDO = "apellido";
    private static final String CN_CORREO = "correo";
    private static final String CN_NICK = "nick";
    private static final String CN_PASSWORD = "password";
    private static final String CN_RESTAURANTE = "restaurante";

    /*db.update(String table, Content values,String where clause,String[]where args);
        db.update(TABLE_INGREDIENES, generarValores(nombre,stock),CN_INGREDIENTE +"= ?",new String[]{nombre});*/

        /* db.insert(String table, String nullColumnHack, ContentValues values
        * db.insert(TABLE_PRODUCTO_TIPO, null, generarValoresProducto_Tipo(producto, tipo));*/

        /* db.query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having,
            String orderBy)
        * db.query(TABLE_PRODUCTO_TIPO,columnas,CN_IDPRODUCTO+ " = ?",new String[]{producto},null,null,null);*/
    private static final String CREATE_TABLE_PERSONAL = "create table " + TABLE_PERSONAL + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NOMBRE + " text not null,"
            + CN_APELLIDO + " text not null,"
            + CN_CORREO + " text not null unique,"
            + CN_NICK + " text not null unique,"
            + CN_PASSWORD + " text not null,"
            + CN_RESTAURANTE + " text not null"
            + ");";

    private static final String TABLE_PRODUCTO = "producto";


    private static final String CN_DESCRIPCION = "descripcion";
    private static final String CN_CATEGORIA = "categoria";
    private static final String CN_STOCK = "stock";
    private static final String CN_PRECIO = "precio";
    private static final String CREATE_TABLE_PRODUCTO = "create table " + TABLE_PRODUCTO + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NOMBRE + " text not null,"
            + CN_DESCRIPCION + " text,"
            + CN_STOCK + " text not null,"
            + CN_CATEGORIA + " text not null,"
            + CN_PRECIO + " real not null"
            + ");";

    private static final String TABLE_TIPO = "tipo";
    private static final String CN_TIPO = "tipo";
    private static final String CREATE_TABLE_TIPO = "create table " + TABLE_TIPO + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_TIPO + " text not null,"
            + CN_PRECIO + " real not null"
            + ");";

    private static final String TABLE_PRODUCTO_TIPO = "producto_tipo";
    private static final String CN_IDPRODUCTO = "idproducto";
    private static final String CN_IDTIPO = "idtipo";
    private static final String CREATE_TABLE_PRODUCTO_TIPO = "create table " + TABLE_PRODUCTO_TIPO + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_IDPRODUCTO + " integer not null,"
            + CN_IDTIPO + " integer not null"
            + ");";

    private static final String TABLE_PRODUCTO_CATEGORIA = "producto_categoria";
    private static final String CN_IDCATEGORIA = "idcategoria";
    private static final String CREATE_TABLE_PRODUCTO_CATEGORIA = "create table " + TABLE_PRODUCTO_CATEGORIA + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_IDPRODUCTO + " integer not null,"
            + CN_IDCATEGORIA + " integer not null"
            + ");";

    private static final String TABLE_INGREDIENTE = "ingrediente";
    private static final String CN_INGREDIENTE = "ingrediente";
    private static final String CREATE_TABLE_INGREDIENTE = "create table " + TABLE_INGREDIENTE + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_INGREDIENTE + " text not null,"
            + CN_STOCK + " text not null"
            + ");";

    private static final String TABLE_PRODUCTO_INGREDIENTE = "producto_ingrediente";
    private static final String CN_NOMBREINGREDIENTE = "nombreingrediente";
    private static final String CN_NOMBREPRODUCTO = "nombreproducto";
    private static final String CREATE_TABLE_PRODUCTO_INGREDIENTE = "create table " + TABLE_PRODUCTO_INGREDIENTE + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NOMBREPRODUCTO + " text not null,"
            + CN_NOMBREINGREDIENTE + " text not null"
            + ");";

    private static final String TABLE_CATEGORIA = "categoria";
    private static final String CREATE_TABLE_CATEGORIA = "create table " + TABLE_CATEGORIA + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_CATEGORIA + " text not null"
            + ");";

    private static final String TABLE_COMANDA = "comanda";
    private static final String CN_IDMESA = "idmesa";
    private static final String CN_CANTIDAD = "cantidad";
    private static final String CN_ESTADO = "estado";
    private static final String CN_TOKEN = "token";
    private static final String CN_FECHA = "fecha";
    private static final String CN_HORA = "hora";
    private static final String CN_COMENTARIO = "comentario";
    private static final String CREATE_TABLE_COMANDA = "create table " + TABLE_COMANDA + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_IDMESA + " integer not null,"
            + CN_IDPRODUCTO + " integer not null,"
            + CN_IDCATEGORIA + " integer not null,"
            + CN_COMENTARIO + " text not null,"
            + CN_ESTADO + " text not null,"
            + CN_CANTIDAD + " integer not null,"
            + CN_TOKEN + " text not null,"
            + CN_FECHA + " text not null,"
            + CN_HORA + " text not null"
            + ");";

    private static final String TABLE_RESTAURANTE = "restaurante";
    private static final String CN_DIRECCION = "direccion";
    private static final String CN_LOCALIDAD = "localidad";
    private static final String CN_PROVINCIA = "provincia";
    private static final String CN_LATITUD = "latitud";
    private static final String CN_LONGITUD = "longitud";
    private static final String CREATE_TABLE_RESTAURANTE = "create table " + TABLE_RESTAURANTE + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NOMBRE + " text not null,"
            + CN_DIRECCION + " text not null,"
            + CN_PROVINCIA + " text not null,"
            + CN_LOCALIDAD + " text not null,"
            + CN_LATITUD + " text not null,"
            + CN_LONGITUD + " text not null"
            + ");";

    private static final String TABLE_EMPLEADO = "empleado";
    private static final String CREATE_TABLE_EMPLEADO = "create table " + TABLE_EMPLEADO + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NOMBRE + " text not null,"
            + CN_RESTAURANTE + " text not null,"
            + CN_TOKEN + " text not null,"
            + CN_PASSWORD + " text not null"
            + ");";

    private static final String TABLE_CLIENTE = "cliente";
    private static final String CREATE_TABLE_CLIENTE = "create table " + TABLE_CLIENTE + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NOMBRE + " text not null,"
            + CN_PASSWORD + " text not null,"
            + CN_IDMESA + " text"
            + ");";

    private static final String TABLE_SESION = "sesion";
    private static final String CREATE_TABLE_SESION = "create table " + TABLE_SESION + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NICK + " text not null,"
            + CN_PASSWORD + " text not null,"
            + CN_RESTAURANTE + " text not null,"
            + CN_TIPO + " text not null,"
            + CN_TOKEN + " text not null"
            + ");";

    private static final String TABLE_MESA = "mesa";
    private static final String CN_CAPACIDAD = "capacidad";
    private static final String CN_FORMA = "forma";
    private static final String CN_ESPACIO = "espacio";
    private static final String CN_CLIENTE = "cliente";
    private static final String CN_EMPLEADO = "empleado";
    private static final String CREATE_TABLE_MESA = "create table " + TABLE_MESA + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_NOMBRE + " text not null,"
            + CN_CAPACIDAD + " integer not null,"
            + CN_FORMA + " text not null,"
            + CN_ESTADO + " text not null,"
            + CN_ESPACIO + " text not null,"
            + CN_CLIENTE + " text not null,"
            + CN_EMPLEADO + " text not null"
            + ");";

    private static final String CN_IDEMPLEADO = "idempleado";
    private static final String TABLE_EMPLEADO_MESA = "empleado_mesa";
    private static final String CREATE_TABLE_EMPLEADO_MESA = "create table " + TABLE_EMPLEADO_MESA + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_IDMESA + " text not null,"
            + CN_IDEMPLEADO + " text not null"
            + ");";

    private static final String TABLE_ESPECIAL = "especial";
    private static final String CN_TITULO = "titulo";
    private static final String CN_ACTIVO = "activo";
    private static final String CREATE_TABLE_ESPECIAL = "create table " + TABLE_ESPECIAL + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_TITULO + " text not null,"
            + CN_NOMBREPRODUCTO + " text not null,"
            + CN_PRECIO + " text not null,"
            + CN_ACTIVO + " text not null"
            + ");";

    private static final String TABLE_ESPACIO = "espacio";
    private static final String CREATE_TABLE_ESPACIO = "create table " + TABLE_ESPACIO + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_ESPACIO + " text not null"
            + ");";

    private static final String TABLE_TOKEN = "token";
    private static final String CREATE_TABLE_TOKEN = "create table " + TABLE_TOKEN + "("
            + CN_ID + " integer primary key autoincrement,"
            + CN_TOKEN + " text not null"
            + ");";
    static boolean admin;

    static String ip = "https://comandual.herokuapp.com/api/";
    static String restaurante;
    static String tipoUsuario;
    static boolean demo;
    static String idUsuario;
    public Context context;
    private String url;
    private SQLiteDatabase db;

    public DataBaseManager(Context context) {

        DBHelper helper;
        helper = new DBHelper(context);
        this.context = context;
        db = helper.getWritableDatabase();
        //this.comprobarBaseDatos();
        //this.actualizar();
    }

    static boolean isAdmin() {
        return admin;
    }

    public static String stringToUnicode(String cadena) {
        ArrayList<String> acentos = new ArrayList<>();
        String[] acentosArr = {"á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "ñ", "Ñ", "à", "è", "ì", "ò", "ù", "À", "È", "Ì", "Ò", "Ù", "©", "º", "°", "-", "¿"};
        Collections.addAll(acentos, acentosArr);
        String[] unicodesArr = {"u00E1"/*á*/, "u00E9"/*é*/, "u00ED"/*í*/, "u00F3"/*ó*/, "u00FA"/*ú*/, "u00C1"/*Á*/, "u00c9"/*É*/, "u00CD"/*Í*/, "u00D3"/*Ó*/, "u00DA"/*Ú*/, "u00F1"/*ñ*/, "u00f1"/*ñ*/, "u00D1"/*Ñ*/, "u00e0"/*à*/, "u00e8"/*è*/, "u00ec"/*ì*/, "u00f2"/*ò*/, "u00f9"/*ù*/, "u00c0"/*À*/, "u00c8"/*È*/, "u00cc"/*Ì*/, "u00d2"/*Ò*/, "u00d9"/*Ù*/, "u00A9"/*©*/, "u00B0"/*º*/, "u00BA"/*°*/, "u002D"/*-*/, "u00BF"/*¿*/, "u0022"/*\"*/};
        ArrayList<String> unicodes = new ArrayList<>();
        Collections.addAll(unicodes, unicodesArr);
        String nuevaCadena = "";
        for (int i = 0; i < cadena.length(); i++) {
            char c = cadena.charAt(i);
            String s = "" + c;
            if (acentos.contains(s)) {
                nuevaCadena += unicodes.get(acentos.indexOf(s));
            } else {
                nuevaCadena += c;
            }
        }
        return nuevaCadena;
    }

    public static String removeAcentos(String cadena) {
        ArrayList<String> acentos = new ArrayList<>();
        String[] acentosArr = {"á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "à", "è", "ì", "ò", "ù", "À", "È", "Ì", "Ò", "Ù", "©", "º", "°", "¿"};
        Collections.addAll(acentos, acentosArr);
        String[] unicodesArr = {"a"/*á*/, "e"/*é*/, "i"/*í*/, "o"/*ó*/, "u"/*ú*/, "A"/*Á*/, "E"/*É*/, "I"/*Í*/, "O"/*Ó*/, "U"/*Ú*/, "a"/*à*/, "e"/*è*/, "i"/*ì*/, "o"/*ò*/, "u"/*ù*/, "A"/*À*/, "E"/*È*/, "I"/*Ì*/, "O"/*Ò*/, "U"/*Ù*/, ""/*©*/, ""/*º*/, ""/*°*/, ""/*¿*/, ""/*\"*/};
        ArrayList<String> unicodes = new ArrayList<>();
        Collections.addAll(unicodes, unicodesArr);
        String nuevaCadena = "";
        for (int i = 0; i < cadena.length(); i++) {
            char c = cadena.charAt(i);
            String s = "" + c;
            if (acentos.contains(s)) {
                nuevaCadena += unicodes.get(acentos.indexOf(s));
            } else {
                nuevaCadena += c;
            }
        }
        return nuevaCadena;
    }

    static boolean enviarNotificacion(String title, String body, String to) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String urlCMS = "https://fcm.googleapis.com/fcm/send";
        final String jsonString =
                "{" +
                        //"'to':'ddp0zVEORB4:APA91bEvq1z7eDONhLehlSZXR-pjHZrIec9JeCDdYmQM3VDlHkG0eDePAFzW7qHlBc4ohAyqZXV4TlConEWgDY0AZZq05Ki75pZ23SFb07HyvVDWKK9I012kyc9KEsyP_lW0tKsYcCpx',\n" +
                        "'to':'/topics/" + to + "'," +
                        "'notification':{" +
                        "'title':'" + title + "'," +
                        "'body':'" + body + "'" +
                        "}" +
                        "}";
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    urlCMS, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Response", "Error: " + error.getMessage());
                }
            }) {
                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "key=AIzaSyDAE9MwmYI9VjGjTqd6qLzOadga0tqvurs");
                    return params;
                }
            };
            mRequestQueue.add(jsonObjReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public void gestionarMenu(MenuItem item, Context context) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_home) {
            if (tipoUsuario.equals("empleado")) {
                intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
            } else {
                ClienteClass cliente = obtenerCliente();
                if (cliente.hasMesa()) {
                    intent = new Intent(context, MesaActivity.class);
                    intent.putExtra("idMesa", Integer.parseInt(cliente.getMesa()));
                    context.startActivity(intent);
                } else {
                    intent = new Intent(context, InicioCliente.class);
                    context.startActivity(intent);
                }
            }
        } else if (id == R.id.nav_stock) {
            intent = new Intent(context, StockActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_comandas) {
            intent = new Intent(context, ComandasActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_sugerencias) {
            intent = new Intent(context, SugerenciasActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_manage) {
            intent = new Intent(context, SettingsActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_graphs) {
            intent = new Intent(context, EstadisticasActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_refresh) {
            actualizar();
            Toast.makeText(context, "Base de datos actualizada", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_mesas) {
            intent = new Intent(context, GestionarMesasActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_logout) {
            eliminarSesion();
            eliminarCliente();
            intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_mapa) {
            intent = new Intent(context, MapActivity.class);
            context.startActivity(intent);
        } else if (id == R.id.nav_info) {
            intent = new Intent(context, InfoActivity.class);
            context.startActivity(intent);
        }
    }

    public void mostrarMenu(Menu menu) {
        if (tipoUsuario.equals("cliente")) {
            // find MenuItem you want to change
            MenuItem nav_comandas = menu.findItem(R.id.nav_comandas);
            // set new title to the MenuItem
            nav_comandas.setVisible(false);

            MenuItem nav_mesas = menu.findItem(R.id.nav_mesas);
            nav_mesas.setVisible(false);

            MenuItem nav_refresh = menu.findItem(R.id.nav_refresh);
            nav_refresh.setVisible(false);

            MenuItem nav_graphs = menu.findItem(R.id.nav_graphs);
            nav_graphs.setVisible(false);

            MenuItem nav_home = menu.findItem(R.id.nav_home);
            // set new title to the MenuItem
            nav_home.setChecked(true);

        } else if (isAdmin()) {
            // find MenuItem you want to change
            MenuItem nav_mapa = menu.findItem(R.id.nav_mapa);
            // set new title to the MenuItem
            nav_mapa.setVisible(false);
        }
    }

    public void eliminarTabla(String nombreTabla) {
        db.execSQL("Drop table if exists " + nombreTabla);
    }

    void reiniciarBaseDatos() {

        crearTablaUsuario();

        //crearTablaSesion();

        crearTablaEspacio();

        crearTablaMesa();

        crearTablaIngrediente();

        crearTablaProducto();

        crearTablaProducto_Ingrediente();

        crearTablaCategoria();

        crearTablaComanda();

        crearTablaEspecial();

        crearTablaEmpleado();
    }

    /*public void comprobarBaseDatos() {

        //eliminarTabla("personal");
        if (!isTableExists("personal")) {
            crearTablaUsuario();
        }

        //eliminarTabla("sesion");
        if (!isTableExists("sesion")) {
            crearTablaSesion();
        }

        //eliminarTabla("espacio");
        if (!isTableExists("espacio")) {
            crearTablaEspacio();
        }

        //eliminarTabla("mesa");
        if (!isTableExists("mesa")) {
            crearTablaMesa();
        }

        //eliminarTabla("ingrediente");
        if (!isTableExists("ingrediente")) {
            crearTablaIngrediente();
        }

        //eliminarTabla("tipo");
        if (!isTableExists("tipo")) {
            crearTablaTipo();
        }

        if (!isTableExists("producto")) {
            crearTablaProducto();
        }

        if (!isTableExists("producto_tipo")) {
            crearTablaProducto_Tipo();
        }

        if (!isTableExists("producto_ingrediente")) {
            crearTablaProducto_Ingrediente();
        }

        if (!isTableExists("categoria")) {
            crearTablaCategoria();
        }

        if (!isTableExists("comanda")) {
            crearTablaComanda();
        }

    }*/

    public boolean isTableExists(String tableName) {
        if (tableName == null || db == null || !db.isOpen()) {
            return false;
        }
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
        if (!cursor.moveToFirst()) {
            return false;
        }
        int count = cursor.getInt(0);
        cursor.close();
        return count > 0;
    }

    public void modificarUbicacion(final LatLng latLng, final ArrayList<String> restaurante) {
        String post_resturante = ip + "restaurantes/" + restaurante.get(0) + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        StringRequest putRequest = new StringRequest(Request.Method.PUT, post_resturante,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                        Log.d("Response", response);
                        try {

                            //JSONObject object = new JSONObject(response);
                            generarMensaje("Ubicación modificada con exito");
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                            generarMensaje("Fallo al modificar la ubicación");
                        }
                        //String id = response.
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", restaurante.get(1));
                params.put("latitud", String.valueOf(latLng.latitude));
                params.put("longitud", String.valueOf(latLng.longitude));


                return params;
            }

        };
        mRequestQueue.add(putRequest);
    }

    public int login(final ArrayList<String> datos, final String usuario) {
        try {
            tipoUsuario = usuario;
            String tUsuario = usuario;
            if (isAdmin()) {
                tUsuario = "restaurantes";
            }
            if (datos.get(3).equals("")) {
                String msgToEncode = datos.get(4);
                final String encMsg = Encrypt.encrypt(SEED, msgToEncode);

                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = ip + tUsuario + "s/" + datos.get(0) + "/";
                StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);
                                datos.remove(3);
                                datos.add(3, encMsg);
                                login(datos, usuario);

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();

                        //params.put("idUsuario", datos.get(1));
                        if (usuario.equals("empleado") && !isAdmin()) {
                            params.put("token", obtenerToken());
                            //params.put("restaurante", datos.get(5));
                        }/*else if(usuario.equals("cliente")){
                            //params.put("nacimiento", datos.get(5));
                            //params.put("etProvincia", datos.get(6));
                            //params.put("etLocalidad", datos.get(7));
                            //params.put("codigo", datos.get(8));
                        }*/
                        params.put("nombre", datos.get(2));
                        params.put("password", encMsg);


                        return params;
                    }

                };
                mRequestQueue.add(putRequest);
            } else {
                String msgToDecode = datos.get(2);
                String decMsg = Encrypt.decrypt(SEED, msgToDecode);
                if (decMsg.equals(datos.get(3))) {
                    idUsuario = datos.get(0);
                    demo = false;
                    return 0;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /*private void insertarPersonal(final String id, final String username, final String password, final String idRestaurante) {
        String get_ingredientes = ip + "user/" + id;
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, get_ingredientes, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);

                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");

                    String email = object.getString("etEmail");
                    boolean insert =
                    insertarUsuario(first_name.toLowerCase(), last_name.toLowerCase(), email, username, password, idRestaurante);
                    if(insert){
                        //login(id,username,password,idRestaurante,password);

                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);

    }*/

    /*private ArrayList<String> toLowerCase(ArrayList<String> strings) {
        ArrayList<String> lower = new ArrayList<>();
        for (int i = 0; i < strings.size(); i++) {
            lower.add(strings.get(i).toLowerCase());
        }
        return lower;
    }*/
    public void crearTablaProducto() {
        this.eliminarTabla(TABLE_PRODUCTO);
        db.execSQL(CREATE_TABLE_PRODUCTO);
    }

    private ContentValues generarValoresProducto(String id, String nombre, String descripcion, String stock, String categoria, String precio) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_NOMBRE, nombre.toLowerCase());
        valores.put(CN_DESCRIPCION, descripcion.toLowerCase());
        valores.put(CN_STOCK, stock);
        valores.put(CN_CATEGORIA, categoria);
        valores.put(CN_PRECIO, precio);
        return valores;
    }

    private ContentValues generarValoresProductoStock(boolean stock) {
        ContentValues valores = new ContentValues();
        if (stock) {
            valores.put(CN_STOCK, "1");
        } else {
            valores.put(CN_STOCK, "0");
        }
        return valores;
    }

    @Override
    public boolean insertarProducto(String id, String nombre, String descripcion, String stock, String categoria, String precio) {
        long result = db.insert(TABLE_PRODUCTO, null, generarValoresProducto(id, nombre, descripcion, stock, categoria, precio));
        return result != -1;
    }

    @Override
    public boolean modificarProducto(String id, String nombre, String descripcion, String stock, String categoria, String precio) {
        return false;
    }

    /*public void modificarNombreProducto(final Producto producto) {
        try {
            producto.setNombre(removeAcentos(producto.getNombre()));
            RequestQueue mRequestQueue;

            mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

            final JSONObject jsonBody = new JSONObject(
                    "{\"nombre\":\"" + producto.getNombre() + "\"" + "}");

            String url = DataBaseManager.ip + "/api/productos/" + producto.getId() + "/";

            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.PUT, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String id = response.getString("id");
                        ContentValues valores = new ContentValues();
                        valores.put(CN_NOMBRE, producto.getNombre());
                        db.update(TABLE_PRODUCTO, valores, CN_ID + "= ?", new String[]{id});
                    } catch (Throwable t) {
                        Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Err", error.toString());
                }
            });
            mRequestQueue.add(jsObjRequest);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

    void modificarProducto(final Producto producto) {
        try {
            producto.setNombre(removeAcentos(producto.getNombre()));
            RequestQueue mRequestQueue;

            final String stock = (producto.getStock()) ? "1" : "0";

            final String idCategoria = this.obtenerCategoria(producto.getCategoria()).get(0);

            mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

            final JSONObject jsonBody = new JSONObject(
                    "{\"nombre\":\"" + producto.getNombre() + "\"," +
                            "\"descripcion\":\"" + producto.getDescripcion() + "\"," +
                            "\"categoria\":" + idCategoria + "," +
                            "\"stock\":" + stock + "," +
                            "\"precio\":" + producto.getPrecio() + "," +
                            "\"ingredientes\":" + producto.getAlteracionIngredientes() + "," +
                            "\"user\":" + DataBaseManager.restaurante + "}");

            String url = DataBaseManager.ip + "productos/" + producto.getId() + "/";

            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.PUT, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray ingredientes = new JSONArray(response.getString("ingredientes"));
                        try {
                            db.delete(TABLE_PRODUCTO_INGREDIENTE, CN_NOMBREPRODUCTO + "=?", new String[]{String.valueOf(producto.getId())});
                        } catch (Exception e) {
                            String err = e.toString();
                            Log.e("Err", err);
                        }
                        for (int j = 0; j < ingredientes.length(); j++) {
                            insertarProducto_Ingrediente(String.valueOf(producto.getId()), ingredientes.getString(j));

                        }
                        String id = response.getString("id");
                        ContentValues valores = new ContentValues();
                        valores.put(CN_NOMBRE, producto.getNombre());
                        valores.put(CN_DESCRIPCION, producto.getDescripcion());
                        valores.put(CN_STOCK, stock);
                        valores.put(CN_CATEGORIA, idCategoria);
                        valores.put(CN_PRECIO, producto.getPrecio());
                        db.update(TABLE_PRODUCTO, valores, CN_ID + "= ?", new String[]{id});
                    } catch (Throwable t) {
                        Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Err", error.toString());
                }
            });
            mRequestQueue.add(jsObjRequest);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean modificarStockProducto(final Producto producto) {
        try {
            RequestQueue mRequestQueue;

            mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
            String stock = producto.isStock() ? "1" : "0";
            final JSONObject jsonBody = new JSONObject(
                    "{\"nombre\":\"" + producto.getNombre() + "\"," +
                            "\"stock\":" + stock +
                            "}");

            String url = DataBaseManager.ip + "productos/" + producto.getId() + "/";

            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.PUT, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        db.update(TABLE_PRODUCTO, generarValoresProductoStock(producto.getStock()), CN_NOMBRE + "= ?", new String[]{producto.getNombre()});
                    } catch (Throwable t) {
                        Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Err", error.toString());
                }
            });
            mRequestQueue.add(jsObjRequest);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;

    }

    @Override
    public ArrayList<Producto> obtenerProductos() {
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_DESCRIPCION, CN_CATEGORIA, CN_STOCK, CN_PRECIO};
        ArrayList<Producto> tabla = new ArrayList<>();
        Producto producto;
        Cursor cursor = db.query(TABLE_PRODUCTO, columnas, null, null, null, null, CN_CATEGORIA);
        if (cursor.moveToFirst()) {
            do {
                producto = new Producto();
                producto.setId(cursor.getInt(0));
                producto.setNombre(cursor.getString(1));
                producto.setDescripcion(cursor.getString(2));
                producto.setCategoria(cursor.getString(3));
                producto.setStock(cursor.getString(4).equals("1"));
                producto.setPrecio(cursor.getDouble(5));
                tabla.add(producto);

            } while (cursor.moveToNext());
        }
        cursor.close();

        return tabla;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerProductosPorCategoria(String categoria) {
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_DESCRIPCION, CN_CATEGORIA};
        ArrayList<ArrayList<String>> tabla = new ArrayList<>();
        ArrayList<String> columna;
        Cursor cursor = db.query(TABLE_PRODUCTO, columnas, CN_CATEGORIA + " = ?", new String[]{categoria}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna = new ArrayList<>();
                columna.add("" + cursor.getInt(0));
                columna.add(cursor.getString(1));
                columna.add(cursor.getString(2));
                columna.add(cursor.getString(3));
                tabla.add(columna);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerProductosPorCategoria(int pos) {
        ArrayList<ArrayList<String>> productos;
        ArrayList<String> categorias = this.obtenerCategorias();
        String categoria = categorias.get(pos);
        productos = this.obtenerProductosPorCategoria(categoria);
        return productos;
    }

    @Override
    public Producto obtenerProducto(String nombre) {
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_DESCRIPCION, CN_STOCK, CN_CATEGORIA, CN_PRECIO};
        Producto producto = new Producto();
        String stock;
        Cursor cursor = db.query(TABLE_PRODUCTO, columnas, CN_NOMBRE + " = ?", new String[]{nombre}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                producto.setId(cursor.getInt(0));
                producto.setNombre(cursor.getString(1));
                producto.setDescripcion(cursor.getString(2));
                stock = cursor.getString(3);
                if (stock.equals("1")) {
                    producto.setStock(true);
                } else {
                    producto.setStock(false);
                }
                producto.setCategoria(obtenerCategoriabyId(cursor.getString(4)).get(1));
                producto.setPrecio(cursor.getDouble(5));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return producto;
    }

    @Override
    public Producto obtenerProducto(int id) {
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_DESCRIPCION};
        Cursor cursor = db.query(TABLE_PRODUCTO, columnas, CN_ID + " = ?", new String[]{String.valueOf(id)}, null, null, null);
        Producto producto = new Producto();
        if (cursor.moveToFirst()) {
            do {
                producto.setId(cursor.getInt(0));
                producto.setNombre(cursor.getString(1));
                producto.setDescripcion(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return producto;
    }

    public ArrayList<String> obtenerNombreProductos() {
        String[] columnas = new String[]{CN_NOMBRE};
        ArrayList<String> columna = new ArrayList<>();
        Cursor cursor = db.query(TABLE_PRODUCTO, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return columna;
    }

    @Override
    public boolean inStockProducto(String nombre) {
        String[] columnas = new String[]{CN_STOCK};
        String codigo;
        boolean stock = false;
        Cursor cursor = db.query(TABLE_PRODUCTO, columnas, CN_NOMBRE + " = ?", new String[]{nombre}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                codigo = cursor.getString(0);
                if (codigo.equals("1")) {
                    stock = true;
                }

            } while (cursor.moveToNext());
        }
        cursor.close();
        return stock;
    }

    public boolean eliminarProducto(String nombre) {
        final String id = String.valueOf(obtenerProducto(nombre).getId());
        String url = ip + "productos/" + id + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
        db.delete(TABLE_PRODUCTO, CN_ID + "=?", new String[]{id});

        return true;
    }

    @Override
    public void crearTablaTipo() {
        this.eliminarTabla(TABLE_TIPO);
        db.execSQL(CREATE_TABLE_TIPO);
    }

    @Override
    public boolean insertarTipo(String id, String tipo, String precio) {
        db.insert(TABLE_TIPO, null, generarValoresTipo(id, tipo, precio));
        return true;
    }

    @Override
    public ArrayList<String> obtenerTipo(String tipo) {
        String[] columnas = new String[]{CN_ID, CN_TIPO, CN_PRECIO};
        ArrayList<String> columna = new ArrayList<>();
        Cursor cursor = db.query(TABLE_TIPO, columnas, CN_ID + " = ?", new String[]{tipo}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna.add("" + cursor.getInt(0));
                columna.add(cursor.getString(1));
                columna.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return columna;
    }

    @Override
    public ArrayList<String> obtenerTipoPorNombre(String tipo) {
        String[] columnas = new String[]{CN_ID, CN_TIPO, CN_PRECIO};
        ArrayList<String> columna = new ArrayList<>();
        Cursor cursor = db.query(TABLE_TIPO, columnas, CN_TIPO + " = ?", new String[]{tipo}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna.add("" + cursor.getInt(0));
                columna.add(cursor.getString(1));
                columna.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return columna;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerTipos() {
        String[] columnas = new String[]{CN_ID, CN_TIPO, CN_PRECIO};
        ArrayList<ArrayList<String>> tabla = new ArrayList<>();
        ArrayList<String> columna;
        Cursor cursor = db.query(TABLE_TIPO, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna = new ArrayList<>();
                columna.add("" + cursor.getInt(0));
                columna.add(cursor.getString(1));
                tabla.add(columna);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    private ContentValues generarValoresTipo(String id, String tipo, String precio) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_TIPO, tipo.toLowerCase());
        valores.put(CN_PRECIO, precio);
        return valores;
    }

    @Override
    public void crearTablaProducto_Tipo() {
        this.eliminarTabla(TABLE_PRODUCTO_TIPO);
        db.execSQL(CREATE_TABLE_PRODUCTO_TIPO);
    }

    @Override
    public boolean insertarProducto_Tipo(String producto, String tipo) {
        db.insert(TABLE_PRODUCTO_TIPO, null, generarValoresProducto_Tipo(producto, tipo));
        return true;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerProductos_Tipos() {
        return null;
    }

    @Override
    public ArrayList<String> obtenerProducto_Tipos(String producto) {
        String[] columnas = new String[]{CN_ID, CN_IDPRODUCTO, CN_IDTIPO};
        ArrayList<String> columna = new ArrayList<>();
        Cursor cursor = db.query(TABLE_PRODUCTO_TIPO, columnas, CN_IDPRODUCTO + " = ?", new String[]{producto}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return columna;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerProductos_Tipo(String tipo) {
        String[] columnas = new String[]{CN_ID, CN_IDPRODUCTO, CN_IDTIPO};
        ArrayList<ArrayList<String>> tabla = new ArrayList<>();
        ArrayList<String> columna;
        Cursor cursor = db.query(TABLE_PRODUCTO_TIPO, columnas, CN_IDTIPO + " = ?", new String[]{tipo}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna = new ArrayList<>();
                columna.add("" + cursor.getInt(0));
                columna.add(cursor.getString(1));
                tabla.add(columna);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    private ContentValues generarValoresProducto_Tipo(String idProducto, String idTipo) {
        ContentValues valores = new ContentValues();
        valores.put(CN_IDPRODUCTO, idProducto);
        valores.put(CN_IDTIPO, idTipo);
        return valores;
    }

    @Override
    public void crearTablaProducto_Categoria() {
        this.eliminarTabla(TABLE_PRODUCTO_CATEGORIA);
        db.execSQL(CREATE_TABLE_PRODUCTO_CATEGORIA);
    }

    @Override
    public boolean insertarProducto_Categoria(String producto, String categoria) {
        db.insert(TABLE_PRODUCTO_CATEGORIA, null, generarValoresProducto_Categoria(producto, categoria));
        return true;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerProductos_Categorias() {
        return null;
    }

    @Override
    public ArrayList<String> obtenerProducto_Categorias(String producto) {
        String[] columnas = new String[]{CN_ID, CN_IDPRODUCTO, CN_IDCATEGORIA};
        ArrayList<String> columna = new ArrayList<>();
        Cursor cursor = db.query(TABLE_PRODUCTO_CATEGORIA, columnas, CN_IDPRODUCTO + " = ?", new String[]{producto}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return columna;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerProductos_Categoria(String categoria) {
        String[] columnas = new String[]{CN_ID, CN_IDPRODUCTO, CN_IDCATEGORIA};
        ArrayList<ArrayList<String>> tabla = new ArrayList<>();
        ArrayList<String> columna;
        Cursor cursor = db.query(TABLE_PRODUCTO_CATEGORIA, columnas, CN_IDCATEGORIA + " = ?", new String[]{categoria}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna = new ArrayList<>();
                columna.add("" + cursor.getInt(0));
                columna.add(cursor.getString(1));
                tabla.add(columna);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    private ContentValues generarValoresProducto_Categoria(String idProducto, String idCategoria) {
        ContentValues valores = new ContentValues();
        valores.put(CN_IDPRODUCTO, idProducto);
        valores.put(CN_IDCATEGORIA, idCategoria);
        return valores;
    }

    @Override
    public String obtenerCategoriaProductobyId(String id) {
        String categoria = "";
        String[] columnas = new String[]{CN_ID, CN_IDPRODUCTO, CN_IDCATEGORIA};
        Cursor cursor = db.query(TABLE_PRODUCTO_CATEGORIA, columnas, CN_IDPRODUCTO + " = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                categoria = this.obtenerCategoriabyId(cursor.getString(2)).get(1);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return categoria;
    }

    @Override
    public void crearTablaIngrediente() {
        this.eliminarTabla(TABLE_INGREDIENTE);
        db.execSQL(CREATE_TABLE_INGREDIENTE);
    }

    @Override
    public boolean insertarIngrediente(String id, String ingrediente, String stock) {
        ingrediente = ingrediente.toLowerCase();
        long insert = db.insert(TABLE_INGREDIENTE, null, generarValoresIngrediente(id, ingrediente, stock));
        return insert == 1;
    }

    public boolean insertarIngredienteApi(final String ingrediente, final String stock) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        url = ip + "ingredientes/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String id = object.getString("id");
                            //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                            insertarIngrediente(id, ingrediente, stock);


                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", ingrediente);
                params.put("stock", stock);
                params.put("user", restaurante);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
        return true;
    }

    @Override
    public boolean insertarIngredientes(ArrayList<String> ingredientes) {
        /*for(String ingrediente:ingredientes){
            insertarIngrediente(ingrediente,"1");
        }*/
        return true;
    }

    @Override
    public boolean modificarStockIngrediente(final String nombre, final Boolean stock) {
        ArrayList<ArrayList<String>> ingredientes = obtenerArrayIngredientes();
        String id = "";
        for (ArrayList<String> ingrediente : ingredientes) {
            if (ingrediente.get(1).equals(nombre)) {
                id = ingrediente.get(0);
            }
        }

        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = ip + "ingredientes/" + id + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (stock) {
                    params.put("nombre", nombre);
                    params.put("stock", "1");
                }

                if (!stock) {
                    params.put("nombre", nombre);
                    params.put("stock", "0");
                }


                return params;
            }

        };
        mRequestQueue.add(putRequest);

        ContentValues values;
        if (stock) {
            values = generarValoresIngrediente2(nombre, "1");
        } else {
            values = generarValoresIngrediente2(nombre, "0");
        }
        int result = db.update(TABLE_INGREDIENTE, values, CN_INGREDIENTE + " = ?", new String[]{nombre});
        return result == 1;
    }

    boolean modificarIngrediente(final ArrayList<String> ingrediente, final Boolean stock) {

        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = ip + "ingredientes/" + ingrediente.get(0) + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", ingrediente.get(1));
                if (stock) {
                    params.put("stock", "1");
                }
                if (!stock) {
                    params.put("stock", "0");
                }


                return params;
            }

        };
        mRequestQueue.add(putRequest);

        ContentValues values;
        if (stock) {
            values = generarValoresIngrediente2(ingrediente.get(1), "1");
        } else {
            values = generarValoresIngrediente2(ingrediente.get(1), "0");
        }
        int result = db.update(TABLE_INGREDIENTE, values, CN_ID + " = ?", new String[]{ingrediente.get(0)});
        return result == 1;
    }

    @Override
    public ArrayList<String> obtenerIngredientes() {
        String[] columnas = new String[]{CN_INGREDIENTE};
        ArrayList<String> columna = new ArrayList<>();
        Cursor cursor = db.query(TABLE_INGREDIENTE, columnas, null, null, null, null, CN_INGREDIENTE);
        if (cursor.moveToFirst()) {
            do {
                columna.add(cursor.getString(0).toLowerCase());

            } while (cursor.moveToNext());
        }
        //Collections.sort(columna);
        cursor.close();
        return columna;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerArrayIngredientes() {
        String[] columnas = new String[]{CN_ID, CN_INGREDIENTE};
        ArrayList<String> columna;
        ArrayList<ArrayList<String>> tabla = new ArrayList<>();
        Cursor cursor = db.query(TABLE_INGREDIENTE, columnas, null, null, null, null, CN_INGREDIENTE);
        if (cursor.moveToFirst()) {
            do {
                columna = new ArrayList<>();
                columna.add(cursor.getString(0));
                columna.add(cursor.getString(1).toLowerCase());
                tabla.add(columna);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    @Override
    public String obtenerIngrediente(String id) {
        String[] columnas = new String[]{CN_INGREDIENTE};
        Cursor cursor = db.query(TABLE_INGREDIENTE, columnas, CN_ID + "= ?", new String[]{String.valueOf(id)}, null, null, CN_INGREDIENTE);
        String nombre = "";
        if (cursor.moveToFirst()) {
            do {
                nombre = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return nombre;
    }

    @Override
    public ArrayList<String> obtenerIngredienteByNombre(String ingrediente) {
        ArrayList<String> arrayList = new ArrayList<>();
        String[] columnas = new String[]{CN_ID, CN_INGREDIENTE};
        Cursor cursor = db.query(TABLE_INGREDIENTE, columnas, CN_INGREDIENTE + " = ?", new String[]{ingrediente}, null, null, null);
        if (cursor.moveToFirst()) {
            do {

                arrayList.add(cursor.getString(0));//id
                arrayList.add(cursor.getString(1));//nombre
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

    @Override
    public boolean eliminarIngrediente(final String id) {
        String url = ip + "ingredientes/" + id + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
        db.delete(TABLE_INGREDIENTE, CN_ID + "=?", new String[]{id});
        return true;
    }

    @Override
    public boolean inStrockIngrediente(String nombre) {
        String[] columnas = new String[]{CN_STOCK};
        String codigo;
        boolean stock = false;
        Cursor cursor = db.query(TABLE_INGREDIENTE, columnas, CN_INGREDIENTE + " = ?", new String[]{nombre}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                codigo = cursor.getString(0);
                if (codigo.equals("1")) {
                    stock = true;
                }

            } while (cursor.moveToNext());
        }
        cursor.close();
        return stock;
    }

    @Override
    public Map<String, Boolean> obtenerStockIngredientes() {
        String[] columnas = new String[]{CN_INGREDIENTE, CN_STOCK};
        String nombre;
        String stock;
        Map<String, Boolean> mapa = new HashMap<>();
        Cursor cursor = db.query(TABLE_INGREDIENTE, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                nombre = cursor.getString(0).toLowerCase();
                stock = cursor.getString(1);
                if (stock.equals("1")) {
                    mapa.put(nombre, true);
                } else {
                    mapa.put(nombre, false);
                }

            } while (cursor.moveToNext());
        }
        cursor.close();
        return mapa;
    }

    private ContentValues generarValoresIngrediente2(String ingrediente, String stock) {
        ContentValues valores = new ContentValues();
        valores.put(CN_INGREDIENTE, ingrediente.toLowerCase());
        valores.put(CN_STOCK, stock);

        return valores;
    }

    private ContentValues generarValoresIngrediente(String id, String ingrediente, String stock) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_INGREDIENTE, ingrediente.toLowerCase());
        valores.put(CN_STOCK, stock);

        return valores;
    }

    @Override
    public void crearTablaProducto_Ingrediente() {
        this.eliminarTabla(TABLE_PRODUCTO_INGREDIENTE);
        db.execSQL(CREATE_TABLE_PRODUCTO_INGREDIENTE);
    }

    @Override
    public boolean insertarProducto_Ingrediente(String producto, String ingrediente) {
        db.insert(TABLE_PRODUCTO_INGREDIENTE, null, generarValoresProducto_Ingrediente(producto, ingrediente));
        return true;
    }

    @Override
    public ArrayList<String> obtenerProducto_Ingredientes(String nombreProducto) {
        Producto producto = this.obtenerProducto(nombreProducto);
        String[] columnas = new String[]{CN_NOMBREINGREDIENTE};
        ArrayList<String> tabla = new ArrayList<>();

        String id;
        Cursor cursor = db.query(TABLE_PRODUCTO_INGREDIENTE, columnas, CN_NOMBREPRODUCTO + "= ?", new String[]{String.valueOf(producto.getId())}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getString(0);
                String ingrediente = this.obtenerIngrediente(id);
                tabla.add(ingrediente);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    private ContentValues generarValoresProducto_Ingrediente(String producto, String ingrediente) {
        ContentValues valores = new ContentValues();
        valores.put(CN_NOMBREPRODUCTO, producto);
        valores.put(CN_NOMBREINGREDIENTE, ingrediente);
        return valores;
    }

    @Override
    public void crearTablaCategoria() {
        this.eliminarTabla(TABLE_CATEGORIA);
        db.execSQL(CREATE_TABLE_CATEGORIA);
    }

    @Override
    public boolean insertarCategoria(String id, String categoria) {
        long result = db.insert(TABLE_CATEGORIA, null, generarValoresCategoria(id, categoria));
        return result != -1;
    }

    @Override
    public ArrayList<String> obtenerCategorias() {
        String[] columnas = new String[]{CN_ID, CN_CATEGORIA};
        ArrayList<String> tabla = new ArrayList<>();
        Cursor cursor = db.query(TABLE_CATEGORIA, columnas, null, null, null, null, CN_CATEGORIA);
        String lowercase;
        if (cursor.moveToFirst()) {
            do {
                lowercase = cursor.getString(1).toLowerCase(); //Tratamos todos los espacios en minusculas para poder comparar mejor
                tabla.add(lowercase);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    @Override
    public ArrayList<String> obtenerCategoriabyId(String id) {
        ArrayList<String> arrayList = new ArrayList<>();
        String[] columnas = new String[]{CN_ID, CN_CATEGORIA};
        Cursor cursor = db.query(TABLE_CATEGORIA, columnas, CN_ID + " = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            do {

                arrayList.add(cursor.getString(0));//id
                arrayList.add(cursor.getString(1));//nombre
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

    @Override
    public ArrayList<String> obtenerCategoria(String categoria) {
        ArrayList<String> arrayList = new ArrayList<>();
        String[] columnas = new String[]{CN_ID, CN_CATEGORIA};
        Cursor cursor = db.query(TABLE_CATEGORIA, columnas, CN_CATEGORIA + " = ?", new String[]{categoria}, null, null, null);
        if (cursor.moveToFirst()) {
            do {

                arrayList.add(cursor.getString(0));//id
                arrayList.add(cursor.getString(1));//nombre
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

    /*boolean insertarCategoriaApi(final String ingrediente) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        url = ip + "categorias/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String id = object.getString("id");
                            //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                            insertarCategoria(id, ingrediente);


                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", ingrediente);
                params.put("user", restaurante);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
        return true;
    }*/

    @Override
    public boolean modificarCategoria(String id, String nombre) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CN_CATEGORIA, nombre);
        int result = db.update(TABLE_CATEGORIA, contentValues, CN_ID + "= ?", new String[]{id});
        return result != -1;
    }

    @Override
    public boolean eliminarCategoria(String categoria) {
        int result = db.delete(TABLE_CATEGORIA, CN_ID + "=?", new String[]{categoria});
        return result == 1;
    }

    private ContentValues generarValoresCategoria(String id, String categoria) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_CATEGORIA, categoria.toLowerCase());

        return valores;
    }

    @Override
    public void crearTablaComanda() {
        this.eliminarTabla(TABLE_COMANDA);
        db.execSQL(CREATE_TABLE_COMANDA);
    }

    @Override
    public boolean insertarComandaApi(final String id, final String idMesa, final String idProducto, final String categoria, final String comentario, final String estado, final String cantidad, final String token, final String fecha, final String hora) {
        RequestQueue mRequestQueue;
        final String idCategoria = this.obtenerCategoria(categoria).get(0);
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = ip + "comandas/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String idComanda = object.getString("id");
                            //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                            db.insert(TABLE_COMANDA, null, generarValoresComanda(idComanda, idMesa, idProducto, idCategoria, comentario, estado, cantidad, token, fecha, hora));

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("mesa", idMesa);
                params.put("producto", idProducto);
                params.put("estado", estado);
                params.put("categoria", idCategoria);
                params.put("cantidad", cantidad);
                params.put("comentario", comentario);
                params.put("user", restaurante);
                if (tipoUsuario.equals("cliente")) {
                    params.put("cliente", idUsuario);
                }
                params.put("token", token);
                params.put("fecha", fecha);
                params.put("hora", hora);


                return params;
            }

        };
        mRequestQueue.add(putRequest);
        return true;
    }

    void limpiarMesa(int id) {
        ArrayList<ArrayList<String>> comandas = this.obtenerComandasEnCola(String.valueOf(id));
        //ArrayList<String > mesa = this.obtenerHoras(id);
        //mesa.get()
        //modificarCodigoMesa(mesa,"0");
        //modificarEmpleado(mesa);
        modificarEstadoMesa(String.valueOf(id), "LIBRE");
        for (ArrayList<String> comanda : comandas) {
            if (!comanda.get(4).equals("FINALIZADO")) {
                modificarComanda(comanda.get(6), "FINALIZADO");
            }
        }
    }

    /*private void modificarEmpleado(final ArrayList<String> mesa) {
        String url = ip + "clientes/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String idMesa = object.getString("mesa");

                    }
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Error",err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Error", err);
            }
        });
        mRequestQueue.add(request);
    }*/

    @Override
    public boolean insertarComanda(String id, String idMesa, String idProducto, String idCategoria, String comentario, String estado, String cantidad, String token, String fecha, String hora) {
        Long result = (long) -1;
        try{
            result = db.insert(TABLE_COMANDA, null, generarValoresComanda(id, idMesa, idProducto, idCategoria, comentario, estado, cantidad, token, fecha, hora));
        }catch (Exception e){
            e.printStackTrace();
        }
        return result != -1;
    }

    int obtenerUlitmoIdComanda() {
        int id = 0;
        String[] columnas = new String[]{CN_ID};
        Cursor cursor = db.query(TABLE_COMANDA, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return id;
    }

    @Override
    public ArrayList<ComandaClass> obtenerComandasEnCola() {
        String[] columnas = new String[]{CN_IDMESA, CN_IDPRODUCTO, CN_CANTIDAD, CN_IDCATEGORIA, CN_COMENTARIO, CN_ESTADO, CN_ID, CN_TOKEN, CN_FECHA, CN_HORA};
        ComandaClass comanda;
        ArrayList<ComandaClass> comandas = new ArrayList<>();
        Producto producto;
        Cursor cursor = db.query(TABLE_COMANDA, columnas, CN_ESTADO + " = ?", new String[]{"COLA"}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                //CN_IDMESA,0
                // CN_IDPRODUCTO,1
                // CN_CANTIDAD,2
                // CN_IDCATEGORIA,3
                // CN_COMENTARIO,4
                // CN_ESTADO,5
                // CN_ID,6
                // CN_TOKEN,7
                // CN_FECHA,8
                // CN_HORA,9

                producto = this.obtenerProducto(cursor.getInt(1));
                String nombreProducto = producto.getNombre();

                comanda = new ComandaClass();
                comanda.setIdComanda(cursor.getString(6));
                comanda.setProducto(nombreProducto);
                comanda.setComentario(cursor.getString(4));
                comanda.setEstado(cursor.getString(5));
                comanda.setCantidad(cursor.getInt(2));
                comanda.setTipo(cursor.getString(3));
                comanda.setIdMesa(cursor.getString(0));
                comanda.setFecha(cursor.getString(8));
                comanda.setHora(cursor.getString(9));
                comanda.setToken(cursor.getString(7));

                comandas.add(comanda);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return comandas;
    }

    @Override
    public ArrayList<ComandaClass> obtenerComandas() {
        String[] columnas = new String[]{CN_IDMESA, CN_IDPRODUCTO, CN_CANTIDAD, CN_IDCATEGORIA, CN_COMENTARIO, CN_ESTADO, CN_ID, CN_TOKEN, CN_FECHA, CN_HORA};
        ComandaClass comanda;
        ArrayList<ComandaClass> comandas = new ArrayList<>();
        Producto producto;
        Cursor cursor = db.query(TABLE_COMANDA, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                //CN_IDMESA,0
                // CN_IDPRODUCTO,1
                // CN_CANTIDAD,2
                // CN_IDCATEGORIA,3
                // CN_COMENTARIO,4
                // CN_ESTADO,5
                // CN_ID,6
                // CN_TOKEN,7
                // CN_FECHA,8
                // CN_HORA,9

                producto = this.obtenerProducto(cursor.getInt(1));
                String nombreProducto = producto.getNombre();

                comanda = new ComandaClass();
                comanda.setIdComanda(cursor.getString(6));
                comanda.setProducto(nombreProducto);
                comanda.setComentario(cursor.getString(4));
                comanda.setEstado(cursor.getString(5));
                comanda.setCantidad(cursor.getInt(2));
                comanda.setTipo(cursor.getString(3));
                comanda.setIdMesa(cursor.getString(0));
                comanda.setFecha(cursor.getString(8));
                comanda.setHora(cursor.getString(9));

                comandas.add(comanda);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return comandas;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerComandasEnCola(String idMesa) {
        String[] columnas = new String[]{CN_IDMESA, CN_IDPRODUCTO, CN_CANTIDAD, CN_IDCATEGORIA, CN_COMENTARIO, CN_ESTADO, CN_ID};
        ArrayList<String> arrayList;
        ArrayList<ArrayList<String>> comandas = new ArrayList<>();
        Producto producto;
        ArrayList<String> mesa;
        Cursor cursor = db.query(TABLE_COMANDA, columnas, CN_IDMESA + " = ?", new String[]{idMesa}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                //0: nombre mesa
                //1: cantidad
                //2: idcategoria
                //3: comentario
                //4: estado
                //5: nombreProducto
                //6: id
                arrayList = new ArrayList<>();
                producto = this.obtenerProducto(cursor.getInt(1));
                mesa = this.obtenerMesa(cursor.getInt(0));

                int cantidad = cursor.getInt(2);

                String nombreProducto = producto.getNombre();
                String nombreMesa = mesa.get(5);

                arrayList.add(nombreMesa);
                arrayList.add(String.valueOf(cantidad));
                arrayList.add(cursor.getString(3));
                arrayList.add(cursor.getString(4));
                arrayList.add(cursor.getString(5));
                arrayList.add(nombreProducto);
                arrayList.add(cursor.getString(6));

                comandas.add(arrayList);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return comandas;
    }

    ArrayList<ComandaClass> obtenerObjetoComandas(String idMesa) {
        String[] columnas = new String[]{CN_IDMESA, CN_IDPRODUCTO, CN_CANTIDAD, CN_IDCATEGORIA, CN_COMENTARIO, CN_ESTADO, CN_ID};
        ComandaClass comanda;
        ArrayList<ComandaClass> comandas = new ArrayList<>();
        Producto producto;
        Cursor cursor = db.query(TABLE_COMANDA, columnas, CN_IDMESA + " = ?", new String[]{idMesa}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                //0: nombre mesa
                //1: nombreProducto
                //2: cantidad
                //3: idcategoria
                //4: comentario
                //5: estado
                //6: id

                int idProducto = cursor.getInt(1);
                producto = this.obtenerProducto(idProducto);
                String tipo = this.obtenerCategoriabyId(cursor.getString(3)).get(1);

                comanda = new ComandaClass();
                comanda.setIdComanda(cursor.getString(6));
                comanda.setIdMesa(cursor.getString(0));
                comanda.setProducto(producto.getNombre());
                comanda.setComentario(cursor.getString(4));
                comanda.setEstado(cursor.getString(5));
                comanda.setCantidad(cursor.getInt(2));
                comanda.setTipo(tipo);

                comandas.add(comanda);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return comandas;
    }

    @Override
    public ArrayList<String> obtenerComandabyId(String id) {
        ArrayList<String> arrayList = new ArrayList<>();
        String[] columnas = new String[]{CN_ID, CN_IDMESA, CN_IDPRODUCTO, CN_CANTIDAD, CN_IDCATEGORIA, CN_COMENTARIO, CN_ESTADO};
        Cursor cursor = db.query(TABLE_COMANDA, columnas, CN_ID + " = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            do {

                float cantidad = cursor.getFloat(3);

                arrayList.add(cursor.getString(0));//id
                arrayList.add(cursor.getString(1));//mesa
                arrayList.add(cursor.getString(2));//id producto
                arrayList.add(cursor.getString(4));//id categoria
                arrayList.add(cursor.getString(6));//estado
                arrayList.add(String.valueOf(cantidad));//cantidad
                if (!cursor.getString(5).equals("")) {
                    arrayList.add(cursor.getString(5));//comentarios
                } else {
                    arrayList.add("sin comentarios");//comentarios
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

    @Override
    public boolean modificarComanda(final String id, final String estado) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        final ArrayList<String> comanda = this.obtenerComandabyId(id);
        String url = ip + "comandas/" + id + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("mesa", comanda.get(1));
                params.put("producto", comanda.get(2));
                params.put("categoria", comanda.get(3));
                params.put("estado", estado);
                params.put("cantidad", comanda.get(5));
                params.put("comentario", comanda.get(6));
                //params.put("codigo","0");
                //params.put("empleado","null");


                return params;
            }

        };
        mRequestQueue.add(putRequest);
        ContentValues values = new ContentValues();
        values.put(CN_ESTADO, estado);
        int result = db.update(TABLE_COMANDA, values, CN_ID + " = ?", new String[]{id});
        return result == 1;

    }

    private ContentValues generarValoresComanda(String id, String idmesa, String idproducto, String idCategoria, String comentario, String estado, String cantidad, String token, String fecha, String hora) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_IDMESA, idmesa);
        valores.put(CN_IDPRODUCTO, idproducto);
        valores.put(CN_IDCATEGORIA, idCategoria);
        valores.put(CN_COMENTARIO, comentario);
        valores.put(CN_ESTADO, estado);
        valores.put(CN_CANTIDAD, cantidad);
        valores.put(CN_TOKEN, token);
        valores.put(CN_FECHA, fecha);
        valores.put(CN_HORA, hora);
        return valores;
    }

    @Override
    public void crearTablaRestaurante() {
        this.eliminarTabla(TABLE_RESTAURANTE);
        db.execSQL(CREATE_TABLE_RESTAURANTE);
    }

    @Override
    public boolean insertarRestaurante(String id, String nombre, String direccion, String provincia, String localidad, String latitud, String longitud) {
        long result = db.insert(TABLE_MESA, null, generarValoresRestaurante(id, nombre, direccion, provincia, localidad, latitud, longitud));
        return result != -1;
    }

    @Override
    public boolean insertarRestaurante(RestauranteClass restaurante) {
        String id = restaurante.getId();
        String nombre = restaurante.getNombre();
        String direccion = restaurante.getDireccion();
        String localidad = restaurante.getLocalidad();
        String provincia = restaurante.getProvincia();
        String latitud = restaurante.getLatitud();
        String longitud = restaurante.getLongitud();

        long result = db.insert(TABLE_RESTAURANTE, null, generarValoresRestaurante(id, nombre, direccion, localidad, provincia, latitud, longitud));
        return result != -1;
    }

    @Override
    public ArrayList<RestauranteClass> obtenerRestaurantes() {
        ArrayList<RestauranteClass> restaurantes = new ArrayList<>();

        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_DIRECCION, CN_PROVINCIA, CN_LOCALIDAD, CN_LATITUD, CN_LONGITUD};
        Cursor cursor = db.query(TABLE_RESTAURANTE, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(0);
                String nombre = cursor.getString(1);
                String direccion = cursor.getString(2);
                String provincia = cursor.getString(3);
                String localidad = cursor.getString(4);
                String latitud = cursor.getString(5);
                String longitud = cursor.getString(6);
                RestauranteClass restaurante = new RestauranteClass(id, nombre, direccion, localidad, provincia, latitud, longitud);
                restaurantes.add(restaurante);

            } while (cursor.moveToNext());
        }
        cursor.close();


        return restaurantes;
    }

    private ContentValues generarValoresRestaurante(String id, String nombre, String direccion, String provincia, String localidad, String latitud, String longitud) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_NOMBRE, nombre);
        valores.put(CN_DIRECCION, direccion);
        valores.put(CN_PROVINCIA, provincia);
        valores.put(CN_LOCALIDAD, localidad);
        valores.put(CN_LATITUD, latitud);
        valores.put(CN_LONGITUD, longitud);
        return valores;
    }

    private ContentValues generarValoresEmpleado(String id, String nombre, String restaurante, String token, String password) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_NOMBRE, nombre);
        valores.put(CN_RESTAURANTE, restaurante);
        valores.put(CN_TOKEN, token);
        valores.put(CN_PASSWORD, password);
        return valores;
    }

    @Override
    public void crearTablaEmpleado() {
        if (isTableExists("empleado")) {
            this.eliminarTabla(TABLE_EMPLEADO);
        }
        db.execSQL(CREATE_TABLE_EMPLEADO);
    }

    @Override
    public boolean insertarEmpleado(String id, String nombre, String restaurante, String token, String password) {
        long result = db.insert(TABLE_EMPLEADO, null, generarValoresEmpleado(id, nombre, restaurante, token, password));
        return result != -1;
    }

    @Override
    public ArrayList<String> obtenerNombreEmpleados() {
        ArrayList<String> tabla = new ArrayList<>();
        String[] columnas = new String[]{CN_ID, CN_NOMBRE,};
        Cursor cursor = db.query(TABLE_EMPLEADO, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                tabla.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    @Override
    public EmpleadoClass obtenerEmpleado(String id) {
        EmpleadoClass empleado = new EmpleadoClass();
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_RESTAURANTE, CN_TOKEN};
        Cursor cursor = db.query(TABLE_EMPLEADO, columnas, CN_ID + " = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                empleado.setId(cursor.getString(0));
                empleado.setNombre(cursor.getString(1));
                empleado.setRestaurante(cursor.getString(2));
                empleado.setToken(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return empleado;
    }

    @Override
    public EmpleadoClass obtenerEmpleadoByName(String nombre) {
        EmpleadoClass empleado = new EmpleadoClass();
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_RESTAURANTE, CN_TOKEN};
        Cursor cursor = db.query(TABLE_EMPLEADO, columnas, CN_NOMBRE + " = ?", new String[]{nombre}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                empleado.setId(cursor.getString(0));
                empleado.setNombre(cursor.getString(1));
                empleado.setRestaurante(cursor.getString(2));
                empleado.setToken(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return empleado;
    }

    /*public boolean actualizarPasswordEmpelado(final ArrayList<String> datos, final String usuario) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String id = datos.get(0);
        final String password = datos.get(4);
        String url = ip + "empleados/" + id + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        login(datos, usuario);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("password", password);


                return params;
            }
        };
        mRequestQueue.add(putRequest);
        ContentValues values = new ContentValues();
        values.put(CN_PASSWORD, password);
        int result = db.update(TABLE_EMPLEADO, values, CN_ID + " = ?", new String[]{id});
        return result == 1;

    }*/

    @Override
    public boolean actualizarApiEmpelado(String id, final String token) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        final EmpleadoClass empleado = obtenerEmpleado(id);
        empleado.setToken(token);
        String url = ip + "empleados/" + id + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", empleado.getToken());


                return params;
            }
        };
        mRequestQueue.add(putRequest);
        ContentValues values = new ContentValues();
        values.put(CN_TOKEN, token);
        int result = db.update(TABLE_EMPLEADO, values, CN_ID + " = ?", new String[]{id});
        return result == 1;

    }

    @Override
    public boolean actualizarTokenEmpelado(final ArrayList<String> sesion, final String token) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String id = sesion.get(0);
        String url = ip + "empleados/" + id + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("nombre", sesion.get(1));
                params.put("token", token);


                return params;
            }
        };
        mRequestQueue.add(putRequest);
        ContentValues values = new ContentValues();
        values.put(CN_TOKEN, token);
        int result = db.update(TABLE_EMPLEADO, values, CN_ID + " = ?", new String[]{id});
        return result == 1;

    }

    private ContentValues generarValoresCliente(String id, String nombre, String password) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_NOMBRE, nombre);
        valores.put(CN_PASSWORD, password);
        return valores;
    }

    @Override
    public void crearTablaCliente() {
        if (isTableExists("cliente")) {
            this.eliminarTabla(TABLE_CLIENTE);
        }
        db.execSQL(CREATE_TABLE_CLIENTE);
    }

    @Override
    public boolean insertarCliente(String id, String nombre, String password) {
        crearTablaCliente();
        long result = db.insert(TABLE_CLIENTE, null, generarValoresCliente(id, nombre, password));
        return result != -1;
    }

    @Override
    public boolean modificarClientePassword(String password) {
        String id = obtenerCliente().getId();
        ContentValues values = new ContentValues();
        values.put(CN_PASSWORD, password);
        int result = db.update(TABLE_CLIENTE, values, CN_ID + " = ?", new String[]{id});
        return result != -1;
    }

    @Override
    public boolean modificarMesaCliente(String idMesa) {
        ContentValues valores = new ContentValues();
        valores.put(CN_IDMESA, idMesa);
        int result = db.update(TABLE_CLIENTE, valores, null, null);
        return result == 1;
    }

    @Override
    public ClienteClass obtenerCliente(String id) {
        ClienteClass cliente = new ClienteClass();
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_PASSWORD};
        Cursor cursor = db.query(TABLE_EMPLEADO, columnas, CN_ID + " = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                cliente.setId(cursor.getString(0));
                cliente.setNombre(cursor.getString(1));
                cliente.setPassword(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return cliente;
    }

    @Override
    public ClienteClass obtenerCliente() {
        ClienteClass cliente = new ClienteClass();
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_PASSWORD, CN_IDMESA};
        Cursor cursor = db.query(TABLE_CLIENTE, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                cliente.setId(cursor.getString(0));
                cliente.setNombre(cursor.getString(1));
                cliente.setPassword(cursor.getString(2));
                cliente.setMesa("" + cursor.getString(3));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return cliente;
    }

    private ContentValues generarValoresUsuario(String nombre, String apellido, String correo, String nick, String password, String restaurante) {
        ContentValues valores = new ContentValues();
        valores.put(CN_NOMBRE, nombre);
        valores.put(CN_APELLIDO, apellido);
        valores.put(CN_CORREO, correo);
        valores.put(CN_NICK, nick);
        valores.put(CN_PASSWORD, password);
        valores.put(CN_RESTAURANTE, restaurante);

        return valores;
    }

    @Override
    public void crearTablaUsuario() {
        this.eliminarTabla(TABLE_PERSONAL);
        db.execSQL(CREATE_TABLE_PERSONAL);
    }

    @Override
    public boolean insertarUsuario(String nombre, String apellido, String correo, String nick, String password, String restaurante) {
        long insert = db.insert(TABLE_PERSONAL, null, generarValoresUsuario(nombre, apellido, correo, nick, password, restaurante));
        return insert == 1;
    }

    @Override
    public boolean modificarUsuario(String nombre, String apellido, String puesto, String correo, String nick, String password, String restaurante) {
        return false;
    }

    @Override
    public UsuarioClass obtenerUsuario(String nick) {
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_APELLIDO, CN_NICK, CN_CORREO, CN_PASSWORD, CN_RESTAURANTE};
        UsuarioClass empleado = new UsuarioClass();
        Cursor cursor = db.query(TABLE_PERSONAL, columnas, CN_NICK + " = ?", new String[]{nick}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                empleado.setNombre(cursor.getString(1));
                empleado.setApellido(cursor.getString(2));
                empleado.setCorreo(cursor.getString(3));
                empleado.setNick(cursor.getString(4));
                empleado.setPassword(cursor.getString(5));
                empleado.setPassword(cursor.getString(5));
                empleado.setRestaurante(cursor.getString(6));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return empleado;
    }

    @Override
    public ArrayList<UsuarioClass> obtenerUsuarios() {
        String[] columnas = new String[]{CN_ID, CN_NOMBRE, CN_APELLIDO, CN_NICK, CN_CORREO, CN_PASSWORD, CN_RESTAURANTE};
        ArrayList<UsuarioClass> arrayList = new ArrayList<>();
        Cursor cursor = db.query(TABLE_PERSONAL, columnas, null, null, null, null, CN_NOMBRE);
        if (cursor.moveToFirst()) {
            do {
                UsuarioClass empleado = new UsuarioClass();
                empleado.setNombre(cursor.getString(1));
                empleado.setApellido(cursor.getString(2));
                empleado.setNick(cursor.getString(3));
                empleado.setCorreo(cursor.getString(4));
                empleado.setPassword(cursor.getString(5));
                empleado.setRestaurante(cursor.getString(6));
                arrayList.add(empleado);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

    public boolean eliminarEmpleado(final String id) {
        int result = db.delete(TABLE_EMPLEADO, CN_ID + "=?", new String[]{id});
        return (result != -1);
    }

    @Override
    public String obtenerPassword(String id) {
        String[] columnas = new String[]{CN_PASSWORD};
        String password = "";
        Cursor cursor = db.query(TABLE_EMPLEADO, columnas, CN_ID + " = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                password = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return password;
    }

    private ContentValues generarValoresSesion(String id, String nick, String password, String restaurante, String tipo, String token) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_NICK, nick);
        valores.put(CN_PASSWORD, password);
        valores.put(CN_RESTAURANTE, restaurante);
        valores.put(CN_TIPO, tipo);
        valores.put(CN_TOKEN, token);
        return valores;
    }

    @Override
    public void crearTablaSesion() {
        if (isTableExists("sesion")) {
            this.eliminarTabla(TABLE_SESION);
        }
        db.execSQL(CREATE_TABLE_SESION);
    }

    @Override
    public void insertarSesion(String id, String nick, String password, String restaurante, String tipo, String token) {
        crearTablaSesion();
        db.insert(TABLE_SESION, null, generarValoresSesion(id, nick, password, restaurante, tipo, token));
    }

    @Override
    public boolean modificarSesionPassword(String password) {
        String id = obtenerSesion().get(0);
        ContentValues values = new ContentValues();
        values.put(CN_PASSWORD, password);
        int result = db.update(TABLE_SESION, values, CN_ID + " = ?", new String[]{id});
        return result != -1;
    }

    @Override
    public ArrayList<String> obtenerSesion() {
        ArrayList<String> arrayList = new ArrayList<>();
        String[] columnas = new String[]{CN_ID, CN_NICK, CN_PASSWORD, CN_RESTAURANTE, CN_TIPO};
        Cursor cursor = db.query(TABLE_SESION, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                arrayList.add(cursor.getString(0));
                arrayList.add(cursor.getString(1));
                arrayList.add(cursor.getString(2));
                arrayList.add(cursor.getString(3));
                arrayList.add(cursor.getString(4));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

    @Override
    public void modificarSesion(String nick, String password, String recordar) {
        ContentValues values = new ContentValues();
        values.put(CN_NICK, nick);
        values.put(CN_PASSWORD, password);
        values.put(CN_RESTAURANTE, recordar);
        db.update(TABLE_SESION, values, CN_NICK + " = ?", new String[]{nick});
    }

    @Override
    public void eliminarSesion() {
        if(isTableExists("sesion")){
            db.delete(TABLE_SESION, null, null);
        }
    }

    @Override
    public void eliminarCliente() {
        if(isTableExists("cliente")){
            db.delete(TABLE_CLIENTE, null, null);
        }
    }

    @Override
    public void crearTablaMesa() {
        this.eliminarTabla(TABLE_MESA);
        db.execSQL(CREATE_TABLE_MESA);
    }

    @Override
    public boolean insertarMesa(final String id, final String nombre, final String capacidad, final String forma, final String estado, final String espacio, final String cliente, final String empleado) {
        long result = db.insert(TABLE_MESA, null, generarValoresMesa(id, nombre, capacidad, forma, estado, espacio, cliente, empleado));
        return result != -1;
    }

    @Override
    public boolean insertarMesaApi(final String nombre, final String capacidad, final String forma, final String estado, final String espacio) {
        final ArrayList<String> user = this.obtenerSesion();
        final String idEspacio = this.obtenerIdEspacio(espacio).get(0);

        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = ip + "mesas/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String id = object.getString("id");
                            //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                            boolean insert = insertarMesa(id, nombre, capacidad, forma, estado, idEspacio, "", "");
                            if (insert) {
                                generarMensaje("Mesa insertada con exito");
                                reiniciarIntent(MainActivity.class);
                            } else {
                                generarMensaje("Fallo al insertar");
                            }

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", nombre);
                params.put("capacidad", capacidad);
                params.put("forma", forma);
                params.put("estado", estado);
                params.put("espacio", idEspacio);
                params.put("user", user.get(3));

                return params;
            }

        };
        mRequestQueue.add(putRequest);

        //db.insert(TABLE_MESA, null, generarValoresMesa(id,nombre,capacidad, forma, estado, idEspacio,"null","null"));
        return true;
    }

    @Override
    public boolean modificarEstadoMesa(final String idMesa, final String estado) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        final ArrayList<String> mesa = this.obtenerMesa(Integer.parseInt(idMesa));
        String url = ip + "mesas/" + idMesa + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("nombre", mesa.get(5));
                params.put("capacidad", mesa.get(1));
                params.put("forma", mesa.get(2));
                params.put("estado", estado);
                params.put("espacio", mesa.get(4));
                if (estado.equals("LIBRE")) {
                    params.put("cliente", "");
                }

                return params;
            }
        };
        mRequestQueue.add(putRequest);
        ContentValues values = new ContentValues();
        values.put(CN_ESTADO, estado);
        int result = db.update(TABLE_MESA, values, CN_ID + " = ?", new String[]{idMesa});
        return result == 1;
    }

    @Override
    public boolean asignarMesaCliente(final String idMesa) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = ip + "mesas/" + idMesa + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("estado", "OCUPADA");
                params.put("cliente", idUsuario);

                return params;
            }
        };
        mRequestQueue.add(putRequest);
        ContentValues values = new ContentValues();
        values.put(CN_ESTADO, "OCUPADA");
        values.put(CN_CLIENTE, idUsuario);
        int result = db.update(TABLE_MESA, values, CN_ID + " = ?", new String[]{idMesa});
        return result == 1;
    }

    @Override
    public boolean modificarEmpleadoMesa(final String idMesa, final String idEmpleado) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        final ArrayList<String> mesa = this.obtenerMesa(Integer.parseInt(idMesa));
        String url = ip + "mesas/" + idMesa + "/";
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("nombre", mesa.get(5));
                params.put("empleado", idEmpleado);

                return params;
            }
        };
        mRequestQueue.add(putRequest);
        ContentValues values = new ContentValues();
        if (idEmpleado.isEmpty()) {
            values.put(CN_EMPLEADO, "null");
        } else {
            values.put(CN_EMPLEADO, idEmpleado);
        }
        int result = db.update(TABLE_MESA, values, CN_ID + " = ?", new String[]{idMesa});
        return result == 1;
    }

    @Override
    public boolean modificarMesa(String id, String nombre, String capacidad, String forma, String estado, String espacio) {
        ContentValues values = new ContentValues();
        values.put(CN_NOMBRE, nombre);
        values.put(CN_CAPACIDAD, capacidad);
        values.put(CN_FORMA, forma);
        values.put(CN_ESPACIO, espacio);

        int result = db.update(TABLE_MESA, values, CN_ID + " = ?", new String[]{id});
        return result != -1;
    }

    @Override
    public ArrayList<String> obtenerMesa(int codigo) {
        ArrayList<String> mesa = new ArrayList<>();
        String[] columnas = new String[]{CN_ID, CN_CAPACIDAD, CN_FORMA, CN_ESTADO, CN_ESPACIO, CN_NOMBRE, CN_CLIENTE, CN_EMPLEADO};
        Cursor cursor = db.query(TABLE_MESA, columnas, CN_ID + " = ?", new String[]{String.valueOf(codigo)}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                mesa.add("" + cursor.getInt(0));//ID
                mesa.add("" + cursor.getInt(1));//Capacidad
                mesa.add("" + cursor.getString(2));//Forma
                mesa.add("" + cursor.getInt(3));//Estado
                mesa.add("" + cursor.getString(4));//Espacio
                mesa.add(cursor.getString(5));//Nombre
                mesa.add(cursor.getString(6));//Cliente
                mesa.add(cursor.getString(7));//Empleado
            } while (cursor.moveToNext());
        }
        cursor.close();
        return mesa;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerMesaPorEspacio(String espacio) {
        ArrayList<ArrayList<String>> tabla = new ArrayList<>();
        ArrayList<String> columna;
        String[] columnas = new String[]{CN_ID, CN_CAPACIDAD, CN_FORMA, CN_ESTADO, CN_ESPACIO, CN_NOMBRE, CN_CLIENTE, CN_EMPLEADO};
        Cursor cursor = db.query(TABLE_MESA, columnas, CN_ESPACIO + " = ?", new String[]{espacio}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna = new ArrayList<>();
                columna.add("" + cursor.getInt(0));
                columna.add("" + cursor.getInt(1));
                columna.add("" + cursor.getString(2));
                columna.add("" + cursor.getString(3));
                columna.add("" + cursor.getString(4));
                columna.add(cursor.getString(5));
                columna.add(cursor.getString(6));
                columna.add(cursor.getString(7));
                tabla.add(columna);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerMesaPorEspacio(int pos) {
        ArrayList<ArrayList<String>> mesas;
        ArrayList<String> espacios = this.obtenerIdEspacios();
        String espacio = espacios.get(pos);
        mesas = this.obtenerMesaPorEspacio(espacio);
        return mesas;
    }

    @Override
    public void modificarCodigoMesa(final ArrayList<String> mesa, final String codigo) {
        String url = ip + "mesas/" + mesa.get(0) + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String id = object.getString("id");
                            //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                            if (!codigo.equals("0")) {
                                modificarEstadoMesa(id, "OCUPADA");
                            } else {
                                modificarEstadoMesa(id, "LIBRE");
                            }


                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", mesa.get(5));
                params.put("forma", mesa.get(2));
                if (!codigo.equals("0")) {
                    params.put("estado", "OCUPADA");
                } else {
                    params.put("estado", "LIBRE");
                }
                params.put("espacio", mesa.get(4));
                params.put("capacidad", mesa.get(0));
                params.put("user", restaurante);
                params.put("empleado", idUsuario);
                params.put("codigo", codigo);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
    }

    @Override
    public ArrayList<ArrayList<String>> obtenerMesas() {
        String[] columnas = new String[]{CN_ID, CN_CAPACIDAD, CN_FORMA, CN_ESTADO, CN_ESPACIO, CN_NOMBRE, CN_CLIENTE, CN_EMPLEADO};
        ArrayList<ArrayList<String>> tabla = new ArrayList<>();
        ArrayList<String> columna;
        Cursor cursor = db.query(TABLE_MESA, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                columna = new ArrayList<>();
                columna.add("" + cursor.getInt(0));//ID
                columna.add("" + cursor.getInt(1));//Capacidad
                columna.add(cursor.getString(2));//Forma
                columna.add(cursor.getString(3));//Estado
                columna.add(cursor.getString(4));//Espacio
                columna.add(cursor.getString(5));//Nombre
                columna.add(cursor.getString(6));//Cliente
                columna.add(cursor.getString(7));//Empleado
                tabla.add(columna);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    @Override
    public boolean eliminarMesa(final String codigo) {
        String url = ip + "mesas/" + codigo + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
        db.delete(TABLE_MESA, CN_ID + "=?", new String[]{codigo});
        return true;
    }

    private ContentValues generarValoresMesa(String id, String nombre, String capacidad, String forma, String estado, String espacio, String cliente, String empleado) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_CAPACIDAD, capacidad);
        valores.put(CN_FORMA, forma);
        valores.put(CN_ESTADO, estado);
        valores.put(CN_ESPACIO, espacio);
        valores.put(CN_NOMBRE, nombre);
        valores.put(CN_CLIENTE, cliente);
        valores.put(CN_EMPLEADO, empleado);
        return valores;
    }

    int obtenerUlitmoIdMesa() {
        int id = 0;
        String[] columnas = new String[]{CN_ID};
        Cursor cursor = db.query(TABLE_MESA, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return id;
    }

    @Override
    public void crearTablaEspecial() {
        this.eliminarTabla(TABLE_ESPECIAL);
        db.execSQL(CREATE_TABLE_ESPECIAL);
    }

    @Override
    public boolean insertarEspecial(String id, String titulo, String producto, String precio, String activo) {
        long result = db.insert(TABLE_ESPECIAL, null, generarValoresEspecial(id, titulo, producto, precio, activo));
        return result != -1;
    }

    @Override
    public boolean insertarEspecialApi(final String titulo, final String producto, final String precio, final String activo) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        url = ip + "especiales/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String id = object.getString("id");
                            insertarEspecial(id, titulo, producto, precio, activo);


                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", titulo);
                params.put("producto", producto);
                params.put("precio", precio);
                params.put("activo", activo);
                params.put("user", restaurante);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
        return true;
    }

    @Override
    public ArrayList<SugerenciaClass> obtenerEspeciales() {
        ArrayList<SugerenciaClass> array = new ArrayList<>();
        SugerenciaClass especial;
        String[] columnas = new String[]{CN_ID, CN_TITULO, CN_NOMBREPRODUCTO, CN_PRECIO, CN_ACTIVO};
        Cursor cursor = db.query(TABLE_ESPECIAL, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                especial = new SugerenciaClass();
                especial.setId(cursor.getString(0));
                especial.setTitulo(cursor.getString(1));
                especial.setProducto(cursor.getString(2));
                especial.setPrecio(cursor.getString(3));
                if (cursor.getString(4).equals("0")) {
                    especial.setActivo(false);
                } else {
                    especial.setActivo(true);
                }
                array.add(especial);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return array;
    }

    @Override
    public SugerenciaClass obtenerEspecialActivo() {
        SugerenciaClass especial = new SugerenciaClass();
        String[] columnas = new String[]{CN_ID, CN_TITULO, CN_NOMBREPRODUCTO, CN_PRECIO, CN_ACTIVO};
        Cursor cursor = db.query(TABLE_ESPECIAL, columnas, CN_ACTIVO + " = ?", new String[]{"1"}, null, null, null);
        if (cursor.moveToFirst()) {
            do {

                especial.setId(cursor.getString(0));
                especial.setTitulo(cursor.getString(1));
                especial.setProducto(cursor.getString(2));
                especial.setPrecio(cursor.getString(3));
                if (cursor.getString(4).equals("0")) {
                    especial.setActivo(false);
                } else {
                    especial.setActivo(true);
                }

            } while (cursor.moveToNext());
        }
        cursor.close();
        return especial;
    }

    @Override
    public boolean modificarActivoEspecial(String id, final boolean activo) {
        String post_resturante = ip + "especiales/" + id + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        StringRequest putRequest = new StringRequest(Request.Method.PUT, post_resturante,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        /*try {

                            //JSONObject object = new JSONObject(response);

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }*/
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (activo) {
                    params.put("activo", "1");
                } else {
                    params.put("activo", "0");
                }

                return params;
            }

        };
        mRequestQueue.add(putRequest);

        ContentValues values = new ContentValues();
        if (activo) {
            values.put(CN_ACTIVO, "1");
        } else {
            values.put(CN_ACTIVO, "0");
        }
        int result = db.update(TABLE_ESPECIAL, values, CN_ID + " = ?", new String[]{id});
        return result == 1;
    }

    @Override
    public boolean modificarEspecial(String id, final String titulo, final String producto, final String precio) {
        String post_resturante = ip + "especiales/" + id + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        StringRequest putRequest = new StringRequest(Request.Method.PUT, post_resturante,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        /*try {

                            JSONObject object = new JSONObject(response);

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }*/
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", titulo);
                params.put("producto", producto);
                params.put("precio", precio);

                return params;
            }

        };
        mRequestQueue.add(putRequest);

        ContentValues values = new ContentValues();
        values.put(CN_TITULO, titulo);
        values.put(CN_NOMBREPRODUCTO, producto);
        values.put(CN_PRECIO, precio);
        int result = db.update(TABLE_ESPECIAL, values, CN_ID + " = ?", new String[]{id});
        return result == 1;
    }

    @Override
    public boolean eliminarEspecial(final String id) {
        String url = ip + "especiales/" + id + "/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    db.delete(TABLE_ESPECIAL, CN_ID + "=?", new String[]{id});

                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);

        return true;
    }

    private ContentValues generarValoresEspecial(String id, String titulo, String producto, String precio, String activo) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ID, id);
        valores.put(CN_TITULO, titulo);
        valores.put(CN_NOMBREPRODUCTO, producto);
        valores.put(CN_PRECIO, precio);
        valores.put(CN_ACTIVO, activo);
        return valores;
    }

    @Override
    public void crearTablaEspacio() {
        this.eliminarTabla(TABLE_ESPACIO);
        db.execSQL(CREATE_TABLE_ESPACIO);
    }

    @Override
    public boolean insertarEspacio(String id, String espacio) {
        long result = db.insert(TABLE_ESPACIO, null, generarValoresEspacio(id, espacio));
        return result != -1;
    }

    ArrayList<String> obtenerIdEspacio(String nombre) {
        String[] columnas = new String[]{CN_ID, CN_ESPACIO};
        ArrayList<String> tabla = new ArrayList<>();
        Cursor cursor = db.query(TABLE_ESPACIO, columnas, CN_ESPACIO + "= ?", new String[]{String.valueOf(nombre)}, null, null, null);
        String lowercase;
        if (cursor.moveToFirst()) {
            do {
                lowercase = cursor.getString(0).toLowerCase(); //Tratamos todos los espacios en minusculas para poder comparar mejor
                tabla.add(lowercase);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    ArrayList<String> obtenerEspacioById(String id) {
        String[] columnas = new String[]{CN_ID, CN_ESPACIO};
        ArrayList<String> tabla = new ArrayList<>();
        Cursor cursor = db.query(TABLE_ESPACIO, columnas, CN_ID + "= ?", new String[]{String.valueOf(id)}, null, null, null);
        String lowercase;
        if (cursor.moveToFirst()) {
            do {
                lowercase = cursor.getString(1).toLowerCase(); //Tratamos todos los espacios en minusculas para poder comparar mejor
                tabla.add(lowercase);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    public ArrayList<String> obtenerIdEspacios() {
        String[] columnas = new String[]{CN_ID, CN_ESPACIO};
        ArrayList<String> tabla = new ArrayList<>();
        Cursor cursor = db.query(TABLE_ESPACIO, columnas, null, null, null, null, null);
        String lowercase;
        if (cursor.moveToFirst()) {
            do {
                lowercase = cursor.getString(0).toLowerCase(); //Tratamos todos los espacios en minusculas para poder comparar mejor
                tabla.add(lowercase);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    @Override
    public ArrayList<String> obtenerEspacios() {
        String[] columnas = new String[]{CN_ID, CN_ESPACIO};
        ArrayList<String> tabla = new ArrayList<>();
        Cursor cursor = db.query(TABLE_ESPACIO, columnas, null, null, null, null, null);
        String lowercase;
        if (cursor.moveToFirst()) {
            do {
                lowercase = cursor.getString(1).toLowerCase(); //Tratamos todos los espacios en minusculas para poder comparar mejor
                tabla.add(lowercase);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tabla;
    }

    boolean modificarEspacio(String id, String nombre) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CN_ESPACIO, nombre);
        int result = db.update(TABLE_ESPACIO, contentValues, CN_ID + "= ?", new String[]{id});
        return result != -1;
    }

    @Override
    public boolean eliminarEspacio(String espacio) {
        int result = db.delete(TABLE_ESPACIO, CN_ID + "=?", new String[]{espacio});
        return result == 1;
    }

    private ContentValues generarValoresEspacio(String id, String espacio) {
        ContentValues valores = new ContentValues();
        valores.put(CN_ESPACIO, espacio.toLowerCase());
        valores.put(CN_ID, id);

        return valores;
    }

    public boolean isEspacioExist(String espacio) {
        ArrayList<String> espacios = this.obtenerEspacios();
        espacio = espacio.toLowerCase();
        return espacios.contains(espacio);
    }

    /*boolean insertarEspacioApi(final String nombre) {
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        url = ip + "espacios/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String id = object.getString("id");
                            //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                            insertarEspacio(id, nombre);


                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user", restaurante);
                params.put("nombre", nombre);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
        return true;
    }*/

    /*public int obtenerUlitmoIdEspacio() {
        int id = 0;
        String[] columnas = new String[]{CN_ID};
        Cursor cursor = db.query(TABLE_ESPACIO, columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return id;
    }*/

    @Override
    public void crearTablaToken() {
        this.eliminarTabla(TABLE_TOKEN);
        db.execSQL(CREATE_TABLE_TOKEN);
    }

    @Override
    public boolean insertarToken(String token) {
        db.insert(TABLE_TOKEN, null, generarValoresToken(token));
        return true;
    }

    @Override
    public String obtenerToken() {
        String[] columnas = new String[]{CN_TOKEN};
        Cursor cursor = db.query(TABLE_TOKEN, columnas, null, null, null, null, null);
        String token = "";
        if (cursor.moveToFirst()) {
            do {
                token = cursor.getString(0); //Tratamos todos los espacios en minusculas para poder comparar mejor
            } while (cursor.moveToNext());
        }
        cursor.close();
        return token;
    }

    @Override
    public void actualizarToken(ArrayList<String> sesion) {
        String tokenActual = this.obtenerToken();
        String tokenEmpleado = this.obtenerEmpleado(idUsuario).getToken();
        if (!tokenActual.equals(tokenEmpleado) && !isAdmin()) {
            actualizarTokenEmpelado(sesion, tokenActual);
        }
    }

    private ContentValues generarValoresToken(String token) {
        ContentValues valores = new ContentValues();
        valores.put(CN_TOKEN, token);

        return valores;
    }

    @Override
    public void actualizar() {

        //this.crearTablaUsuario();
        //this.generarEmpleados();
        //this.eliminarTabla("sesion");
        //this.eliminarTabla("producto");
        this.eliminarTabla("comanda");

        if (!isTableExists("cliente")) {
            this.crearTablaCliente();
        }

        if (!isTableExists("token")) {
            this.crearTablaToken();
        }

        if (!isTableExists("sesion")) {
            this.crearTablaSesion();
        }

        //eliminarSesion();

        ArrayList<String> sesion = this.obtenerSesion();
        if (sesion.size() > 0 && (sesion.get(4).equals("empleado") || sesion.get(4).equals("restaurante"))) {
            restaurante = this.obtenerSesion().get(3);

            //this.crearTablaEspacio();
            //this.generarEspacios(restaurante);

            //this.crearTablaMesa();
            this.generarMesas(restaurante);

            //this.crearTablaIngrediente();
            this.generarIngredientes(restaurante);

            //this.crearTablaCategoria();
            this.generarCategorias(restaurante);

            //this.crearTablaProducto();
            this.generarProductos(restaurante);

            //this.crearTablaComanda();
            this.generarComandas(restaurante);

            //this.crearTablaSesion();

            this.generarEspeciales(restaurante);

            this.generarEmpleados(restaurante);
        }
    }

    @Override
    public void actualizarCliente() {

        //this.crearTablaEspacio();
        //this.generarEspacios(restaurante);

        //this.crearTablaMesa();
        this.generarMesas(restaurante);

        //this.crearTablaIngrediente();
        this.generarIngredientes(restaurante);

        //this.crearTablaCategoria();
        this.generarCategorias(restaurante);

        //this.crearTablaTipo();
        //this.generarTipos(restaurante);

        //this.crearTablaProducto();
        this.generarProductos(restaurante);

        //this.crearTablaProducto_Tipo();
        //this.generarProducto_Tipo();

        //this.crearTablaProducto_Ingrediente();
        //this.generarProducto_Ingredientes();

        //this.crearTablaComanda();
        this.generarComandas(restaurante);

        //this.crearTablaEmpleado()
        this.generarEmpleados(restaurante);

        this.generarEspeciales(restaurante);

        this.generarRestaurantes();
    }

    @Override
    public  void generarEspeciales(final String restaurante) {
        crearTablaEspecial();

        String url = ip + "especiales/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);
                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String titulo = object.getString("nombre").toLowerCase();
                            String producto = object.getString("producto").toLowerCase();
                            String precio = object.getString("precio").toLowerCase();
                            String activo = object.getString("activo").toLowerCase();
                            insertarEspecial(id, titulo, producto, precio, activo);
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
    }

    /*private void generarEspacios(final String restaurante) {
        crearTablaEspacio();

        String url = ip + "espacios/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);
                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre").toLowerCase();
                            insertarEspacio(id, nombre);
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);

            }
        });
        mRequestQueue.add(request);
    }*/


    @Override
    public void generarMesas(final String restaurante) {
        crearTablaMesa();

        String get_mesas = ip + "mesas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_mesas, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);
                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            //{"id":3,"nombre":"Mesa Baja 1","capacidad":4,"forma":"CUADRADA","estado":"LIBRE","espacio":11,"user":8}
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String capacidad = object.getString("capacidad");
                            String forma = object.getString("forma");
                            String estado = object.getString("estado");
                            String espacio = object.getString("espacio");
                            String cliente = object.getString("cliente");
                            String empleado = object.getString("empleado");
                            insertarMesa(id, nombre, capacidad, forma, estado, espacio, cliente, empleado);
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
    }

    @Override
    public void generarIngredientes(final String restaurante) {
        crearTablaIngrediente();

        String get_ingredientes = ip + "ingredientes/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_ingredientes, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String stock = object.getString("stock");
                            insertarIngrediente(id, nombre, stock);
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
    }

    @Override
    public void generarCategorias(final String restaurante) {
        crearTablaCategoria();

        String get_categorias = ip + "categorias/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_categorias, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            insertarCategoria(id, nombre);
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
    }

    /*private void generarTipos(final String restaurante){
        crearTablaTipo();

        String get_tipos = ip+"tipos/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_tipos, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if(rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String precio = object.getString("precio");
                            insertarTipo(id, nombre.toLowerCase(),precio);
                        }
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }*/

    @Override
    public void generarProductos(final String restaurante) {
        crearTablaProducto_Ingrediente();
        crearTablaProducto();

        String get_productos = ip + "productos/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_productos, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String descripcion = object.getString("descripcion");
                            String categoria = object.getString("categoria");
                            String precio = object.getString("precio");
                            String stock = object.getString("stock");


                            JSONArray ingredientes = new JSONArray(object.getString("ingredientes"));
                            for (int j = 0; j < ingredientes.length(); j++) {
                                insertarProducto_Ingrediente(id, ingredientes.getString(j));

                            }
                            insertarProducto_Categoria(id, categoria);
                            insertarProducto(id, nombre, descripcion, stock, categoria, precio);


                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        mRequestQueue.add(request);
    }

    @Override
    public void generarComandas(final String restaurante) {
        crearTablaComanda();

        String get_comandas = ip + "comandas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_comandas, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String mesa = object.getString("mesa");
                            String producto = object.getString("producto");
                            String categoria = object.getString("categoria");
                            String estado = object.getString("estado");
                            String cantidad = object.getString("cantidad");
                            String comentario = object.getString("comentario");
                            String token = object.getString("token");
                            String fecha = object.getString("fecha");
                            String hora = object.getString("hora");

                            if (!estado.equals("FINALIZADO")) {
                                insertarComanda(id, mesa, producto, categoria, comentario, estado, cantidad, token, fecha, hora);
                            }
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        //request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);

    }

    @Override
    public void generarEmpleados(final String restaurante) {
        crearTablaEmpleado();

        String url = ip + "empleados/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);

                        String rest = object.getString("restaurante");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String restaurante = object.getString("restaurante");
                            String token = object.getString("token");
                            //String password = object.getString("password");

                            insertarEmpleado(id, nombre, restaurante, token, "");
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                generarMensaje(err);
            }
        });
        //request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);

    }

    @Override
    public void generarRestaurantes() {
        crearTablaRestaurante();

        String url = ip + "restaurantes/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);
                        String id = object.getString("id");
                        String nombre = object.getString("nombre");
                        String direccion = object.getString("direccion");
                        String localidad = object.getString("etLocalidad");
                        String provincia = object.getString("etProvincia");
                        String latitud = object.getString("latitud");
                        String longitud = object.getString("longitud");

                        RestauranteClass restaurante = new RestauranteClass(id, nombre, direccion, provincia, localidad, latitud, longitud);
                        insertarRestaurante(restaurante);

                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);

            }
        });
        mRequestQueue.add(request);

    }

    void lanzarDemo() {

        ip = "";

        demo = true;

        this.demoEspacios();

        this.demoMesas();

        this.demoIngredientes();

        this.demoCategorias();

        this.demoProductos();

        this.demoProducto_Ingredientes();

        this.demoComandas();

        this.demoRestaurante();

        this.demoEspeciales();

        this.demoEmpleados();

    }

    private void demoEspeciales() {
        insertarEspecial("1", "racion del dia", "racion de chopitos", "5.5", "0");
        insertarEspecial("2", "pescado del dã£â­a", "sardinas", "7.5", "0");
        insertarEspecial("3", "tapa del dia", "ajo y agua", "2.5", "1");
    }

    private void demoRestaurante() {
        RestauranteClass restaurante = new RestauranteClass("0", "DemoBar", "", "Almeria", "Almeria", "", "");
        insertarRestaurante(restaurante);
    }

    //Para pruebas
    private void demoProductos() {
        //insertarProducto(id,Nombre, Descripcion, Categoria, Stock, Categoria, Precio);//0
        insertarProducto("1", "arroz negro", "Arroz negro con setas y champiñones", "1", "0", "1.5");//1
        insertarProducto("2", "paella", "Paella de marisco", "1", "0", "1.5");//2
        insertarProducto("3", "paella", "de verduras", "1", "0", "1.5");//3
        insertarProducto("4", "carne con tomate", "carne con tomate", "1", "1", "1.5");//4
        insertarProducto("5", "merluza", "merluza con mayonesa", "1", "2", "1.5");//5
        insertarProducto("6", "alhambra 1925", "", "1", "3", "2.0");//6
        insertarProducto("7", "fanta", "de limon", "1", "3", "2.0");//7
        insertarProducto("8", "arroz a la cubana", "arroz cocido con tomate y huevo", "1", "0", "1.5");//8
    }

    //Para pruebas
    /*private void demoProducto_Tipo() {
        //insertarProducto_Tipo(idProducto,idTipo);
        insertarProducto_Categoria("1", "1");
        insertarProducto_Categoria("1", "2");
        insertarProducto_Categoria("2", "1");
        insertarProducto_Categoria("2", "2");
        insertarProducto_Categoria("4", "1");
        insertarProducto_Categoria("4", "2");
        insertarProducto_Categoria("5", "1");
        insertarProducto_Categoria("6", "3");
        insertarProducto_Categoria("7", "4");
        insertarProducto_Categoria("8", "1");
        insertarProducto_Categoria("8", "2");
    }*/

    //Para Pruebas
    private void demoIngredientes() {
        //insertarIngrediente(id,nombre ingrediente,stock);
        insertarIngrediente("0", "atun", "1");//1
        insertarIngrediente("1", "mayonesa", "1");//1
        insertarIngrediente("2", "tomate", "1");//2
        insertarIngrediente("3", "ketchup", "1");//3
        insertarIngrediente("4", "ali-oli", "1");//4
        insertarIngrediente("5", "huevo", "1");//5
        insertarIngrediente("6", "lechuga", "1");//6
        insertarIngrediente("7", "chorizo", "1");//7
        insertarIngrediente("8", "arroz", "1");//8
        insertarIngrediente("9", "patatas fritas", "1");//9
        insertarIngrediente("10", "mostaza", "1");//10
        insertarIngrediente("11", "sal", "1");//11
        insertarIngrediente("12", "pimiento", "1");//12
        insertarIngrediente("13", "vinagre", "1");//13
        insertarIngrediente("14", "cebolla", "1");//14
        insertarIngrediente("15", "ajos", "1");//15
        insertarIngrediente("16", "queso", "1");//16
        insertarIngrediente("17", "champiñon", "1");//17
        insertarIngrediente("18", "calabaza", "1");//18
        insertarIngrediente("19", "puerro", "1");//19
        insertarIngrediente("20", "esparragos", "1");//20
        insertarIngrediente("21", "alcachofa", "0");//21
    }

    //Para Pruebas
    private void demoProducto_Ingredientes() {
        insertarProducto_Ingrediente("8", "2");
        insertarProducto_Ingrediente("8", "5");
        insertarProducto_Ingrediente("8", "8");
        insertarProducto_Ingrediente("8", "9");
        insertarProducto_Ingrediente("1", "Arroz");
        insertarProducto_Ingrediente("1", "Pimiento");
        insertarProducto_Ingrediente("1", "Champiñon");
        insertarProducto_Ingrediente("1", "Puerro");
        insertarProducto_Ingrediente("1", "Alcachofa");
        insertarProducto_Ingrediente("1", "Ajos");
    }

    //Para pruebas
    private void demoCategorias() {
        insertarCategoria("0", "arroz");
        insertarCategoria("1", "carne");
        insertarCategoria("2", "pescado");
        insertarCategoria("3", "bebida");
    }

    //Para pruebas
    private void demoEmpleados() {
        //insertarUsuario(nombre, apellido, correo, nick, password, restaurante);
        insertarEmpleado("0", "demo", "0", this.obtenerToken(), "admin");
        insertarEmpleado("1", "demo2", "0", this.obtenerToken(), "admin");
        //insertarUsuario("carlos", "garrido", "carlos@gmail.com", "cgv", "admin","0");
        //insertarUsuario("jose", "vergara", "jose@gmail.com", "jgm", "admin","0");
        //insertarUsuario("juan", "manzano", "juan@gmail.com", "jagm", "admin","0");
        //insertarUsuario("admin", "admin", "admin@gmail.com", "admin", "admin","0");
    }

    //Para pruebas
    private void demoMesas() {
        //insertarMesa(id, capacidad, forma, reservado, espacio, cliente, empleado);
        insertarMesa("1", "Mesa 1", "4", "cuadrado", "LIBRE", "0", "", "0");
        insertarMesa("2", "Mesa 2", "4", "cuadrado", "LIBRE", "0", "", "0");
        insertarMesa("3", "Mesa 3", "6", "cuadrado", "LIBRE", "0", "", "1");
        insertarMesa("4", "Mesa 4", "8", "redonda", "LIBRE", "4", "", "1");
    }

    //Para pruebas
    private void demoEspacios() {
        //insertarEspacio(Nombre espacio);
        insertarEspacio("0", "comedor");
        insertarEspacio("1", "terraza");
        insertarEspacio("4", "barra");
    }

    private void demoComandas() {
        //"65","4","21","12","Sin comentarios","PAGADO","1","","3-5-2017","11:3"
        insertarComanda("1", "4", "1", "0", "Sin comentarios", "PAGADO", "1", "", "3-5-2017", "11:3");
        insertarComanda("2", "3", "3", "0", "Sin comentarios", "PAGADO", "1", "", "1-6-2017", "6:9");
        insertarComanda("3", "3", "3", "0", "Sin comentarios", "PAGADO", "1", "", "1-6-2017", "6:9");
        insertarComanda("5", "3", "7", "2", "Con azucar", "SERVIDO", "1", "", "1-4-2017", "8:41");
        insertarComanda("6", "3", "5", "3", "Sin comentarios", "COLA", "1", "", "7-4-2017", "11:3");

    }

    void generarMensaje(String menssage) {
        Toast.makeText(context, menssage, Toast.LENGTH_SHORT).show();
    }

    void reiniciarIntent(Class clase) {
        Intent intent = new Intent(context, clase);
        context.startActivity(intent);
    }

    /*private void arreglarComandas() {
        crearTablaComanda();

        String get_comandas = ip + "comandas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_comandas, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if (rest.equals(restaurante)) {
                            String id = object.getString("id");
                            String mesa = object.getString("mesa");
                            String producto = object.getString("producto");
                            String categoria = object.getString("categoria");
                            String estado = object.getString("estado");
                            String cantidad = object.getString("cantidad");
                            String comentario = object.getString("comentario");
                            String token = object.getString("token");
                            String fecha = object.getString("fecha");
                            String hora = object.getString("hora");

                            if (!estado.equals("FINALIZADO")) {
                                insertarComanda(id, mesa, producto, categoria, comentario, estado, cantidad, token, fecha, hora);
                            }
                        }
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);
            }
        });
        //request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }*/

}
