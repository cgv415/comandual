package com.example.carlos.comandual;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 23/05/2017
 */

class SugerenciasAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<SugerenciaClass> especiales;
    private ArrayList<CheckBox> checkBoxArrayList;

    private DataBaseManager manager;

    /*public MesasAdapter(Activity activity, ArrayList<String> especiales, ArrayList<ArrayList<String>> reparto) {
        this.activity = activity;
        this.especiales = especiales;
        this.reparto = reparto;

    }*/

    SugerenciasAdapter(Activity activity, ArrayList<SugerenciaClass> especiales) {
        this.activity = activity;
        this.especiales = especiales;

    }

    @Override
    public int getCount() {
        return especiales.size();
    }

    @Override
    public Object getItem(int position) {
        return especiales.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View v = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_elegir_especiales, null);
        }

        manager = new DataBaseManager(v.getContext());

        ImageButton button = (ImageButton) v.findViewById(R.id.imgbtn_ver);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),especiales.get(position).getTitulo(),Toast.LENGTH_SHORT).show();
                popUpDetalles(especiales.get(position),v);
            }
        });

        if(checkBoxArrayList == null){
            checkBoxArrayList = new ArrayList<>();
        }

        TextView textView = (TextView) v.findViewById(R.id.textview_camarero);
        textView.setText(especiales.get(position).getProducto());

        CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox_mesa);
        checkBox.setText(especiales.get(position).getTitulo());
        checkBox.setTag(especiales.get(position).getId());

        if(especiales.get(position).isActivo()){
            checkBox.setChecked(true);
            actualizarArrayCheckBox(especiales.get(position).getId(),true);
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                actualizarArrayCheckBox(buttonView.getTag().toString(),isChecked);
                manager.modificarActivoEspecial(buttonView.getTag().toString(),isChecked);
            }
        });
        if(DataBaseManager.tipoUsuario.equals("cliente")){
            checkBox.setClickable(false);
        }
        checkBoxArrayList.add(checkBox);



        return v;
    }

    public boolean isCheck(String nombre){
        for(int i = 0; i < checkBoxArrayList.size();i++){
            String check = checkBoxArrayList.get(i).getText().toString();
            if(check.equals(nombre)){
                return checkBoxArrayList.get(i).isChecked();
            }
        }
        return false;
    }

    private void actualizarArrayCheckBox(String id,boolean check){
        for(int i = 0; i < checkBoxArrayList.size();i++){
            String checkbox = checkBoxArrayList.get(i).getTag().toString();
            if(checkbox.equals(id)){
                checkBoxArrayList.get(i).setChecked(check);
                break;
            }
        }
    }

    public ArrayList<CheckBox> getCheckBoxArrayList(){
        return checkBoxArrayList;
    }

    private void popUpDetalles(final SugerenciaClass especial, final View view){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        LayoutInflater inflater = activity.getLayoutInflater();

        final EditText  etTitulo;
        final EditText etProducto;
        final EditText etPrecio;
        CheckBox cbVisible;

        final View v = inflater.inflate(R.layout.fragment_detalle_sugerencia, null);

        etTitulo = (EditText) v.findViewById(R.id.et_tituloSugerencia);
        etTitulo.setText(especial.getTitulo());
        etProducto = (EditText) v.findViewById(R.id.et_productoSugerido);
        etProducto.setText(especial.getProducto());
        etPrecio = (EditText) v.findViewById(R.id.et_precioProducto);
        etPrecio.setText(especial.getPrecio());
        cbVisible = (CheckBox) v.findViewById(R.id.cb_visible);

        if(especial.isActivo()){
            cbVisible.setChecked(true);
            cbVisible.setText("Visible al publico");
        }else{
            cbVisible.setChecked(false);
            cbVisible.setText("Oculto al publico");

        }

        if(DataBaseManager.tipoUsuario.equals("cliente")){
            etTitulo.setEnabled(false);
            etProducto.setEnabled(false);
            etPrecio.setEnabled(false);
            cbVisible.setVisibility(View.INVISIBLE);
        }

        builder.setTitle("Informacion");
        builder.setView(v);

       builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                boolean update = false;
                if(!etTitulo.getText().toString().equals(especial.getTitulo())){
                    update = true;
                }
                if(!etProducto.getText().toString().equals(especial.getProducto())){
                    update = true;
                }
                if(!etPrecio.getText().toString().equals(especial.getPrecio())){
                    update = true;
                }
                if(update){
                    manager.modificarEspecial(especial.getId(),etTitulo.getText().toString(),etProducto.getText().toString(),etPrecio.getText().toString());
                    Toast.makeText(v.getContext(),"Sugerencia modificada",Toast.LENGTH_SHORT).show();
                    reiniciarIntent(v);
                }
            }
        }) ;
        if(DataBaseManager.tipoUsuario.equals("empleado")){
            builder.setNegativeButton("Eliminar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    confirmarEliminar(v,especial);
                }
            });
        }
        builder.show();
    }

    public void reiniciarIntent(View v){
        activity.finish();
        Intent intent = new Intent(v.getContext(),SugerenciasActivity.class);
        activity.startActivity(intent);
    }

    public void confirmarEliminar(final View v,final SugerenciaClass especial){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

        builder.setTitle("¿Realmente deseas eliminar la sugerencia?");

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                manager.eliminarEspecial(especial.getId());
                Toast.makeText(v.getContext(),"Sugerencia eliminada",Toast.LENGTH_SHORT).show();
                reiniciarIntent(v);
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

}
