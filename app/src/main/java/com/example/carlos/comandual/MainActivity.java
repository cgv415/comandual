package com.example.carlos.comandual;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, EspacioFragment.OnFragmentInteractionListener, ActionBar.TabListener, StockFragment.OnFragmentInteractionListener {

    private DataBaseManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Mostrar el boton de back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(R.string.mainActivity);

        //Mostrar el navigation
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if(DataBaseManager.demo){
            AlertDialog alertDialog= createSimpleDialog();
            alertDialog.show();
        }


        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem nav = menu.findItem(R.id.nav_home);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView.setNavigationItemSelectedListener(this);

        generarEspacios(DataBaseManager.restaurante,savedInstanceState);

    }

    public void generarEspacios(final String restaurante,final Bundle savedInstanceState){
        String url = DataBaseManager.ip+"espacios/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    manager.crearTablaEspacio();
                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String rest = object.getString("user");
                        if(rest.equals(restaurante)){
                            String id = object.getString("id");
                            String nombre = object.getString("nombre").toLowerCase();
                            manager.insertarEspacio(id,nombre);
                        }
                    }
                    pintarEspacios(savedInstanceState);
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);

            }
        });
        mRequestQueue.add(request);
    }

    /**
     * Crea un diálogo de alerta sencillo
     * @return Nuevo diálogo
     */
    public AlertDialog createSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Version Demo!")
                .setMessage("Esta version puede no realizar todas las funciones correctamente. Para disfrutar de la version completa, resgistrese de manera gratuita.")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }) ;

        return builder.create();
    }

    public void pintarEspacios(Bundle savedInstanceState){
        TabHost mTabHost = (TabHost) findViewById(R.id.tabHostMain);
        mTabHost.setup();

        ViewPager mViewPager = (ViewPager) findViewById(R.id.pagerMain);
        //mTabsAdapter = new TabsAdapter(this, mTabHost, mViewPager);
        TabAdapter mTabsAdapter = new TabAdapter(this, mTabHost, mViewPager);

        Button btnDetalles = (Button) findViewById(R.id.btnDetalles);
        if(DataBaseManager.tipoUsuario.equals("cliente")){
            btnDetalles.setVisibility(View.GONE);
        }
        btnDetalles.setOnClickListener(this);

        ArrayList<String> espacios = manager.obtenerEspacios();
        for(int i = 0; i< espacios.size(); i++) {
            String tabActual = espacios.get(i);
            mTabsAdapter.addTab(mTabHost.newTabSpec("espacio").setIndicator(tabActual), EspacioFragment.class,null);
        }
       /* if(espacios.size()==0){
            mTabsAdapter.addTab(mTabHost.newTabSpec("espacio").setIndicator("default"), EspacioFragment.class,null);
        }*/
        //mTabsAdapter.addTab(mTabHost.newTabSpec("productos").setIndicator("Productos"), StockFragment.class,null);

        //Si queremos mostrar el boton de añadir espacio
        /*if(DataBaseManager.tipoUsuario.equals("empleado") && espacios.size() == 0){
            mTabsAdapter.addTab(mTabHost.newTabSpec("NuevoEspacio").setIndicator("+"), EspacioFragment.class,null);
        }*/

        /*if(manager.tipoUsuario.equals("empleado")){
            //mTabsAdapter.addTab(mTabHost.newTabSpec("NuevoEspacio").setIndicator("+"), NuevoEspacioFragment.class,null);
        }*/

        if (savedInstanceState != null)
        {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        }else{
            //super.onBackPressed();
            new AlertDialog.Builder(this)
                    .setTitle("Salir")
                    .setMessage("Realmente quieres salir de la aplicación?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            int p = android.os.Process.myPid();
                            android.os.Process.killProcess(p);
                        }})
                    .setNegativeButton(android.R.string.no, null).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(DataBaseManager.tipoUsuario.equals("cliente")){
            getMenuInflater().inflate(R.menu.main_cliente, menu);
        }else{
            getMenuInflater().inflate(R.menu.main, menu);

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnDetalles:
                Intent intent = new Intent(getApplicationContext(),ComandasActivity.class);
                startActivity(intent);
                break;
        }
    }
}
