package com.example.carlos.comandual;

import java.util.Comparator;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 24/08/2017
 */

public class ProductoComparator implements Comparator<Producto>
{
    @Override
    public int compare(Producto o1, Producto o2) {
        return o1.getNombre().compareTo(o2.getNombre());
    }
}