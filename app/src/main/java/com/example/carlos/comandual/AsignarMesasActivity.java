package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class AsignarMesasActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private ArrayList<ArrayList<String>> mesas;
    private DataBaseManager manager;
    private MesasAdapter mesasAdapter;
    private Button generar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asignar_mesas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();

        MenuItem nav = menu.findItem(R.id.nav_manage);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        mesas = manager.obtenerMesas();

        ListView listView = (ListView) findViewById(R.id.listView_mesas);

        mesasAdapter = new MesasAdapter(this,mesas);
        listView.setAdapter(mesasAdapter);
        listView.setItemsCanFocus(true);

        Button asignar = (Button) findViewById(R.id.btn_eliminar);
        asignar.setOnClickListener(this);

        generar = (Button) findViewById(R.id.btn_generar);
        generar.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up asignar, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            case R.id.action_call:
                //makeJsonObjReq();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_eliminar:
                new AlertDialog.Builder(v.getContext())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Asignar mesas")
                        .setMessage("¿Deseas asignar las mesas seleccionadas a tu control?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<CheckBox> arrayList = mesasAdapter.getCheckBoxArrayList();
                                String idEmpleado = DataBaseManager.idUsuario;
                                for(int i = 0 ; i < arrayList.size() ; i++){
                                    CheckBox checkBox = arrayList.get(i);
                                    Boolean check = checkBox.isChecked();
                                    if(check){
                                        if(!idEmpleado.equals(mesas.get(i).get(7))){
                                            manager.modificarEmpleadoMesa(mesas.get(i).get(0),idEmpleado);
                                        }
                                    }else if(idEmpleado.equals(mesas.get(i).get(7))){
                                        manager.modificarEmpleadoMesa(mesas.get(i).get(0),"");
                                    }
                                }
                                DataBaseManager.enviarNotificacion("Asignación de mesas modificada","","empleados");
                                finish();
                            }

                        })
                        .setNegativeButton("Cancelar", null)
                        .show();
                break;
            case R.id.btn_generar:
                popUpGenerarCodigos();
                break;
        }
    }

    private void popUpGenerarCodigos() {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_generar_codigos, null);
        LinearLayout layout = (LinearLayout) v.findViewById(R.id.layout_generar_codigos);

        final ArrayList<ArrayList<String>> mesas = manager.obtenerMesas();
        final ArrayList<CheckBox> checkBoxes = new ArrayList<>();
        for(ArrayList<String> mesa:mesas){
            CheckBox checkBox = new CheckBox(v.getContext());
            checkBox.setText(mesa.get(5));
            checkBox.setId(Integer.parseInt(mesa.get(0)));
            checkBoxes.add(checkBox);
            layout.addView(checkBox);
        }

        builder.setTitle("Generar codigos qr");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArrayList<ArrayList<String>> mesasGenerar = new ArrayList<>();
                for(int i = 0 ; i < checkBoxes.size();i++){
                    if(checkBoxes.get(i).isChecked()){
                        mesasGenerar.add(mesas.get(i));
                    }
                }
                if(mesasGenerar.size()>0){
                    PDFCreator creator = new PDFCreator(v.getContext(),mesasGenerar);
                }
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

}

