package com.example.carlos.comandual;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ComandasActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DataBaseManager manager;
    private ArrayList<ComandaClass> todasComandas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comandas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Comandas");

        //Mostrar el boton de back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Menu menu = navigationView.getMenu();
        MenuItem nav = menu.findItem(R.id.nav_comandas);
        nav.setChecked(true);

        // get menu from navigationView
        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView.setNavigationItemSelectedListener(this);


        //productos = manager.obtenerComandasEnCola();

        //ArrayList<ComandaClass> todasComandas = manager.obtenerObjetoComandas(String.valueOf(idMesa));
        todasComandas = manager.obtenerComandasEnCola();
        final ArrayList<ComandaClass> comandas = new ArrayList<>();
        for(int i = 0; i < todasComandas.size();i++){
            ComandaClass comandaClass = todasComandas.get(i);
            int pos = comandaClass.getIndexContains(comandas);
            if(pos!=-1){
                int cantidad = comandas.get(pos).getCantidad();
                comandas.get(pos).setCantidad(cantidad+1);
            }else{
                comandas.add(comandaClass);
            }
        }

        //0: id
        //1: id mesa
        //2: id producto
        //3: id categoria
        //4: estado
        //5: cantidad
        //6: comentarios

        ListView listView = (ListView) findViewById(R.id.listViewEstadoMesa);
        EstadoAdapter estadoAdapter = new EstadoAdapter(this,comandas,"general");
        listView.setAdapter(estadoAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> comanda = manager.obtenerComandabyId(comandas.get(position).getIdComanda());
                Intent intent = new Intent(getApplicationContext(),DetalleComandaActivity.class);
                intent.putExtra("producto",comanda);
                intent.putExtra("estado","SIRVIENDO");
                intent.putExtra("activity","comandas");
                ArrayList<String> ids = new ArrayList<>();
                for(int i = 0; i < todasComandas.size();i++){
                    ComandaClass comandaClass = todasComandas.get(i);
                    String nombreProducto = manager.obtenerProducto(Integer.parseInt(comanda.get(2))).getNombre();
                    if(comandaClass.getComentario().equals(comanda.get(6))&& comandaClass.getEstado().equals(comanda.get(4)) && comandaClass.getProducto().equals(nombreProducto) && comandaClass.getIdMesa().equals(comanda.get(1))){
                        ids.add(comandaClass.getIdComanda());
                    }
                }
                intent.putExtra("ids",ids);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            case R.id.action_call:
                //
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
