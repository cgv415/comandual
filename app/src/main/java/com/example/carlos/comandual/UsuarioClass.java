package com.example.carlos.comandual;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 04/12/2016
 */

public class UsuarioClass {
    private String nombre;
    private String apellido;
    private String nick;
    private String correo;
    private String password;
    private String restaurante;

    public UsuarioClass(String nombre, String apellido, String nick, String correo, String password, String restaurante) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nick = nick;
        this.correo = correo;
        this.password = password;
        this.restaurante = restaurante;
    }

    public UsuarioClass() {
        setNick("vacio");
        setNombre("vacio");
        setCorreo("vacio");
        setPassword("vacio");
        setApellido("vacio");
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRestaurante() { return restaurante; }

    public void setRestaurante(String restaurante) { this.restaurante = restaurante; }
}
