package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class DetalleProductoFragment extends DialogFragment {
    private int idProducto;
    private int idMesa;
    private DataBaseManager manager;
    private NumberPicker cantidad;
    private String producto;
    private Spinner spinner;

    public DetalleProductoFragment() {
        // Required empty public constructor
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_detalle_producto, null);

        manager = new DataBaseManager(v.getContext());
        producto = manager.obtenerProducto(idProducto).getNombre();

        //TODO crear metodo obtenerproducto_tipos(int id);
        ArrayList<String> tiposProducto = manager.obtenerProducto_Tipos(idProducto+"");
        ArrayList<String> tipo = new ArrayList<>();

        cantidad = (NumberPicker) v.findViewById(R.id.number_cantidad);
        cantidad.setMinValue(0);
        cantidad.setMaxValue(50);

        spinner = (Spinner) v.findViewById(R.id.spinner_detalle_producto);
        for(int i = 0; i < tiposProducto.size();i++){
            tipo.add(manager.obtenerTipo(tiposProducto.get(i)).get(1));
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,tipo);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaptador);

        builder.setTitle(producto);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                Log.i("Mesa",idMesa+"");
                Log.i("Producto",producto);
                Spinner spinner = (Spinner) v.findViewById(R.id.spinner_detalle_producto);
                String tipo = spinner.getSelectedItem().toString();
                Log.i("Tipo", tipo);
                int cantidad = ((NumberPicker) v.findViewById(R.id.number_cantidad)).getValue();

                //TODO
                TextView textView = (TextView) getActivity().findViewById(R.id.tvCantidad);
                textView.setText(cantidad+"");

            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idProducto = getArguments().getInt("idProducto");
            idMesa = getArguments().getInt("idMesa");
        }
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id Parameter 1.
     * @return A new instance of fragment DetalleProductoFragment.
     */

    /*public static DetalleProductoFragment newInstance(int id) {
        DetalleProductoFragment fragment = new DetalleProductoFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt("id");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detalle_producto, container, false);

        manager = new DataBaseManager(v.getContext());
        ArrayList<String> tiposProducto = manager.obtenerProducto_Tipos(id+"");
        ArrayList<String> tipo = new ArrayList<>();

        TextView textView = (TextView) v.findViewById(R.id.nombreProducto);
        textView.setText(manager.obtenerProducto(id).get(1));

        spinner = (Spinner) v.findViewById(R.id.spinner_detalle_producto);
        for(int i = 0; i < tiposProducto.size();i++){
            tipo.add(manager.obtenerTipo(tiposProducto.get(i)).get(1));
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,tipo);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaptador);

        getDialog().setTitle("Hello");

        return v;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
    */
}
