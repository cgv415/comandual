package com.example.carlos.comandual;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdministrarPerfilActicity extends AppCompatActivity implements View.OnClickListener {

    EditText nombreCompleto;
    EditText apellidos;
    EditText codigoPostal;
    AutoCompleteTextView localidad;
    EditText antiguaPassword;
    EditText password;
    EditText confirmarPassword;
    EditText horario;
    EditText telefono;
    EditText direccion;
    AutoCompleteTextView provincia;

    Button cancelar;
    Button registrar;
    public static final String SEED = "Your_seed_....";
    DataBaseManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrar_perfil_acticity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String[] provincias = {"Alava","Albacete","Alicante","Almería","Asturias","Avila","Badajoz","Barcelona","Burgos","Cáceres",
                "Cádiz","Cantabria","Castellón","Ciudad Real","Córdoba","La Coruña","Cuenca","Gerona","Granada","Guadalajara",
                "Guipúzcoa","Huelva","Huesca","Islas Baleares","Jaén","León","Lérida","Lugo","Madrid","Málaga","Murcia","Navarra",
                "Orense","Palencia","Las Palmas","Pontevedra","La Rioja","Salamanca","Segovia","Sevilla","Soria","Tarragona",
                "Santa Cruz de Tenerife","Teruel","Toledo","Valencia","Valladolid","Vizcaya","Zamora","Zaragoza"};

        manager = new DataBaseManager(this);

        nombreCompleto = (EditText) findViewById(R.id.et_nombreCompleto);
        apellidos = (EditText) findViewById(R.id.et_apellidos);
        codigoPostal = (EditText) findViewById(R.id.et_codigoPostal);
        horario = (EditText) findViewById(R.id.et_horario);
        telefono = (EditText) findViewById(R.id.et_telefono);
        direccion = (EditText) findViewById(R.id.et_direccion);

        provincia = (AutoCompleteTextView) findViewById(R.id.et_provincia);
        ArrayAdapter<String> adapterProvincia = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, provincias);
        provincia.setAdapter(adapterProvincia);
        provincia.setThreshold(1);

        localidad = (AutoCompleteTextView) findViewById(R.id.et_localidad);
        ArrayAdapter<String> adapterLocalidad = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, Localidades.getLocalidades());
        localidad.setThreshold(1);
        localidad.setAdapter(adapterLocalidad);

        if(DataBaseManager.tipoUsuario.equals("empleado") && !DataBaseManager.isAdmin()){
            codigoPostal.setVisibility(View.GONE);
            provincia.setVisibility(View.GONE);
            localidad.setVisibility(View.GONE);
            horario.setVisibility(View.GONE);
            telefono.setVisibility(View.GONE);
            direccion.setVisibility(View.GONE);
        }else if(DataBaseManager.tipoUsuario.equals("cliente")){
            horario.setVisibility(View.GONE);
            telefono.setVisibility(View.GONE);
            direccion.setVisibility(View.GONE);
        }

        antiguaPassword = (EditText) findViewById(R.id.et_antiguaPassword);

        password = (EditText) findViewById(R.id.et_password);

        confirmarPassword = (EditText) findViewById(R.id.et_confirmarPassword);

        cancelar = (Button) findViewById(R.id.btn_cancelar);
        cancelar.setOnClickListener(this);
        registrar = (Button) findViewById(R.id.btn_registrar);
        registrar.setOnClickListener(this);

        apellidos.setText("");

        if(DataBaseManager.tipoUsuario.equals("cliente")){
            obtenerDatosCliente(DataBaseManager.idUsuario);
        }else if(!DataBaseManager.isAdmin()){
            obtenerDatosEmpleado(DataBaseManager.idUsuario);
        }else{
            obtenerDatosRestaurante(DataBaseManager.idUsuario);
        }
    }

    public void obtenerDatosCliente(String id){
        String url = DataBaseManager.ip+"clientes/"+id+"/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    //JSONArray array = new JSONArray(response);
                    JSONObject object  = new JSONObject(response);
                    localidad.setText(object.getString("localidad"));
                    provincia.setText(object.getString("provincia"));
                    codigoPostal.setText(object.getString("codigo"));
                    nombreCompleto.setText(object.getString("nombre_completo"));
                    apellidos.setText(object.getString("apellidos"));


                }catch (Exception e){
                    String err = e.toString();
                    manager.generarMensaje(err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                manager.generarMensaje(err);
            }
        });
        mRequestQueue.add(request);
    }

    public void obtenerDatosRestaurante(String id){
        String url = DataBaseManager.ip+"clientes/"+id+"/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    //JSONArray array = new JSONArray(response);
                    JSONObject object  = new JSONObject(response);
                    localidad.setText(object.getString("localidad"));
                    provincia.setText(object.getString("provincia"));
                    codigoPostal.setText(object.getString("codigo"));
                    direccion.setText(object.getString("direccion"));
                    horario.setText(object.getString("horario"));
                    telefono.setText(object.getString("telefono"));
                    //btenerDatosUsuario(object.getString("idUsuario"));


                }catch (Exception e){
                    String err = e.toString();
                    manager.generarMensaje(err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                manager.generarMensaje(err);
            }
        });
        mRequestQueue.add(request);
    }

    public void obtenerDatosEmpleado(String id){
        String url = DataBaseManager.ip+"empleados/"+id+"/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    //JSONArray array = new JSONArray(response);
                    JSONObject object  = new JSONObject(response);
                    nombreCompleto.setText(object.getString("nombre_completo"));
                    apellidos.setText(object.getString("apellidos"));


                }catch (Exception e){
                    String err = e.toString();
                    manager.generarMensaje(err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                manager.generarMensaje(err);
            }
        });
        mRequestQueue.add(request);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_registrar:
                if(DataBaseManager.tipoUsuario.equals("cliente")) {
                    actualizarCliente();
                }else if(!DataBaseManager.isAdmin()){
                    actualizarEmpleado();
                }
            break;
            case R.id.btn_cancelar:
                finish();
        }
    }

    public void actualizarCliente(){
        final String pass = password.getText().toString();
        final RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"clientes/"+DataBaseManager.idUsuario + "/";
        final StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);
                        try {
                            if(manager.isTableExists("cliente")){
                                manager.modificarClientePassword(pass);
                            }
                            generarMensaje("Perfil modificado con éxito");
                            finish();
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                if(!codigoPostal.getText().toString().equals("")){
                    params.put("codigo",codigoPostal.getText().toString());
                }
                if(!localidad.getText().toString().equals("")){
                    params.put("localidad",localidad.getText().toString());
                }
                if(!provincia.getText().toString().equals("")){
                    params.put("provincia",provincia.getText().toString());
                }
                if(!nombreCompleto.getText().toString().equals("")){
                    params.put("nombre_completo",nombreCompleto.getText().toString());
                }
                if(!apellidos.getText().toString().equals("")){
                    params.put("apellidos",apellidos.getText().toString());
                }

                ClienteClass cliente = manager.obtenerCliente();
                boolean actualizar = true;
                if(!password.getText().toString().equals("")){
                    if(!(password.getText().toString().length()<4)){
                        if(antiguaPassword.getText().toString().equals(cliente.getPassword())){
                            if(password.getText().toString().equals(confirmarPassword.getText().toString())){
                                try {
                                    String code = Encrypt.encrypt(SEED,pass);
                                    params.put("password",code);
                                }catch (Exception e){
                                    e.printStackTrace();
                                    actualizar = false;
                                }
                            }else{
                                generarMensaje("Contraseñas no coinciden");
                                actualizar = false;
                            }
                        }else{
                            generarMensaje("Contraseña antigua no coincide");
                            actualizar = false;
                        }
                    }else{
                        generarMensaje("Contraseña demasiado corta (min 4)");
                        actualizar = false;
                    }
                }
                if(!actualizar){
                    mRequestQueue.cancelAll("put");
                }
                return params;
            }

        };
        putRequest.setTag("put");
        mRequestQueue.add(putRequest);
    }

    public void generarMensaje(final String menssage){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),menssage,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void actualizarEmpleado(){
        final String pass = password.getText().toString();
        final RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"empleados/"+DataBaseManager.idUsuario + "/";
        final StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        if(manager.isTableExists("sesion")){
                            manager.modificarSesionPassword(pass);
                        }
                        Toast.makeText(getApplicationContext(),"Perfil modificado con exito",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                //EmpleadoClass empleado = manager.comprobrarEmpleado(DataBaseManager.idUsuario);
                ArrayList<String> sesion = manager.obtenerSesion();
                boolean actualizar = true;
                if(!password.getText().toString().equals("")){
                    if(!(password.getText().toString().length()<4)){
                        if(antiguaPassword.getText().toString().equals(sesion.get(2))){
                            if(password.getText().toString().equals(confirmarPassword.getText().toString())){
                                try {
                                    String code = Encrypt.encrypt(SEED,pass);
                                    params.put("password",code);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }else{
                                manager.generarMensaje("Contraseñas no coinciden");
                                actualizar = false;
                            }
                        }else{
                            manager.generarMensaje("Contraseña antigua no coincide");
                            actualizar = false;
                        }
                    }else{
                        manager.generarMensaje("Contraseña demasiado corta (min 4)");
                        actualizar = false;
                    }
                }
                params.put("nombre",sesion.get(1));
                params.put("nombre_completo",nombreCompleto.getText().toString());
                params.put("apellidos",apellidos.getText().toString());
                if(!actualizar){
                    mRequestQueue.cancelAll("put");
                }
                return params;


            }

        };
        putRequest.setTag("put");
        mRequestQueue.add(putRequest);
    }


}