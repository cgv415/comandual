package com.example.carlos.comandual;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NuevoEspacioFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NuevoEspacioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NuevoEspacioFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private DataBaseManager manager;
    private TextView espacio;

    private OnFragmentInteractionListener mListener;

    public NuevoEspacioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NuevoEspacioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NuevoEspacioFragment newInstance(String param1, String param2) {
        NuevoEspacioFragment fragment = new NuevoEspacioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_nuevo_espacio, container, false);

        manager = new DataBaseManager(v.getContext());

        espacio = (TextView) v.findViewById(R.id.etEspacio);

        Button btn_crear_espacio = (Button) v.findViewById(R.id.btn_crear_espacio);
        btn_crear_espacio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String e = espacio.getText().toString();
                if(e.equals("")){
                    Toast.makeText(v.getContext(),"Debe introducir un nombre", Toast.LENGTH_SHORT).show();//Remove
                } else if(manager.isEspacioExist(e)){//Si el espacio ya existe avisamos al tipoUsuario
                    Toast.makeText(v.getContext(),"El espacio ya existe", Toast.LENGTH_SHORT).show();//Remove
                }else{//Si no existe
                    if(insertarEspacioApi(DataBaseManager.removeAcentos(e))){//Intentamos introducirlo en la base de datos
                        Toast.makeText(v.getContext(), e+" insertado con exito", Toast.LENGTH_SHORT).show();//Remove
                    }else{//Si no, se muestra un mensaje de error
                        Toast.makeText(v.getContext(), "Error al insertar " + e, Toast.LENGTH_SHORT).show();//Remove
                    }
                }
            }
        });

        return v;
    }
    public void reiniciarFragment(){
        //Volvemos a la actividad de introducir mesa
        FragmentTransaction trans = getFragmentManager().beginTransaction();
        trans.replace(R.id.content_nueva_mesa, new NuevaMesaFragment(),"Mesa");
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack("Mesa");
        trans.commit();
    }

    boolean insertarEspacioApi(final String nombre){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"espacios/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);

                            String id = object.getString("id");
                            //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                            manager.insertarEspacio(id,nombre);
                            reiniciarFragment();

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("user",DataBaseManager.restaurante);
                params.put("nombre",nombre);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
        return true;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
