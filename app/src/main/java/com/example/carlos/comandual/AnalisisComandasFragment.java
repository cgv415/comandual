package com.example.carlos.comandual;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AnalisisComandasFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AnalisisComandasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnalisisComandasFragment extends Fragment implements View.OnClickListener{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_FECHAIN = "fechaIn";
    private static final String ARG_FECHAFIN = "param2";
    private static final String ARG_HORAIN = "param3";
    private static final String ARG_HORAFIN = "param4";
    private DataBaseManager manager;
    private ArrayList<ComandaClass> comandas;

    private String fechaIn;
    private String fechaFin;
    private String horaIn;
    private String horaFin;

    private TextView etComandasGeneradas;
    private TextView tv_mesaMasComandas;
    private TextView tv_fechaMasComandas;
    private TextView tv_fechaMenosComandas;
    private TextView tv_productoMasComandas;
    private TextView tv_productoMenosComandas;
    private TextView tv_mesaMenosComandas;

    private ListView listView;
    private ToggleButton tb_tipo;

    private Map<String,Integer> mesas;
    private Map<String,Integer> fechas;
    private Map<String,Integer> productos;

    private OnFragmentInteractionListener mListener;

    public AnalisisComandasFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param fechaIn Parameter 1.
     * @param fechaFin Parameter 2.
     * @param horaIn Parameter 3.
     * @param horaFin Parameter 4.
     * @return A new instance of fragment AnalisisComandasFragment.
     */
    public static AnalisisComandasFragment newInstance(String fechaIn, String fechaFin, String horaIn, String horaFin) {
        AnalisisComandasFragment fragment = new AnalisisComandasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FECHAIN, fechaIn);
        args.putString(ARG_FECHAFIN, fechaFin);
        args.putString(ARG_HORAIN, horaIn);
        args.putString(ARG_HORAFIN, horaFin);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fechaIn = getArguments().getString(ARG_FECHAIN);
            fechaFin = getArguments().getString(ARG_FECHAFIN);
            horaIn = getArguments().getString(ARG_HORAIN);
            horaFin = getArguments().getString(ARG_HORAFIN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mesas = new TreeMap<>();
        fechas = new TreeMap<>();
        productos = new TreeMap<>();
        View v = inflater.inflate(R.layout.fragment_analisis_comandas, container, false);
        manager = new DataBaseManager(v.getContext());
        onCreate(savedInstanceState);
        etComandasGeneradas = (TextView) v.findViewById(R.id.tv_comandasGeneradas);
        tv_mesaMasComandas = (TextView) v.findViewById(R.id.tv_masmesaComanda);
        tv_mesaMenosComandas = (TextView) v.findViewById(R.id.tv_mesaMenosComandas);
        tv_fechaMasComandas = (TextView) v.findViewById(R.id.tv_fechaMasComandas);
        tv_fechaMenosComandas = (TextView) v.findViewById(R.id.tv_fechaMenosComandas);
        tv_productoMasComandas = (TextView) v.findViewById(R.id.tv_productoMasComandas);
        tv_productoMenosComandas = (TextView) v.findViewById(R.id.tv_productoMenosComadas);

        ImageButton btnMesas = (ImageButton) v.findViewById(R.id.btn_mesa);
        btnMesas.setOnClickListener(this);
        ImageButton btnFechas = (ImageButton) v.findViewById(R.id.btn_fecha);
        btnFechas.setOnClickListener(this);
        ImageButton btnProductos = (ImageButton) v.findViewById(R.id.btn_producto);
        btnProductos.setOnClickListener(this);

        obtenerTotalComandas();

        return v;
    }

    public void popUpGrafica(final Context context, final String nombre){
        //true: ascendente
        //false: descendente

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_graph, null);
        final GraphView graph = (GraphView) v.findViewById(R.id.graph);

        tb_tipo = (ToggleButton) v.findViewById(R.id.toggleButton_tipo);
        tb_tipo.setChecked(true);
        tb_tipo.setTextOff("Descendente");
        tb_tipo.setTextOn("Ascendente");

        final Spinner spinnerFiltro = (Spinner) v.findViewById(R.id.spinner_filtro);
        String[] filtro = new String[]{"Tres elementos","Cinco elementos","Diez elementos","Quince elementos","Todos los elementos"};
        spinnerFiltro.setAdapter(new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,filtro));
        spinnerFiltro.setSelection(1);

        switch (nombre) {
            case "Mesas": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerGraficaMesas(position,graph,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerGraficaMesas(spinnerFiltro.getSelectedItemPosition(),graph,true);
                        }else{
                            obtenerGraficaMesas(spinnerFiltro.getSelectedItemPosition(),graph,false);
                        }
                    }
                });
                break;
            }
            case "Fechas": {
                obtenerGraficaFechas(graph);
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);
                break;
            }
            case "Productos": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerGraficaProductos(position,graph,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerGraficaProductos(spinnerFiltro.getSelectedItemPosition(),graph,true);
                        }else{
                            obtenerGraficaProductos(spinnerFiltro.getSelectedItemPosition(),graph,false);
                        }
                    }
                });
                break;
            }
        }

        builder.setTitle(nombre);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


            }
        }).setNeutralButton("Ver lista", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                popUpLista(context,nombre);
            }
        });
        builder.show();
    }

    public void popUpLista(final Context context,final String nombre){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_list, null);


        final ToggleButton tb_tipo = (ToggleButton) v.findViewById(R.id.toggleButton_tipo);
        tb_tipo.setChecked(true);
        final Spinner spinnerFiltro = (Spinner) v.findViewById(R.id.spinner_filtro);
        String[] filtro = new String[]{"Tres elementos","Cinco elementos","Diez elementos","Quince elementos","Todos los elementos"};
        spinnerFiltro.setAdapter(new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,filtro));
        spinnerFiltro.setSelection(1);

        listView = (ListView)v.findViewById(R.id.lv_analisisProductos);

        switch (nombre) {
            case "Mesas": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerListaMesas(position,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerListaMesas(spinnerFiltro.getSelectedItemPosition(),true);
                        }else{
                            obtenerListaMesas(spinnerFiltro.getSelectedItemPosition(),false);
                        }
                    }
                });
                break;
            }
            case "Fechas": {
                obtenerListaFechas();
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);
                break;
            }
            case "Productos": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerListaProductos(position,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerListaProductos(spinnerFiltro.getSelectedItemPosition(),true);
                        }else{
                            obtenerListaProductos(spinnerFiltro.getSelectedItemPosition(),false);
                        }
                    }
                });
                break;
            }
        }

        builder.setTitle(nombre);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


            }
        }).setNeutralButton("Ver gráfica", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                popUpGrafica(context,nombre);
            }
        });
        builder.show();
    }

    public void obtenerTotalComandas(){
        comandas = new ArrayList<>();
        String get_comandas = DataBaseManager.ip+"comandas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        if(comandas.size()==0){
            request = new StringRequest(Request.Method.GET, get_comandas, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONArray array = new JSONArray(response);
                        JSONObject object;
                        for(int i = 0 ; i < array.length();i++){
                            object = array.getJSONObject(i);

                            String rest = object.getString("user");
                            if(rest.equals(DataBaseManager.restaurante)) {
                                String id = object.getString("id");
                                String mesa = object.getString("mesa");
                                String producto = object.getString("producto");
                                String categoria = object.getString("categoria");
                                String estado = object.getString("estado");
                                String cantidad = object.getString("cantidad");
                                String comentario = object.getString("comentario");
                                String token = object.getString("token");

                                boolean insert = true;

                                String hora = object.getString("hora");

                                //Hora de la comanda
                                StringTokenizer tokenizerHoraApi = new StringTokenizer(hora,":");
                                Integer horaC = Integer.parseInt(tokenizerHoraApi.nextToken());
                                Integer minutoC = Integer.parseInt(tokenizerHoraApi.nextToken());

                                //Hora de inicio en el filtro
                                StringTokenizer tokenizerHoraIn = new StringTokenizer(horaIn,":");
                                Integer horaIn = Integer.parseInt(tokenizerHoraIn.nextToken());
                                Integer minutoIn = Integer.parseInt(tokenizerHoraIn.nextToken());

                                //Hora de fin en el filtro
                                StringTokenizer tokenizerHoraFin = new StringTokenizer(horaFin,":");
                                Integer horaFin = Integer.parseInt(tokenizerHoraFin.nextToken());
                                Integer minutoFin = Integer.parseInt(tokenizerHoraFin.nextToken());

                                if(horaIn!=0||minutoIn!=0){
                                    if(horaIn>horaC){
                                        insert = false;
                                    }else if(horaIn.equals(horaC)){
                                        if(minutoIn>minutoC){
                                            insert = false;
                                        }
                                    }
                                }
                                if(horaFin!=0||minutoIn!=0){
                                    if(horaFin<horaC){
                                        insert = false;
                                    } else if(horaFin.equals(horaC)){
                                        if(minutoFin<minutoC){
                                            insert = false;
                                        }
                                    }
                                }

                                String fecha = object.getString("fecha");

                                //Fecha de la comanda
                                StringTokenizer tokenizerFechaApi = new StringTokenizer(fecha,"-");
                                Integer diaApi = Integer.parseInt(tokenizerFechaApi.nextToken());
                                Integer mesApi = Integer.parseInt(tokenizerFechaApi.nextToken());
                                Integer añoApi = Integer.parseInt(tokenizerFechaApi.nextToken());

                                //Fecha de inicio
                                StringTokenizer tokenizerFechaIn = new StringTokenizer(fechaIn,"-");
                                Integer diaIn = Integer.parseInt(tokenizerFechaIn.nextToken());
                                Integer mesIn = Integer.parseInt(tokenizerFechaIn.nextToken());
                                Integer añoIn = Integer.parseInt(tokenizerFechaIn.nextToken());

                                //Fecha de fin
                                StringTokenizer tokenizerFechaFin = new StringTokenizer(fechaFin,"-");
                                Integer diaFin = Integer.parseInt(tokenizerFechaFin.nextToken());
                                Integer mesFin = Integer.parseInt(tokenizerFechaFin.nextToken());
                                Integer añoFin = Integer.parseInt(tokenizerFechaFin.nextToken());

                                if(añoIn>añoApi||añoApi>añoFin){
                                    insert = false;
                                }else if(añoIn.equals(añoApi)||añoFin.equals(añoApi)){
                                        if(añoIn.equals(añoApi)&&mesIn>mesApi){
                                            insert = false;
                                        }else if(añoIn.equals(añoApi)&&mesApi>mesFin){
                                            insert = false;
                                        }else if(mesIn.equals(mesApi)||mesApi.equals(mesFin)){
                                            if(mesIn.equals(mesApi)&&diaIn<diaApi){
                                                insert = false;
                                            }else if(mesIn.equals(mesApi)&&diaApi>diaFin){
                                                insert = false;
                                            }
                                        }
                                }
                                if(insert){
                                    ComandaClass comanda = new ComandaClass(id,mesa,producto,comentario,estado,categoria,Integer.parseInt(cantidad),fecha,hora,token);
                                    comandas.add(comanda);

                                    insertarFecha(fecha);
                                    insertarMesa(comanda.getIdMesa());
                                    insertarProducto(comanda.getProducto());
                                }
                            }
                        }
                        etComandasGeneradas.setText(String.valueOf(comandas.size()));
                        String nombreMesa = obtenerMesa(true);
                        tv_mesaMasComandas.setText(nombreMesa);
                        nombreMesa = obtenerMesa(false);
                        tv_mesaMenosComandas.setText(nombreMesa);
                        String fecha = obtenerFecha(true);
                        tv_fechaMasComandas.setText(fecha);
                        fecha = obtenerFecha(false);
                        tv_fechaMenosComandas.setText(fecha);
                        String producto = obtenerProducto(true);
                        tv_productoMasComandas.setText(producto);
                        producto = obtenerProducto(false);
                        tv_productoMenosComandas.setText(producto);

                    }catch (Exception e){
                        String err = e.toString();
                        Log.e("Error",err);
                    }
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                    String err = error.toString();
                    Log.e("Error",err);
                }
            });
            //request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequestQueue.add(request);
        }
    }

    public void obtenerListaFechas(){
        StringTokenizer fecha = new StringTokenizer(fechaIn, "-");
        fecha.nextToken();
        int mesIn = Integer.parseInt(fecha.nextToken());
        Map<String, Integer> mapaMeses = new TreeMap<>();

        fecha = new StringTokenizer(fechaFin, "-");
        fecha.nextToken();
        int mesFin = Integer.parseInt(fecha.nextToken());

        int cantMeses;
        if (mesIn < mesFin) {
            cantMeses = mesFin - mesIn + 1;
        } else {
            cantMeses = mesIn - mesFin;
        }
        ArrayList<Integer> ventas = new ArrayList<>();
        ArrayList<String> meses = new ArrayList<>();
        for (int j = 0; j < cantMeses; j++) {
            mapaMeses.put(String.valueOf(mesIn), 0);
            switch (mesIn) {
                case 1:
                    meses.add("ene");
                    break;
                case 2:
                    meses.add("feb");
                    break;
                case 3:
                    meses.add("mar");
                    break;
                case 4:
                    meses.add("abr");
                    break;
                case 5:
                    meses.add("may");
                    break;
                case 6:
                    meses.add("jun");
                    break;
                case 7:
                    meses.add("jul");
                    break;
                case 8:
                    meses.add("ago");
                    break;
                case 9:
                    meses.add("sep");
                    break;
                case 10:
                    meses.add("oct");
                    break;
                case 11:
                    meses.add("nov");
                    break;
                case 12:
                    meses.add("dic");
                    break;
            }
            mesIn++;
            if (mesIn == 13) {
                mesIn = 1;
            }
        }
        Set<Map.Entry<String,Integer>> set = fechas.entrySet();
        Iterator<Map.Entry<String,Integer>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            StringTokenizer tokenizerFecha = new StringTokenizer(entry.getKey(), "-");
            tokenizerFecha.nextToken();
            String tokenMes = tokenizerFecha.nextToken();
            if (mapaMeses.containsKey(tokenMes)) {
                int cant = mapaMeses.get(tokenMes);
                cant += entry.getValue();
                mapaMeses.put(tokenMes, cant);
            }
        }
        set = mapaMeses.entrySet();
        iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            ventas.add(entry.getValue());
        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),meses,ventas);
        listView.setAdapter(mesasAdapter);
    }

    public void obtenerGraficaFechas(GraphView graph){
        graph.removeAllSeries();
        StringTokenizer fecha = new StringTokenizer(fechaIn, "-");
        fecha.nextToken();
        int mesIn = Integer.parseInt(fecha.nextToken());
        Map<String, Integer> mapaMeses = new TreeMap<>();

        fecha = new StringTokenizer(fechaFin, "-");
        fecha.nextToken();
        int mesFin = Integer.parseInt(fecha.nextToken());

        int cantMeses;
        if (mesIn < mesFin) {
            cantMeses = mesFin - mesIn + 1;
        } else {
            cantMeses = mesIn - mesFin;
        }
        String[] horizontal = new String[cantMeses + 1];
        int[] ventas = new int[horizontal.length];
        for (int j = 0; j < cantMeses; j++) {
            mapaMeses.put(String.valueOf(mesIn), 0);
            switch (mesIn) {
                case 1:
                    horizontal[j] = "ene";
                    break;
                case 2:
                    horizontal[j] = "feb";
                    break;
                case 3:
                    horizontal[j] = "mar";
                    break;
                case 4:
                    horizontal[j] = "abr";
                    break;
                case 5:
                    horizontal[j] = "may";
                    break;
                case 6:
                    horizontal[j] = "jun";
                    break;
                case 7:
                    horizontal[j] = "jul";
                    break;
                case 8:
                    horizontal[j] = "ago";
                    break;
                case 9:
                    horizontal[j] = "sep";
                    break;
                case 10:
                    horizontal[j] = "oct";
                    break;
                case 11:
                    horizontal[j] = "nov";
                    break;
                case 12:
                    horizontal[j] = "dic";
                    break;
            }
            mesIn++;
            if (mesIn == 13) {
                mesIn = 1;
            }
        }
        Set<Map.Entry<String, Integer>> set = fechas.entrySet();
        Iterator<Map.Entry<String, Integer>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            StringTokenizer tokenizerFecha = new StringTokenizer(entry.getKey(), "-");
            tokenizerFecha.nextToken();
            String tokenMes = tokenizerFecha.nextToken();
            if (mapaMeses.containsKey(tokenMes)) {
                int cant = mapaMeses.get(tokenMes);
                cant += entry.getValue();
                mapaMeses.put(tokenMes, cant);
            }
        }
        set = mapaMeses.entrySet();
        iterator = set.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            ventas[i] = entry.getValue();
            i++;
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);
    }

    public void obtenerListaMesas(int numElement, boolean orden){
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = mesas.size();
                break;
        }
        ArrayList<String> listMesas = new ArrayList<>();
        ArrayList<Integer> listVentas = new ArrayList<>();
        Map<String, Integer> auxMesas = new TreeMap<>();
        auxMesas.putAll(mesas);
        Set<Map.Entry<String, Integer>> setEntry = auxMesas.entrySet();
        for (int i = 0; i < numElement; i++) {
            String mes = "";
            String idMesa = "";
            int cantMesa;

            if(orden){
                cantMesa = 0;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() > cantMesa) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        cantMesa = entry.getValue();
                    }
                }
            }else{
                cantMesa = -1;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() < cantMesa || cantMesa== -1) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        cantMesa = entry.getValue();
                    }
                }
            }
            if(!idMesa.equals("")){
                auxMesas.remove(idMesa);
                listMesas.add(mes);
                listVentas.add(cantMesa);
            }

        }
        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),listMesas,listVentas);
        listView.setAdapter(mesasAdapter);
    }

    public void obtenerGraficaMesas(int numElement,GraphView graph, boolean orden){
        graph.removeAllSeries();
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = mesas.size();
                break;
        }

        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Integer> arrVentas = new ArrayList<>();
        Map<String, Integer> auxMesas = new TreeMap<>();
        auxMesas.putAll(mesas);
        Set<Map.Entry<String, Integer>> setEntry = auxMesas.entrySet();
        for (int i = 0; i < numElement; i++) {
            String mes = "";
            String idMesa = "";
            int cantMesa;

            if(orden){
                cantMesa = 0;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() > cantMesa) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        cantMesa = entry.getValue();
                    }
                }
            }else{
                cantMesa = -1;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() < cantMesa || cantMesa== -1) {
                        idMesa = entry.getKey();
                        mes = manager.obtenerMesa(Integer.parseInt(entry.getKey())).get(5);
                        cantMesa = entry.getValue();
                    }
                }
            }
            if(!idMesa.equals("")){
                auxMesas.remove(idMesa);
                arrHorizontal.add(mes);
                arrVentas.add(cantMesa);
            }


        }
        String[] horizontal;
        int[] ventas;
        if(arrHorizontal.size()<2){
            horizontal = new String[2];
        }else{
            horizontal = new String[arrHorizontal.size()];
        }
        for(int i = 0 ; i < arrHorizontal.size(); i++){
            horizontal[i] = arrHorizontal.get(i);
        }
        if(arrVentas.size()<2){
            ventas = new int[2];
        }else{
            ventas = new int[arrVentas.size()];
        }
        for(int i = 0 ; i < arrVentas.size(); i++){
            ventas[i] = arrVentas.get(i);
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        //staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);

    }

    public void obtenerGraficaProductos(int numElement,GraphView graph, boolean orden){
        graph.removeAllSeries();
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = productos.size();
                break;
        }

        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Integer> arrVentas = new ArrayList<>();
        Map<String, Integer> auxProductos = new TreeMap<>();
        auxProductos.putAll(productos);
        Set<Map.Entry<String, Integer>> setEntry = auxProductos.entrySet();
        for (int i = 0; i < numElement; i++) {
            String pro = "";
            String idPro = "";
            int cantPro;

            if(orden){
                cantPro = 0;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() > cantPro) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }else{
                cantPro = -1;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() < cantPro || cantPro== -1) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }
            if(!idPro.equals("")){
                auxProductos.remove(idPro);
                arrHorizontal.add(pro);
                arrVentas.add(cantPro);
            }


        }
        String[] horizontal;
        int[] ventas;
        if(arrHorizontal.size()<2){
            horizontal = new String[2];
        }else{
            horizontal = new String[arrHorizontal.size()];
        }
        for(int i = 0 ; i < arrHorizontal.size(); i++){
            horizontal[i] = arrHorizontal.get(i);
        }
        if(arrVentas.size()<2){
            ventas = new int[2];
        }else{
            ventas = new int[arrVentas.size()];
        }
        for(int i = 0 ; i < arrVentas.size(); i++){
            ventas[i] = arrVentas.get(i);
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        //staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);

    }

    public void obtenerListaProductos(int numElement, boolean orden){
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = productos.size();
                break;
        }
        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Integer> arrVentas = new ArrayList<>();
        Map<String, Integer> auxProductos = new TreeMap<>();
        auxProductos.putAll(productos);
        Set<Map.Entry<String, Integer>> setEntry = auxProductos.entrySet();
        for (int i = 0; i < numElement; i++) {
            String pro = "";
            String idPro = "";
            int cantPro;

            if(orden){
                cantPro = 0;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() > cantPro) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }else{
                cantPro = -1;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() < cantPro || cantPro== -1) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }
            if(!idPro.equals("")){
                auxProductos.remove(idPro);
                arrHorizontal.add(pro);
                arrVentas.add(cantPro);
            }


        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),arrHorizontal,arrVentas);
        listView.setAdapter(mesasAdapter);

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void insertarFecha(String fecha){
        if(fechas.containsKey(fecha)){
            int x = fechas.get(fecha);
            fechas.put(fecha,x+1);
        }else{
            fechas.put(fecha,1);
        }
    }

    public void insertarProducto(String producto){
        if(productos.containsKey(producto)){
            int x = productos.get(producto);
            productos.put(producto,x+1);
        }else{
            productos.put(producto,1);
        }
    }

    public String obtenerFecha(boolean mayor){
        String fecha="";
        int cant = -1;
        Set<Map.Entry<String, Integer>> entry= fechas.entrySet();
        for (Map.Entry<String, Integer> ent : entry) {
            if (mayor) {
                if (cant < ent.getValue()) {
                    cant = ent.getValue();
                    fecha = ent.getKey();
                }
            } else {
                if (cant > ent.getValue() || cant == -1) {
                    cant = ent.getValue();
                    fecha = ent.getKey();
                }
            }
        }
        return fecha + " ("+cant+")";
    }

    public String obtenerProducto(boolean mayor){
        String producto="";
        int cant = -1;
        Set<Map.Entry<String, Integer>> entry= productos.entrySet();
        for (Map.Entry<String, Integer> ent : entry) {
            if (mayor) {
                if (cant < ent.getValue()) {
                    cant = ent.getValue();
                    producto = ent.getKey();
                }
            } else {
                if (cant > ent.getValue() || cant == -1) {
                    cant = ent.getValue();
                    producto = ent.getKey();
                }
            }
        }
        producto = manager.obtenerProducto(Integer.parseInt(producto)).getNombre();
        return producto + " ("+cant+")";
    }

    public String obtenerMesa(boolean mayor){
        String mesa = "";
        int cant = -1;
        Set<Map.Entry<String, Integer>> entryMesa= mesas.entrySet();
        for (Map.Entry<String, Integer> entry : entryMesa) {
            if (mayor) {
                if (cant < entry.getValue()) {
                    cant = entry.getValue();
                    mesa = entry.getKey();
                }
            } else {
                if (cant > entry.getValue() || cant == -1) {
                    cant = entry.getValue();
                    mesa = entry.getKey();
                }
            }
        }
        mesa = manager.obtenerMesa(Integer.parseInt(mesa)).get(5);
        return mesa+" (" + cant + ")";
    }

    public void insertarMesa(String idMesa){
        if(mesas.containsKey(idMesa)){
            int x = mesas.get(idMesa);
            mesas.put(idMesa,x+1);
        }else{
            mesas.put(idMesa,1);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_mesa:
                popUpGrafica(v.getContext(),"Mesas");
                break;
            case R.id.btn_fecha:
                popUpGrafica(v.getContext(),"Fechas");
                break;
            case R.id.btn_producto:
                popUpGrafica(v.getContext(),"Productos");
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
