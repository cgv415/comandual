package com.example.carlos.comandual;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class AnalisisActivity extends AppCompatActivity implements View.OnClickListener,
        AnalisisProductosFragment.OnFragmentInteractionListener,
        AnalisisComandasFragment.OnFragmentInteractionListener,
        AnalisisMesasFragment.OnFragmentInteractionListener,
        AnalisisValoracionesFragment.OnFragmentInteractionListener {

    DatePicker datePicker;

    NumberPicker npHora;
    NumberPicker npMinuto;

    ImageButton timeFin;
    ImageButton timeIn;
    ImageButton  dateFin;
    ImageButton dateIn;

    EditText etTimeFin;
    EditText etTimeIn;
    EditText  etDateFin;
    EditText etDateIn;

    String txTimeFin;
    String txTimeIn;
    String txDateFin;
    String txDateIn;
    String categoria;


    int posSpinnerFiltro;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analisis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;

        posSpinnerFiltro = 0;

        Calendar calendarNow =new GregorianCalendar(TimeZone.getTimeZone("Europe/Madrid"));
        int day = calendarNow.get(Calendar.DAY_OF_MONTH);
        int month = calendarNow.get(Calendar.MONTH)+1;
        int year = calendarNow.get(Calendar.YEAR);

        txDateFin = day+"-"+month+"-"+year;
        if(month>6){
            month -=6;
        }else{
            month +=6;
            year -=1;
        }
        txDateIn = day+"-"+month+"-"+year;
        txTimeIn = "00:00";
        txTimeFin = "00:00";

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        AnalisisComandasFragment fragment;
        fragment = AnalisisComandasFragment.newInstance(txDateIn,txDateFin,txTimeIn,txTimeFin);
        transaction.replace(R.id.fragment_analisis,fragment);

        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_analisis, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_filters:
                popUpFiltros();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void popUpFiltros(){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_filtro, null);

        dateIn = (ImageButton) v.findViewById(R.id.btn_date_in);
        dateFin = (ImageButton) v.findViewById(R.id.btn_date_fin);
        timeIn = (ImageButton) v.findViewById(R.id.btn_time_in);
        timeFin = (ImageButton) v.findViewById(R.id.btn_time_fin);

        etDateIn = (EditText) v.findViewById(R.id.et_date_in);
        etDateFin = (EditText) v.findViewById(R.id.et_date_fin);
        etTimeIn = (EditText) v.findViewById(R.id.et_time_in);
        etTimeFin = (EditText) v.findViewById(R.id.et_time_fin);

        etDateIn.setText(txDateIn);
        etTimeIn.setText(txTimeIn);
        etTimeFin.setText(txTimeFin);
        etDateFin.setText(txDateFin);

        dateIn.setOnClickListener(this);
        dateFin.setOnClickListener(this);
        timeIn.setOnClickListener(this);
        timeFin.setOnClickListener(this);

        String[] array = {"Comandas","Mesas","Productos","Valoraciones"};
        final Spinner spinner = (Spinner) v.findViewById(R.id.spinner_analisis);
        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,array);
        spinner.setAdapter(adapter);
        spinner.setSelection(posSpinnerFiltro);

        builder.setTitle("Filtrar búsqueda");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                categoria = spinner.getSelectedItem().toString();
                txDateIn = etDateIn.getText().toString();
                txDateFin = etDateFin.getText().toString();
                txTimeIn = etTimeIn.getText().toString();
                txTimeFin = etTimeFin.getText().toString();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                posSpinnerFiltro = spinner.getSelectedItemPosition();

                switch (categoria){
                    case "Comandas":
                        AnalisisComandasFragment fragmentComandas;
                        fragmentComandas = AnalisisComandasFragment.newInstance(txDateIn,txDateFin,txTimeIn,txTimeFin);
                        transaction.replace(R.id.fragment_analisis,fragmentComandas);
                        transaction.commit();
                        break;
                    case "Mesas":
                        AnalisisMesasFragment fragmentMesas;
                        fragmentMesas = AnalisisMesasFragment.newInstance(txDateIn,txDateFin,txTimeIn,txTimeFin,0);
                        transaction.replace(R.id.fragment_analisis,fragmentMesas);
                        transaction.commit();
                        break;
                    case "Productos":
                        AnalisisProductosFragment fragmentProductos;
                        fragmentProductos = AnalisisProductosFragment.newInstance(txDateIn,txDateFin,txTimeIn,txTimeFin);
                        transaction.replace(R.id.fragment_analisis,fragmentProductos);
                        transaction.commit();
                        break;
                    case "Valoraciones":
                        AnalisisValoracionesFragment fragmentValoraciones;
                        fragmentValoraciones = AnalisisValoracionesFragment.newInstance(txDateIn,txDateFin,txTimeIn,txTimeFin);
                        transaction.replace(R.id.fragment_analisis,fragmentValoraciones);
                        transaction.commit();
                        break;
                }
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                }).setNeutralButton(R.string.predeterminado, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Calendar calendarNow =new GregorianCalendar(TimeZone.getTimeZone("Europe/Madrid"));
                int day = calendarNow.get(Calendar.DAY_OF_MONTH);
                int month = calendarNow.get(Calendar.MONTH)+1;
                int year = calendarNow.get(Calendar.YEAR);
                txDateFin = day+"-"+month+"-"+year;
                if(month>6){
                    month -=6;
                }else{
                    month +=6;
                    year -=1;
                }
                txDateIn = day+"-"+month+"-"+year;
                txTimeIn = "00:00";
                txTimeFin = "00:00";

                etDateIn.setText(txDateIn);
                etDateFin.setText(txDateFin);
                etTimeIn.setText(txTimeIn);
                etTimeFin.setText(txTimeFin);
                popUpFiltros();
            }
        });
        builder.show();
    }

    public void popUpDate(final String pos){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_date, null);
        datePicker = (DatePicker) v.findViewById(R.id.datePicker);
        String posicion;
        if(pos.equals("in")){
            posicion = " desde";
        }else{
            posicion = " hasta";
        }
        builder.setTitle("Seleccionar fecha"+ posicion);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                String date = datePicker.getDayOfMonth()+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getYear();
                if(pos.equals("in")){
                    etDateIn.setText(date);
                }else{
                    etDateFin.setText(date);
                }
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void popUpTime(final String pos){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_time, null);

        npHora = (NumberPicker) v.findViewById(R.id.numberPicker_hora);
        npHora.setMinValue(0);
        npHora.setMaxValue(23);
        npHora.setValue(0);
        npMinuto = (NumberPicker) v.findViewById(R.id.numberPicker_minuto);
        npMinuto.setMinValue(0);
        npMinuto.setMaxValue(59);
        npMinuto.setValue(0);
        String posicion;
        if(pos.equals("in")){
            posicion = " desde";
        }else{
            posicion = " hasta";
        }
        builder.setTitle("Seleccionar hora"+ posicion);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String time = npHora.getValue()+":"+npMinuto.getValue();
                if(pos.equals("in")){
                    etTimeIn.setText(time);
                }else{
                    etTimeFin.setText(time);
                }
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_date_in:
                popUpDate("in");
                break;
            case R.id.btn_date_fin:
                popUpDate("fin");
                break;
            case R.id.btn_time_in:
                popUpTime("in");
                break;
            case R.id.btn_time_fin:
                popUpTime("fin");
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
