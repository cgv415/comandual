package com.example.carlos.comandual;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 24/08/2017
 */

public class AnalisisAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<String> objetos;
    private ArrayList<Integer> ventas;
    private List<Double> valores;
    private DataBaseManager manager;
    private View v;
    /**/
    private TextView tv_nombre;
    private TextView tv_cantidad;

    public AnalisisAdapter(Activity activity, ArrayList<String> objetos, ArrayList<Integer> ventas) {
        this.activity = activity;
        this.objetos = objetos;
        this.ventas = ventas;

        manager = new DataBaseManager(activity.getApplicationContext());
    }

    public AnalisisAdapter(Activity activity, ArrayList<String> objetos, List<Double> valores) {
        this.activity = activity;
        this.objetos = objetos;
        this.valores = valores;
        ventas = new ArrayList<>();

        manager = new DataBaseManager(activity.getApplicationContext());
    }

    @Override
    public int getCount() {
        return objetos.size();
    }

    @Override
    public Object getItem(int position) {
        return objetos.get(position);
    }

    @Override
    public long getItemId(int position) {
        String id = manager.obtenerMesa(Integer.parseInt(objetos.get(position))).get(0);
        return Integer.parseInt(id);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        v = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_analisis, null);
        }

        String mesa = objetos.get(position);

        tv_nombre = (TextView) v.findViewById(R.id.tv_objeto);
        tv_nombre.setText(mesa);

        tv_cantidad = (TextView) v.findViewById(R.id.tv_cantidad);
        if(ventas.size()>0){
            int cantidad = ventas.get(position);
            tv_cantidad.setText(String.valueOf(cantidad));
        }else{
            double valor = valores.get(position);
            tv_cantidad.setText(String.format(Locale.getDefault(),"%.1f",valor));
        }



        return v;
    }
}
