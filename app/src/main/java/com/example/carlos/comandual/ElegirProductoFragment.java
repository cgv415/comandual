package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.TreeMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ElegirProductoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ElegirProductoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ElegirProductoFragment extends Fragment {
    private TreeMap<String,ArrayList<Producto>> mapa;
    private ArrayList<Producto> arrayProductos;
    private ArrayList<Producto> arrayPedido;
    private OnFragmentInteractionListener mListener;
    private Producto productoClass;
    private ElegirProductoAdapter adapter;
    private FragmentActivity myContext;
    private DataBaseManager manager;
    private NumberPicker cantidad;
    private String categoria;
    private Button confirmar;
    private String producto;
    private Spinner spinner;
    private ListView lv;
    private int idMesa;


    public ElegirProductoFragment() {
        // Required empty public constructor
    }

    /**
     * @param categoria Parameter 1.
     * @return A new instance of fragment ElegirProductoFragment.
     */
    public static ElegirProductoFragment newInstance(String categoria) {
        ElegirProductoFragment fragment = new ElegirProductoFragment();
        Bundle args = new Bundle();
        args.putString("categoria", categoria);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoria = getArguments().getString("categoria");
            idMesa = getArguments().getInt("idMesa");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_elegir_producto, container, false);

        arrayPedido = new ArrayList<>();

        mapa = new TreeMap<>();

        manager = new DataBaseManager(v.getContext());

        lv = (ListView)v.findViewById(R.id.listViewElegirProducto);

        arrayProductos = obtenerProductos(categoria);

        adapter = new ElegirProductoAdapter(getActivity(), arrayProductos);

        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int idProducto = arrayProductos.get(position).getId();
                popUpCantidad(position, idProducto);
            }
        });


        return v;
    }

    public void popUpCantidad(final int pos, final int idProducto){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_detalle_producto, null);

        manager = new DataBaseManager(v.getContext());
        producto = manager.obtenerProducto(idProducto).getNombre();

        //TODO crear metodo obtenerproducto_tipos(int id);
        ArrayList<String> tiposProducto = manager.obtenerProducto_Tipos(idProducto+"");
        ArrayList<String> tipo = new ArrayList<>();

        cantidad = (NumberPicker) v.findViewById(R.id.number_cantidad);
        float oldCantidad = arrayProductos.get(pos).getCantidad();
        int old = Math.round(oldCantidad);
        cantidad.setMinValue(old);
        cantidad.setMaxValue(50);

        builder.setTitle(producto);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                float cant = ((NumberPicker) v.findViewById(R.id.number_cantidad)).getValue();

/*
                if(mapa.containsKey(tipo)){
                    arrayPedido = mapa.get(tipo);
                    if(!arrayPedido.contains(arrayProductos.get(pos))){
                        arrayPedido.add(arrayProductos.get(pos));
                    }
                }else{
                    arrayPedido = new ArrayList<>();
                    arrayPedido.add(arrayProductos.get(pos));
                    mapa.put(tipo,arrayPedido);
                }
*/

            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    private ArrayList<Producto> obtenerProductos(String categoria){
        ArrayList<Producto> productos = new ArrayList<>();

        ArrayList<ArrayList<String>> array = manager.obtenerProductosPorCategoria(categoria);

        for(int i = 0; i < array.size();i++){
            productos.add(new Producto(Integer.parseInt(array.get(i).get(0)), array.get(i).get(1), array.get(i).get(2), 0));
        }

        return productos;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            myContext= getActivity();
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
