package com.example.carlos.comandual;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class MapActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,OnMapReadyCallback {
    private GoogleMap mMap;
    private DataBaseManager manager;
    private Marker marcador;
    double lat = 0.0;
    double lng = 0.0;
    private ArrayList<String> restaurante;
    private ArrayList<Marker> marcadores;
    private Map<String,String> mapa;
    private Activity activity;
    private AutoCompleteTextView acRestaurante;
    private ArrayList<RestauranteClass> restaurantes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem nav_mapa = menu.findItem(R.id.nav_mapa);
        nav_mapa.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);


        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        generarRestaurantes();

        activity = this;
    }

    public void rellenarAutocomplete(){
        restaurantes = manager.obtenerRestaurantes();
        final ArrayList<String> arrayRest = new ArrayList<>();
        for(RestauranteClass rest:restaurantes){
            arrayRest.add(rest.toString());
        }

        acRestaurante = (AutoCompleteTextView) findViewById(R.id.ac_restaurante);
        ArrayAdapter<String> adapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,arrayRest);
        acRestaurante.setThreshold(1);
        acRestaurante.setAdapter(adapter);
        if(DataBaseManager.tipoUsuario.equals("empleado")){
            acRestaurante.setVisibility(View.GONE);
        }

        acRestaurante.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                try{
                    String [] arRest = acRestaurante.getText().toString().split(",");
                    String nombre = arRest[0];
                    String lati = "";
                    String lngi = "";
                    for(RestauranteClass r:restaurantes){
                        if(r.getNombre().equals(nombre)){
                            lati = r.getLatitud();
                            lngi = r.getLongitud();
                            break;
                        }
                    }

                    Double lat = Double.parseDouble(lati);
                    Double lng = Double.parseDouble(lngi);

                    LatLng latLng = new LatLng(lat,lng);
                    CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(latLng, 20);
                    //marcador = mMap.addMarker(new MarkerOptions().position(latLng));
                    mMap.animateCamera(miUbicacion);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Lo sentimos, este restaurante no dispone de ubicación actualmente.",Toast.LENGTH_SHORT).show();
                }
                acRestaurante.setText("");
            }
        });
    }

    public void generarRestaurantes(){
        manager.crearTablaRestaurante();

        String url = DataBaseManager.ip+"restaurantes/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String id = object.getString("id");
                        String nombre = object.getString("nombre");
                        String direccion = object.getString("direccion");
                        String localidad = object.getString("localidad");
                        String provincia = object.getString("provincia");
                        String latitud = object.getString("latitud");
                        String longitud = object.getString("longitud");

                        RestauranteClass restaurante = new RestauranteClass(id,nombre,direccion,provincia,localidad,latitud,longitud);
                        manager.insertarRestaurante(restaurante);
                    }
                    rellenarAutocomplete();
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);

            }
        });
        mRequestQueue.add(request);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        /*if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            new AlertDialog.Builder(this)
                    .setTitle("Salir")
                    .setMessage("Realmente quieres salir de la aplicación?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            int p = android.os.Process.myPid();
                            android.os.Process.killProcess(p);
                        }})
                    .setNegativeButton(android.R.string.no, null).show();

        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapa = new TreeMap<>();
        marcadores = new ArrayList<>();

        String url = DataBaseManager.ip+"restaurantes"+"/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray array = new JSONArray(response);
                    LatLng latLng;
                    CameraUpdate miUbicacion;
                    for(int i = 0;i < array.length();i++){
                        JSONObject object = array.getJSONObject(i);
                        if(DataBaseManager.tipoUsuario.equals("empleado")){
                            String usuario = object.getString("id");
                            if(usuario.equals(DataBaseManager.restaurante)){
                                restaurante = new ArrayList<>();
                                restaurante.add(object.getString("id"));
                                restaurante.add(object.getString("nombre"));
                                restaurante.add(object.getString("codigo"));
                                restaurante.add(object.getString("direccion"));
                                restaurante.add(object.getString("provincia"));
                                restaurante.add(object.getString("localidad"));
                                restaurante.add(object.getString("latitud"));
                                restaurante.add(object.getString("longitud"));
                                restaurante.add(object.getString("idUsuario"));


                                if(!restaurante.get(6).equals("null")&&!restaurante.get(7).equals("null")){
                                    lat = Double.parseDouble(restaurante.get(6));
                                    lng = Double.parseDouble(restaurante.get(7));
                                    latLng = new LatLng(lat,lng);
                                    miUbicacion = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                                    marcador = mMap.addMarker(new MarkerOptions().position(latLng).title("Mi posicion Actual"));
                                    mMap.animateCamera(miUbicacion);
                                }else{
                                    //Mientras no funcione el gps
                                    lat = 36.82861216269808;
                                    lng = -2.453636527061463;
                                    //36.833284,-2.460216
                                    latLng = new LatLng(lat,lng);
                                    miUbicacion = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                                    //marcador = mMap.addMarker(new MarkerOptions().position(latLng));
                                    mMap.animateCamera(miUbicacion);
                                }
                                break;
                            }
                        }else{
                            try{
                                String lati = object.getString("latitud");
                                String longi = object.getString("longitud");
                                if(!longi.equals("")&&!lati.equals("")){
                                    latLng = new LatLng(Double.parseDouble(lati),Double.parseDouble(longi));
                                    mapa.put(object.getString("nombre"),object.getString("id"));

                                    MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(object.getString("nombre"));
                                    marcador = mMap.addMarker(markerOptions);
                                    marcadores.add(marcador);

                                    //mMap.animateCamera(miUbicacion);
                                    //Mientras no funcione el gps
                                    lat = 36.82861216269808;
                                    lng = -2.453636527061463;
                                    //36.833284,-2.460216
                                    latLng = new LatLng(lat,lng);
                                    miUbicacion = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                                    //marcador = mMap.addMarker(new MarkerOptions().position(latLng));
                                    mMap.animateCamera(miUbicacion);
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                    }

                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();
            }
        });
        mRequestQueue.add(request);
        //Para obtener ubicacion via gps

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                if(DataBaseManager.tipoUsuario.equals("empleado")){
                    manager.modificarUbicacion(latLng,restaurante);
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().anchor(0.0f, 1.0f).position(latLng));
                }/*else{
                    for(Marker marker : marcadores) {
                        Double lat1 = marker.getPosition().latitude;
                        Double lat2 = latLng.latitude;


                        Double lng1 = marker.getPosition().longitude;
                        Double lng2 = latLng.longitude;
                        if(Math.abs(marker.getPosition().latitude - latLng.latitude) < 0.01 && Math.abs(marker.getPosition().longitude - latLng.longitude) < 0.01) {
                            Toast.makeText(MapActivity.this, marker.getTitle()+": long", Toast.LENGTH_SHORT).show(); //do some stuff
                            break;
                        }

                    }
                }*/
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(DataBaseManager.tipoUsuario.equals("cliente")){
                    String title = marker.getTitle();
                    final String idRest = mapa.get(title);
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            DataBaseManager.restaurante = idRest;
                            manager.actualizarCliente();
                            Intent intent = new Intent(activity,MainActivity.class);
                            startActivity(intent);
                        }
                    })
                            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                    builder.setTitle("Visitar "+marker.getTitle()).show();
                }
                return false;
            }
        });
    }
}
