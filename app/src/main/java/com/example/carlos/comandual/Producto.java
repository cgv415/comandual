package com.example.carlos.comandual;

import android.util.Log;

import java.util.Comparator;
import java.util.concurrent.locks.LockSupport;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 07/03/2016
 */
public class Producto {
    private int id;
    private String nombre;
    private String descripcion;
    private String tipo;
    private float cantidad;
    private String alteracionIngredientes;
    private boolean stock;
    private String categoria;
    private Double precio;

    public Producto() {
        this.nombre = "";
        this.descripcion = "";
        this.cantidad = 0;
        this.alteracionIngredientes = "";
        this.tipo = "";
        this.categoria = "";
        this.precio = 0.0;
    }

    public Producto(int id, String nombre, String descripcion, float cantidad) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
    }

    public Producto(int id, String nombre, String descripcion, String tipo, float cantidad, String alteracionIngredientes, boolean stock) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.alteracionIngredientes = alteracionIngredientes;
        this.stock = stock;
    }

    public Producto(int id, String nombre, String descripcion, String tipo, float cantidad, String alteracionIngredientes, boolean stock, String categoria, Double precio) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.alteracionIngredientes = alteracionIngredientes;
        this.stock = stock;
        this.categoria = categoria;
        this.precio = precio;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getCategoria(){
        return categoria;
    }

    public void setCategoria(String categoria){
        this.categoria = categoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTipo(String tipo) { this.tipo = tipo; }

    public String getTipo() { return tipo; }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public boolean isStock() {
        return stock;
    }

    public void setStock(boolean stock) {
        this.stock = stock;
    }

    public boolean getStock(){ return this.stock; }

    public String getAlteracionIngredientes() {
        if(alteracionIngredientes==null){
            return "Sin comentarios";
        }else if(alteracionIngredientes.equals("")){
            return "Sin comentarios";
        }else{
            return alteracionIngredientes;
        }
    }

    public void setAlteracionIngredientes(String alteracionIngredientes) {
        this.alteracionIngredientes = alteracionIngredientes;
    }

    @Override
    public String toString() {
        return "Producto " + nombre;
    }

    @Override
    public boolean equals(Object o) {
        Producto otro = (Producto) o;

        String alter = getAlteracionIngredientes();
        String oalter = otro.getAlteracionIngredientes();

        if(otro.getTipo() == null || this.tipo == null){
            return false;
        }
        else if(!getNombre().equals(otro.getNombre())){
            return false;
        }else if(!getTipo().equals(otro.getTipo())){
            return false;
        //Si o un AlteracionIngredientes o el otro AlteracionIngredientes son distintos de nulo, entra
        }else if(alter != null || oalter != null){
            if(alter == null && oalter != null){
                return false;
            }else if(alter == null && oalter != null){
                return false;
            }else if(!alter.equals(oalter)){
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }

    }


}
