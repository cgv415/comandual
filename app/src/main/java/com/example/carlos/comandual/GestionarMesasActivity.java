package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GestionarMesasActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    DataBaseManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionar_mesas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),NuevaMesaActivity.class);
                startActivity(intent);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem nav = menu.findItem(R.id.nav_mesas);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        ArrayList<String> nombreMesas = new ArrayList<>();
        final ArrayList<ArrayList<String>> mesas = manager.obtenerMesas();
        for (ArrayList<String> m:mesas) {
            nombreMesas.add(m.get(5));
        }

        ListView listView = (ListView) findViewById(R.id.listview_mesas);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,nombreMesas);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> mesa = mesas.get(position);
                popUp(mesa);
            }
        });

        Button asignar = (Button) findViewById(R.id.btn_eliminar);
        asignar.setOnClickListener(this);

    }

    public void popUp(final ArrayList<String> mesa) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_modificar_mesa, null);

        manager = new DataBaseManager(v.getContext());

        final EditText capacidad = (EditText) v.findViewById(R.id.et_capacidad);
        capacidad.setText(mesa.get(1));

        //Manejar la forma
        final Spinner spinner_forma = (Spinner) v.findViewById(R.id.spinner_forma);
        ArrayList<String> lista = new ArrayList<>();
        lista.add("CUADRADA");
        lista.add("REDONDA");
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(v.getContext(), android.R.layout.simple_spinner_item, lista);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_forma.setAdapter(adaptador);
        int forma = 1;
        if(mesa.get(2).equals("CUADRADA")){
            forma = 0;
        }
        spinner_forma.setSelection(forma);

        //Manejar los espacios
        final Spinner spinner_espacio = (Spinner) v.findViewById(R.id.spinner_espacio);
        lista = manager.obtenerEspacios();

        adaptador = new ArrayAdapter<>(v.getContext(), android.R.layout.simple_spinner_item, lista);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_espacio.setAdapter(adaptador);

        String strEspacio = manager.obtenerEspacioById(mesa.get(4)).get(0);
        int posEspacio = 0;
        for(int i = 0;i<lista.size();i++){
            if(lista.get(i).equals(strEspacio)){
                posEspacio = i;
                break;
            }
        }

        spinner_espacio.setSelection(posEspacio);


        final EditText etNombre = (EditText) v.findViewById(R.id.et_nombre);
        int id = manager.obtenerUlitmoIdMesa() + 1;
        etNombre.setText(String.valueOf(id));

        etNombre.setText(mesa.get(5));

        builder.setTitle("Modificar mesa");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final int cap = Integer.parseInt(capacidad.getText().toString());
                final String forma =(String) spinner_forma.getSelectedItem();
                final String espacio =(String) spinner_espacio.getSelectedItem();
                //String idEspacio = manager.obtenerIdEspacio(espacio).get(0);
                final String nombre = DataBaseManager.removeAcentos(etNombre.getText().toString());

                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip + "mesas/"+mesa.get(0)+"/";
                StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);
                                try {

                                    JSONObject object = new JSONObject(response);
                                    manager.modificarMesa(object.getString("id"),object.getString("nombre"),object.getString("capacidad"),object.getString("forma"),"LIBRE",object.getString("espacio"));
                                    finish();
                                    Intent intent = new Intent(getApplicationContext(),GestionarMesasActivity.class);
                                    startActivity(intent);

                                } catch (Throwable t) {
                                    Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        //params.put("nombre",manager.removeAcentos(editText.getText().toString()));
                        if(Integer.parseInt(mesa.get(1))!=cap){
                            params.put("capacidad",String.valueOf(cap));
                        }
                        if(!mesa.get(2).equals(forma)){
                            params.put("forma",forma);
                        }
                        String espacioMesa = manager.obtenerEspacioById(mesa.get(4)).get(0);
                        if(!espacioMesa.equals(espacio)){
                            String idEspacio = manager.obtenerIdEspacio(espacio).get(0);
                            params.put("espacio",idEspacio);
                        }
                        if(!mesa.get(5).equals(nombre)){
                            params.put("nombre",DataBaseManager.removeAcentos(nombre));
                        }
                        //params.put("user", DataBaseManager.restaurante);

                        return params;
                    }

                };
                mRequestQueue.add(putRequest);

            }
        })
                .setNegativeButton(R.string.eliminar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        popUpEliminar(mesa.get(0));
                    }
                });
        builder.show();
    }

    public void popUpEliminar(final String idMesa){
        String nombre = manager.obtenerMesa(Integer.parseInt(idMesa)).get(5);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Eliminar mesa");
        builder.setMessage("Desea eliminar la mesa " + nombre + "?");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                manager.eliminarMesa(idMesa);
                finish();
                Intent intent = new Intent(getApplicationContext(),GestionarMesasActivity.class);
                startActivity(intent);
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){}
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up asignar, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            case R.id.action_call:
                //makeJsonObjReq();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_eliminar:
                Intent intent = new Intent(getApplicationContext(),AsignarMesasActivity.class);
                startActivity(intent);
        }
    }
}
