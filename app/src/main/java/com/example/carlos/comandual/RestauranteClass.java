package com.example.carlos.comandual;

import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 16/04/2017
 */

public class RestauranteClass {
    private String id;
    private String nombre;
    private String direccion;
    private String provincia;
    private String localidad;
    private String latitud;
    private String longitud;

    public RestauranteClass(String id, String nombre, String direccion, String provincia, String localidad, String latitud, String longitud) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.provincia = provincia;
        this.localidad = localidad;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public RestauranteClass() {
        this.id = "";
        this.nombre = "";
        this.direccion = "";
        this.localidad = "";
        this.provincia = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @Override
    public String toString() {
        return nombre + ", " + getDireccion() + ", " + localidad + " (" + provincia + ")";
    }

    public static ArrayList<ArrayList<String>> toArray(ArrayList<RestauranteClass> restaurantes){
        ArrayList<ArrayList<String>> array1 = new ArrayList<>();
        ArrayList<String> array2;
        for(RestauranteClass restaurante:restaurantes){
            array2 = new ArrayList<>();
            array2.add(restaurante.getNombre());
            array2.add(restaurante.getDireccion());
            array2.add(restaurante.getLocalidad());
            array2.add(restaurante.getProvincia());

            array1.add(array2);
        }

        return array1;
    }
}
