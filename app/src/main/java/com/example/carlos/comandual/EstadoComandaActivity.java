package com.example.carlos.comandual;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class EstadoComandaActivity extends Activity implements View.OnClickListener{

    private ArrayList<ArrayList<String>> productos;
    private DataBaseManager manager;
    private int idMesa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado_comanda);

        Bundle bundle = getIntent().getExtras();
        idMesa = bundle.getInt("idMesa");

        manager = new DataBaseManager(this);

        productos = manager.obtenerComandasEnCola(String.valueOf(idMesa));
        /*ArrayList<ArrayList<String>> aux = (ArrayList<ArrayList<String>>) productos.clone();
        for(ArrayList<String> producto:productos){
            if(producto.get(4).equals("FINALIZADO")){
                aux.remove(producto);
            }
        }
        //0: nombre mesa
        //1: cantidad
        //2: idcategoria
        //3: comentario
        //4: estado
        //5: nombreProducto
        //6: id
        productos = (ArrayList<ArrayList<String>>) aux.clone();
*/
        final ArrayList<ComandaClass> todasComandas = manager.obtenerObjetoComandas(String.valueOf(idMesa));
        final  ArrayList<ComandaClass> comandas = new ArrayList<>();
        for(int i = 0; i < todasComandas.size();i++){
           ComandaClass comandaClass = todasComandas.get(i);
            int pos = comandaClass.getIndexContains(comandas);
            if(pos!=-1){
                int cantidad = comandas.get(pos).getCantidad();
                comandas.get(pos).setCantidad(cantidad+1);
            }else{
                if(!comandaClass.getEstado().equals("FINALIZADO")){
                    comandas.add(comandaClass);
                }
            }
        }

        TextView textView = (TextView) findViewById(R.id.textView7);
        textView.setText(R.string.precio_total);

        ListView listView = (ListView) findViewById(R.id.listViewEstadoMesa);
        EstadoAdapter estadoAdapter = new EstadoAdapter(this,comandas,"mesa");


        //0: id
        //1: mesa
        //2: id producto
        //3: id categoria
        //4: estado
        //5: cantidad
        //6: comentarios

        listView.setAdapter(estadoAdapter);
        listView.setEnabled(true);
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> comanda = manager.obtenerComandabyId(comandas.get(position).getIdComanda());
                if(DataBaseManager.tipoUsuario.equals("empleado")){
                    Intent intent = new Intent(getApplicationContext(),DetalleComandaActivity.class);
                    comanda.set(5,String.valueOf(comandas.get(position).getCantidad()));
                    intent.putExtra("producto",comanda);
                    ArrayList<String> ids = new ArrayList<>();
                    for(int i = 0; i < todasComandas.size();i++){
                        ComandaClass comandaClass = todasComandas.get(i);
                        String nombreProducto = manager.obtenerProducto(Integer.parseInt(comanda.get(2))).getNombre();
                        if(comandaClass.getComentario().equals(comanda.get(6))&& comandaClass.getEstado().equals(comanda.get(4)) && comandaClass.getProducto().equals(nombreProducto)){
                            ids.add(comandaClass.getIdComanda());
                        }

                    }
                    intent.putExtra("ids",ids);
                    if(comanda.get(4).equals("SIRVIENDO")||comanda.get(4).equals("COLA")){
                        intent.putExtra("estado","SERVIDO");
                    }else if(comanda.get(4).equals("SERVIDO")){
                        intent.putExtra("estado","PAGADO");
                    }else{
                        intent.putExtra("estado","PAGADO");
                    }

                    intent.putExtra("activity","comanda");
                    intent.putExtra("idMesa",idMesa);
                    startActivity(intent);
                }
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int pos = position;
                new android.support.v7.app.AlertDialog.Builder(view.getContext())
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Eliminar Producto")
                        .setMessage("Estas seguro de que deseas cancelar la comanda ")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                manager.modificarComanda(comandas.get(pos).getIdComanda(),"CANCELADA");
                                //adapter.notifyDataSetChanged();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                return false;
            }
        });

        Button cuenta = (Button) findViewById(R.id.btn_cuenta);
        cuenta.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cuenta:
                popUpCuenta();
                break;
        }
    }

    public void popUpCuenta(){
        // Use the Builder class for convenient dialog construction

        //dropdow https://amsheer007.wordpress.com/2015/03/20/show-listview-as-dropdown-in-android-a-spinner-alternative/
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_cuenta, null);

        TextView tvPrecio = (TextView) v.findViewById(R.id.tv_precio);
        Double precioFinal = 0.0;

        int gratis = 0;
        String categoria;
        for(ArrayList<String> producto:productos){
            categoria = manager.obtenerCategoriabyId(producto.get(2)).get(1);
            if(categoria.equals("bebidas")){
                gratis = gratis + Integer.valueOf(producto.get(1));
            }
        }


        for(ArrayList<String> producto: productos) {
            int cantidad = Integer.valueOf(producto.get(1));
            Double precioUnidad = manager.obtenerProducto(producto.get(5)).getPrecio();

            Double precio = precioUnidad * cantidad;

            if (manager.obtenerCategoriabyId(producto.get(2)).get(1).equals("tapa")) {
                int cantidadGratis = gratis - cantidad;
                int cantidadPagar = 0;
                if (cantidadGratis < 0) {
                    cantidadPagar = Math.abs(cantidadGratis);
                    gratis = 0;
                } else {
                    gratis -= cantidad;
                }

                precio = cantidadPagar * precioUnidad;
            }
            if(!producto.get(4).equals("PAGADO")&&!producto.get(4).equals("CANCELADO")){
                precioFinal += precio;
            }
        }

        tvPrecio.setText(String.valueOf(String.format(Locale.getDefault(),"%.2f",precioFinal)));
/*
        String url = manager.ip+"/api/productos/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<String> arrayList;
                    JSONArray array = new JSONArray(response);

                    actualizarMapa(array);


                }catch (Exception e){
                    String err = e.toString();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
            }
        });
        mRequestQueue.add(request);

        /*for(String categoria:categorias){
            Spinner spinner = new Spinner(this);
            String[] valores = {categoria, "ocho"};
            spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, valores));
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
                {
                    //Toast.makeText(adapterView.getContext(), (String) adapterView.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {
                    // vacio

                }
            });
            layout.addView(spinner);
        }*/

        ArrayList<String> mesa = manager.obtenerMesa(idMesa);
        final String nombreMesa = mesa.get(5);
        EmpleadoClass empleado = manager.obtenerEmpleado(mesa.get(7));
        final String token = empleado.getToken();
        final String idEmpleado = empleado.getId();

        builder.setTitle("Cuenta");
        builder.setView(v);
        builder.setPositiveButton("Efectivo", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(DataBaseManager.tipoUsuario.equals("cliente")){
                    String cuenta = "Cuenta - Efectivo";
                    DataBaseManager.enviarNotificacion(nombreMesa, cuenta , token);
                    Toast.makeText(getApplicationContext(),"Cuenta solicitada",Toast.LENGTH_SHORT).show();
                    popUpValorar(idEmpleado);
                }else{
                    manager.limpiarMesa(idMesa);
                    finish();
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }
            }
        });
        builder.setNegativeButton("Tarjeta", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(DataBaseManager.tipoUsuario.equals("cliente")){
                    String cuenta = "Cuenta - Tarjeta";
                    DataBaseManager.enviarNotificacion(nombreMesa, cuenta , token);
                    Toast.makeText(getApplicationContext(),"Cuenta solicitada",Toast.LENGTH_SHORT).show();
                    popUpValorar(idEmpleado);
                }else{
                    manager.limpiarMesa(idMesa);
                    finish();
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }
            }
        });
        builder.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                //Toast.makeText(getApplicationContext(),"Servicio no disponible, disculpe las molestias",Toast.LENGTH_SHORT).show();
            }
        });


        builder.show();
    }

    public void popUpValorar(final String idEmpleado) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_valoracion, null);

        final RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratingBar_valoracion);
        final EditText comentarios = (EditText) v.findViewById(R.id.et_comentarios);

        Calendar calendarNow =new GregorianCalendar(TimeZone.getTimeZone("Europe/Madrid"));
        int day = calendarNow.get(Calendar.DAY_OF_MONTH);
        int month = calendarNow.get(Calendar.MONTH)+1;
        int year = calendarNow.get(Calendar.YEAR);

        final String fecha = day+"-"+month+"-"+year;

        builder.setTitle("Valorar servicios.");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip + "valoraciones/";
                StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                mostrarMensaje("Gracias por tu colaboración!");

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        //params.put("nombre",manager.removeAcentos(editText.getText().toString()));
                        params.put("valoracion", String.valueOf(ratingBar.getRating()));
                        params.put("comentario", comentarios.getText().toString());
                        params.put("cliente", DataBaseManager.idUsuario);
                        params.put("restaurante", DataBaseManager.restaurante);
                        params.put("empleado", idEmpleado);
                        params.put("fecha", fecha);

                        return params;
                    }

                };
                mRequestQueue.add(putRequest);


                //manager.insertarCategoriaApi(editText.getText().toString());


            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void mostrarMensaje(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
