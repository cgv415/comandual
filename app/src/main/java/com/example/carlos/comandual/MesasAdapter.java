package com.example.carlos.comandual;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 23/05/2017
 */

public class MesasAdapter extends BaseAdapter {

    private Activity activity;
   // private ArrayList<String> mesas;
    private ArrayList<ArrayList<String>> mesas;
    private ArrayList<ArrayList<String>> reparto;
    private ArrayList<CheckBox> checkBoxArrayList;

    private DataBaseManager manager;

    private CheckBox checkBox;
    private TextView textView;


    /*public MesasAdapter(Activity activity, ArrayList<String> mesas, ArrayList<ArrayList<String>> reparto) {
        this.activity = activity;
        this.mesas = mesas;
        this.reparto = reparto;

    }*/

    public MesasAdapter(Activity activity, ArrayList<ArrayList<String>> mesas) {
        this.activity = activity;
        this.mesas = mesas;

    }

    @Override
    public int getCount() {
        return mesas.size();
    }

    @Override
    public Object getItem(int position) {
        return mesas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View v = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_elegir_mesas, null);
        }

        manager = new DataBaseManager(v.getContext());

        if(checkBoxArrayList == null){
            checkBoxArrayList = new ArrayList<>();
            String mesa = "";
            for(int i = 0 ; i < mesas.size();i++){
                mesa = mesas.get(i).get(0);
                CheckBox checkBox = new CheckBox(v.getContext());
                checkBox.setText(mesa);

                checkBoxArrayList.add(checkBox);
            }
        }
        String empleado = "";
        String id = mesas.get(position).get(7);
        if(!id.equals("null")){
            empleado = manager.obtenerEmpleado(id).getNombre();
        }else{
            empleado = "Sin asignar";
        }
        textView = (TextView) v.findViewById(R.id.textview_camarero) ;
        textView.setText(empleado);
        String mesa = mesas.get(position).get(5);
        String idMesa = mesas.get(position).get(0);
        checkBox = (CheckBox) v.findViewById(R.id.checkBox_mesa);
        checkBox.setText(mesa);
        checkBox.setTag(idMesa);

        if(manager.idUsuario.equals(mesas.get(position).get(7))){
            checkBox.setChecked(true);
            actualizarArrayCheckBox(idMesa,true);
        }
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //String idMesa = buttonView.getTag().toString();
                actualizarArrayCheckBox(buttonView.getTag().toString(),isChecked);
            }
        });

        return v;
    }

    public boolean isCheck(String nombre){
        for(int i = 0; i < checkBoxArrayList.size();i++){
            String check = checkBoxArrayList.get(i).getText().toString();
            if(check.equals(nombre)){
                return checkBoxArrayList.get(i).isChecked();
            }
        }
        return false;
    }

    public void actualizarArrayCheckBox(String id,boolean check){
        for(int i = 0; i < checkBoxArrayList.size();i++){
            String checkbox = checkBoxArrayList.get(i).getText().toString();
            if(checkbox.equals(id)){
                checkBoxArrayList.get(i).setChecked(check);
                break;
            }
        }
    }

    public ArrayList<CheckBox> getCheckBoxArrayList(){
        return checkBoxArrayList;
    }
}
