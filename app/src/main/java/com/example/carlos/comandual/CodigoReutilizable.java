package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 14/07/2017
 */

public class CodigoReutilizable extends AppCompatActivity {
    public void popUp() {
        // Use the Builder class for convenient dialog construction
       /* AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_categoria, null);

        final EditText editText = (EditText) v.findViewById(R.id.et_nombre);

        builder.setTitle("Insertar Categoria");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip + "/api/categorias/";
                StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response
                                // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                Log.d("Response", response);
                                try {

                                    //JSONObject object = new JSONObject(response);

                                    //String id = object.getString("id");
                                    //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                                    //boolean insert = manager.insertarCategoria(id,editText.getText().toString());

                                } catch (Throwable t) {
                                    Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                }
                                //String id = response.

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        //params.put("nombre",manager.removeAcentos(editText.getText().toString()));
                        params.put("user", DataBaseManager.restaurante);

                        return params;
                    }

                };
                mRequestQueue.add(putRequest);


                //manager.insertarCategoriaApi(editText.getText().toString());


            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();*/

        /*
        * Fragment:
        FragmentManager fragmentManager = getSupportFragmentManager();
        //Fragment fragment =  new AnalisisComandasFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_analisis,new AnalisisComandasFragment());

        transaction.commit();

        <FrameLayout
        android:id="@+id/fragment_analisis"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
    </FrameLayout>
    */
        String url = "";

        //Creamos un objeto RequestQueue, lo inicializamos.
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        //Creamos un objeto StringRequest y lo inicializamos con los parámetros(tipo de consulta, url, listener)
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    //Inicializamos el JSON donde se van a guardar todos los datos.
                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    //Recorremos el array
                    for (int i = 0; i < array.length(); i++) {
                        object = array.getJSONObject(i);
                        //Obtenemos los elementos del objeto JSON
                        String id = object.getString("id");
                        String nombre = object.getString("nombre");
                        String direccion = object.getString("direccion");
                        String localidad = object.getString("etLocalidad");
                        String provincia = object.getString("etProvincia");
                        String latitud = object.getString("latitud");
                        String longitud = object.getString("longitud");
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e("Err", err);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("Err", err);

            }
        });
        //Añadimos el StringRequest a la cola de Request
        mRequestQueue.add(request);

    }

}
