package com.example.carlos.comandual;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetalleComandaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    private DataBaseManager manager;
    private String estado;
    private String activity;
    private ArrayList<String> producto;
    private ArrayList<String> ids;
    private Bundle bundle;
    private RequestQueue mRequestQueue;
    private static final String urlCMS = "https://fcm.googleapis.com/fcm/send";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_comanda);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        manager = new DataBaseManager(this);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        manager.mostrarMenu(menu);

        MenuItem nav = menu.findItem(R.id.nav_home);
        nav.setChecked(true);

        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView.setNavigationItemSelectedListener(this);

        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        /*
        id//0
        mesa//1
        id producto//2
        id categoria//3
        estado//4
        cantidad//5
        comentarios//6
          */

        bundle = getIntent().getExtras();
        producto = bundle.getStringArrayList("producto");
        estado = bundle.getString("estado");
        activity = bundle.getString("activity");
        ids = bundle.getStringArrayList("ids");

        String nombre;
        String mesa;
        String cantidad;
        String categoria;
        String comentarios;

        if(activity.equals("comanda")){
            nombre = manager.obtenerProducto(Integer.parseInt(producto.get(2))).getNombre();//2
            mesa = manager.obtenerMesa(Integer.parseInt(producto.get(1))).get(5);//1
            cantidad = String.valueOf(ids.size());
            categoria = manager.obtenerCategoriabyId(producto.get(3)).get(1);//3
            comentarios = producto.get(6);//6
        }else{
            nombre = manager.obtenerProducto(Integer.parseInt(producto.get(2))).getNombre();
            mesa = manager.obtenerMesa(Integer.parseInt(producto.get(1))).get(5);
            cantidad = String.valueOf(ids.size());
            categoria = manager.obtenerCategoriabyId(producto.get(3)).get(1);
            comentarios = producto.get(6);
        }

        if(comentarios.equals("")){
            comentarios = "Sin comentarios";
        }

        TextView tvMesa = (TextView) findViewById(R.id.tvMesa);
        tvMesa.setText(mesa);

        TextView tvProducto = (TextView) findViewById(R.id.tvProducto);
        tvProducto.setText(nombre);

        TextView tvCategoria = (TextView) findViewById(R.id.tvCategoria);
        tvCategoria.setText(categoria);

        TextView tvCantidad = (TextView) findViewById(R.id.tvCantidad);
        tvCantidad.setText(cantidad);

        TextView tvComentarios = (TextView) findViewById(R.id.tvComentarios);
        tvComentarios.setText(comentarios);

        Button listo = (Button) findViewById(R.id.btnListo);
        listo.setOnClickListener(this);

        switch (estado) {
            case "SERVIDO":
                listo.setText("Marcar como Servido");
                break;
            case "PAGADO":
                listo.setText("Marcar como Pagado");
                break;
            case "SIRVIENDO":
                listo.setText("Marcar como SIRVIENDO");
                break;
        }

        Button cancelar = (Button) findViewById(R.id.btnCancelar);
        cancelar.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btnCancelar:
                switch (activity){
                    case "comandas":
                        new android.support.v7.app.AlertDialog.Builder(v.getContext())
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("Eliminar comanda")
                                .setMessage("Estas seguro de que deseas cancelar la comanda?")
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        manager.modificarComanda(producto.get(0),"CANCELADO");
                                        Intent intent = new Intent(getApplicationContext(),ComandasActivity.class);
                                        startActivity(intent);
                                    }

                                })
                                .setNegativeButton("No", null)
                                .show();

                        break;
                    case "comanda":
                        new android.support.v7.app.AlertDialog.Builder(v.getContext())
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("Eliminar comanda")
                                .setMessage("Estas seguro de que deseas cancelar la comanda?")
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        manager.modificarComanda(producto.get(0),"CANCELADO");
                                        int idMesa = bundle.getInt("idMesa");
                                        Intent intent = new Intent(getBaseContext(),MesaActivity.class);
                                        intent.putExtra("idMesa",idMesa);
                                        startActivity(intent);
                                    }

                                })
                                .setNegativeButton("No", null)
                                .show();
                        break;
                }

                break;
            case R.id.btnListo:
                switch (activity){
                    case "comandas":
                        for(int i = 0 ; i < ids.size();i++){
                            manager.modificarComanda(ids.get(i),estado);
                        }
                        makeJsonObjReq(producto);
                        intent = new Intent(this,ComandasActivity.class);
                        startActivity(intent);
                        break;
                    case "comanda":
                        for(int i = 0 ; i < ids.size();i++){
                            manager.modificarComanda(ids.get(i),estado);
                        }
                        int idMesa = bundle.getInt("idMesa");
                        Boolean pendiente = false;
                        ArrayList<ArrayList<String >> comandas = manager.obtenerComandasEnCola(String.valueOf(idMesa));
                        for(int i = 0;i < comandas.size();i++){
                            ArrayList<String> comanda = comandas.get(i);
                            if(comanda.get(4).equals("COLA")||comanda.get(4).equals("SIRVIENDO")){
                                pendiente = true;
                                break;
                            }
                        }
                        if(!pendiente){
                            manager.modificarEstadoMesa(String.valueOf(idMesa),"OCUPADA");
                        }
                        intent = new Intent(this,MesaActivity.class);
                        intent.putExtra("idMesa",idMesa);
                        intent.putExtra("tab",1);
                        startActivity(intent);
                        break;
                }
                break;
        }
    }

    private void makeJsonObjReq(ArrayList<String> comanda){
        Integer idMesa = Integer.parseInt(comanda.get(1));
        String idEmpleado = manager.obtenerMesa(idMesa).get(7);
        String mesa = manager.obtenerMesa(idMesa).get(5);
        String token = manager.obtenerEmpleado(idEmpleado).getToken();
        String producto = manager.obtenerProducto(Integer.parseInt(comanda.get(2))).getNombre();

        final String jsonString =
                "{" +
                        "'to':'topics/"+ token +"'," +
                        "'notification':{" +
                        "'title':'" + mesa + "'," +
                        "'body':'" + producto + "'" +
                        "}" +
                        "}";
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    urlCMS, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(getApplicationContext(),"Comanda preparada",Toast.LENGTH_SHORT).show();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Response", "Error: " + error.getMessage());
                }
            }) {
                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("Content-Type","application/json");
                    params.put("Authorization","key=AIzaSyDAE9MwmYI9VjGjTqd6qLzOadga0tqvurs");
                    return params;
                }
            };
            mRequestQueue.add(jsonObjReq);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
