package com.example.carlos.comandual;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 01/12/2016
 */
//https://www.youtube.com/watch?v=VM_6tnGf0fY
public class MiFirebaseInstanceIdService extends FirebaseInstanceIdService{

    //Token Nexus 1 "euv1DC986KU:APA91bEvlGyQSGtcgIgeq_QdltoXPKsVopKAmvxilLtYEnEi27UyX-WWxp4PoVHRFuBZiSdskhqnn1HEZU1fUaKdJ0vxgrUTE8dA5gRHesWHZmOz_vABW5Ogb3u2WWNHMDnmHOCrMkuU"
    //Token Nexus 2 "ddp0zVEORB4:APA91bEvq1z7eDONhLehlSZXR-pjHZrIec9JeCDdYmQM3VDlHkG0eDePAFzW7qHlBc4ohAyqZXV4TlConEWgDY0AZZq05Ki75pZ23SFb07HyvVDWKK9I012kyc9KEsyP_lW0tKsYcCpx"
    //Token Sony "fH4lgIkZlqs:APA91bHlnwJKTwMmGuJECDNgLzt28ZZFmOskqnd6sEH0OcjzkarIblwEbKgdvjJdk8-U1rCe_Ak88_fbMNRpGRQn7E1mTRaCsM-dvvdqH0LtsqqR4__SgJivgNpfJ1mPMzv9JlHqeJVV"
    public static final String TAG = "NOTICIAS";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        DataBaseManager manager = new DataBaseManager(getApplicationContext());

        String token = FirebaseInstanceId.getInstance().getToken();

        if(!manager.isTableExists("sesion")){
            manager.crearTablaSesion();
        }

        if(!manager.isTableExists("cliente")){
            manager.crearTablaCliente();
        }

        if(!manager.isTableExists("token")){
            manager.crearTablaToken();
        }
        manager.insertarToken(token);
        Log.d(TAG,"Token: " + token);

        Intent intent = new Intent(this, RegistrationService.class);
        startService(intent);
        //sendRegistrationToServer(refreshedToken);

    }
}
