package com.example.carlos.comandual;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Map;


interface Database {
    void eliminarTabla(String nombreTabla);
    boolean isTableExists(String tableName);
    void actualizar();
    void actualizarCliente();
    int login(ArrayList<String> datos,String usuario);

    /*Mesa*/
    void crearTablaMesa();
    boolean insertarMesa(String id,String nombre, String capacidad, String forma, String estado, String espacio,String cliente, String empleado);
    boolean modificarMesa(String id, String nombre, String capacidad, String forma, String estado, String espacio);
    boolean modificarEstadoMesa(String idMesa, String estado);
    boolean modificarEmpleadoMesa(String idMesa, String idEmpleado);
    ArrayList<String> obtenerMesa(int codigo);
    ArrayList<ArrayList<String>> obtenerMesaPorEspacio(String espacio);
    ArrayList<ArrayList<String>> obtenerMesas();
    boolean eliminarMesa(String capacidad);
    void modificarCodigoMesa(ArrayList<String> mesa, String codigo);
    ArrayList<ArrayList<String>> obtenerMesaPorEspacio(int pos);

    /*Restaurantes*/
    void crearTablaRestaurante();
    boolean insertarRestaurante(String id,String nombre, String direccion, String provincia, String localidad, String latitud, String longitud);
    boolean insertarRestaurante(RestauranteClass restaurante);
    ArrayList<RestauranteClass> obtenerRestaurantes();

    /*Espacio*/
    void crearTablaEspacio();
    boolean insertarEspacio(String id, String espacio);
    ArrayList<String> obtenerEspacios();
    ArrayList<String> obtenerIdEspacios();
    boolean eliminarEspacio(String espacio);
    boolean isEspacioExist(String espacio);

    /*Ingredientes*/
    void crearTablaIngrediente();
    boolean insertarIngrediente(String id, String ingrediente, String stock);
    boolean insertarIngredienteApi(String ingrediente, String stock);
    boolean insertarIngredientes(ArrayList<String> ingredientes);
    boolean modificarStockIngrediente(String nombre,Boolean stock);
    ArrayList<String> obtenerIngredientes();
    ArrayList<ArrayList<String>> obtenerArrayIngredientes();
    boolean inStrockIngrediente(String nombre);
    Map<String,Boolean> obtenerStockIngredientes();
    String obtenerIngrediente(String id);
    ArrayList<String> obtenerIngredienteByNombre(String ingrediente);
    boolean eliminarIngrediente(final String id);

    /*Tipos*/
    void crearTablaTipo();
    boolean insertarTipo(String id, String tipo,String precio);
    ArrayList<String> obtenerTipo(String tipo);
    ArrayList<String> obtenerTipoPorNombre(String tipo);
    ArrayList<ArrayList<String>> obtenerTipos();

    /*Categorias*/
    void crearTablaCategoria();
    boolean insertarCategoria(String id, String categoria);
    ArrayList<String> obtenerCategorias();
    ArrayList<String> obtenerCategoriabyId(String id);
    ArrayList<String> obtenerCategoria(String categoria);
    String obtenerCategoriaProductobyId(String id);
    boolean eliminarCategoria(String categoria);
    boolean modificarCategoria(String id, String nombre);

    /*Productos*/
    void crearTablaProducto();
    boolean insertarProducto(String id, String nombre, String descripcion, String stock,String categoria, String precio);
    boolean modificarProducto(String id, String nombre, String descripcion, String stock,String categoria, String precio);
    boolean modificarStockProducto(Producto producto);
    ArrayList<Producto> obtenerProductos();
    ArrayList<ArrayList<String >> obtenerProductosPorCategoria(String categoria);
    ArrayList<ArrayList<String >> obtenerProductosPorCategoria(int categoria);
    Producto obtenerProducto(String producto);
    Producto obtenerProducto(int id);
    ArrayList<String> obtenerNombreProductos();
    boolean inStockProducto(String nombre);
    boolean eliminarProducto(String nombre);

    /*Producto_Tipo*/
    void crearTablaProducto_Tipo();
    boolean insertarProducto_Tipo(String producto,String tipo);
    ArrayList<ArrayList<String>> obtenerProductos_Tipos();
    ArrayList<String> obtenerProducto_Tipos(String producto);
    ArrayList<ArrayList<String>> obtenerProductos_Tipo(String tipo);

    /*Producto_Categoria*/
    void crearTablaProducto_Categoria();
    boolean insertarProducto_Categoria(String producto,String categoria);
    ArrayList<ArrayList<String>> obtenerProductos_Categorias();
    ArrayList<String> obtenerProducto_Categorias(String producto);
    ArrayList<ArrayList<String>> obtenerProductos_Categoria(String categoria);

    /*Producto_Ingrediente*/
    void crearTablaProducto_Ingrediente();
    boolean insertarProducto_Ingrediente(String producto, String ingrediente);
    ArrayList<String> obtenerProducto_Ingredientes(String producto);

    /*Comandas*/
    void crearTablaComanda();
    boolean insertarComanda(String id, String idMesa,String idProducto,String idTipo,String comentario,String estado,String cantidad,String token, String fecha, String hora);
    boolean insertarComandaApi(String id, String idMesa,String idProducto,String idTipo,String comentario,String estado,String cantidad,String token, String fecha, String hora);
    boolean modificarComanda(String idMesa, String estado);
    ArrayList<ComandaClass> obtenerComandasEnCola();
    ArrayList<ComandaClass> obtenerComandas();
    ArrayList<String> obtenerComandabyId(String id);
    ArrayList<ArrayList<String>> obtenerComandasEnCola(String mesa);

    //boolean eliminarComanda()

    /*Usuarios*/
    void crearTablaUsuario();
    boolean insertarUsuario(String nombre, String apellido, String correo, String nick, String password, String restaurante);
    boolean modificarUsuario(String nombre, String apellido, String puesto, String correo, String nick, String password, String restaurante);
    UsuarioClass obtenerUsuario(String nick);
    ArrayList<UsuarioClass> obtenerUsuarios();
    boolean eliminarEmpleado(String nick);
    String obtenerPassword(String nick);

    /*Empleados*/
    void crearTablaEmpleado();
    boolean insertarEmpleado(String id, String nombre, String restaurante, String token, String password);
    boolean actualizarApiEmpelado(String id, String token);
    EmpleadoClass obtenerEmpleado(String id);
    ArrayList<String> obtenerNombreEmpleados();
    EmpleadoClass obtenerEmpleadoByName(String nombre);

    /*Clientes*/
    void crearTablaCliente();
    boolean insertarCliente(String id, String nombre, String password);
    ClienteClass obtenerCliente(String id);
    ClienteClass obtenerCliente();
    boolean modificarMesaCliente(String idMesa);
    boolean modificarClientePassword(String password);

    /*Login*/
    void crearTablaSesion();
    void insertarSesion(String id, String nick,String password,String idRestaurante, String tipo,String token);
    ArrayList<String> obtenerSesion();
    void modificarSesion(String nick,String password,String idRestaurante);
    boolean modificarSesionPassword(String password);
    void eliminarSesion();
    void eliminarCliente();

    /*Token*/
    void crearTablaToken();
    boolean insertarToken(String token);
    boolean actualizarTokenEmpelado(ArrayList<String> sesion, String token);
    void actualizarToken(ArrayList<String> sesion);
    String obtenerToken();

    /*Especiales*/
    void crearTablaEspecial();
    boolean insertarEspecial(String id, String titulo, String producto, String precio, String activo);
    ArrayList<SugerenciaClass> obtenerEspeciales();
    SugerenciaClass obtenerEspecialActivo();
    boolean modificarActivoEspecial(String id,boolean activo);
    boolean modificarEspecial(String id,String titulo, String producto, String precio);
    boolean eliminarEspecial(String id);
    boolean insertarEspecialApi(final String titulo, final String producto, final String precio, final String activo);

    /*Ubicacion*/
    void modificarUbicacion(LatLng latLng,ArrayList<String> restaurante);

    //public void insertarMesa();
}
