package com.example.carlos.comandual;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AnalisisMesasFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AnalisisMesasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnalisisMesasFragment extends Fragment implements View.OnClickListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_FECHAIN = "fechaIn";
    private static final String ARG_FECHAFIN = "fechaFin";
    private static final String ARG_HORAIN = "horaIn";
    private static final String ARG_HORAFIN = "horaFin";
    private static final String ARG_MESA = "mesa";

    private String fechaIn;
    private String fechaFin;
    private String horaIn;
    private String horaFin;
    private int mesa;

    private Spinner spinnerMesa;
    private DataBaseManager manager;

    private OnFragmentInteractionListener mListener;

    private ArrayList<ComandaClass> comandas;
    private Map<Integer,Integer> horas;
    private Map<String,Integer> fechas;
    private Map<String,Integer> productos;

    private ArrayList<ArrayList<String>> arrMesas;

    private TextView tv_comandasGeneradas;
    private TextView tv_fechasolicitada;
    private TextView tv_horaSolicitada;
    private TextView tv_productoSolicitado;

    private ListView listView;

    private ToggleButton tb_tipo;
    private Spinner spinnerFiltro;

    public AnalisisMesasFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param fechaIn Parameter 1.
     * @param fechaFin Parameter 2.
     * @param horaIn Parameter 3.
     * @param horaFin Parameter 4.
     * @param mesa Parameter 5.
     * @return A new instance of fragment AnalisisMesasFragment.
     */
    public static AnalisisMesasFragment newInstance(String fechaIn, String fechaFin, String horaIn, String horaFin, int mesa) {
        AnalisisMesasFragment fragment = new AnalisisMesasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FECHAIN, fechaIn);
        args.putString(ARG_FECHAFIN, fechaFin);
        args.putString(ARG_HORAIN, horaIn);
        args.putString(ARG_HORAFIN, horaFin);
        args.putInt(ARG_MESA, mesa);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fechaIn = getArguments().getString(ARG_FECHAIN);
            fechaFin = getArguments().getString(ARG_FECHAFIN);
            horaIn = getArguments().getString(ARG_HORAIN);
            horaFin = getArguments().getString(ARG_HORAFIN);
            mesa = getArguments().getInt(ARG_MESA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_analisis_mesas, container, false);
        manager = new DataBaseManager(v.getContext());
        spinnerMesa = (Spinner) v.findViewById(R.id.spinner_producto);
        tv_comandasGeneradas = (TextView) v.findViewById(R.id.tv_comandasGeneradas);
        tv_fechasolicitada = (TextView) v.findViewById(R.id.tv_fechasolicitada);
        tv_horaSolicitada = (TextView) v.findViewById(R.id.tv_fechaMenosSolicitada);
        tv_productoSolicitado = (TextView) v.findViewById(R.id.tv_mesaSolicitada);

        ImageButton btn_fecha = (ImageButton) v.findViewById(R.id.btn_fecha);
        btn_fecha.setOnClickListener(this);

        ImageButton btn_hora = (ImageButton) v.findViewById(R.id.btn_hora);
        btn_hora.setOnClickListener(this);

        ImageButton btn_producto = (ImageButton) v.findViewById(R.id.btn_producto);
        btn_producto.setOnClickListener(this);


        arrMesas = manager.obtenerMesas();
        ArrayList<String> nombreMesas = new ArrayList<>();
        for(ArrayList<String> mesa:arrMesas){
            nombreMesas.add(mesa.get(5));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,nombreMesas);
        spinnerMesa.setAdapter(adapter);
        spinnerMesa.setSelection(mesa);
        spinnerMesa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                obtenerTotalComandas();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*spinnerMesa.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerTotalComandas();
            }
        });*/

        return v;
    }

    public void obtenerTotalComandas(){
        horas = new TreeMap<>();
        fechas = new TreeMap<>();
        productos = new TreeMap<>();
        comandas = new ArrayList<>();

        String get_comandas = DataBaseManager.ip+"comandas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        if(comandas.size()==0){
            request = new StringRequest(Request.Method.GET, get_comandas, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONArray array = new JSONArray(response);
                        JSONObject object;
                        for(int i = 0 ; i < array.length();i++){
                            object = array.getJSONObject(i);

                            String rest = object.getString("user");
                            if(rest.equals(DataBaseManager.restaurante)) {
                                String id = object.getString("id");
                                String mesa = object.getString("mesa");
                                String producto = object.getString("producto");
                                String categoria = object.getString("categoria");
                                String estado = object.getString("estado");
                                String cantidad = object.getString("cantidad");
                                String comentario = object.getString("comentario");
                                String token = object.getString("token");
                                String hora = object.getString("hora");
                                String fecha = object.getString("fecha");

                                boolean insert = true;
                                String idMesa = arrMesas.get(spinnerMesa.getSelectedItemPosition()).get(0);

                                if(mesa.equals(idMesa)) {

                                    //Hora de la comanda
                                    StringTokenizer tokenizerHoraApi = new StringTokenizer(hora, ":");
                                    Integer horaC = Integer.parseInt(tokenizerHoraApi.nextToken());
                                    Integer minutoC = Integer.parseInt(tokenizerHoraApi.nextToken());

                                    //Hora de inicio en el filtro
                                    StringTokenizer tokenizerHoraIn = new StringTokenizer(horaIn, ":");
                                    Integer horaIn = Integer.parseInt(tokenizerHoraIn.nextToken());
                                    Integer minutoIn = Integer.parseInt(tokenizerHoraIn.nextToken());

                                    //Hora de fin en el filtro
                                    StringTokenizer tokenizerHoraFin = new StringTokenizer(horaFin, ":");
                                    Integer horaFin = Integer.parseInt(tokenizerHoraFin.nextToken());
                                    Integer minutoFin = Integer.parseInt(tokenizerHoraFin.nextToken());

                                    if (horaIn != 0 || minutoIn != 0) {
                                        if (horaIn > horaC) {
                                            insert = false;
                                        } else if (horaIn.equals(horaC)) {
                                            if (minutoIn > minutoC) {
                                                insert = false;
                                            }
                                        }
                                    }
                                    if (horaFin != 0 || minutoIn != 0) {
                                        if (horaFin < horaC) {
                                            insert = false;
                                        } else if (horaFin.equals(horaC)) {
                                            if (minutoFin < minutoC) {
                                                insert = false;
                                            }
                                        }
                                    }
                                }else {
                                    insert = false;
                                }

                                //Fecha de la comanda
                                StringTokenizer tokenizerFechaApi = new StringTokenizer(fecha,"-");
                                Integer diaApi = Integer.parseInt(tokenizerFechaApi.nextToken());
                                Integer mesApi = Integer.parseInt(tokenizerFechaApi.nextToken());
                                Integer añoApi = Integer.parseInt(tokenizerFechaApi.nextToken());

                                //Fecha de inicio
                                StringTokenizer tokenizerFechaIn = new StringTokenizer(fechaIn,"-");
                                Integer diaIn = Integer.parseInt(tokenizerFechaIn.nextToken());
                                Integer mesIn = Integer.parseInt(tokenizerFechaIn.nextToken());
                                Integer añoIn = Integer.parseInt(tokenizerFechaIn.nextToken());

                                //Fecha de fin
                                StringTokenizer tokenizerFechaFin = new StringTokenizer(fechaFin,"-");
                                Integer diaFin = Integer.parseInt(tokenizerFechaFin.nextToken());
                                Integer mesFin = Integer.parseInt(tokenizerFechaFin.nextToken());
                                Integer añoFin = Integer.parseInt(tokenizerFechaFin.nextToken());

                                if(añoIn>añoApi||añoApi>añoFin){
                                    insert = false;
                                }else if(añoIn.equals(añoApi)||añoFin.equals(añoApi)){
                                    if(añoIn.equals(añoApi)&&mesIn>mesApi){
                                        insert = false;
                                    }else if(añoIn.equals(añoApi)&&mesApi>mesFin){
                                        insert = false;
                                    }else if(mesIn.equals(mesApi)||mesApi.equals(mesFin)){
                                        if(mesIn.equals(mesApi)&&diaIn<diaApi){
                                            insert = false;
                                        }else if(mesIn.equals(mesApi)&&diaApi>diaFin){
                                            insert = false;
                                        }
                                    }
                                }

                                if(insert){
                                    ComandaClass comanda = new ComandaClass(id,mesa,producto,comentario,estado,categoria,Integer.parseInt(cantidad),fecha,hora,token);
                                    comandas.add(comanda);

                                    insertarFecha(comanda.getFecha());
                                    insertarHora(comanda.getHora());
                                    insertarProducto(comanda.getProducto());
                                }
                            }
                        }
                        tv_comandasGeneradas.setText(String.valueOf(comandas.size()));

                        String hora = obtenerHoras(true);
                        tv_horaSolicitada.setText(hora);

                        String fecha = obtenerFecha(true);
                        tv_fechasolicitada.setText(fecha);

                        String producto = obtenerProducto(true);
                        tv_productoSolicitado.setText(producto);

                    }catch (Exception e){
                        String err = e.toString();
                        Log.e("Error",err);
                    }
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                    String err = error.toString();
                    Log.e("Error",err);
                }
            });
            //request.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequestQueue.add(request);
        }
    }

    public void insertarFecha(String fecha){
        if(fechas.containsKey(fecha)){
            int x = fechas.get(fecha);
            fechas.put(fecha,x+1);
        }else{
            fechas.put(fecha,1);
        }
    }

    public void insertarHora(String time){
        StringTokenizer timeToken = new StringTokenizer(time,":");
        int hora = Integer.parseInt(timeToken.nextToken());
        if(horas.containsKey(hora)){
            int x = horas.get(hora);
            horas.put(hora,x+1);
        }else{
            horas.put(hora,1);
        }
    }

    public void insertarProducto(String producto){
        if(productos.containsKey(producto)){
            int x = productos.get(producto);
            productos.put(producto,x+1);
        }else{
            productos.put(producto,1);
        }
    }

    public String obtenerFecha(boolean mayor){
        String fecha="";
        int cant = -1;
        Set<Map.Entry<String, Integer>> entry= fechas.entrySet();
        for (Map.Entry<String, Integer> ent : entry) {
            if (mayor) {
                if (cant < ent.getValue()) {
                    cant = ent.getValue();
                    fecha = ent.getKey();
                }
            } else {
                if (cant > ent.getValue() || cant == -1) {
                    cant = ent.getValue();
                    fecha = ent.getKey();
                }
            }
        }
        return fecha + " ("+cant+")";
    }

    public String obtenerProducto(boolean mayor){
        String producto="";
        int cant = -1;
        Set<Map.Entry<String, Integer>> entry= productos.entrySet();
        for (Map.Entry<String, Integer> ent : entry) {
            if (mayor) {
                if (cant < ent.getValue()) {
                    cant = ent.getValue();
                    producto = ent.getKey();
                }
            } else {
                if (cant > ent.getValue() || cant == -1) {
                    cant = ent.getValue();
                    producto = ent.getKey();
                }
            }
        }
        producto = manager.obtenerProducto(Integer.parseInt(producto)).getNombre();
        return producto + " ("+cant+")";
    }

    public String obtenerHoras(boolean mayor){
        int hora = -1;
        int cant = -1;
        Set<Map.Entry<Integer, Integer>> entryMesa= horas.entrySet();
        for (Map.Entry<Integer, Integer> entry : entryMesa) {
            if (mayor) {
                if (cant < entry.getValue()) {
                    cant = entry.getValue();
                    hora = entry.getKey();
                }
            } else {
                if (cant > entry.getValue() || cant == -1) {
                    cant = entry.getValue();
                    hora = entry.getKey();
                }
            }
        }
        return hora+" (" + cant + ")";
    }

    public void popUpGrafica(final Context context,final String nombre){
        //true: grafica
        //false: lista

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_graph, null);
        final GraphView graph = (GraphView) v.findViewById(R.id.graph);

        tb_tipo = (ToggleButton) v.findViewById(R.id.toggleButton_tipo);
        tb_tipo.setChecked(true);

        final Spinner spinnerFiltro = (Spinner) v.findViewById(R.id.spinner_filtro);
        String[] filtro = new String[]{"Tres elementos","Cinco elementos","Diez elementos","Quince elementos","Todos los elementos"};
        spinnerFiltro.setAdapter(new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,filtro));
        spinnerFiltro.setSelection(1);

        switch (nombre) {
            case "Horas":
                obtenerGraficaHoras(graph);
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);

                break;
            case "Fechas": {
                obtenerGraficaFechas(graph);
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);

                break;
            }
            case "Productos": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerGraficaProductos(position,graph,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerGraficaProductos(spinnerFiltro.getSelectedItemPosition(),graph,true);
                        }else{
                            obtenerGraficaProductos(spinnerFiltro.getSelectedItemPosition(),graph,false);
                        }
                    }
                });
                break;
            }
        }

        builder.setTitle(nombre);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


            }
        }).setNeutralButton("Ver lista", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                popUpLista(context,nombre);
            }
        });
        builder.show();
    }

    public void popUpLista(final Context context,final String nombre){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_list, null);

        final ToggleButton tb_tipo = (ToggleButton) v.findViewById(R.id.toggleButton_tipo);
        tb_tipo.setChecked(true);
        spinnerFiltro = (Spinner) v.findViewById(R.id.spinner_filtro);
        String[] filtro = new String[]{"Tres elementos","Cinco elementos","Diez elementos","Quince elementos","Todos los elementos"};
        spinnerFiltro.setAdapter(new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,filtro));
        spinnerFiltro.setSelection(1);

        listView = (ListView)v.findViewById(R.id.lv_analisisProductos);

        switch (nombre) {
            case "Horas":
                obtenerListaHoras();
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);
                break;
            case "Fechas": {
                obtenerListaFechas();
                spinnerFiltro.setVisibility(View.GONE);
                tb_tipo.setVisibility(View.GONE);

                break;
            }
            case "Productos": {
                spinnerFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obtenerListaProductos(position,tb_tipo.isChecked());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tb_tipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            obtenerListaProductos(spinnerFiltro.getSelectedItemPosition(),true);
                        }else{
                            obtenerListaProductos(spinnerFiltro.getSelectedItemPosition(),false);
                        }
                    }
                });
                break;
            }
        }

        builder.setTitle(nombre);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


            }
        }).setNeutralButton("Ver gráfica", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                popUpGrafica(context,nombre);
            }
        });
        builder.show();
    }

    public void obtenerGraficaHoras(GraphView graph){
        graph.removeAllSeries();
        StringTokenizer time = new StringTokenizer(horaIn, ":");
        time.nextToken();
        time = new StringTokenizer(horaFin, ":");
        time.nextToken();

        String[] horizontal = new String[horas.size()];
        int[] ventas = new int[horas.size()];
        int i = 0;
        Set<Map.Entry<Integer, Integer>> setHora = horas.entrySet();
        for (Map.Entry<Integer, Integer> entry : setHora) {
            ventas[i] = entry.getValue();
            horizontal[i] = String.valueOf(entry.getKey());
            i++;
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);
    }

    public void obtenerListaHoras(){
        StringTokenizer time = new StringTokenizer(horaIn, ":");
        time.nextToken();
        time = new StringTokenizer(horaFin, ":");
        time.nextToken();

        ArrayList<String> horizontal = new ArrayList<>();
        ArrayList<Integer> ventas = new ArrayList<>();
        Set<Map.Entry<Integer, Integer>> setHora = horas.entrySet();
        for (Map.Entry<Integer, Integer> entry : setHora) {
            ventas.add(entry.getValue());
            horizontal.add(String.valueOf(entry.getKey()));
        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),horizontal,ventas);
        listView.setAdapter(mesasAdapter);
    }

    public void obtenerListaFechas(){
        StringTokenizer fecha = new StringTokenizer(fechaIn, "-");
        fecha.nextToken();
        int mesIn = Integer.parseInt(fecha.nextToken());
        Map<String, Integer> mapaMeses = new TreeMap<>();

        fecha = new StringTokenizer(fechaFin, "-");
        fecha.nextToken();
        int mesFin = Integer.parseInt(fecha.nextToken());

        int cantMeses;
        if (mesIn < mesFin) {
            cantMeses = mesFin - mesIn + 1;
        } else {
            cantMeses = mesIn - mesFin;
        }
        ArrayList<Integer> ventas = new ArrayList<>();
        ArrayList<String> meses = new ArrayList<>();
        for (int j = 0; j < cantMeses; j++) {
            mapaMeses.put(String.valueOf(mesIn), 0);
            switch (mesIn) {
                case 1:
                    meses.add("ene");
                    break;
                case 2:
                    meses.add("feb");
                    break;
                case 3:
                    meses.add("mar");
                    break;
                case 4:
                    meses.add("abr");
                    break;
                case 5:
                    meses.add("may");
                    break;
                case 6:
                    meses.add("jun");
                    break;
                case 7:
                    meses.add("jul");
                    break;
                case 8:
                    meses.add("ago");
                    break;
                case 9:
                    meses.add("sep");
                    break;
                case 10:
                    meses.add("oct");
                    break;
                case 11:
                    meses.add("nov");
                    break;
                case 12:
                    meses.add("dic");
                    break;
            }
            mesIn++;
            if (mesIn == 13) {
                mesIn = 1;
            }
        }
        Set<Map.Entry<String,Integer>> set = fechas.entrySet();
        Iterator<Map.Entry<String,Integer>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            StringTokenizer tokenizerFecha = new StringTokenizer(entry.getKey(), "-");
            tokenizerFecha.nextToken();
            String tokenMes = tokenizerFecha.nextToken();
            if (mapaMeses.containsKey(tokenMes)) {
                int cant = mapaMeses.get(tokenMes);
                cant += entry.getValue();
                mapaMeses.put(tokenMes, cant);
            }
        }
        set = mapaMeses.entrySet();
        iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            ventas.add(entry.getValue());
        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),meses,ventas);
        listView.setAdapter(mesasAdapter);
    }

    public void obtenerGraficaFechas(GraphView graph){
        graph.removeAllSeries();
        StringTokenizer fecha = new StringTokenizer(fechaIn, "-");
        fecha.nextToken();
        int mesIn = Integer.parseInt(fecha.nextToken());
        Map<String, Integer> mapaMeses = new TreeMap<>();

        fecha = new StringTokenizer(fechaFin, "-");
        fecha.nextToken();
        int mesFin = Integer.parseInt(fecha.nextToken());

        int cantMeses;
        if (mesIn < mesFin) {
            cantMeses = mesFin - mesIn + 1;
        } else {
            cantMeses = mesIn - mesFin;
        }
        String[] horizontal = new String[cantMeses + 1];
        int[] ventas = new int[horizontal.length];
        for (int j = 0; j < cantMeses; j++) {
            mapaMeses.put(String.valueOf(mesIn), 0);
            switch (mesIn) {
                case 1:
                    horizontal[j] = "ene";
                    break;
                case 2:
                    horizontal[j] = "feb";
                    break;
                case 3:
                    horizontal[j] = "mar";
                    break;
                case 4:
                    horizontal[j] = "abr";
                    break;
                case 5:
                    horizontal[j] = "may";
                    break;
                case 6:
                    horizontal[j] = "jun";
                    break;
                case 7:
                    horizontal[j] = "jul";
                    break;
                case 8:
                    horizontal[j] = "ago";
                    break;
                case 9:
                    horizontal[j] = "sep";
                    break;
                case 10:
                    horizontal[j] = "oct";
                    break;
                case 11:
                    horizontal[j] = "nov";
                    break;
                case 12:
                    horizontal[j] = "dic";
                    break;
            }
            mesIn++;
            if (mesIn == 13) {
                mesIn = 1;
            }
        }
        Set<Map.Entry<String, Integer>> set = fechas.entrySet();
        Iterator<Map.Entry<String, Integer>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            StringTokenizer tokenizerFecha = new StringTokenizer(entry.getKey(), "-");
            tokenizerFecha.nextToken();
            String tokenMes = tokenizerFecha.nextToken();
            if (mapaMeses.containsKey(tokenMes)) {
                int cant = mapaMeses.get(tokenMes);
                cant += entry.getValue();
                mapaMeses.put(tokenMes, cant);
            }
        }
        set = mapaMeses.entrySet();
        iterator = set.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            ventas[i] = entry.getValue();
            i++;
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        //staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);
    }

    public void obtenerGraficaProductos(int numElement,GraphView graph, boolean orden){
        graph.removeAllSeries();
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = productos.size();
                break;
        }

        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Integer> arrVentas = new ArrayList<>();
        Map<String, Integer> auxProductos = new TreeMap<>();
        auxProductos.putAll(productos);
        Set<Map.Entry<String, Integer>> setEntry = auxProductos.entrySet();
        for (int i = 0; i < numElement; i++) {
            String pro = "";
            String idPro = "";
            int cantPro;

            if(orden){
                cantPro = 0;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() > cantPro) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }else{
                cantPro = -1;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() < cantPro || cantPro== -1) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }
            if(!idPro.equals("")){
                auxProductos.remove(idPro);
                arrHorizontal.add(pro);
                arrVentas.add(cantPro);
            }


        }
        String[] horizontal;
        int[] ventas;
        if(arrHorizontal.size()<2){
            horizontal = new String[2];
        }else{
            horizontal = new String[arrHorizontal.size()];
        }
        for(int i = 0 ; i < arrHorizontal.size(); i++){
            horizontal[i] = arrHorizontal.get(i);
        }
        if(arrVentas.size()<2){
            ventas = new int[2];
        }else{
            ventas = new int[arrVentas.size()];
        }
        for(int i = 0 ; i < arrVentas.size(); i++){
            ventas[i] = arrVentas.get(i);
        }

        DataPoint[] datas = new DataPoint[horizontal.length];
        for (int j = 0 ; j < horizontal.length; j++){
            datas[j] = new DataPoint(j,ventas[j]);
        }

        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(horizontal);
        //staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datas);
        graph.addSeries(series);

    }

    public void obtenerListaProductos(int numElement, boolean orden){
        switch (numElement){
            case 0:
                numElement = 3;
                break;
            case 1:
                numElement = 5;
                break;
            case 2:
                numElement = 10;
                break;
            case 3:
                numElement = 15;
                break;
            case 4:
                numElement = productos.size();
                break;
        }
        ArrayList<String> arrHorizontal = new ArrayList<>();
        ArrayList<Integer> arrVentas = new ArrayList<>();
        Map<String, Integer> auxProductos = new TreeMap<>();
        auxProductos.putAll(productos);
        Set<Map.Entry<String, Integer>> setEntry = auxProductos.entrySet();
        for (int i = 0; i < numElement; i++) {
            String pro = "";
            String idPro = "";
            int cantPro;

            if(orden){
                cantPro = 0;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() > cantPro) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }else{
                cantPro = -1;
                for (Map.Entry<String, Integer> entry : setEntry) {
                    if (entry.getValue() < cantPro || cantPro== -1) {
                        idPro = entry.getKey();
                        pro = manager.obtenerProducto(Integer.parseInt(entry.getKey())).getNombre();
                        cantPro = entry.getValue();
                    }
                }
            }
            if(!idPro.equals("")){
                auxProductos.remove(idPro);
                arrHorizontal.add(pro);
                arrVentas.add(cantPro);
            }


        }

        AnalisisAdapter mesasAdapter = new AnalisisAdapter(getActivity(),arrHorizontal,arrVentas);
        listView.setAdapter(mesasAdapter);

    }



    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_hora:
                popUpGrafica(v.getContext(),"Horas");
                break;
            case R.id.btn_fecha:
                popUpGrafica(v.getContext(),"Fechas");
                break;
            case R.id.btn_producto:
                popUpGrafica(v.getContext(),"Productos");
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
