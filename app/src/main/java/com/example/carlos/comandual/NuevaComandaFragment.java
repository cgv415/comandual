package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.TreeMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NuevaComandaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NuevaComandaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */


/*No se usa, se usa Nueva Comanda Activity*/
public class NuevaComandaFragment extends Fragment implements View.OnClickListener {
    private AutoCompleteTextView autoCompleteIngredientes;
    private AutoCompleteTextView autocompleteComida;

    private ArrayList<Producto> arrayProductos;
    private ArrayList<String> categorias;
    private ArrayList<Producto> arrayPedido;
    private ArrayList<String> ingredientes_producto;
    private ArrayList<String> arrayIngredientes;
    private ArrayList<String> arrayAdapterListView;
    private ArrayList<CheckBox> checkBoxesIngredientes;
    private ArrayList<CheckBox> ingredientesIncluidos;
    private ArrayList<CheckBox> ingredientesEliminados;


    private Button ingredientes;
    private Button button;
    private Button search;
    private Button confirmar;

    private CheckBox media;

    private DataBaseManager manager;

    private ElegirProductoAdapter adapter;

    private IngredientesAdapter ingredientesAdapter;

    private NumberPicker cantidad;

    private String categoria;
    //private String producto;

    private Spinner spinner;

    private ListView listViewIngredientes;

    private int idMesa;

    private OnFragmentInteractionListener mListener;

    //private GridView gridView;

    public NuevaComandaFragment() {
        // Required empty public constructor
    }

    /**
     * @param idMesa Parameter 1.
     * @return A new instance of fragment NuevaComandaFragment.
     */
    public static NuevaComandaFragment newInstance(int idMesa) {
        NuevaComandaFragment fragment = new NuevaComandaFragment();
        Bundle args = new Bundle();
        args.putInt("idMesa", idMesa);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idMesa = getArguments().getInt("idMesa");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v  = inflater.inflate(R.layout.fragment_nueva_comanda, container, false);

        manager = new DataBaseManager(v.getContext());
        ArrayList<String> listaComida = manager.obtenerNombreProductos();

        arrayPedido = new ArrayList<>();
        adapter = new ElegirProductoAdapter(getActivity(), arrayPedido);
        //ArrayAdapter<CheckBox> adapter1 = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_checked,arrayPedido);
        ListView listViewProductos = (ListView) v.findViewById(R.id.listViewElegirProducto);
        listViewProductos.setAdapter(adapter);

        listViewProductos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, final int position, long id) {
                new android.support.v7.app.AlertDialog.Builder(getContext())
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Eliminar Producto")
                        .setMessage("Estas seguro de que deseas eliminar el producto " + arrayPedido.get(position) + "? ")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                arrayPedido.remove(position);
                                adapter.notifyDataSetChanged();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                return false;
            }
        });

        listViewProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Producto producto = arrayPedido.get(position);
                popUpDetalles(producto);
            }
        });

        autocompleteComida = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteComida);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,listaComida);
        autocompleteComida.setAdapter(adapter);
        autocompleteComida.setThreshold(1);
        autocompleteComida.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                String name = autocompleteComida.getText().toString();
                Producto producto = manager.obtenerProducto(name);

                popUpDetalles(producto);
                autocompleteComida.setText("");
            }
        });
        //search = (Button) v.findViewById(R.id.btn_search);

        confirmar = (Button) v.findViewById(R.id.btnConfirmarComanda);
        /*confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0;i<arrayPedido.size();i++){
                    Producto producto = arrayPedido.get(i);
                    int id = manager.obtenerUlitmoIdComanda();
                    manager.insertarComanda(String.valueOf(id),String.valueOf(idMesa),String.valueOf(producto.getId()),producto.getTipo(),producto.getAlteracionIngredientes(),"En cocina",String.valueOf(producto.getCantidad()));
                }
            }
        });*/

        return v;
    }

    public void popUpDetalles(final Producto producto){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_detalle_producto, null);

        cantidad = (NumberPicker) v.findViewById(R.id.number_cantidad);
        float oldCantidad = producto.getCantidad();
        int old = Math.round(oldCantidad);
        cantidad.setMinValue(0);
        cantidad.setMaxValue(50);
        if(old>0){
            cantidad.setValue(old);
        }else{
            cantidad.setValue(1);
        }

        ingredientes = (Button) v.findViewById(R.id.btnIngredientes);
        ingredientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpIngredientes(producto);
            }
        });

        builder.setTitle(producto.getNombre());

        builder.setView(v);

        if(!producto.getStock()){
            Toast.makeText(v.getContext(),"Este producto no se encuentra disponible",Toast.LENGTH_SHORT).show();
            cantidad.setValue(0);
            cantidad.setEnabled(false);
            ingredientes.setEnabled(false);
            spinner.setEnabled(false);
            media.setEnabled(false);

        }
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(producto.getStock()){
                    float cant = cantidad.getValue();
                    if(media.isChecked()){
                        cant += 0.5;
                    }
                    producto.setTipo(spinner.getSelectedItem().toString());
                    int pos = arrayPedido.indexOf(producto);
                    if(!arrayPedido.contains(producto)){
                        producto.setCantidad(cant);
                        arrayPedido.add(producto);
                    }else{
                        arrayPedido.get(pos).setCantidad(cant);
                    }

                    if(cant == 0){
                        if(pos < 0){
                            arrayPedido.remove(0);
                        }else{
                            arrayPedido.remove(pos);
                        }
                    }

                    adapter.notifyDataSetChanged();
                }
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void popUpIngredientes(final Producto producto){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_ingredientes_producto, null);

        ingredientesEliminados = new ArrayList<>();
        ingredientesIncluidos = new ArrayList<>();
        ingredientes_producto = manager.obtenerProducto_Ingredientes(producto.getNombre());
        arrayIngredientes = manager.obtenerIngredientes();
        arrayAdapterListView = new ArrayList<>();
        arrayAdapterListView.addAll(arrayIngredientes);

        listViewIngredientes = (ListView) v.findViewById(R.id.listViewIngredientes);
        ingredientesAdapter = new IngredientesAdapter(getActivity(),arrayAdapterListView,ingredientes_producto/*,ingredientesIncluidos,ingredientesEliminados*/);
        listViewIngredientes.setAdapter(ingredientesAdapter);
        listViewIngredientes.setItemsCanFocus(true);

        final ArrayAdapter adapterIngredientes = new ArrayAdapter(v.getContext(), R.layout.support_simple_spinner_dropdown_item,new ArrayList());
        autoCompleteIngredientes = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteIngredientes);
        autoCompleteIngredientes.setHint("Buscar Ingrediente");
        autoCompleteIngredientes.setAdapter(adapterIngredientes);

        autoCompleteIngredientes.setThreshold(1);
        autoCompleteIngredientes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String aux = s.toString();
                aux = aux.toLowerCase();
                s = aux;
                actualizarLista(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        builder.setTitle(producto.getNombre());
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArrayList<CheckBox> arrayList = ingredientesAdapter.getCheckBoxArrayList();

                String ingrediente;
                String cadena = "";
                for(int i = 0 ; i < arrayList.size() ; i++){
                    CheckBox checkBox = arrayList.get(i);
                    if(checkBox.isChecked()){
                        ingredientesIncluidos.add(checkBox);
                    }else{
                        ingredientesEliminados.add(checkBox);
                    }
                }

                for(int i = 0 ; i < ingredientesIncluidos.size();i++){
                    ingrediente = ingredientesIncluidos.get(i).getText().toString();
                    if(!ingredientes_producto.contains(ingrediente)){
                        ingredientes_producto.add(ingrediente);
                        cadena += "Con "+ ingrediente + " ";
                    }
                }
                for(int i = 0; i < ingredientesEliminados.size();i++){
                    ingrediente = ingredientesEliminados.get(i).getText().toString();
                    if(ingredientes_producto.contains(ingrediente)){
                        ingredientes_producto.remove(ingrediente);
                        cadena += "Sin " + ingrediente +" ";
                    }
                }
                producto.setAlteracionIngredientes(cadena);
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    private ArrayList<Producto> obtenerProductos(String categoria){
        ArrayList<Producto> productos = new ArrayList<>();

        ArrayList<ArrayList<String>> array = manager.obtenerProductosPorCategoria(categoria);

        for(int i = 0; i < array.size();i++){
            productos.add(new Producto(Integer.parseInt(array.get(i).get(0)), array.get(i).get(1), array.get(i).get(2), 0));
        }

        return productos;
    }

    public void actualizarLista(CharSequence s){
        ArrayList<String> array = new ArrayList<>();

        if(s.toString().equals("")){
            array = manager.obtenerIngredientes();
        }else{
            for(int i = 0;i<arrayAdapterListView.size();i++){
                if(arrayAdapterListView.get(i).contains(s)){
                    array.add(arrayAdapterListView.get(i));
                }
            }
        }

        arrayAdapterListView.clear();
        arrayAdapterListView.addAll(array);
        ingredientesAdapter.notifyDataSetChanged();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnConfirmarComanda:
                Log.d("Estado","Confirmar Comanda");
                break;
            case R.id.btnIngredientes:
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


}
