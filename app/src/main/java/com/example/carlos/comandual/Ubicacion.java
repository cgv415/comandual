package com.example.carlos.comandual;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.StringBuilderPrinter;
import android.widget.Toast;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 29/03/2017
 */

public class Ubicacion implements LocationListener {
    private Context context;
    LocationManager locationManager;
    String provider;
    private boolean networkOn;

    public Ubicacion(Context context){
        this.context = context;
        locationManager =(LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        provider = LocationManager.NETWORK_PROVIDER;
        if (ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            networkOn = locationManager.isProviderEnabled(provider);
            locationManager.requestLocationUpdates(provider,1000,1,this);
        }

    }

    private void getLocation(){
        if(networkOn){

            if (ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                Location lc = locationManager.getLastKnownLocation(provider);
                if(lc != null){
                    StringBuilder builder = new StringBuilder();
                    builder.append("Altitud: ")
                            .append(lc.getAltitude()).append("Longitud: ")
                            .append(lc.getLongitude());

                    Toast.makeText(context,builder.toString(),Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        //El proveedor está activado
    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
