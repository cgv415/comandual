package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StockFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StockFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StockFragment extends Fragment {

    private DataBaseManager manager;
    private int tab;

    private StockAdapter adapter;

    private Spinner spinnerCategoria;

    private ArrayAdapter<String> categoriasAdapter;
    private ArrayAdapter<String> ingredientesProductoAdapter;

    private ArrayList<String> arrayAdapter;
    private ArrayList<String> productos;
    private ArrayList<String> ingredientes;
    private ArrayList<String> completeIngredientes;
    private ArrayList<String> categorias;

    private ArrayAdapter adapterAutoComplete;
    private ListView listView;
    private AutoCompleteTextView autoCompleteProductos;
    private FloatingActionButton fab;

    private MultiAutoCompleteTextView complete_ingredientes;



    private OnFragmentInteractionListener mListener;

    public StockFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment StockFragment.
     */
    public static StockFragment newInstance(int tab) {
        StockFragment fragment = new StockFragment();
        Bundle args = new Bundle();
        args.putInt("tab", tab);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tab = getArguments() != null ? getArguments().getInt("tab") : 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        if(tab == 0){
            //Tab Productos
            final View v = inflater.inflate(R.layout.fragment_productos, container, false);
            manager = new DataBaseManager(v.getContext());
            generarProductos(v);
            return v;

        }else {
            //Tab Ingredientes
            final View v = inflater.inflate(R.layout.fragment_ingredientes, container, false);
            manager = new DataBaseManager(v.getContext());
            generarIngredientes(v);
            return v;
        }
    }

    public void generarProductos(final View v){
        manager.crearTablaProducto_Ingrediente();
        manager.crearTablaProducto();

        String get_productos = DataBaseManager.ip+"productos/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_productos, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if(rest.equals(DataBaseManager.restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String descripcion = object.getString("descripcion");
                            String categoria = object.getString("categoria");
                            String precio = object.getString("precio");
                            String stock = object.getString("stock");


                            JSONArray ingredientes = new JSONArray(object.getString("ingredientes"));
                            for(int j = 0 ; j < ingredientes.length();j++) {
                                manager.insertarProducto_Ingrediente(id,ingredientes.getString(j));

                            }
                            manager.insertarProducto_Categoria(id,categoria);
                            manager.insertarProducto(id,nombre, descripcion, stock,categoria,precio);


                        }
                    }
                    pintarProductos(v);
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }

    public void pintarProductos(final View v){
        fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popUpProducto();
            }
        });

        if(DataBaseManager.tipoUsuario.equals("cliente")){
            fab.setVisibility(View.GONE);
        }

        manager = new DataBaseManager(v.getContext());
        productos = manager.obtenerNombreProductos();
        arrayAdapter = productos;
        adapterAutoComplete = new ArrayAdapter(v.getContext(),R.layout.support_simple_spinner_dropdown_item,new ArrayList());

        autoCompleteProductos = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteProductos);
        autoCompleteProductos.setAdapter(adapterAutoComplete);
        autoCompleteProductos.setThreshold(1);
        autoCompleteProductos.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String aux = s.toString();
                aux = aux.toLowerCase();
                s = aux;
                actualizarListaProductos(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        adapter = new StockAdapter(getActivity(),arrayAdapter,"productos");
        //ArrayAdapter adapterListView = new ArrayAdapter(v.getContext(),R.layout.support_simple_spinner_dropdown_item,arrayAdapter);
        listView = (ListView) v.findViewById(R.id.listProductos);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(v.getContext(),DetalleProductoActivity.class);
                intent.putExtra("producto",arrayAdapter.get(position));
                startActivity(intent);

            }
        });

    }

    public void generarIngredientes(final View v){
        manager.crearTablaIngrediente();

        String get_ingredientes = DataBaseManager.ip+"ingredientes/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_ingredientes, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);

                        String rest = object.getString("user");
                        if(rest.equals(DataBaseManager.restaurante)) {
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String stock = object.getString("stock");
                            manager.insertarIngrediente(id, nombre, stock);
                        }
                    }
                    pintarIngredientes(v);
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }

    public void pintarIngredientes(final View v){
        fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popUpIngrediente(false);
            }
        });

        if(DataBaseManager.tipoUsuario.equals("cliente")){
            fab.setVisibility(View.GONE);
        }

        ingredientes = manager.obtenerIngredientes();
        arrayAdapter = ingredientes;
        adapterAutoComplete = new ArrayAdapter(v.getContext(),R.layout.support_simple_spinner_dropdown_item,new ArrayList());

        autoCompleteProductos = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteIngredientes);
        autoCompleteProductos.setAdapter(adapterAutoComplete);
        autoCompleteProductos.setThreshold(1);
        autoCompleteProductos.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String aux = s.toString();
                aux = aux.toLowerCase();
                s = aux;
                actualizarListaIngredientes(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        adapter = new StockAdapter(getActivity(),arrayAdapter,"ingredientes");
        listView = (ListView) v.findViewById(R.id.listViewIngredientes);
        listView.setAdapter(adapter);

        if(DataBaseManager.tipoUsuario.equals("empleado")){
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final String nombre = arrayAdapter.get(position);
                    popUpModificarIngrediente(nombre);
                        /*if(manager.inStrockIngrediente(nombre)){
                            new AlertDialog.Builder(v.getContext())
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle(nombre)
                                    .setMessage("Desea seleccionar el ingrediente " + nombre + " como FUERA de stock?")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            manager.modificarStockIngrediente(nombre,false);
                                            adapter.notifyDataSetChanged();
                                        }

                                    })
                                    .setNegativeButton("No", null)
                                    .show();

                        }else{
                            new AlertDialog.Builder(v.getContext())
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle(nombre)
                                    .setMessage("Desea seleccionar el ingrediente " + nombre + " como DISPONIBLE en stock?")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            manager.modificarStockIngrediente(nombre,true);
                                            adapter.notifyDataSetChanged();
                                        }

                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                        }*/
                }
            });
        }


    }

    public void popUpModificarIngrediente(String nombre){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final ArrayList<String> ingrediente = manager.obtenerIngredienteByNombre(nombre);
        final boolean stock = manager.inStrockIngrediente(nombre);

        final View v = inflater.inflate(R.layout.content_detalle_ingrediente, null);

        final EditText etNombreIngrediente = (EditText) v.findViewById(R.id.et_nombre);
        etNombreIngrediente.setText(ingrediente.get(1));

        final CheckBox stockIngrediente = (CheckBox) v.findViewById(R.id.checkBoxStockIngrediente);
        stockIngrediente.setChecked(stock);

        builder.setTitle("Modificar ingrediente");
        builder.setView(v);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //manager.modificarStockIngrediente(ingrediente.get(1),stock);

                ingrediente.set(1,DataBaseManager.removeAcentos(etNombreIngrediente.getText().toString()));
                manager.modificarIngrediente(ingrediente,stockIngrediente.isChecked());
                Toast.makeText(getContext(),"Ingrediente modificado",Toast.LENGTH_SHORT).show();
                actualizarListaIngredientes("");
                //multiCompleteIngredientes.setText(complete_ingredientes.getText().toString());
            }
        })
                .setNegativeButton("Eliminar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        manager.eliminarIngrediente(ingrediente.get(0));
                        Toast.makeText(getContext(),"Ingrediente eliminado",Toast.LENGTH_SHORT).show();
                        actualizarListaIngredientes("");
                    }
                });
        builder.show();
    }

    public void actualizarCategorias(){
        Toast.makeText(getContext(),"Categoria insertada",Toast.LENGTH_SHORT).show();
        ArrayList<String> array;
        array = manager.obtenerCategorias();
        categorias.clear();
        categorias.addAll(array);
        categoriasAdapter.notifyDataSetChanged();
    }

    public void actualizarAutoCompleteIngredientesProducto(){
        ArrayList<String> array;
        array = manager.obtenerIngredientes();
        //completeIngredientes = manager.obtenerIngredientes();
        completeIngredientes.clear();
        completeIngredientes.addAll(array);
        ingredientesProductoAdapter.notifyDataSetChanged();

    }

    public void actualizarListaIngredientes(CharSequence s){
        ArrayList<String> array = new ArrayList<>();

        if(s.toString().equals("")){
            array = manager.obtenerIngredientes();
        }else{
            for(int i = 0;i<ingredientes.size();i++){
                if(ingredientes.get(i).contains(s)){
                    array.add(ingredientes.get(i));
                }
            }
        }

        arrayAdapter.clear();
        arrayAdapter.addAll(array);
        adapter.notifyDataSetChanged();
    }

    public void actualizarListaProductos(CharSequence s){
        ArrayList<String> array = new ArrayList<>();

        if(s.toString().equals("")){
            array = manager.obtenerNombreProductos();
        }else{
            for(int i = 0;i<productos.size();i++){
                if(productos.get(i).contains(s)){
                    array.add(productos.get(i));
                }
            }
        }

        arrayAdapter.clear();
        arrayAdapter.addAll(array);
        adapter.notifyDataSetChanged();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void popUpIngrediente(final Boolean producto){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_ingrediente, null);

        final CheckBox checkBox = (CheckBox) v.findViewById(R.id.cb_stock);
        final EditText editText = (EditText) v.findViewById(R.id.et_nombre);

        builder.setTitle("Insertar ingrediente");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final String stock;
                if(checkBox.isChecked()){
                    stock = "1";
                }else{
                    stock = "0";
                }

                /*String ing = complete_ingredientes.getText().toString();
                ing +=editText.getText().toString()+", ";
                complete_ingredientes.setText(ing);*/
                /**/
                if(!existsIngrediente(editText.getText().toString())){
                    RequestQueue mRequestQueue;
                    mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                    String url = DataBaseManager.ip+"ingredientes/";
                    StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                    Log.d("Response", response);
                                    try {

                                        JSONObject object = new JSONObject(response);

                                        String id = object.getString("id");
                                        //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                                        manager.insertarIngrediente(id,editText.getText().toString(),stock);
                                        Toast.makeText(v.getContext(),"Ingrediente insertado",Toast.LENGTH_SHORT).show();
                                        if(producto){
                                            actualizarAutoCompleteIngredientesProducto();
                                            String ing = complete_ingredientes.getText().toString();
                                            ing +=editText.getText().toString()+", ";
                                            complete_ingredientes.setText(ing);
                                        }else{
                                            actualizarListaIngredientes("");
                                        }


                                    } catch (Throwable t) {
                                        Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                    }
                                    //String id = response.

                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", error.toString());
                                }
                            }
                    ) {

                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String>  params = new HashMap<>();
                            params.put("nombre",DataBaseManager.removeAcentos(editText.getText().toString()));
                            params.put("stock",stock);
                            params.put("user",DataBaseManager.restaurante);

                            return params;
                        }

                    };
                    mRequestQueue.add(putRequest);
                }else{
                    Toast.makeText(getContext(),"Ingrediente ya existente",Toast.LENGTH_SHORT).show();
                }

                /**/
                //manager.insertarIngredienteApi(editText.getText().toString(),stock);

            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public boolean existsIngrediente(String nombre){
        ArrayList<String> ingredientes = manager.obtenerIngredientes();
        Boolean exist = false;
        for (String ingrediente:ingredientes) {
            if(ingrediente.equals(nombre)){
                exist = true;
                break;
            }
        }
        return exist;
    }

    public void popUpCategoria(){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_categoria, null);

        final EditText editText = (EditText) v.findViewById(R.id.et_nombre);

        builder.setTitle("Insertar Categoria");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                /**/
                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip+"categorias/";
                StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                Log.d("Response", response);
                                try {

                                    JSONObject object = new JSONObject(response);
                                    String id = object.getString("id");
                                    String nombre = object.getString("nombre");

                                    manager.insertarCategoria(id,nombre);
                                    //db.insert(TABLE_MESA, null, generarValoresMesa(idMesa,nombre,capacidad, forma, estado, idEspacio));
                                    //boolean insert = manager.insertarCategoria(id,editText.getText().toString());
                                    actualizarCategorias();

                                } catch (Throwable t) {
                                    Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                }
                                //String id = response.

                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<> ();
                        params.put("nombre",DataBaseManager.removeAcentos(editText.getText().toString()));
                        params.put("user",DataBaseManager.restaurante);

                        return params;
                    }

                };
                mRequestQueue.add(putRequest);
                /**/

                //manager.insertarCategoriaApi(editText.getText().toString());


            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void popUpProducto(){
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_producto, null);

        final CheckBox checkBox = (CheckBox) v.findViewById(R.id.cb_stock);
        final EditText nombreProducto = (EditText) v.findViewById(R.id.et_nombre);
        final EditText descripcionProducto = (EditText) v.findViewById(R.id.et_descripcion);
        final EditText precioProducto = (EditText) v.findViewById(R.id.et_precio);
        spinnerCategoria = (Spinner) v.findViewById(R.id.spinner_categoria);

        categorias = manager.obtenerCategorias();
        categoriasAdapter = new ArrayAdapter(v.getContext(),R.layout.support_simple_spinner_dropdown_item,categorias);
        spinnerCategoria.setAdapter(categoriasAdapter);

        complete_ingredientes = (MultiAutoCompleteTextView) v.findViewById(R.id.autocomplete_ingredientes);

        completeIngredientes = manager.obtenerIngredientes();
        ingredientesProductoAdapter = new ArrayAdapter<>(v.getContext(),android.R.layout.simple_dropdown_item_1line, completeIngredientes);

        complete_ingredientes.setAdapter(ingredientesProductoAdapter);
        complete_ingredientes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        complete_ingredientes.setThreshold(1);

        ImageButton btn_ingrediente = (ImageButton) v.findViewById(R.id.btn_insertarIngredientes);
        ImageButton btn_categoria = (ImageButton) v.findViewById(R.id.btn_insertarCategoria);
        btn_ingrediente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpIngrediente(true);
            }
        });

        btn_categoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popUpCategoria();
            }
        });

        builder.setTitle("Insertar producto");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final String stock;
                final String nombre = nombreProducto.getText().toString();
                final String descripcion = descripcionProducto.getText().toString();
                final String precio = precioProducto.getText().toString();
                final String categoria = manager.obtenerCategoria(spinnerCategoria.getSelectedItem().toString()).get(0);

                final String nombreIngredientes = complete_ingredientes.getText().toString();
                final String[] arrayIngredientes = nombreIngredientes.split(", ");

                String aux = "[";
                try {
                    for (int i = 0; i < arrayIngredientes.length; i++) {
                        aux += manager.obtenerIngredienteByNombre(arrayIngredientes[i]).get(0);
                        if (i < arrayIngredientes.length - 1) {
                            aux += ",";
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(getContext(), "No se han seleccionado ingredientes", Toast.LENGTH_SHORT).show();
                }
                aux += "]";

                final String idIngredientes = aux;

                if (checkBox.isChecked()) {
                    stock = "1";
                } else {
                    stock = "0";
                }

                try {
                    RequestQueue mRequestQueue;

                    mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                    final JSONObject jsonBody = new JSONObject(
                            "{\"nombre\":\""+nombre+"\"," +
                            "\"descripcion\":\""+descripcion+"\"," +
                            "\"categoria\":" +categoria+ "," +
                            "\"stock\":" +stock+ "," +
                            "\"precio\":"+precio+"," +
                            "\"ingredientes\":"+idIngredientes+"," +
                            "\"user\":"+ DataBaseManager.restaurante +"}");

                    String url = DataBaseManager.ip+"productos/";

                    JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,url, jsonBody, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                String id = response.getString("id");
                                manager.insertarProducto(id,nombre,descripcion,stock,categoria,precio);
                                manager.insertarProducto_Categoria(id,categoria);
                                for (String arrayIngrediente : arrayIngredientes) {
                                    manager.insertarProducto_Ingrediente(id, arrayIngrediente);
                                }
                                Toast.makeText(v.getContext(),"Producto insertado",Toast.LENGTH_SHORT).show();
                                actualizarListaProductos("");

                            } catch (Throwable t) {
                                Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                Toast.makeText(v.getContext(),t.toString(),Toast.LENGTH_SHORT).show();

                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(),error.toString(),Toast.LENGTH_SHORT).show();
                        }
                    });
                    mRequestQueue.add(jsObjRequest);



                    //insertarProducto();

/*
                    RequestQueue mRequestQueue;
                    StringRequest request;

                    mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                    StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                    Log.d("Response", response);
                                    try {

                                        JSONObject object = new JSONObject(response);

                                        String id = object.getString("id");
                                        boolean insert = manager.insertarProducto(id,nombre,descripcion,stock,categoria,precio);
                                        manager.insertarProducto_Categoria(id,categoria);
                                        for(int i = 0; i < arrayIngredientes.length;i++){
                                            manager.insertarProducto_Ingrediente(id,arrayIngredientes[i]);
                                        }
                                        Toast.makeText(v.getContext(),"Producto insertado",Toast.LENGTH_SHORT).show();
                                        actualizarListaProductos("");

                                    } catch (Throwable t) {
                                        Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                        Toast.makeText(v.getContext(),t.toString(),Toast.LENGTH_SHORT).show();

                                    }
                                    //String id = response.

                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", error.toString());
                                    Toast.makeText(v.getContext(),error.getLocalizedMessage(),Toast.LENGTH_SHORT).show();

                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String>  params = new HashMap<String, String> ();

                            params.put("nombre",nombre);
                            params.put("descripcion",descripcion);
                            params.put("categoria",categoria);
                            params.put("tipo","1");
                            params.put("stock",stock);
                            params.put("precio",precio);
                            //params.put("ingredientes",idIngredientes);
                            params.put("user",manager.restaurante);
                            for(int i = 0; i < arrayIngredientes.length; i++){
                                params.put("ingredientes",manager.obtenerIngredienteByNombre(arrayIngredientes[i]).get(0));
                            }

                            //String body = "nombre=Limpioespin&user=8&user=8&descripcion=que%20si%20pasa&categoria=1&tipo=1&stock=1&precio=0&ingredientes=4&ingredientes=6";
                            //params.put("body",body);
                            return params;
                        }

                    };
                    mRequestQueue.add(putRequest);
                    */
                    //actualizarListaIngredientes("");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    /*public void insertarProducto(){
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "nombre=limpioespin&user=8&user=8&descripcion=que%20si%20pasa&categoria=1&tipo=1&stock=1&precio=0&ingredientes=4&ingredientes=6");
        com.squareup.okhttp.Request requesthttp;
        requesthttp = new com.squareup.okhttp.Request.Builder()
                .url("http://127.0.0.1:8000/api/productos/")
                .put(body)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("authorization", "key=AIzaSyDJVBSMhuAY3nE75Lg7h-Mr6dfQps-CWFQ")
                .addHeader("cache-control", "no-cache")
                .build();

        try {
            com.squareup.okhttp.Response responsehttp = client.newCall(requesthttp).execute();
            String resp = responsehttp.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}
