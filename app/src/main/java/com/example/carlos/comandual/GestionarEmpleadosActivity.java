package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GestionarEmpleadosActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DataBaseManager manager;
    EditText etUsuario;
    EditText etNombre;
    EditText etApellidos;
    EditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionar_empleados);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popUpInsertarEmpleado();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem nav = menu.findItem(R.id.nav_manage);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        ListView listViewEmpleados = (ListView) findViewById(R.id.listview_empleados);
        final ArrayList<String> empleados = manager.obtenerNombreEmpleados();
        ArrayAdapter<String> adapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,empleados);
        listViewEmpleados.setAdapter(adapter);

        listViewEmpleados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(view.getContext())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Eliminar empleado")
                        .setMessage("Desea eliminar el empleado " + empleados.get(position) +"?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                eliminarEmpleado(empleados.get(position));
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
    }

    public void popUpInsertarEmpleado(){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_empleado, null);

        etUsuario = (EditText) v.findViewById(R.id.et_usuario);
        etNombre = (EditText) v.findViewById(R.id.et_nombre);
        etApellidos = (EditText) v.findViewById(R.id.et_apellidos);
        etEmail = (EditText) v.findViewById(R.id.et_email);

        builder.setTitle("Insertar Empleado");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                comprobarDatos();
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void eliminarEmpleado(final String nick){
        final String id = manager.obtenerEmpleadoByName(nick).getId();
        String url = DataBaseManager.ip+"empleados/"+id+"/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    manager.eliminarEmpleado(id);
                    reiniciarIntent();
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }

    public void reiniciarIntent(){
        finish();
        Intent intent = new Intent(getApplicationContext(),GestionarEmpleadosActivity.class);
        startActivity(intent);
    }

    public boolean comprobarDatos(){
        if(etUsuario.getText().toString().equals("")){
            etUsuario.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Nombre de usuario vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etUsuario.setBackgroundColor(Color.WHITE);}

        if(etNombre.getText().toString().equals("")){
            etNombre.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Nombre completo vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etNombre.setBackgroundColor(Color.WHITE);}

        if(etApellidos.getText().toString().equals("")){
            etApellidos.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Apellidos vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            etApellidos.setBackgroundColor(Color.WHITE);}

        if(etEmail.getText().toString().equals("")){
            etEmail.setBackgroundColor(Color.RED);
            Toast.makeText(getApplicationContext(),"Email vacío",Toast.LENGTH_SHORT).show();
            return false;
        }else {
            if (!isEmailValid(etEmail.getText().toString())) {
                etEmail.setBackgroundColor(Color.RED);
                Toast.makeText(getApplicationContext(), "Email incorrecto", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                etEmail.setBackgroundColor(Color.WHITE);}
        }
        registrado(etUsuario.getText().toString(), etEmail.getText().toString());
        return true;
    }

    public void registrado(final String username, final String email){
        String url = DataBaseManager.ip+"empleados/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    boolean registrado = false;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String nombre = object.getString("nombre");
                        String em = object.getString("email");
                        if(username.equals(nombre)){
                            mostrarMensaje("Usuario ya existe");
                            registrado = true;
                            break;
                        }else if(email.equals(em)){
                            mostrarMensaje("Email ya existe");
                            registrado = true;
                            break;
                        }
                    }
                    if(!registrado){
                        registrarEmpleado();
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();

            }
        });
        mRequestQueue.add(request);
    }

    public void mostrarMensaje(String mensaje){
        Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_SHORT).show();
    }

    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    /*public void registrarUsuario(){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"/api/user/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String id = object.getString("id");

                            registrarEmpleado(id);

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                String username = DataBaseManager.removeAcentos(etUsuario.getText().toString());
                String first_name = DataBaseManager.removeAcentos(etNombre.getText().toString());
                String last_name = DataBaseManager.removeAcentos(etApellidos.getText().toString());
                String email = DataBaseManager.removeAcentos(etEmail.getText().toString());


                Map<String, String>  params = new HashMap<>();
                params.put("username",username);
                params.put("first_name", first_name);
                params.put("last_name", last_name);
                params.put("email", email);
                params.put("password","null");
                params.put("groups","3");

                //params.put("last_name",restaurante);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
    }*/

    public void registrarEmpleado(){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"empleados/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String restaurante = object.getString("restaurante");
                            String token = object.getString("token");
                            String password = object.getString("password");
                            manager.insertarEmpleado(id,nombre,restaurante,token,password);
                            mostrarMensaje("Empleado registrado con exito");
                            reiniciarIntent();

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();

                String pass = "null";

                String nombreUsuario = DataBaseManager.removeAcentos(etUsuario.getText().toString());
                String nombre_completo = DataBaseManager.removeAcentos(etNombre.getText().toString());
                String apellidos = DataBaseManager.removeAcentos(etApellidos.getText().toString());
                String email = etEmail.getText().toString();
                if(!isEmailValid(email)){
                    mostrarMensaje("Email no valido");
                }else{
                    params.put("nombre", nombreUsuario);
                    params.put("password",pass);
                    params.put("nombre_completo", nombre_completo);
                    params.put("apellidos", apellidos);
                    params.put("email", email);
                    params.put("restaurante",DataBaseManager.restaurante);
                }
                return params;
            }

        };
        mRequestQueue.add(putRequest);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
