package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SugerenciasActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    private DataBaseManager manager;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_especiales);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                popUpInsertar(view);
            }
        });
        if(DataBaseManager.tipoUsuario.equals("cliente")){
            fab.setVisibility(View.GONE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();

        MenuItem nav = menu.findItem(R.id.nav_sugerencias);
        nav.setChecked(true);

        navigationView.setNavigationItemSelectedListener(this);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        generarEspeciales(DataBaseManager.restaurante);

    }

    private void generarEspeciales(final String restaurante){
        manager.crearTablaEspecial();

        String url = DataBaseManager.ip+"especiales/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String rest = object.getString("user");
                        if(rest.equals(restaurante)){
                            String id = object.getString("id");
                            String titulo = object.getString("nombre").toLowerCase();
                            String producto = object.getString("producto").toLowerCase();
                            String precio = object.getString("precio").toLowerCase();
                            String activo = object.getString("activo").toLowerCase();
                            manager.insertarEspecial(id,titulo,producto,precio,activo);
                        }
                    }
                    pintarSugerencias();
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }

    public void pintarSugerencias(){
        ArrayList<SugerenciaClass> especiales = manager.obtenerEspeciales();

        if(DataBaseManager.tipoUsuario.equals("cliente")){
            ArrayList<SugerenciaClass> aux = new ArrayList<>();
            for(SugerenciaClass especial : especiales){
                if(especial.isActivo()){
                    aux.add(especial);
                }
            }
            especiales =(ArrayList<SugerenciaClass>) aux.clone();
        }

        ListView listView = (ListView) findViewById(R.id.listView_sugerencias);

        SugerenciasAdapter sugerenciasAdapter = new SugerenciasAdapter(this, especiales);
        listView.setAdapter(sugerenciasAdapter);
        listView.setItemsCanFocus(true);
    }

    public void popUpInsertar(final View view){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        LayoutInflater inflater = this.getLayoutInflater();

        final EditText etTitulo;
        final EditText etProducto;
        final EditText etPrecio;
        final CheckBox cbVisible;

        final View v = inflater.inflate(R.layout.fragment_detalle_sugerencia, null);

        etTitulo = (EditText) v.findViewById(R.id.et_tituloSugerencia);
        etProducto = (EditText) v.findViewById(R.id.et_productoSugerido);
        etPrecio = (EditText) v.findViewById(R.id.et_precioProducto);
        cbVisible = (CheckBox) v.findViewById(R.id.cb_visible);
        cbVisible.setClickable(true);
        cbVisible.setText("Visible al publico");

        builder.setTitle("Insertar Sugerencia");

        builder.setView(v);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                manager.insertarEspecialApi(DataBaseManager.removeAcentos(etTitulo.getText().toString()),DataBaseManager.removeAcentos(etProducto.getText().toString()),etPrecio.getText().toString(),cbVisible.isChecked()?"1":"0");
                Snackbar.make(view, "Sugerencia insertada con exito", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                finish();
                Intent intent = new Intent(getApplicationContext(),SugerenciasActivity.class);
                startActivity(intent);
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            case R.id.action_call:
                //makeJsonObjReq();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {

    }
}
