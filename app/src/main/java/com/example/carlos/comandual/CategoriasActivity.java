package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CategoriasActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    DataBaseManager manager;
    String tipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        tipo = bundle.getString("tipo");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tipo.equals("categoria")){
                    popUpInsertarCategoria();
                }else{
                    popUpInsertarEspacio();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Menu menu = navigationView.getMenu();

        MenuItem nav = menu.findItem(R.id.nav_manage);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        ListView listView = (ListView) findViewById(R.id.listview);
        final ArrayList<String> array;
        if(tipo.equals("categoria")){
            array = manager.obtenerCategorias();
        }else{
            setTitle(R.string.title_activity_espacios);
            array = manager.obtenerEspacios();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,array);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nombre = array.get(position);
                if(tipo.equals("categoria")){
                    popUpModificarCategoria(nombre);
                }else{
                    popUpModificarEspacio(nombre);
                }
            }
        });

    }

    public void popUpModificarCategoria(final String nombre){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_modificar_categoria, null);

        final String idCategoria = manager.obtenerCategoria(nombre).get(0);
        final EditText etNombre = (EditText) v.findViewById(R.id.et_nombre);
        etNombre.setText(nombre);

        builder.setTitle("Modificar categoria");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if(!nombre.equals(etNombre.getText().toString())){
                    RequestQueue mRequestQueue;
                    mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                    String url = DataBaseManager.ip+"categorias/"+idCategoria+"/";
                    StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                    Log.d("Response", response);
                                    try {
                                        JSONObject object = new JSONObject(response);
                                        String id = object.getString("id");
                                        String nombre = object.getString("nombre");

                                        manager.modificarCategoria(id,nombre);
                                        reiniciarIntent();

                                    } catch (Throwable t) {
                                        Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                    }
                                    //String id = response.

                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", error.toString());
                                }
                            }
                    ) {

                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String>  params = new HashMap<>();
                            params.put("nombre",DataBaseManager.removeAcentos(etNombre.getText().toString()));

                            return params;
                        }

                    };
                    mRequestQueue.add(putRequest);
                }
            }
        })
                .setNegativeButton(R.string.eliminar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        popUpEliminarCategoria(nombre);

                    }
                });
        builder.show();
    }



    public void popUpModificarEspacio(final String nombre){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_modificar_categoria, null);

        final String idEspacio = manager.obtenerIdEspacio(nombre).get(0);
        final EditText etNombre = (EditText) v.findViewById(R.id.et_nombre);
        etNombre.setText(nombre);

        builder.setTitle("Modificar espacio");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if(!nombre.equals(etNombre.getText().toString())){
                    RequestQueue mRequestQueue;
                    mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                    String url = DataBaseManager.ip+"espacios/"+idEspacio+"/";
                    StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                    Log.d("Response", response);
                                    try {
                                        JSONObject object = new JSONObject(response);
                                        String id = object.getString("id");
                                        String nombre = object.getString("nombre");

                                        manager.modificarEspacio(id,nombre);
                                        reiniciarIntent();

                                    } catch (Throwable t) {
                                        Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                    }
                                    //String id = response.

                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", error.toString());
                                }
                            }
                    ) {

                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String>  params = new HashMap<>();
                            params.put("nombre",DataBaseManager.removeAcentos(etNombre.getText().toString()));

                            return params;
                        }

                    };
                    mRequestQueue.add(putRequest);
                }
            }
        })
                .setNegativeButton(R.string.eliminar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        popUpEliminarEspacio(nombre);
                    }
                });
        builder.show();

    }

    public void popUpEliminarEspacio(final String nombre){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final String idEspacio = manager.obtenerIdEspacio(nombre).get(0);

        builder.setTitle("Eliminar espacio");
        builder.setMessage("¿Desea eliminar el espacio "+nombre+"? (Todas las mesas de este espacio serán eliminados)");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip+"espacios/"+idEspacio+"/";
                StringRequest deleteRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            ArrayList<ArrayList<String>> mesas = manager.obtenerMesas();
                            for(int i=0; i<mesas.size(); i++){
                                if(mesas.get(i).get(4).equals(idEspacio)){
                                    manager.eliminarMesa(String.valueOf(mesas.get(i).get(0)));
                                }
                            }
                            manager.eliminarEspacio(idEspacio);
                            reiniciarIntent();
                        }catch (Exception e){
                            String err = e.toString();
                            Log.e("Err",err);
                        }
                    }
                },new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        String err = error.toString();
                        Log.e("Err",err);
                    }
                });
                mRequestQueue.add(deleteRequest);
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();

    }

    public void popUpEliminarCategoria(final String nombre){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final String idCategoria = manager.obtenerCategoria(nombre).get(0);

        builder.setTitle("Eliminar categoria");
        builder.setMessage("¿Desea eliminar la categoria "+ nombre +"? (Todos los productos de esta categoría serán eliminados)");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip+"categorias/"+idCategoria+"/";
                StringRequest deleteRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            ArrayList<Producto> productos = manager.obtenerProductos();
                            for(int i=0; i<productos.size(); i++){
                                if(productos.get(i).getCategoria().equals(idCategoria)){
                                    manager.eliminarProducto(String.valueOf(productos.get(i).getNombre()));
                                }
                            }
                            manager.eliminarCategoria(idCategoria);
                            reiniciarIntent();

                        }catch (Exception e){
                            String err = e.toString();
                            Log.e("Err",err);
                        }
                    }
                },new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        String err = error.toString();
                        Log.e("Err",err);
                    }
                });
                mRequestQueue.add(deleteRequest);
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();

    }

    /*public void eliminarProducto(final String idProducto){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"/api/productos/"+idProducto+"/";
        StringRequest deleteRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                manager.eliminarProducto()
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(deleteRequest);
    }*/

    public void eliminarMesa(String idMesa){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"mesas/"+idMesa+"/";
        StringRequest deleteRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(deleteRequest);
    }

    public void reiniciarIntent(){
        finish();
        Intent intent = new Intent(getApplicationContext(),CategoriasActivity.class);
        intent.putExtra("tipo",tipo);
        startActivity(intent);
    }

    public void popUpInsertarEspacio(){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_espacio, null);

        final EditText editText = (EditText) v.findViewById(R.id.et_nombre);

        builder.setTitle("Insertar Espacio");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            /**/
                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip+"espacios/";
                StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                Log.d("Response", response);
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String id = object.getString("id");
                                    String nombre = object.getString("nombre");

                                    manager.insertarEspacio(id,nombre);
                                    reiniciarIntent();

                                } catch (Throwable t) {
                                    Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                }
                                //String id = response.

                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("nombre",DataBaseManager.removeAcentos(editText.getText().toString()));
                        params.put("user",DataBaseManager.restaurante);

                        return params;
                    }

                };
                mRequestQueue.add(putRequest);
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }

    public void popUpInsertarCategoria(){
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_categoria, null);

        final EditText editText = (EditText) v.findViewById(R.id.et_nombre);

        builder.setTitle("Insertar Categoria");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            /**/
                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                String url = DataBaseManager.ip+"categorias/";
                StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                                Log.d("Response", response);
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String id = object.getString("id");
                                    String nombre = object.getString("nombre");

                                    manager.insertarCategoria(id,nombre);
                                    reiniciarIntent();

                                } catch (Throwable t) {
                                    Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                }
                                //String id = response.

                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("nombre",DataBaseManager.removeAcentos(editText.getText().toString()));
                        params.put("user",DataBaseManager.restaurante);

                        return params;
                    }

                };
                mRequestQueue.add(putRequest);
            /**/

                //manager.insertarCategoriaApi(editText.getText().toString());


            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(DataBaseManager.tipoUsuario.equals("cliente")){
            getMenuInflater().inflate(R.menu.main_cliente, menu);
        }else{
            getMenuInflater().inflate(R.menu.main, menu);

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
