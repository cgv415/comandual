package com.example.carlos.comandual;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 30/05/2017
 */

public class SugerenciaClass {
    private String titulo;
    private String producto;
    private String precio;
    private boolean activo;
    private String id;

    public SugerenciaClass() {
    }

    public SugerenciaClass(String titulo, String producto, String precio, boolean activo, String id) {
        this.titulo = titulo;
        this.producto = producto;
        this.precio = precio;
        this.activo = activo;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
