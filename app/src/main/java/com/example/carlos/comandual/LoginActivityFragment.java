package com.example.carlos.comandual;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginActivityFragment extends Fragment implements View.OnClickListener{

    private EditText user;
    private EditText password;
    private String usuario;
    private DataBaseManager manager;
    public String ip = DataBaseManager.ip;
    public static final String SEED = "Your_seed_....";

    public LoginActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        manager = new DataBaseManager(getContext());

        //manager.comprobarBaseDatos();
        manager.reiniciarBaseDatos();
        /*if(!manager.isTableExists("token")){
            //RegistrationService registration = new RegistrationService();
        }*/
        //manager.actualizar();


        View view = inflater.inflate(R.layout.fragment_login, container, false);


        user = (EditText) view.findViewById(R.id.et_User);
        password = (EditText) view.findViewById(R.id.et_Password);

        Button login = (Button) view.findViewById(R.id.btn_login);
        login.setOnClickListener(this);

        Button registrar = (Button) view.findViewById(R.id.btn_registro);
        registrar.setOnClickListener(this);

        Button demo = (Button) view.findViewById(R.id.btn_demo);
        demo.setOnClickListener(this);

        Button contraseña = (Button) view.findViewById(R.id.btn_contraseña);
        contraseña.setOnClickListener(this);

        if(manager.isTableExists("cliente")){
            ClienteClass cliente = manager.obtenerCliente();
            if(cliente.getNombre().length()!=0){
                user.setVisibility(View.GONE);
                password.setVisibility(View.GONE);
                DataBaseManager.tipoUsuario = "cliente";
                registrar.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                demo.setVisibility(View.GONE);
                contraseña.setVisibility(View.GONE);
                DataBaseManager.idUsuario = cliente.getId();
                existeUsuario("clientes",cliente.getId());
            }
        }
        if (manager.isTableExists("sesion")) {
            ArrayList<String> sesion = manager.obtenerSesion();
            if(sesion.size()>0){
                user.setVisibility(View.GONE);
                password.setVisibility(View.GONE);
                contraseña.setVisibility(View.GONE);
                DataBaseManager.tipoUsuario = "cliente";
                registrar.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                demo.setVisibility(View.GONE);
                String tipoUsuario = sesion.get(4);
                if(tipoUsuario.equals("restaurante")){
                    DataBaseManager.tipoUsuario = "empleado";
                    DataBaseManager.admin = true;
                }else{
                    DataBaseManager.tipoUsuario = sesion.get(4);
                }
                if(DataBaseManager.tipoUsuario.equals("cliente")){
                    DataBaseManager.idUsuario = sesion.get(0);
                }else{
                    DataBaseManager.idUsuario = sesion.get(0);
                    manager.actualizarToken(sesion);
                }
                if(DataBaseManager.isAdmin()){
                    existeUsuario("restaurantes",sesion.get(0));
                }else{
                    existeUsuario("empleados",sesion.get(0));
                }
            }
        }

        return view;
    }

    public void loginSuccess(){
        Intent intent;
        if(DataBaseManager.tipoUsuario.equals("empleado")){
            manager.actualizar();
            intent = new Intent(getActivity(),MainActivity.class);
            startActivity(intent);
        }else{
            ClienteClass cliente = manager.obtenerCliente();
            if(cliente.getMesa().equals("null")){
                String url = ip+"mesas/";
                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                StringRequest request;
                request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray array = new JSONArray(response);
                            JSONObject object;
                            for(int i = 0 ; i < array.length();i++){
                                object = array.getJSONObject(i);
                                final String cliente = object.getString("cliente");
                                if(cliente.equals(DataBaseManager.idUsuario)){
                                    obtenerRestaurante(object.getString("id"));
                                    manager.modificarMesaCliente(object.getString("id"));
                                }
                            }
                            obtenerRestaurante("null");

                        }catch (Exception e){
                            String err = e.toString();
                            Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                        }
                    }
                },new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        String err = error.toString();
                        Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                    }
                });
                mRequestQueue.add(request);
            }else{
                String idMesa = cliente.getMesa();
                String url = ip+"mesas/"+idMesa+"/";
                RequestQueue mRequestQueue;
                mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                StringRequest request;
                request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);

                                final String cliente = object.getString("cliente");
                                if(cliente.equals(DataBaseManager.idUsuario)){
                                    obtenerRestaurante(object.getString("id"));
                                }else{
                                    obtenerRestaurante("null");
                                }


                        }catch (Exception e){
                            String err = e.toString();
                            Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                        }
                    }
                },new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        String err = error.toString();
                        Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                    }
                });
                mRequestQueue.add(request);
            }
        }

    }

    public void existeUsuario(String tipo, String id){
        String url = ip + tipo + "/"+id+"/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    loginSuccess();
                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                manager.generarMensaje("No existe el usuario");
                manager.eliminarTabla("cliente");
                manager.eliminarTabla("sesion");

            }
        });
        mRequestQueue.add(request);
    }

    public void obtenerRestaurante(final String mesa){
        if(mesa.equals("null")){
            /*manager.generarRestaurantes();
            Intent intent = new Intent(getActivity(),MapActivity.class);
            startActivity(intent);*/

            //Intent intent = new Intent(getContext(),SimpleScannerActivity.class);
            Intent intent = new Intent(getContext(),InicioCliente.class);
            startActivity(intent);
        }else{
            String url = ip+"mesas/"+mesa+"/";
            RequestQueue mRequestQueue;
            mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
            StringRequest request;
            request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                                        try {
                        JSONObject object = new JSONObject(response);
                        DataBaseManager.restaurante = object.getString("user");
                        manager.actualizarCliente();
                        Thread.sleep(1000);
                        Intent intent;
                        intent = new Intent(getActivity(),MesaActivity.class);
                        intent.putExtra("idMesa",Integer.valueOf(mesa));
                        startActivity(intent);

                    }catch (Exception e){
                        String err = e.toString();
                        Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                    }
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                    String err = error.toString();
                    Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                }
            });
            mRequestQueue.add(request);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_demo:
                manager.lanzarDemo();
                DataBaseManager.tipoUsuario = "empleado";
                final Intent intent = new Intent(getActivity(),MainActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_registro:
                //String api = DataBaseManager.ip+"/users/";
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Registrarse como...")
                        .setPositiveButton("Empresa", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                                manager.reiniciarIntent(RegistroRestaurante.class);
                            }
                        })
                        .setNegativeButton("Cliente", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                /*String api =DataBaseManager.ip +"/users/" + "registroCliente";
                                Uri uri = Uri.parse(api);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);*/
                                getActivity().finish();
                                manager.reiniciarIntent(RegistroRestaurante.class);
                            }
                        }).show();

                break;
            case R.id.btn_login:
                //final String stPass = password.getText().toString();
                final String stUs = user.getText().toString();

                /**/
                cliente(stUs);
                break;
            case R.id.btn_contraseña:
                popUpModificarContraseña();
                break;
        }
    }

    public void popUpModificarContraseña(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.fragment_insertar_categoria, null);

        final EditText editText = (EditText) v.findViewById(R.id.et_nombre);
        editText.setHint("usuario o email");

        builder.setTitle("Contraseña olvidada");
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                comprobarCliente(editText.getText().toString());
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.show();

    }

    public void comprobarCliente(final String editText){

        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        final String url = DataBaseManager.ip + "clientes/";
        StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object;
                            String email;
                            String user;
                            String id = "";
                            String tipo = "";
                            JSONArray array = new JSONArray(response);
                            for(int i = 0 ; i < array.length();i++){
                                object = array.getJSONObject(i);
                                email = object.getString("email");
                                user = object.getString("nombre");
                                if(email.equals(editText)||user.equals(editText)){
                                    id = object.getString("id");
                                    tipo = "clientes";
                                    break;
                                }
                            }
                            if(tipo.equals("clientes")){
                                modificarPassword(id,tipo);
                            }else{
                                comprobrarEmpleado(editText);
                            }

                        } catch (Throwable t) {
                            t.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                    }
                }
        );
        mRequestQueue.add(putRequest);
    }

    public void comprobrarEmpleado(final String editText){

        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        final String url = DataBaseManager.ip + "/api/empleados/";
        StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object;
                            String email;
                            String user;
                            String id = "";
                            String tipo = "";
                            JSONArray array = new JSONArray(response);
                            for(int i = 0 ; i < array.length();i++){
                                object = array.getJSONObject(i);
                                email = object.getString("email");
                                user = object.getString("nombre");
                                if(email.equals(editText)){
                                    id = object.getString("id");
                                    break;
                                }else if(user.equals(editText)){
                                    id = object.getString("id");
                                    break;
                                }
                            }
                            if(tipo.equals("")){
                                comprobarRestaurante(editText);
                            }else{
                                tipo = "clientes";
                                modificarPassword(id,tipo);
                            }

                        } catch (Throwable t) {
                            t.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                    }
                }
        );
        mRequestQueue.add(putRequest);
    }

    public void comprobarRestaurante(final String editText){

        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        final String url = DataBaseManager.ip + "/api/empleados/";
        StringRequest putRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object;
                            String email;
                            String user;
                            String id = "";
                            String tipo = "";
                            JSONArray array = new JSONArray(response);
                            for(int i = 0 ; i < array.length();i++){
                                object = array.getJSONObject(i);
                                email = object.getString("email");
                                user = object.getString("nombre");
                                if(email.equals(editText)){
                                    id = object.getString("id");
                                    break;
                                }else if(user.equals(editText)){
                                    id = object.getString("id");
                                    break;
                                }
                            }
                            if(tipo.equals("")){
                                popUpMensaje("No se ha encontrado cuenta asociada");
                            }else{
                                tipo = "clientes";
                                modificarPassword(id,tipo);
                            }

                        } catch (Throwable t) {
                            t.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                    }
                }
        );
        mRequestQueue.add(putRequest);
    }

    public void popUpMensaje(String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Mensaje del sistema");
        builder.setMessage(mensaje);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();

    }

    public void modificarPassword(String id, String tipo){
        try{
            final String password = formarPalabra();
            final String encMsg = Encrypt.encrypt(SEED, password);
            RequestQueue mRequestQueue;
            mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
            String url = DataBaseManager.ip + tipo +"/" + id + "/";
            StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response
                            // {"id":5,"mesa":3,"producto":5,"tipo":3,"estado":"COLA","cantidad":1.0,"comentario":"","user":8}
                            Log.d("Response", response);
                            try {
                                if(manager.isTableExists("cliente")){
                                    manager.modificarClientePassword(password);
                                }else if(manager.isTableExists("sesion")){
                                    manager.modificarSesionPassword(password);
                                }
                                popUpMensaje(password);

                            } catch (Throwable t) {
                                Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                            }
                            //String id = response.

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Error",error.toString());
                        }
                    }
            ) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("password", encMsg);
                    //params.put("nombre", "x");
                    //params.put("email", "x@gmail.com");

                    return params;
                }

            };
            mRequestQueue.add(putRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String formarPalabra(){
        int[] secuencia= new int[5];
        for(int i=0;i<5;i++){
            secuencia[i]=(int)(Math.random()*24);
        }
        String palabra = "";
        for (int aSecuencia : secuencia) {
            switch (aSecuencia) {
                case 0:
                    palabra += "a";
                    break;
                case 1:
                    palabra += "b";
                    break;
                case 2:
                    palabra += "c";
                    break;
                case 3:
                    palabra += "d";
                    break;
                case 4:
                    palabra += "e";
                    break;
                case 5:
                    palabra += "f";
                    break;
                case 6:
                    palabra += "g";
                    break;
                case 7:
                    palabra += "h";
                    break;
                case 8:
                    palabra += "i";
                    break;
                case 9:
                    palabra += "j";
                    break;
                case 10:
                    palabra += "k";
                    break;
                case 11:
                    palabra += "l";
                    break;
                case 12:
                    palabra += "m";
                    break;
                case 13:
                    palabra += "n";
                    break;
                case 14:
                    palabra += "o";
                    break;
                case 15:
                    palabra += "p";
                    break;
                case 16:
                    palabra += "q";
                    break;
                case 17:
                    palabra += "r";
                    break;
                case 18:
                    palabra += "s";
                    break;
                case 19:
                    palabra += "t";
                    break;
                case 20:
                    palabra += "u";
                    break;
                case 21:
                    palabra += "v";
                    break;
                case 22:
                    palabra += "w";
                    break;
                case 23:
                    palabra += "x";
                    break;
                case 24:
                    palabra += "y";
                    break;
                case 25:
                    palabra += "z";
                    break;

            }
        }
        return palabra;
    }

    public void cliente(final String stUs){
        String url = ip.concat("clientes/");
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    boolean access = false;
                    JSONArray array = new JSONArray(response);
                    String url;
                    for(int n = 0; n < array.length(); n++)
                    {
                        JSONObject object = array.getJSONObject(n);
                        if(stUs.equals(object.getString("nombre"))){
                            String id = object.getString("id");
                            url = ip.concat("clientes/"+id+"/");
                            usuario = "cliente";
                            DataBaseManager.admin = false;
                            login(url);
                            access = true;
                            break;
                        }

                    }//end for
                    if(!access){
                        empleado(stUs);
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
            }
        });
        mRequestQueue.add(request);
    }

    public void empleado(final String stUs){
        String url = ip.concat("empleados/");
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    boolean access = false;
                    JSONArray array = new JSONArray(response);
                    String url;
                    for(int n = 0; n < array.length(); n++)
                    {
                        JSONObject object = array.getJSONObject(n);
                        if(stUs.equals(object.getString("nombre"))){
                            String id = object.getString("id");
                            url = ip.concat("empleados/"+id+"/");
                            usuario = "empleado";
                            DataBaseManager.admin = false;
                            login(url);
                            access = true;
                            break;
                        }
                    }//end for
                    if(!access){
                        restaurante(stUs);
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
            }
        });
        mRequestQueue.add(request);
    }

    public void restaurante(final String stUs){
        String url = ip.concat("restaurantes/");
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray array = new JSONArray(response);
                    String url;
                    boolean access = false;
                    for(int n = 0; n < array.length(); n++)
                    {
                        JSONObject object = array.getJSONObject(n);
                        String nombre = object.getString("nombre");
                        String id = object.getString("id");
                        if(stUs.equals(nombre)){

                            url = ip.concat("restaurantes/"+id+"/");
                            usuario = "empleado";
                            DataBaseManager.admin = true;
                            login(url);
                            access = true;
                            break;
                        }

                    }//end for
                    if(!access){
                        manager.generarMensaje("Usuario no encontrado");
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
            }
        });
        mRequestQueue.add(request);
    }

    public void login(String url){
        final String stPass = password.getText().toString();
        final String stUs = user.getText().toString();

        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<String > datos = new ArrayList<>();

                        JSONObject object = new JSONObject(response);
                        if(stUs.equals(object.getString("nombre"))){
                            datos.add(object.getString("id"));
                            datos.add(object.getString("nombre"));
                            datos.add(object.getString("password"));
                            datos.add(stPass);
                            if(usuario.equals("empleado")&&!DataBaseManager.isAdmin()){
                                datos.add(object.getString("restaurante"));
                            }else if(usuario.equals("cliente")){
                                datos.add(object.getString("nacimiento"));
                                datos.add(object.getString("provincia"));
                                datos.add(object.getString("localidad"));
                                datos.add(object.getString("codigo"));
                            }


                    }//end for

                    if(!datos.isEmpty()){
                        int login = manager.login(datos,usuario);
                        if(login == -1){
                            Toast.makeText(getContext(),"Contraseña incorrecta",Toast.LENGTH_SHORT).show();
                        }else{
                            if(usuario.equals("cliente")){
                                manager.insertarCliente(datos.get(0),datos.get(1),datos.get(3));
                            }else{
                                String token = manager.obtenerToken();
                                if(DataBaseManager.isAdmin()){
                                    manager.insertarSesion(datos.get(0),stUs,stPass,datos.get(0),"restaurante",token);
                                }else{
                                    manager.insertarSesion(datos.get(0),stUs,stPass,datos.get(4),usuario,token);
                                }
                            }

                            Toast.makeText(getContext(),"Usuario autentificado",Toast.LENGTH_SHORT).show();
                            loginSuccess();

                        }
                    }else{
                        Toast.makeText(getContext(),"Usuario no existente",Toast.LENGTH_SHORT).show();
                    }
                   /* if (idUser.isEmpty()){
                        Toast.makeText(getContext(),"Usuario no existente",Toast.LENGTH_SHORT).show();
                    }*/

                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
            }
        });
        mRequestQueue.add(request);
    }
}
