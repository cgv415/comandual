package com.example.carlos.comandual;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    DataBaseManager manager;
    RelativeLayout layout;
    ListView listView;
    ArrayList<String> settings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();

        MenuItem nav_settings = menu.findItem(R.id.nav_manage);
        // set new title to the MenuItem
        nav_settings.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        settings = new ArrayList<>();
        if(DataBaseManager.tipoUsuario.equals("empleado")){
            if(DataBaseManager.isAdmin()){
                settings.add("Administrar Perfil Restaurante");
                settings.add("Administrar Empleados");
            }else{
                settings.add("Administrar Perfil Empleado");
            }
            settings.add("Administrar Ubicación");
            settings.add("Administrar Ingredientes");
            settings.add("Administrar Productos");
            settings.add("Administrar Categorias");
            settings.add("Administrar Mesas");
            settings.add("Administrar Espacios");
        }else{
            settings.add("Administrar Perfil");
            settings.add("Proximamente...");
        }

        layout = (RelativeLayout) findViewById(R.id.content_settings);

        Button empleados = new Button(this);
        empleados.setText(R.string.administrarEmpleados);

        listView = (ListView) findViewById(R.id.listview_settings);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, settings);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                String item = settings.get(position);
                switch (item){
                    case "Administrar Perfil Restaurante":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),AdministrarPerfilActicity.class);
                        startActivity(intent);
                        break;
                    case "Administrar Perfil Empleado":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),AdministrarPerfilActicity.class);
                        startActivity(intent);
                        break;
                    case "Administrar Empleados":
                        Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),GestionarEmpleadosActivity.class);
                        startActivity(intent);
                        break;
                    case "Administrar Perfil":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),AdministrarPerfilActicity.class);
                        startActivity(intent);
                        break;
                    case "Proximamente...":
                        Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        //String cadena = DataBaseManager.removeAcentos("Almería, Ávila, España");
                        //Toast.makeText(getApplicationContext(),cadena,Toast.LENGTH_SHORT).show();
                        break;
                    case "Administrar Ubicación":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),MapActivity.class);
                        startActivity(intent);
                        break;
                    case "Administrar Ingredientes":
                        intent = new Intent(getApplicationContext(),StockActivity.class);
                        intent.putExtra("tab",1);
                        startActivity(intent);
                        break;
                    case "Administrar Productos":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),StockActivity.class);
                        startActivity(intent);
                        break;
                    case "Administrar Categorias":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),CategoriasActivity.class);
                        intent.putExtra("tipo","categoria");
                        startActivity(intent);
                        break;
                    case "Administrar Mesas":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),GestionarMesasActivity.class);
                        startActivity(intent);
                        break;
                    case "Administrar Espacios":
                        //Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(),CategoriasActivity.class);
                        intent.putExtra("tipo","espacios");
                        startActivity(intent);
                        break;
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
