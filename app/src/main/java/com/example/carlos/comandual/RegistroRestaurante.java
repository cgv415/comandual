package com.example.carlos.comandual;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistroRestaurante extends AppCompatActivity implements View.OnClickListener {

    DataBaseManager manager;
    EditText nombreCompleto;
    EditText codigoPostal;
    EditText horario;
    EditText telefono;
    EditText direccion;
    AutoCompleteTextView provincia;
    AutoCompleteTextView localidad;
    EditText password;
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_restaurante);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String[] provincias = {"Alava","Albacete","Alicante","Almería","Asturias","Avila","Badajoz","Barcelona","Burgos","Cáceres",
                "Cádiz","Cantabria","Castellón","Ciudad Real","Córdoba","La Coruña","Cuenca","Gerona","Granada","Guadalajara",
                "Guipúzcoa","Huelva","Huesca","Islas Baleares","Jaén","León","Lérida","Lugo","Madrid","Málaga","Murcia","Navarra",
                "Orense","Palencia","Las Palmas","Pontevedra","La Rioja","Salamanca","Segovia","Sevilla","Soria","Tarragona",
                "Santa Cruz de Tenerife","Teruel","Toledo","Valencia","Valladolid","Vizcaya","Zamora","Zaragoza"};

        manager = new DataBaseManager(this);

        nombreCompleto = (EditText) findViewById(R.id.et_nombreCompleto);
        codigoPostal = (EditText) findViewById(R.id.et_codigoPostal);
        horario = (EditText) findViewById(R.id.et_horario);
        telefono = (EditText) findViewById(R.id.et_telefono);
        direccion = (EditText) findViewById(R.id.et_direccion);
        email = (EditText) findViewById(R.id.et_email);

        provincia = (AutoCompleteTextView) findViewById(R.id.et_provincia);
        ArrayAdapter<String> adapterProvincia = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, provincias);
        provincia.setAdapter(adapterProvincia);
        provincia.setThreshold(1);

        localidad = (AutoCompleteTextView) findViewById(R.id.et_localidad);
        ArrayAdapter<String> adapterLocalidad = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, Localidades.getLocalidades());
        localidad.setThreshold(1);
        localidad.setAdapter(adapterLocalidad);

        password = (EditText) findViewById(R.id.et_password);

        Button cancelar = (Button) findViewById(R.id.btn_cancelar);
        cancelar.setOnClickListener(this);
        Button registrar = (Button) findViewById(R.id.btn_registrar);
        registrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_cancelar:
                finish();
                manager.reiniciarIntent(LoginActivity.class);
                break;
            case R.id.btn_registrar:
                registrado(nombreCompleto.getText().toString(),email.getText().toString());
                break;
        }
    }

    public void registrado(final String username, final String email){
        String url = DataBaseManager.ip+"restaurantes/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;

        request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    boolean registrado = false;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String nombre = object.getString("nombre");
                        String em = object.getString("email");
                        if(username.equals(nombre)){
                            manager.generarMensaje("Usuario ya existe");
                            registrado = true;
                            break;
                        }else if(email.equals(em)){
                            manager.generarMensaje("Email ya existe");
                            registrado = true;
                            break;
                        }
                    }
                    if(!registrado){
                        if(isEmailValid(email)){
                            registrarRestaurante();
                        }else{
                            manager.generarMensaje("Email incorrecto");
                        }
                    }else{
                        manager.generarMensaje("Usuario o email registrado");
                    }
                }catch (Exception e){
                    String err = e.toString();
                    Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();

            }
        });
        mRequestQueue.add(request);
    }

    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public void registrarRestaurante(){
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        String url = DataBaseManager.ip+"restaurantes/";
        StringRequest putRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Response", response);
                        try {

                            //JSONObject object = new JSONObject(response);
                            //String id = object.getString("id");
                            manager.generarMensaje("Cuenta creada con exito");
                            finish();
                            manager.reiniciarIntent(LoginActivity.class);

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                        //String id = response.

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {

                String pass = "";
                try {
                    pass = Encrypt.encrypt("Your_seed_....",password.getText().toString());
                }catch (Exception e){
                    Log.d("Err",e.toString());
                }
                String nombreUsuario = DataBaseManager.removeAcentos(nombreCompleto.getText().toString());
                String prov = DataBaseManager.removeAcentos(provincia.getText().toString());
                String loc = DataBaseManager.removeAcentos(localidad.getText().toString());
                String em = DataBaseManager.removeAcentos(email.getText().toString());
                String cod = codigoPostal.getText().toString();
                String tel = telefono.getText().toString();
                String hor = horario.getText().toString();
                String dir = direccion.getText().toString();

                Map<String, String>  params = new HashMap<>();
                params.put("nombre", nombreUsuario);
                params.put("password",pass);
                params.put("codigo", cod);
                params.put("direccion", dir);
                params.put("provincia", prov);
                params.put("localidad", loc);
                params.put("telefono", tel);
                params.put("email", em);
                params.put("horario", hor);
                //params.put("idUsuario",id);
                //params.put("last_name",restaurante);

                return params;
            }

        };
        mRequestQueue.add(putRequest);
    }
}
