package com.example.carlos.comandual;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 28/04/2017
 */

class ClienteClass {
    private String id;
    private String nombre;
    private String password;
    private String mesa;

    private String nombreCompleto;
    private String apellidos;
    private String fechaNacimiento;
    private String codigoPostal;
    private String localiadd;
    private String provincia;

    ClienteClass(String id, String nombre, String password, String mesa) {
        this.id = id;
        this.nombre = nombre;
        this.password = password;
        this.mesa = mesa;
    }

    ClienteClass() {
        this.id = "";
        this.nombre = "";
        this.password = "";
        this.mesa = "";
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getLocaliadd() {
        return localiadd;
    }

    public void setLocaliadd(String localiadd) {
        this.localiadd = localiadd;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    void setPassword(String password) {
        this.password = password;
    }

    public String getMesa() {
        return mesa;
    }

    public void setMesa(String mesa) {
        this.mesa = mesa;
    }

    public boolean isNull(){
        int x = nombre.length();
        if(x==0){
            return true;
        }else if(nombre.length()>=1){
            return false;
        }
        return true;
    }

    boolean hasMesa(){
        return !mesa.equals("null");
    }
}
