package com.example.carlos.comandual;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 04/12/2016
 * subscribe to topics
 * https://code.tutsplus.com/es/tutorials/how-to-get-started-with-push-notifications-on-android--cms-25870
 */

public class RegistrationService extends IntentService {
    public RegistrationService() {
        super("RegistrationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID miId = InstanceID.getInstance(this);

        //FirebaseMessaging.getInstance().subscribeToTopic("/topics/my_little_topic");

        String string1 = getString(R.string.gcm_defaultSenderId);
        String string2 = GoogleCloudMessaging.INSTANCE_ID_SCOPE;
        try{
            String registrationToken = miId.getToken(string1, string2);
            Log.d("Registration Token", registrationToken);

            GcmPubSub subscription = GcmPubSub.getInstance(this);
            String topic = "/topics/"+registrationToken;
            //subscription.subscribe(registrationToken, "/topics/empleados", null);
            subscription.subscribe(registrationToken, topic, null);
            //subscription.unsubscribe(registrationToken,"/topics/my_little_topic");

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
