package com.example.carlos.comandual;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.LocalActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MesaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, EstadoComandaFragment.OnFragmentInteractionListener, ElegirProductoFragment.OnFragmentInteractionListener ,NuevoEspacioFragment.OnFragmentInteractionListener, EspacioFragment.OnFragmentInteractionListener, ActionBar.TabListener {

    private int idMesa;
    private static final String urlCMS = "https://fcm.googleapis.com/fcm/send";
    private static boolean mostrarSugerencias = true;

    public DataBaseManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesa);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem nav = menu.findItem(R.id.nav_home);
        nav.setChecked(true);

        manager = new DataBaseManager(this);
        manager.mostrarMenu(menu);

        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView.setNavigationItemSelectedListener(this);

        Bundle bundle = getIntent().getExtras();
        idMesa = bundle.getInt("idMesa");

        ArrayList<String> arrMesa = manager.obtenerMesa(idMesa);
        if(arrMesa.size()==0){
            generarMesa(idMesa);
        }else{
            String mesa = manager.obtenerMesa(idMesa).get(5);
            setTitle(mesa);
        }

        TabHost tabs= (TabHost)findViewById(R.id.tabHostMesa);
        LocalActivityManager mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState); // state will be bundle your activity state which you get in onCreate
        tabs.setup(mLocalActivityManager);

        Intent intent = new Intent(MesaActivity.this,NuevaComandaActivity.class);
        intent.putExtra("idMesa",idMesa);

        TabHost.TabSpec spec =  tabs.newTabSpec("nuevaComanda");
        spec.setContent(intent);
        spec.setIndicator("Nueva Comanda");
        tabs.addTab(spec);

        intent = new Intent(MesaActivity.this,EstadoComandaActivity.class);
        intent.putExtra("idMesa",idMesa);

        spec =  tabs.newTabSpec("estadoComanda");
        spec.setContent(intent);
        spec.setIndicator("Estado Comanda");
        tabs.addTab(spec);

        bundle = getIntent().getExtras();
        try{
            int bundleTab = bundle.getInt("tab");
            tabs.setCurrentTab(bundleTab);
        }catch (Exception e){
            e.printStackTrace();
            tabs.setCurrentTab(0);

        }

        if(mostrarSugerencias && DataBaseManager.tipoUsuario.equals("cliente")){
            ArrayList<SugerenciaClass> sugerencias = manager.obtenerEspeciales();
            mostrarSugerencias = false;
            for(SugerenciaClass sugerencia:sugerencias){
                if(sugerencia.isActivo()){
                    mostrarSugerencia(sugerencia);
                }
            }
        }
    }

    /*public void mostrarSugerencias(){
        ArrayList<SugerenciaClass> sugerencias = manager.obtenerEspeciales();
        for(SugerenciaClass sugerencia:sugerencias){
            if(sugerencia.isActivo()){
                mostrarSugerencia(sugerencia);
            }
        }
    }*/

    public void generarMesa(final int idMesa){
        /*if(!manager.isTableExists("mesa")){
            manager.crearTablaMesa();
        }*/
        String get_mesas = DataBaseManager.ip+"mesas/"+String.valueOf(idMesa);
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_mesas, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject object;
                    object = new JSONObject(response);
                    //{"id":3,"nombre":"Mesa Baja 1","capacidad":4,"forma":"CUADRADA","estado":"LIBRE","espacio":11,"user":8}
                    String id = object.getString("id");
                    String nombre = object.getString("nombre");
                    String capacidad = object.getString("capacidad");
                    String forma = object.getString("forma");
                    String estado = object.getString("estado");
                    String espacio = object.getString("espacio");
                    String cliente = object.getString("cliente");
                    String empleado = object.getString("empleado");
                    setTitle(nombre);
                    //manager.insertarMesa(id,nombre, capacidad, forma, estado, espacio, cliente,empleado);

                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }

    public void mostrarSugerencia(SugerenciaClass sugerencia){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(sugerencia.getTitulo());

        builder.setMessage(sugerencia.getProducto());

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        }else{
            //super.onBackPressed();
            new AlertDialog.Builder(this)
                    .setTitle("Salir")
                    .setMessage("Realmente quieres salir de la aplicación?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            int p = android.os.Process.myPid();
                            android.os.Process.killProcess(p);
                        }})
                    .setNegativeButton(android.R.string.no, null).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(DataBaseManager.tipoUsuario.equals("cliente")){
            getMenuInflater().inflate(R.menu.main_cliente, menu);
        }else{
            getMenuInflater().inflate(R.menu.main, menu);

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /// Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_back:
                onBackPressed();
                return true;
            case R.id.action_call:
                makeJsonObjReq();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void makeJsonObjReq(){

        RequestQueue mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        String emp = manager.obtenerMesa(idMesa).get(7);
        String token = manager.obtenerEmpleado(emp).getToken();

        String title = "Petición de asistencia";
        String body = "Mesa " + manager.obtenerMesa(idMesa).get(5);
        final String jsonString =
                "{" +
                        //"'to':'ddp0zVEORB4:APA91bEvq1z7eDONhLehlSZXR-pjHZrIec9JeCDdYmQM3VDlHkG0eDePAFzW7qHlBc4ohAyqZXV4TlConEWgDY0AZZq05Ki75pZ23SFb07HyvVDWKK9I012kyc9KEsyP_lW0tKsYcCpx',\n" +
                        "'to':'topics/"+token+"'," +
                        "'notification':{" +
                        "'title':'" + title + "'," +
                        "'body':'" + body + "'" +
                        "}" +
                        "}";
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    urlCMS, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(getApplicationContext(),"Camarero llamado",Toast.LENGTH_SHORT).show();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Response", "Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(),"Camarero llamado",Toast.LENGTH_SHORT).show();
                }
            }) {
                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("Content-Type","application/json");
                    params.put("Authorization","key=AIzaSyDAE9MwmYI9VjGjTqd6qLzOadga0tqvurs");
                    return params;
                }
            };
            mRequestQueue.add(jsonObjReq);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        manager.gestionarMenu(item,this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
