package com.example.carlos.comandual;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 23/11/2016
 */

class StockAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<String> datos;
    private String tipo;

    StockAdapter(Activity activity, ArrayList<String> datos, String tipo) {
        this.activity = activity;
        this.datos = datos;
        this.tipo = tipo;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public String getItem(int position) {
        return datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.stock_adapter, null);
        }
        String dato = getItem(position);
        DataBaseManager manager = new DataBaseManager(v.getContext());

        if(tipo.equals("ingredientes")){

            TextView textView = (TextView) v.findViewById(R.id.textViewDato);
            textView.setText(dato);
            if(!manager.inStrockIngrediente(dato)){
                textView.setTextColor(Color.RED);
            }else{
                textView.setTextColor(Color.BLACK);
            }

        }else{
            TextView textView = (TextView) v.findViewById(R.id.textViewDato);
            textView.setText(dato);

            if(!manager.inStockProducto(dato)){
                textView.setTextColor(Color.RED);
            }else{
                textView.setTextColor(Color.BLACK);
            }
        }


        return  v;
    }
}
