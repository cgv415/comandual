package com.example.carlos.comandual;

import java.util.ArrayList;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 06/09/2017
 */
interface DatabaseApi {

    boolean actualizarApiEmpelado(String id, final String token);
    boolean actualizarTokenEmpelado(final ArrayList<String> sesion, final String token);

    boolean insertarMesaApi(final String nombre, final String capacidad, final String forma, final String estado, final String espacio);
    boolean asignarMesaCliente(final String idMesa);

    void generarEspeciales(final String restaurante);
    void generarMesas(final String restaurante);
    void generarIngredientes(final String restaurante);
    void generarCategorias(final String restaurante);
    void generarProductos(final String restaurante);
    void generarComandas(final String restaurante);
    void generarEmpleados(final String restaurante);
    void generarRestaurantes();


}
