package com.example.carlos.comandual;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 27/04/2017
 */

public class EmpleadoClass {

    private String id;
    private String nombre;
    private String idUsuario;
    private String restaurante;
    private String token;

    public EmpleadoClass(String id, String nombre, String idUsuario, String restaurante, String token) {
        this.id = id;
        this.nombre = nombre;
        this.idUsuario = idUsuario;
        this.restaurante = restaurante;
        this.token = token;
    }

    public EmpleadoClass(){
        this.id = "";
        this.nombre = "";
        this.idUsuario = "";
        this.restaurante = "";
        this.token = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(String restaurante) {
        this.restaurante = restaurante;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
