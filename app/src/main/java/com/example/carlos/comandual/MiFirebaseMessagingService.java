package com.example.carlos.comandual;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 01/12/2016
 */

public class MiFirebaseMessagingService extends FirebaseMessagingService{

    public static final String TAG = "NOTICIAS";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String from = remoteMessage.getFrom();
        Log.d(TAG,"Mensaje recibido de: " + from);

        if(remoteMessage.getNotification() != null){
            Log.d(TAG,"Notificacion: " + remoteMessage.getNotification().getBody());
            String title = remoteMessage.getNotification().getTitle();
            if(!title.equals("Nueva comanda")){
                mostrarNotificacion(remoteMessage);
            }
        }

        if(remoteMessage.getData().size()>0){
            //Log.d(TAG,"Data: " + remoteMessage.getData());
            String data;
            Map<String,String > map = remoteMessage.getData();
            Iterator<String> it = map.keySet().iterator();
            ArrayList<String> arrayList = new ArrayList<>();
            while (it.hasNext()){
                data = it.next();
                data += " = " + map.get(data);
                arrayList.add(data);
            }
            Collections.sort(arrayList,String.CASE_INSENSITIVE_ORDER);
            Log.d(TAG, " Data: " + arrayList.toString());
        }
    }

    private void mostrarNotificacion(RemoteMessage remoteMessage) {

        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_add_alert_black_24dp)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,notificationBuilder.build());
    }
}
