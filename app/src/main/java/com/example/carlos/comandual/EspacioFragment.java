package com.example.carlos.comandual;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EspacioFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EspacioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EspacioFragment extends Fragment {

    private DataBaseManager manager;
    private int tab;

    private OnFragmentInteractionListener mListener;

    public EspacioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param tab Parameter 1.
     * @return A new instance of fragment EspacioFragment.
     */
    public static EspacioFragment newInstance(int tab) {
        EspacioFragment fragment = new EspacioFragment();
        Bundle args = new Bundle();
        args.putInt("tab", tab);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tab = getArguments() != null ? getArguments().getInt("tab") : 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_espacio, container, false);
        manager = new DataBaseManager(getContext());
        generarMesas(DataBaseManager.restaurante,v);

        return v;
    }

    public void generarMesas(final String restaurante,final View v){
        String get_mesas = DataBaseManager.ip+"mesas/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
        StringRequest request;
        request = new StringRequest(Request.Method.GET, get_mesas, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    manager.crearTablaMesa();
                    JSONArray array = new JSONArray(response);
                    JSONObject object;
                    for(int i = 0 ; i < array.length();i++){
                        object = array.getJSONObject(i);
                        String rest = object.getString("user");
                        if(rest.equals(restaurante)){
                            //{"id":3,"nombre":"Mesa Baja 1","capacidad":4,"forma":"CUADRADA","estado":"LIBRE","espacio":11,"user":8}
                            String id = object.getString("id");
                            String nombre = object.getString("nombre");
                            String capacidad = object.getString("capacidad");
                            String forma = object.getString("forma");
                            String estado = object.getString("estado");
                            String espacio = object.getString("espacio");
                            String cliente = object.getString("cliente");
                            String empleado = object.getString("empleado");
                            manager.insertarMesa(id,nombre, capacidad, forma, estado, espacio, cliente,empleado);
                        }
                    }
                    pintarEspacios(v);
                }catch (Exception e){
                    String err = e.toString();
                    Log.e("Err",err);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                String err = error.toString();
                Log.e("Err",err);
            }
        });
        mRequestQueue.add(request);
    }

    public void pintarEspacios(View v){
        GridLayout gridLayout =(GridLayout) v.findViewById(R.id.gridLayout);
        final ArrayList<ArrayList<String>> mesas = manager.obtenerMesaPorEspacio(tab);
        Button btn;
        int id;
        double width;
        int x;
        double height;
        int y;
        for(int i = 0 ; i < mesas.size();i++){
            btn = new Button(getContext());
            if(DataBaseManager.tipoUsuario.equals("empleado")){
                if(mesas.get(i).get(3).equals("LIBRE")){
                    btn.setBackgroundResource(R.mipmap.ic_table_icon_green);
                }else if(mesas.get(i).get(3).equals("OCUPADA")){
                    btn.setBackgroundResource(R.mipmap.ic_table_icon_black);
                }else if(mesas.get(i).get(3).equals("PENDIENTE")){
                    btn.setBackgroundResource(R.mipmap.ic_table_icon_red);
                }else if(mesas.get(i).get(3).equals("RESERVADA")){
                    /* Por si se hace en un futuro el sistema de reservas
                    Calendar calendarNow =new GregorianCalendar(TimeZone.getTimeZone("Europe/Madrid"));
                    int day = calendarNow.get(Calendar.DAY_OF_MONTH);
                    int month = calendarNow.get(Calendar.MONTH)+1;
                    int year = calendarNow.get(Calendar.YEAR);

                    final int hour = calendarNow.get(Calendar.HOUR);
                    final int minute = calendarNow.get(Calendar.MINUTE);

                    final String date = day+"-"+month+"-"+year;

                    String url = manager.ip+"/api/reservas/";
                    RequestQueue mRequestQueue;
                    mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();
                    StringRequest request;
                    request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {

                                JSONArray array = new JSONArray(response);
                                JSONObject object;
                                for(int i = 0 ; i < array.length();i++){
                                    object = array.getJSONObject(i);
                                    String mesa = object.getString("mesa");
                                    String id = object.getString("id");
                                    String fecha =  object.getString("fecha");
                                    String horaInicio = object.getString("horaInicio");
                                    String horaFin = object.getString("horaFin");
                                    if(fecha.equals(date)){
                                        try{
                                            String[] inicio = horaInicio.split(":");
                                            int hora0 = Integer.parseInt(inicio[0]);
                                            int minuto0 = Integer.parseInt(inicio[1]);
                                            String[] fin = horaFin.split(":");
                                            int hora1 = Integer.parseInt(fin[0]);
                                            int minuto1 = Integer.parseInt(fin[1]);

                                            if(hour<hora0){

                                            }
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                    }

                                }
                            }catch (Exception e){
                                String err = e.toString();
                            }
                        }
                    },new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            String err = error.toString();
                        }
                    });
                    mRequestQueue.add(request);*/

                    btn.setBackgroundResource(R.mipmap.ic_table_icon_yellow);
                }
            }else{
                if(mesas.get(i).get(3).equals("LIBRE")){
                    btn.setBackgroundResource(R.mipmap.ic_table_icon_green);
                }else{
                    btn.setBackgroundResource(R.mipmap.ic_table_icon_red);
                }
            }
            id = Integer.parseInt(mesas.get(i).get(0));

            width = btn.getMinWidth() * 0.9;
            x = (int) width;

            height = btn.getMinHeight() * 0.5;
            y = (int) height;


            btn.setLayoutParams(new ViewGroup.LayoutParams(x, btn.getMinHeight()));
            btn.setId(id);
            btn.setText(mesas.get(i).get(5));
            btn.setPadding(0, y, 0, 0);
            //btn.setTextSize(10);

            if(DataBaseManager.tipoUsuario.equals("empleado")){
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(),MesaActivity.class);
                        intent.putExtra("idMesa",v.getId());
                        startActivity(intent);
                    }
                });

                if(mesas.get(i).get(3).equals("LIBRE")){
                    final ArrayList<String> mesa = mesas.get(i);
                    btn.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(final View v) {
                            String idMesa = manager.obtenerMesa(v.getId()).get(5);
                            new AlertDialog.Builder(v.getContext())
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Codigo")
                                    .setMessage("¿Deseas generar un codigo qr la mesa " + idMesa +"?")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            new PDFCreator(getContext(),mesa.get(0),mesa.get(5));

                                        }

                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                            /*new AlertDialog.Builder(v.getContext())
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Codigo")
                                    .setMessage("¿Deseas generar un codigo de uso?")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Random rand =new Random();
                                            //int codigo = Math.abs(rand.nextInt(10000));
                                            String codigo = "";
                                            int c = Math.abs(rand.nextInt(10000));
                                            if(c<10){
                                                codigo = c+"000";
                                            }else if(c<100){
                                                codigo = c+"00";
                                            }else if(c<1000){
                                                codigo = c+"0";
                                            }else{
                                                codigo = String.valueOf(c);
                                            }
                                            codigo = manager.idUsuario + codigo;
                                            manager.modificarCodigoMesa(mesa,codigo);
                                            //Toast.makeText(getContext(),codigo,Toast.LENGTH_LONG).show();
                                            popUpGenerarCodigo(codigo);

                                        }

                                    })
                                    .setNegativeButton("No", null)
                                    .show();*/
                            return true;
                        }
                    });
                }else{
                    btn.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(final View v) {
                            String idMesa = manager.obtenerMesa(v.getId()).get(5);
                            new AlertDialog.Builder(v.getContext())
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Limpieza")
                                    .setMessage("¿Deseas limpiar la mesa " + idMesa+"?")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            manager.limpiarMesa(v.getId());
                                            getActivity().finish();
                                            startActivity(getActivity().getIntent());
                                            Toast.makeText(getContext(),"Mesa limpiada",Toast.LENGTH_SHORT).show();
                                        }

                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                            return true;
                        }
                    });
                }

            }else{
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /* Para el sistema de reservas
                        Toast.makeText(getContext(),"Mesa solicitada",Toast.LENGTH_SHORT).show();
                        */
                        Intent intent = new Intent(getActivity(),MesaActivity.class);
                        intent.putExtra("idMesa",v.getId());
                        startActivity(intent);
                    }
                });
                ClienteClass clienteClass = manager.obtenerCliente();
                //String idMesa = clienteClass.getMesa();
                //btn.getId()
                if(!clienteClass.hasMesa()){
                    btn.setEnabled(false);
                }else if(!Integer.valueOf(clienteClass.getMesa()).equals(btn.getId())){
                    btn.setEnabled(false);
                }

            }


            gridLayout.addView(btn);
        }

        if(DataBaseManager.tipoUsuario.equals("empleado")){

            btn = new Button(getContext());
            btn.setBackgroundResource(R.mipmap.ic_icon_add);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent nuevoform = new Intent(getActivity(),NuevaMesaActivity.class);
                    startActivity(nuevoform);
                }
            });
            width = btn.getMinWidth() * 0.6;
            x = (int) width;
            btn.setLayoutParams(new ViewGroup.LayoutParams(x, btn.getMinHeight()));
            gridLayout.addView(btn);
        }
    }

    public void asignarCliente(final String id){
        if(id.equals("0")){
            Toast.makeText(getContext(),"Codigo fallido",Toast.LENGTH_SHORT).show();
            return;
        }

        String url = DataBaseManager.ip+"mesas/"+id+"/";
        RequestQueue mRequestQueue;
        mRequestQueue = VolleySingleton.getInstance().getmRequestQueue();

        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {

                            Log.d("Response",response);
                            //manager.m
                            Intent intent = new Intent(getActivity(),MesaActivity.class);
                            intent.putExtra("idMesa",Integer.valueOf(id));
                            startActivity(intent);

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                        Toast.makeText(getContext(),error.hashCode()+" "+error.toString(),Toast.LENGTH_LONG).show();//error.printStackTrace();
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                try {
                    //CN_ID,CN_CAPACIDAD,CN_FORMA,CN_ESTADO,CN_ESPACIO,CN_NOMBRE
                    ArrayList<String> mesa = manager.obtenerMesa(Integer.parseInt(id));
                    params.put("cliente",DataBaseManager.idUsuario);
                    params.put("nombre",mesa.get(5));
                    //params.put("capacidad",mesa.get(1));
                    params.put("capacidad","5");
                    params.put("forma",mesa.get(2));
                    //params.put("estado",mesa.get(3));
                    params.put("espacio",mesa.get(4));


                }catch (Exception e){
                    e.printStackTrace();
                }
                return params;
            }

        };
        mRequestQueue.add(putRequest);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
