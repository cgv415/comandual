package com.example.carlos.comandual;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Clase generada por Carlos Garrido para la aplicación ComandUal en 07/10/2016
 * Videotutorial https://www.youtube.com/watch?v=cJGB4TF15AY&list=PLNRxocD70gLoEx4O2pr4Klr3rfvlcAAfs&index=60
 */
public class VolleySingleton {
    private static VolleySingleton instance = null;
    private RequestQueue mRequestQueue;
    private VolleySingleton(){
        mRequestQueue = Volley.newRequestQueue(MyApplication.getAppContext());
    }

    public static VolleySingleton getInstance(){
        if(instance==null){
            instance = new VolleySingleton();
        }
        return instance;
    }

    public RequestQueue getmRequestQueue() {return mRequestQueue;}
}
